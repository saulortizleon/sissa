<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tatm extends Model
{
    protected $table='t_atm';
    protected $primaryKey='ubigeo';
    public $timestamps=true;
    public $incrementing=false;
    protected $keyType='string';

    public function Tatmver()
    {
    return $this->belongTo('App\Model\Tatmver', 'ubigeo');
    }
    public function TverAtm()
    {
      return $this->belongTo('App\Model\TverAtm', 'idVerAtm');
    }
    public function Tusuario()
	  {
		return $this->belongTo('App\Model\Tusuario', 'dniUsuario');
    }
    public function TcentroP()
	  {
		return $this->belongTo('App\Model\TcentroP', 'ubigeoAtm');
    }
    public function Tformacion()
	{
		return $this->belongTo('App\Model\TFormacion', 'ubigeo');
  }
  public function Tfuncion()
	{
		return $this->belongTo('App\Model\Tfuncion', 'idFuncion');
  }

  public function Tpreguntas2()
	{
		return $this->belongTo('App\Model\Tpreguntas2', 'idPreguntas2');
  }
  public function Tpreguntas3()
	{
		return $this->belongTo('App\Model\Tpreguntas3', 'idPreguntas3');
  }
  public function Taccesos()
	{
		return $this->belongTo('App\Model\Taccesos', 'idt_accesos');
  }
}