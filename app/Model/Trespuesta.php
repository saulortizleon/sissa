<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Trespuesta extends Model
{
    protected $table='t_respuesta';
    protected $primaryKey='idRespuesta';
    public $timestamps=true;
    public $incrementing=false;
    protected $keyType='string';

}