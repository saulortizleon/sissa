<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tapppnsr extends Model
{
    protected $table='t_appaguasane';
    protected $primaryKey='idApp';
    public $timestamps=true;
    public $incrementing=false;
    protected $keyType='string';

    public function Tfuncion()
	{
		return $this->hasMany('App\Model\Tfuncion', 'idFuncion');
	}
}