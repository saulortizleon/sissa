<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Trsap extends Model
{
    protected $table='t_sapriesgo';
    protected $primaryKey='idSapR';
    public $timestamps=true;
    public $incrementing=false;
    protected $keyType='string';

    public function Tcsap()
	{
		return $this->hasMany('App\Model\Tcsap', 'idSapC');
	}
}