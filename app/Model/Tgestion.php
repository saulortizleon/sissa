<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tgestion extends Model
{
    protected $table='gestion';
    protected $primaryKey='idgestion';
    public $timestamps=true;
    public $incrementing=false;
    protected $keyType='string';

}