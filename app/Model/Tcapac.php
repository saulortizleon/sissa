<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tcapac extends Model
{
    protected $table='t_capacitacion';
    protected $primaryKey='idCapCom';
    public $timestamps=true;
    public $incrementing=false;
    protected $keyType='string';

    public function Tfuncion()
	{
		return $this->hasMany('App\Model\Tfuncion', 'idFuncion');
	}
}