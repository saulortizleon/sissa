<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tfuncion extends Model
{
    protected $table='t_funcion';
    protected $primaryKey='idFuncion';
    public $timestamps=true;
    public $incrementing=true;
    protected $keyType='string';

    public function Tatm()
	{
		return $this->hasMany('App\Model\Tatm', 'ubigeo');
    }
    public function Tcjass()
	{
		return $this->hasMany('App\Model\Tcjass', 'idFuncion');
    }
    public function Tcsap()
	{
		return $this->hasMany('App\Model\Tcsap', 'idSapC');
    }
    public function Tapppnsr()
	{
		return $this->belongTo('App\Model\Tappnsr', 'idApp');
  }
  public function Tcomuni()
	{
		return $this->belongTo('App\Model\Tcomuni', 'idComu');
  }
  public function Tcapac()
	{
		return $this->belongTo('App\Model\Tcapac', 'idCapCom');
  }
}