<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tcjass extends Model
{
    protected $table='t_jassconsolidado';
    protected $primaryKey='idjass';
    public $timestamps=true;
    public $incrementing=false;
    protected $keyType='string';

    public function Tfuncion()
	{
		return $this->hasMany('App\Model\Tfuncion', 'idFuncion');
    }
}