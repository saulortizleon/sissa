<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tcsap extends Model
{
    protected $table='t_sapconsolidado';
    protected $primaryKey='idSapC';
    public $timestamps=true;
    public $incrementing=false;
    protected $keyType='string';

    public function Tfuncion()
	{
		return $this->hasMany('App\Model\Tfuncion', 'idFuncion');
    }
    public function Trsap()
	{
		return $this->hasMany('App\Model\Trsap', 'idSapR');
	}
}