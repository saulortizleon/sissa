<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tdocumento extends Model
{
    protected $table='t_documento';
    protected $primaryKey='idDocumento';
    public $timestamps=true;
    public $incrementing=true;
    protected $keyType='string';

    public function Ttramite()
	{
		return $this->belongTo('App\Model\Ttramite', 'idDocumento');
	}
    
    public function Tdocumento()
	{
		return $this->belongTo('App\Model\Tdocumento', 'idSolicitante');
	}
}