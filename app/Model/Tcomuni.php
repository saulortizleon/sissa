<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tcomuni extends Model
{
    protected $table='t_comunicacion';
    protected $primaryKey='idComu';
    public $timestamps=true;
    public $incrementing=false;
    protected $keyType='string';

    public function Tfuncion()
	{
		return $this->hasMany('App\Model\Tfuncion', 'idFuncion');
	}
}