<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tarchivo extends Model
{
    protected $table='t_archivo';
    protected $primaryKey='idArchivo';
    public $timestamps=true;
    public $incrementing=true;
    protected $keyType='string';

}