<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tedusa extends Model
{
    protected $table='edusa';
    protected $primaryKey='idEdusa';
    public $timestamps=true;
    public $incrementing=false;
    protected $keyType='string';

}