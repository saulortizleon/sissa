<?php
namespace App\Model\blog;

use Illuminate\Database\Eloquent\Model;

class TPublicacion extends Model
{
    protected $table='t_publicacion';
    protected $primaryKey='idPublicacion';
    public $timestamps=true;
    public $incrementing=true;
    protected $keyType='string';

    public function Tusuario()
	  {
		return $this->belongTo('App\Model\Tusuario', 'dni');
    }
    public function Timg1()
	  {
		return $this->belongTo('App\Model\blog\Timg1', 'idPublicacion');
    }
    public function Timg2()
	  {
		return $this->belongTo('App\Model\blog\Timg2', 'idPublicacion');
    }

}