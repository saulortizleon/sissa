<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tmante extends Model
{
    protected $table='mantenimiento';
    protected $primaryKey='idMante';
    public $timestamps=true;
    public $incrementing=false;
    protected $keyType='string';

}