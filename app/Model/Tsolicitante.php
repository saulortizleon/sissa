<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tsolicitante extends Model
{
    protected $table='t_solicitante';
    protected $primaryKey='idSolicitante';
    public $timestamps=true;
    public $incrementing=false;
    protected $keyType='string';

}