<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Ttramite extends Model
{
    protected $table='t_tramite';
    protected $primaryKey='nroTramite';
    public $timestamps=true;
    public $incrementing=true;
    protected $keyType='string';

}