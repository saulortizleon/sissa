<?php
namespace App\Helpers;

use Illuminate\Http\Request;

use Mail;
use Session;
use Redirect;

class SendEmail
{
    public function SendEmailConfirmation($documento)
    {
        dd($documento);
        Mail::send('documento/correo',function($x) use ($documento)
        {   
          
            $x->from(env('MAIL_USERNAME'),'drvcs.regionapurimac.gob.pe');
            $x->to($documento->email, ($documento->firstName).' '.($documento->lastName))->subject('DRVCS APURÍMAC - MESA DE PARTES VIRTUAL');
        });
    }
}