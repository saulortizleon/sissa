<?php

namespace App\Http\Middleware;
use App\Model\Tusuario;
use Closure;
use Session;
use Redirect;

class MDadmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $user = Tusuario::find(Session::get('t_usuario')[0]);

      if($user != null && $user->cargo == "adm")
        {
            return $next($request);
        }

        Session::forget('t_usuario');

        return Redirect::to('login');
    }
}
