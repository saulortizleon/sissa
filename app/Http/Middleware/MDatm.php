<?php

namespace App\Http\Middleware;
use App\Model\Tusuario;
use Closure;
use Session;
use Redirect;

class MDatm
{
    public function handle($request, Closure $next)
    {

      $tusuario = Tusuario::find(Session::get('t_usuario')[0]);

      if($tusuario != null && $tusuario->cargo == "atm")
        {
          $response = $next($request);
          return $response;
        }
        Session::forget('t_usuario');
        return Redirect::to('login');
    }

}