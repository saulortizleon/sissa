<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use App\Helpers\Messages;
use App\Model\Tcjass;

use Redirect;
use DB;
use File;
use Session;
class CjassController extends Controller
{
    public function listaCjass(Request $request)
    {
        $dni=Session::get('t_usuario')[0];
        if($dni != null)
        {
            $listaCjass=DB::table('t_atm')
                        ->join('t_funcion', 't_atm.ubigeo', '=', 't_funcion.ubigeo')
                        ->join('t_jassconsolidado', 't_jassconsolidado.idFuncion', '=', 't_funcion.idFuncion')
                        ->where('t_atm.dniUsuario',  $dni)
                        ->select('t_jassconsolidado.*')
                        ->get();

            return view('cjass/lista',['listaCjass'=> $listaCjass]);
        }
    }

    public function editarCjass(Request $request,Messages $messages,$idjass=null)
    {
        if($_POST)
        {
            try
            {
                $cjass = Tcjass::find($request->get('idjass'));
                $cjass->totalJass = $request->get('totalJass');
                $cjass->totalJassReg = $request->get('totalJassReg');
                $cjass->cpPadronNominal= $request->get('cpPadronNominal');

                if($request->hasFile('ver1'))
                {
                    $random=rand().time();
                    $extension = strtolower($request->file('ver1')->getClientOriginalExtension());
                    $request->file('ver1')->move(public_path().'/cjass',$random.'.'.$extension );
                    $cjass->ver1 = $request->get('hiddenUrl').'/cjass/'.$random.'.'.$extension;
                }
                $cjass->save();
                DB::commit();
                return $messages->MessageCorrect('Lista de consolidado jass actualizada','cjass/lista');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('No se pudo realizar los cambios, consulte con el administrador','cjass/lista');
            }

        }

        $listaEditar = Tcjass::find($idjass);
        return view('cjass/editar',['listaEditar' => $listaEditar]);
    }
}