<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use App\Helpers\Messages;
use App\Model\Tfuncion;
use App\Model\Tatm;
use Redirect;
use DB;
use File;
use Session;

class FuncionController extends Controller
{
    public function listaFuncion(Request $request)
    {
        $dni=Session::get('t_usuario')[0];
        if($dni != null)
        {
            $listaFuncion=DB::table('t_atm')
                        ->join('t_funcion', 't_atm.ubigeo', '=', 't_funcion.ubigeo')
                        ->where('t_atm.dniUsuario', $dni)
                        ->select('t_funcion.*','t_atm.distrito')
                        ->get();
            return view('funcion/lista',['listaFuncion'=> $listaFuncion]);
        }

    }
    public function insertarHerr(Request $request,Messages $messages)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
                $funcion = new Tfuncion();

                $funcion->idHerr = uniqid();
                $funcion->oficinaPropia = $request->get('oficina');
                $funcion->equipocomputo = $request->get('compu');
                $funcion->multimedia= $request->get('multimedia');
                $funcion->estante= $request->get('estante');
                $funcion->moto= $request->get('moto');
                $funcion->comparadorCloro= $request->get('comparador');
                $funcion->turbidimetro= $request->get('turbi');
                $funcion->gps= $request->get('gps');
                $funcion->wincha= $request->get('wincha');
                $funcion->pastillasdpd= $request->get('pastilla');
                $funcion->cronometro= $request->get('cronometro');
                $funcion->mascarilla= $request->get('mascarilla');
                $funcion->guantes= $request->get('guantes');
                $funcion->overol= $request->get('overol');
                $funcion->lentesProteccion= $request->get('lentes');
                $funcion->botasJebe= $request->get('botas');
                $funcion->casco= $request->get('casco');

                if($request->hasFile('kit'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('kit')->getClientOriginalExtension());
                    $request->file('kit')->move(public_path().'/herramienta',$random.'.'.$extension );

                    $funcion->ver1 = $request->get('hiddenUrl').'/herramienta/'.$random.'.'.$extension;
                }
                if($request->hasFile('epp'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('epp')->getClientOriginalExtension());
                    $request->file('epp')->move(public_path().'/herramienta',$random.'.'.$extension );

                    $funcion->ver2 = $request->get('hiddenUrl').'/herramienta/'.$random.'.'.$extension;
                }
                if($request->hasFile('utiles'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('utiles')->getClientOriginalExtension());
                    $request->file('utiles')->move(public_path().'/herramienta',$random.'.'.$extension );

                    $funcion->ver3 = $request->get('hiddenUrl').'/herramienta/'.$random.'.'.$extension;
                }
                $dni=Session::get('t_usuario')[0];
                $ubigeo= DB::table('t_atm')->where('dniUsuario',$dni)->first()->ubigeo;

                $funcion->ubigeo= $ubigeo;

                $funcion->save();
                DB::commit();

                return $messages->MessageCorrect('Lista de funcion actualizada','herram/lista');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('No se pudo realizar los cambios, consulte con el administrador','herram/lista');
            }
        }
        return view('herram/lista');
    }
    public function editarFuncion(Request $request,Messages $messages,$idFuncion=null)
    {
        if($_POST)
        {
            try
            {
                $funcion = Tfuncion::find($request->get('idFuncion'));
                $funcion->cantidadTrabaj = $request->get('cantidadTrabaj');
                $funcion->estadoSalaSitua= $request->get('estadoSalaSitua');
                $funcion->ordMunjass= $request->get('ordMunjass');
                $funcion->libroJass= $request->get('libroJass');

                if($request->hasFile('ver1'))
                {
                    $random=rand().time();
                    $extension = strtolower($request->file('ver1')->getClientOriginalExtension());
                    $request->file('ver1')->move(public_path().'/funcion',$random.'.'.$extension );
                    $funcion->ver1 = $request->get('hiddenUrl').'/funcion/'.$random.'.'.$extension;
                }

                if($request->hasFile('ver2'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver2')->getClientOriginalExtension());
                    $request->file('ver2')->move(public_path().'/funcion',$random.'.'.$extension );

                    $funcion->ver2 = $request->get('hiddenUrl').'/funcion/'.$random.'.'.$extension;
                }
                if($request->hasFile('ver3'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver3')->getClientOriginalExtension());
                    $request->file('ver3')->move(public_path().'/funcion',$random.'.'.$extension );

                    $funcion->ver3 = $request->get('hiddenUrl').'/funcion/'.$random.'.'.$extension;
                }
                if($request->hasFile('ver4'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver4')->getClientOriginalExtension());
                    $request->file('ver4')->move(public_path().'/funcion',$random.'.'.$extension );

                    $funcion->ver4 = $request->get('hiddenUrl').'/funcion/'.$random.'.'.$extension;
                }
                $dni=Session::get('t_usuario')[0];
                $ubigeo= DB::table('t_atm')->where('dniUsuario',$dni)->first()->ubigeo;

                $funcion->ubigeo= $ubigeo;

                $funcion->save();
                DB::commit();

                return $messages->MessageCorrect('Lista de funcion actualizada','funcion/lista');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('No se pudo realizar los cambios, consulte con el administrador','funcion/lista');
            }
        }

        $listaEditar = Tfuncion::find($idFuncion);

        return view('funcion/editar',['listaEditar' => $listaEditar]);

    }


}
