<?php

namespace App\Http\Controllers;

use App\Model\Tcapac;
use Illuminate\Http\Request;


use Session;
use App\Helpers\Messages;
use DB;
class CapacController extends Controller
{
    public function listaCapac(Request $request)
    {
        $dni=Session::get('t_usuario')[0];
        if($dni != null)
        {
            $listaCapac=DB::table('t_atm')
                        ->join('t_funcion', 't_atm.ubigeo', '=', 't_funcion.ubigeo')
                        ->join('t_capacitacion', 't_capacitacion.idFunci', '=', 't_funcion.idFuncion')
                        ->where('t_atm.dniUsuario',  $dni)
                        ->select('t_capacitacion.*')
                        ->get();

            return view('capac/lista',['listaCapac'=> $listaCapac]);
        }
    }

    public function editarCapac(Request $request,Messages $messages,$idCapCom=null)
    {

        if($_POST)
        {
            try
            {
                $capac = Tcapac::find($request->get('idCapCom'));

                $capac->capEstatutoR = $request->get('capEstatutoR');
                $capac->capPoa = $request->get('capPoa');
                $capac->capAOM= $request->get('capAOM');
                $capac->capPartesSap= $request->get('capPartesSap');
                $capac->capDesinfClorac= $request->get('capDesinfClorac');

                if($request->hasFile('ver1'))
                {
                    $random=rand().time();
                    $extension = strtolower($request->file('ver1')->getClientOriginalExtension());
                    $request->file('ver1')->move(public_path().'/capac',$random.'.'.$extension );

                    $capac->ver1 = $request->get('hiddenUrl').'/capac/'.$random.'.'.$extension;
                }

                if($request->hasFile('ver2'))
                {
                    $random=rand().time();
                    $extension = strtolower($request->file('ver2')->getClientOriginalExtension());
                    $request->file('ver2')->move(public_path().'/capac',$random.'.'.$extension );

                    $capac->ver2 = $request->get('hiddenUrl').'/capac/'.$random.'.'.$extension;
                }

                if($request->hasFile('ver3'))
                {
                    $random=rand().time();
                    $extension = strtolower($request->file('ver3')->getClientOriginalExtension());
                    $request->file('ver3')->move(public_path().'/capac',$random.'.'.$extension );

                    $capac->ver3 = $request->get('hiddenUrl').'/capac/'.$random.'.'.$extension;
                }

                if($request->hasFile('ver4'))
                {
                    $random=rand().time();
                    $extension = strtolower($request->file('ver4')->getClientOriginalExtension());
                    $request->file('ver4')->move(public_path().'/capac',$random.'.'.$extension );

                    $capac->ver4 = $request->get('hiddenUrl').'/capac/'.$random.'.'.$extension;
                }

                if($request->hasFile('ver5'))
                {
                    $random=rand().time();
                    $extension = strtolower($request->file('ver5')->getClientOriginalExtension());
                    $request->file('ver5')->move(public_path().'/capac',$random.'.'.$extension );

                    $capac->ver5 = $request->get('hiddenUrl').'/capac/'.$random.'.'.$extension;
                }
                $capac->save();
                DB::commit();

                return $messages->MessageCorrect('Lista de capacitacion actualizada','capac/lista');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('No se pudo realizar los cambios, consulte con el administrador','capac/lista');
            }
        }
        $listaEditar = Tcapac::find($idCapCom);
        return view('capac/editar',['listaEditar' => $listaEditar]);
    }
}