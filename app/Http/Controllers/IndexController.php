<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Http\ajax;
use Session;
use App\Model\blog\Tpublicacion;
use App\Model\blog\Tagenda;
use Response;

class IndexController extends Controller
{
    public function IndexSup()
    {
        return view('index/indexsup');
    }
    public function IndexVivi()
    {
        return view('index/indexvivienda');
    }
  
    public function IndexMas()
    {
        return view('index/indexmaster');
    }
    public function IndexAdm()
    {
        return view('index/indexadm');
    }
    public function IndexPub()
    {
        return view('index/indexpub');
    }
    public function Index(Request $request)
    {
         //$comunicado=Tpublicacion::where('categoria','comunicado')->orderBy('created_at', 'DESC')->paginate(1);
      // dd($publicacion);
      $noticias=Tpublicacion::where([['estado','activo']])->orderBy('created_at', 'DESC')->paginate(6);
      
        return view('index/indexblog',['noticias'=> $noticias]);
    }
    public function header(Request $request)
    {
        $publicacion=Tpublicacion::where([['estado','activo'],['categoria','noticia'],['portada','SI']])->orderBy('created_at', 'DESC')->paginate(3);
       
        return view('layouts/header',['publicacion'=> $publicacion]);
    }
    //concurso ATM 2021

    public function concursoAtm()
    {
        return view('blog/concurso/principal');
    }
}