<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use App\Helpers\Messages;
use App\Model\Tcomuni;

use Redirect;
use DB;
use File;
use Session;
class ComuniController extends Controller
{
    public function listaComu(Request $request)
    {
        $dni=Session::get('t_usuario')[0];
        if($dni != null)
        {
            $listaComu=DB::table('t_atm')
                        ->join('t_funcion', 't_atm.ubigeo', '=', 't_funcion.ubigeo')
                        ->join('t_comunicacion', 't_comunicacion.idFuncion', '=', 't_funcion.idFuncion')
                        ->where('t_atm.dniUsuario',  $dni)
                        ->select('t_comunicacion.*')
                        ->get();

            return view('comuni/lista',['listaComu'=> $listaComu]);
        }
    }

    public function editarComuni(Request $request,Messages $messages,$idComu=null)
    {
        if($_POST)
        {
            try
            {
                $comuni = Tcomuni::find($request->get('idComu'));

                $comuni->capCuotaF = $request->get('capCuotaF');
                $comuni->capCuidadoAp = $request->get('capCuidadoAp');
                $comuni->capCorrectoUsoAp= $request->get('capCorrectoUsoAp');
                $comuni->capLavadoManos= $request->get('capLavadoManos');
                $comuni->capUsoMantUBS= $request->get('capUsoMantUBS');
                $comuni->capRRSS= $request->get('capRRSS');
                $comuni->fechaReporte= $request->get('fechaReporte');
                if($request->hasFile('verificable'))
                {
                    $random=rand().time();
                    $extension = strtolower($request->file('verificable')->getClientOriginalExtension());
                    $request->file('verificable')->move(public_path().'/comuni',$random.'.'.$extension );

                    $comuni->verificable = $request->get('hiddenUrl').'/comuni/'.$random.'.'.$extension;
                }
                $comuni->save();
                DB::commit();

                return $messages->MessageCorrect('Lista de difusion actualizada','comuni/lista');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('No se pudo realizar los cambios, consulte con el administrador','comuni/lista');
            }
        }
        $listaEditar = Tcomuni::find($idComu);
        return view('comuni/editar',['listaEditar' => $listaEditar]);
    }
}