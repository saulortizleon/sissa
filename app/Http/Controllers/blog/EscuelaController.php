<?php

namespace App\Http\Controllers\blog;

use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;

use Illuminate\Support\Str as Str;

use App\Helpers\RandomStrings;
use App\Helpers\Messages;
use App\Model\blog\Tpublicacion;
use App\Model\Ttemas;
 
use App\Validation\Publicacion;

use Redirect;
use DB;
use Mail;
use Session;

class EscuelaController extends Controller
{ 
    public function listar(Request $request)
    {
        $escuela= Tpublicacion::where('tema','escuela')->orderBy('created_at', 'DESC')->paginate(6);

        return view('blog/app/escuela',compact('escuela'));
    }

}