<?php

namespace App\Http\Controllers\blog;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use App\Model\Tusuario;
use App\Model\blog\Tpublicacion;
use App\Helpers\Messages;

use Session;
use DB;

class Sitemapcontroller extends Controller
{
    public function sitemap()
    {
        $posts = Tpublicacion::orderBy('updated_at', 'DESC')->where('estado','activo')->get();

        return response()->view('sitemap', compact('posts'))->header('Content-Type', 'text/xml');
    }
}
?>