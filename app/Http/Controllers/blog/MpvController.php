<?php

namespace App\Http\Controllers\blog;

use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;

use Illuminate\Support\Str as Str;

use App\Helpers\RandomStrings;
use App\Helpers\Messages;
use App\Helpers\SendEmail;

use App\Model\blog\Tpublicacion;
use App\Model\Ttemas;
use App\Model\Tsolicitante;
use App\Model\Tdocumento;
use App\Model\Ttramite;

use App\Validation\Publicacion;

use DateTime;
use Redirect;
use DB;
use Mail;
use Session;


class MpvController extends Controller
{
    public function insertar(Request $request,Messages $messages)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
                $solicitante = new Tsolicitante();

                $date = date("Y");

                $solicitante->idSolicitante = $date.'-'.$randomString = Str::random(5);

                $solicitante->tipoSolicitante= $request->get('tipoPersona');
                $solicitante->ruc= $request->get('ruc');
                $solicitante->dni = $request->get('dni');
                $solicitante->nombres= $request->get('nombres');
                $solicitante->direccion= $request->get('direccion');
                $solicitante->correoElectronico= $request->get('correo');
                $solicitante->telefono= $request->get('cel');
                $solicitante->razonSocial= $request->get('razonSocial');

                $documento = new Tdocumento();

                $documento->idDocumento = $date.'-'.$randomString = Str::random(5);
                $documento->nroDocumento = $request->get('numeroDocumento');
                $documento->tipoDocumento = $request->get('tipoDocumento');
                $documento->asunto = $request->get('asunto');
                $documento->estado= 'enviado'; /* enviado,recibido*/
                $date = date("Y-m-d");

                if($request->hasFile('documento'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('documento')->getClientOriginalExtension());
                    $request->file('documento')->move(public_path().'/mesadepartes',$date.'-'.$randomString = Str::random(5).'.'.$extension );

                    $documento->archivo = $request->get('hiddenUrl').'/mesadepartes/'.$date.'-'.$randomString = Str::random(5).'.'.$extension ;
                }
                $documento->nroFolios = $request->get('folios');
                $documento->idSolicitante = $solicitante->idSolicitante;

                $solicitante->save();
                $documento->save();

                DB::commit();

                return $messages->MessageCorrect('Documento ingresado correctamente','mpv');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('No se pudo enviar el documento, consulte con el administrador','mpv');
            }
        }
        return view('blog/app/mpv');
    }

    public function tramite(Request $request,Messages $messages)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
                $solicitante = new Tsolicitante();

                $date = date("Y");

                $solicitante->idSolicitante = $date.'-'.$randomString = Str::random(5);

                $solicitante->tipoSolicitante= $request->get('tipoPersona');
                $solicitante->ruc= $request->get('ruc');
                $solicitante->dni = $request->get('dni');
                $solicitante->nombres= $request->get('nombres');
                $solicitante->direccion= $request->get('direccion');
                $solicitante->correoElectronico= $request->get('correo');
                $solicitante->telefono= $request->get('cel');
                $solicitante->razonSocial= $request->get('razonSocial');

                $documento = new Tdocumento();

                $documento->idDocumento = $date.'-'.$randomString = Str::random(5);
                $documento->nroDocumento = $request->get('numeroDocumento');
                $documento->tipoDocumento = $request->get('tipoDocumento');
                $documento->asunto = $request->get('asunto');

                $date = date("Y-m-d");

                if($request->hasFile('documento'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('documento')->getClientOriginalExtension());
                    $request->file('documento')->move(public_path().'/mesadepartes',$date.'-'.$randomString = Str::random(5).'.'.$extension);

                    $documento->archivo = $request->get('hiddenUrl').'/mesadepartes/'.$date.'-'.$randomString = Str::random(5).'.'.$extension;
                }
                $documento->nroFolios = $request->get('folios');
                $documento->idSolicitante = $solicitante->idSolicitante;

                $solicitante->save();
                $documento->save();

                DB::commit();

                return $messages->MessageCorrect('Expediente generado correctamente','lista');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('No se pudo GENERAR el expediente, consulte con el administrador','lista');
            }
        }
        $documento->nroDocumento = $request->get('numeroDocumento');
        $listaDocumentos=DB::table('t_documento')
                        ->join('t_tramite', 't_tramite.idDocumento', '=', 't_documento.idDocumento')
                        ->join('t_solicitante', 't_solicitante.idSolicitante', '=', 't_documento.idSolicitante')
                        ->select('*')
                        ->paginate(2);

        return view('supervision/tramite/lista',['listaDocumentos'=> $listaDocumentos]);

    }

    public function documento(Request $request,Messages $messages,SendEmail $sendEmail)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();

                $tramite = new Ttramite();
                $tramite->idDocumento=$request->get('idDocumento');
                $tramite->estado= 'en tramite';
                $tramite->tipotramite= 'virtual';

                // enviar correo
                $documento = DB::table('t_documento')
                ->join('t_solicitante', 't_documento.idSolicitante', '=', 't_solicitante.idSolicitante')
                ->where('t_documento.idDocumento','=',$request->get('idDocumento'))->get();

                $documento->estado= 'recibido';

                $sendEmail->SendEmailConfirmation($documento);

                $tramite->save();
                $documento->save();

                DB::commit();

                return $messages->MessageCorrect('Expediente generado correctamente','lista');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('No se pudo GENERAR el expediente, consulte con el administrador','lista');
            }
        }

       $buscar=trim($request->get('buscar'));

       $listaDocumentos=DB::table('t_documento')
                        ->join('t_solicitante', 't_solicitante.idSolicitante', '=', 't_documento.idSolicitante')
                        ->where('t_documento.asunto','like','%'.$buscar.'%')
                        ->orwhere('t_solicitante.nombres','like','%'.$buscar.'%')
                        ->orderBy('t_documento.created_at', 'desc')
                        ->paginate(4);

        return view('supervision/documentos/lista',['listaDocumentos'=> $listaDocumentos, 'buscar'=> $buscar]);
    }
}
