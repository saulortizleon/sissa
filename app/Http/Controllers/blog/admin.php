<?php

namespace App\Http\Controllers\blog;

use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;

use Illuminate\Support\Str as Str;

use App\Helpers\RandomStrings;
use App\Helpers\Messages;
use App\Model\blog\Tpublicacion;
use App\Model\Ttemas;
use App\Model\Tcategoria;
use App\Model\Tarchivos;
use App\Model\Timgp;
use App\Model\Timg1;
use App\Model\Timg2;
use App\Model\Tdoc;
use App\Validation\Publicacion;

use Redirect;
use DB;
use Mail;
use Session;

class AdminController extends Controller
{
   public function adminNoticias()
    {
        $publicacion=Tpublicacion::where('estado','activo')->get();

        return view('blog/admin/noticias/lista',['publicacion'=> $publicacion]);
    }

    public function insertarNoticia(Request $request,Messages $messages)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
                $publicacion = new Tpublicacion();
                $publicacion->idPublicacion = uniqid();
                $temp= $publicacion->idPublicacion;
                $publicacion->titulo = $request->get('titulo');
                $publicacion->subtitulo = $request->get('subtitulo');
                $publicacion->contenido= $request->get('contenido');
                $publicacion->estado= $request -> get('estado');
                $dni=Session::get('t_usuario')[0];
                $publicacion->dni= $dni;
                $publicacion->idCat=$request->get('categorias');
                $publicacion->idTema=$request->get('temas');
                $publicacion->save();

                $imgp = new Timgp();
                $imgp->idimgp = uniqid();
                $imgp->titulo = $request->get('tituloimgp');
                $imgp->slug = Str::slug($imgp->titulo);
                $imgp->idpub = $temp;
                if($request->hasFile('imagenp'))
                {
                    $extension = strtolower($request->file('imagenp')->getClientOriginalExtension());
                    $request->file('imagenp')->move(public_path().'/imgprincipal',$imgp->slug.'.'.$extension );

                    $imgp->archivo = $request->get('hiddenUrl').'/imgprincipal/'.$imgp->slug.'.'.$extension;
                }
                $imgp->save();

                $img1 = new Timg1();
                $img1->idimg1 = uniqid();
                $img1->titulo = $request->get('tituloimg1');
                $img1->slug = Str::slug($img1->titulo);
                $img1->idPublicacion = $temp;
                if($request->hasFile('img1'))
                {
                    $extension = strtolower($request->file('img1')->getClientOriginalExtension());
                    $request->file('img1')->move(public_path().'/galeria',$img1->slug.'.'.$extension );

                    $img1->archivo = $request->get('hiddenUrl').'/galeria/'.$img1->slug.'.'.$extension;
                }

                $img1->save();

                $img2 = new Timg2();
                $img2->idimg2 = uniqid();
                $img2->titulo = $request->get('tituloimg2');
                $img2->slug = Str::slug($img2->titulo);
                $img2->idPublicacion = $temp;
                if($request->hasFile('img2'))
                {
                    $extension = strtolower($request->file('img2')->getClientOriginalExtension());
                    $request->file('img2')->move(public_path().'/galeria',$img2->slug.'.'.$extension );

                    $img2->archivo = $request->get('hiddenUrl').'/galeria/'.$img2->slug.'.'.$extension;
                }

                $img2->save();
                $doc = new Tdoc();
                $doc->iddoc = uniqid();
                $doc->nombre = $request->get('titulodoc');
                $doc->slug = Str::slug($doc->nombre);

                if($request->hasFile('archivo'))
                {
                    $extension = strtolower($request->file('archivo')->getClientOriginalExtension());
                    $request->file('archivo')->move(public_path().'/documentos',$doc->slug.'.'.$extension );

                    $doc->archivo = $request->get('hiddenUrl').'/documentos/'.$doc->slug.'.'.$extension;
                }
                $doc->save();
                DB::commit();

                return $messages->MessageCorrect('Noticias publicada','adm/noticias');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('Registro de usuario incorrecto','adm/noticias');
            }
        }
        $temas=Ttemas::where('estado','activo')->get();
        $categoria=Tcategoria::where('estado','activo')->get();
        return view('blog/admin/noticias/insertar',['categoria'=>$categoria, 'temas'=>$temas]);
    }

    public function editarNoticia(Request $request,Messages $messages, $idPublicacion=null)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
                $publicacion = Tpublicacion::find($request->get('idPublicacion'));

                $temp= $publicacion->idPublicacion;
                $publicacion->titulo = $request->get('titulo');
                $publicacion->subtitulo = $request->get('subtitulo');
                $publicacion->contenido= $request->get('contenido');
                $publicacion->estado= $request -> get('estado');
                $dni=Session::get('t_usuario')[0];
                $publicacion->dni= $dni;
                $publicacion->idCat=$request->get('categorias');
                $publicacion->idTema=$request->get('temas');
                $publicacion->save();

                $imgp =Timgp::find($request->get('idPublicacion'));

                $imgp->titulo = $request->get('tituloimgp');
                $imgp->slug = Str::slug($imgp->titulo);
                $imgp->idpub = $temp;
                if($request->hasFile('imagenp'))
                {
                    $extension = strtolower($request->file('imagenp')->getClientOriginalExtension());
                    $request->file('imagenp')->move(public_path().'/imgprincipal',$imgp->slug.'.'.$extension );

                    $imgp->archivo = $request->get('hiddenUrl').'/imgprincipal/'.$imgp->slug.'.'.$extension;
                }
                $imgp->save();

                $img1 =  Timg1::find($request->get('idPublicacion'));

                $img1->titulo = $request->get('tituloimg1');
                $img1->slug = Str::slug($img1->titulo);
                $img1->idPublicacion = $temp;
                if($request->hasFile('img1'))
                {
                    $extension = strtolower($request->file('img1')->getClientOriginalExtension());
                    $request->file('img1')->move(public_path().'/galeria',$img1->slug.'.'.$extension );

                    $img1->archivo = $request->get('hiddenUrl').'/galeria/'.$img1->slug.'.'.$extension;
                }

                $img1->save();

                $img2 = Timg2::find($request->get('idPublicacion'));

                $img2->titulo = $request->get('tituloimg2');
                $img2->slug = Str::slug($img2->titulo);
                $img2->idPublicacion = $temp;
                if($request->hasFile('img2'))
                {
                    $extension = strtolower($request->file('img2')->getClientOriginalExtension());
                    $request->file('img2')->move(public_path().'/galeria',$img2->slug.'.'.$extension );

                    $img2->archivo = $request->get('hiddenUrl').'/galeria/'.$img2->slug.'.'.$extension;
                }

                $img2->save();
                $doc = Tdoc::find($request->get('idPublicacion'));

                $doc->nombre = $request->get('titulodoc');
                $doc->slug = Str::slug($doc->nombre);

                if($request->hasFile('archivo'))
                {
                    $extension = strtolower($request->file('archivo')->getClientOriginalExtension());
                    $request->file('archivo')->move(public_path().'/documentos',$doc->slug.'.'.$extension );

                    $doc->archivo = $request->get('hiddenUrl').'/documentos/'.$doc->slug.'.'.$extension;
                }
                $doc->save();
                DB::commit();

                return $messages->MessageCorrect('Noticias editada correctamente','adm/noticias');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('Hubo un error al editar la noticia','adm/noticias');
            }
        }
       /* $temacat=DB::table('t_publicacion as r')
                        ->join('t_temas as a', 'a.idTemas', '=', 'r.idTema')
                        ->join('t_categoria as e', 'e.idCategoria', '=', 'r.idCat')
                        ->where('r.idPublicacion', $idPublicacion)
                        ->select('a.nombre','a.idTemas','e.idCategoria','e.nombre')
                        ->get();
        */
       $listaEditar=Tpublicacion::with('Tcategoria','t_categoria')->get();
       /* $listaEditar=DB::table('t_publicacion as r')
                       ->join('t_imgp as c', 'c.idpub', '=', 'r.idPublicacion')
                        ->join('t_temas as d', 'd.idTemas', '=', 'r.idTema')
                        ->join('t_categoria as e', 'e.idCategoria', '=', 'r.idCat')
                        ->join('t_documento as f', 'f.idPublicacion', '=', 'r.idPublicacion')
                        ->join('t_img1 as a', 'a.idPublicacion', '=', 'r.idPublicacion')
                        ->join('t_img2 as b', 'b.idPublicacion', '=', 'r.idPublicacion')
                        ->where('r.idPublicacion', $idPublicacion)
                        ->select('*')->get();*/
        dd($listaEditar);
        return view('blog/admin/noticias/editar',['listaEditar'=>$listaEditar]);
    }
    public function listaCategoria()
    {
        $categoria= Tcategoria::orderBy('created_at', 'DESC')->paginate(40);

        return view('blog/admin/categoria/lista',['categoria'=>$categoria]);
    }
    public function listaTemas()
    {
        $temas= Ttemas::orderBy('created_at', 'DESC')->paginate(40);

        return view('blog/admin/temas/lista',['temas'=>$temas]);
    }
    public function insertarTema(Request $request,Messages $messages)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
                $tema = new Ttemas();

                $tema->idTemas = uniqid();
                $tema->nombre = $request->get('nombre');
                $tema->descripcion = $request->get('descripcion');
                $tema->slug = Str::slug($tema->nombre);
                $tema->estado = $request->get('estado');

                $tema->save();
                DB::commit();

                return $messages->MessageCorrect('Tema publicado','adm/temas');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('Hay un error con el tema','adm/temas');
            }
        }
        return view('adm/temas');
    }
    public function editarTema(Request $request,Messages $messages, $idTemas=null)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
                $tema = Ttemas::find($request->get('idTemas'));
                $tema->nombre = $request->get('nombre');
                $tema->descripcion = $request->get('descripcion');
                $tema->slug = Str::slug($tema->nombre);
                $tema->estado = $request->get('estado');

                $tema->save();
                DB::commit();

                return $messages->MessageCorrect('Tema editado correctamente','adm/temas');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('Hay un error con el tema','adm/temas');
            }
        }
        $listaEditar = Ttemas::find($idTemas);
        return view('admin/temas/editar',['listaEditar' => $listaEditar]);
    }
    public function insertarCat(Request $request,Messages $messages)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
                $cat = new Tcategoria();

                $cat->idCategoria = uniqid();
                $cat->nombre = $request->get('nombre');
                $cat->descripcion = $request->get('descripcion');
                $cat->slug = Str::slug($cat->nombre);
                $cat->estado = $request->get('estado');
                $cat->save();
                DB::commit();
                return $messages->MessageCorrect('Categoria publicada','adm/categoria');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('Hay un error con la categoria','adm/categoria');
            }
        }
        return view('adm/categoria');
    }
    public function editarCategoria(Request $request,Messages $messages, $idCategoria=null)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
                $caregoria = Tcategoria::find($request->get('idCategoria'));

                $caregoria->nombre = $request->get('nombre');
                $caregoria->descripcion = $request->get('descripcion');
                $caregoria->slug = Str::slug($caregoria->nombre);
                $caregoria->estado = $request->get('estado');

                $caregoria->save();
                DB::commit();

                return $messages->MessageCorrect('Categoria editado correctamente','adm/categoria');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('Hay un error con la categoria','adm/categoria');
            }
        }
        $listaEditar = Tcategoria::find($idCategoria);
        return view('admin/categoria/editar',['listaEditar' => $listaEditar]);
    }
    public function editarAgenda(Request $request,Messages $messages, $idAgenda = null)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
                $agenda =  Tagenda::find($request->get('idAgenda'));

                $agenda->idAgenda = uniqid();
                $agenda->nombre = $request->get('nombre');
                $agenda->fecha = $request->get('fecha');
                $agenda->hora= $request->get('hora');
                $agenda->lugar= $request->get('lugar');
                $agenda->estado= $request->get('estado');
                $agenda->tag= $request->get('tag');
                $dni=Session::get('t_usuario')[0];
                $agenda->dni=$dni;

                $agenda->save();
                DB::commit();

                return $messages->MessageCorrect('Agenda publicada','adm/agenda');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('Hay un error con la agenda','adm/agenda');
            }
        }

 		$listAgenda = Tagenda::find($idAgenda);

 		return view('agenda/edit',['listAgenda' => $listAgenda]);
    }
    public function eliminarArchivo($idDoc, Messages $messages)
    {

        try
        {
            DB::beginTransaction();

            $archivo = Tarchivos::find($idDoc);

            $archivo->estado = "Inactivo";

            $archivo->save();

            DB::commit();
            return $messages->MessageCorrect('archivo  eliminada','adm/archivos');
        }
        catch(\Exception $e)
        {
            DB::rollback();
            return $messages->MessageCorrect('Hubo un error, intente nuevamente','adm/archivos');
        }

    }
    public function eliminarNoticia($idNoticia, Messages $messages)
    {
        try
        {
            DB::beginTransaction();

            $noticia = Tpublicacion::find($idNoticia);

            $noticia->estado = "Inactivo";

            $noticia->save();

            DB::commit();


            return $messages->MessageCorrect('noticia  eliminada','adm/noticias');
        }
        catch(\Exception $e)
        {
            DB::rollback();
            return $messages->MessageCorrect('Hubo un error, intente nuevamente','adm/noticias');
        }

    }
    public function eliminarAgenda($idAgenda, Messages $messages)
 	{
 		try
 		{
 			DB::beginTransaction();

 			$agenda = Tagenda::find($idAgenda);

	 		$agenda->estado = "Inactivo";

	 		$agenda->save();

	 		DB::commit();


	 		return $messages->MessageCorrect('Agenda eliminada','adm/agenda');
 		}
 		catch(\Exception $e)
 		{
 			DB::rollback();
	 		return $messages->MessageCorrect('Hubo un error, intente nuevamente','adm/agenda');
 		}

 	}
    public function adminAgenda()
    {
        $agenda=Tagenda::where('estado','activo')->get();

        return view('admin/agenda/lista',['agenda'=> $agenda]);
    }

    public function insertarArchivos(Request $request,Messages $messages)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
                $archivos = new Tarchivos();

                $archivos->idDoc = uniqid();
                if($request->hasFile('archivo'))
                {
                    $extension = strtolower($request->file('archivo')->getClientOriginalExtension());
                    $request->file('archivo')->move(public_path().'/archivos',$archivos->idDoc.'.'.$extension );

                    $archivos->extension = $request->get('hiddenUrl').'/archivos/'.$archivos->idDoc.'.'.$extension;
                }
                $archivos->nombre = $request->get('nombre');
                $archivos->estado= $request->get('estado');
                $archivos->categoria= $request->get('categoria');
                $dni=Session::get('t_usuario')[0];
                $archivos->dni= $dni;

                $archivos->save();
                DB::commit();

                return $messages->MessageCorrect('archivo publicado','adm/archivos');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('Hay un error con el archivo','adm/archivos');
            }
        }
        return view('adm/archivos');
    }

    public function adminArchivos()
    {
        $archivos=Tarchivos::where('estado','activo')->get();

        return view('admin/archivos/lista',['archivos'=> $archivos]);
    }

    public function mpv()
    {
        return view('blog/app/mpv');
    }
 

}