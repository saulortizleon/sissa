<?php

namespace App\Http\Controllers\blog;
use App\Http\Controllers\Controller;

use App\Helpers\Messages;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Http\ajax;
use Session;
use App\Model\blog\Tpublicacion;

use App\Model\blog\Tatm;
use App\Model\blog\Tagenda;
use App\Model\blog\Tpremio;
use App\Model\Tusuario;
use Response;
use DB;

class NoticiasController extends Controller
{
    public function listaNoticias()
    {
        $noticias= Tpublicacion::where('categoria','noticia')->where('estado','activo')->orderBy('created_at', 'DESC')->paginate(24);

        return view('blog/noticias/lista',['noticias'=>$noticias]);
    }
   //CONCURSO AREAS TECNICAS MUNICIPALES 2020

    public function listaConcurso()
    {
        $listaAtm= DB::table('concurso')
        ->select('idatm','provincia','distrito','finalista')->get();

        return view('admin/concurso/lista',['listaAtm'=> $listaAtm]);
    }
    public function editarConcurso1(Request $request,Messages $messages, $idatm=null)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();

                $atm =  Tpremio::find($request->get('idatm'));

                $atm->edusa= base64_encode($request->get('edusa'));
                $atm->video1= $request->get('video1');

                if($request->hasFile('img'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('img')->getClientOriginalExtension());
                    $request->file('img')->move(public_path().'/logos',$random.'-'.$atm->idatm.'.'.$extension );
                    $atm->img = $request->get('hiddenUrl').'/logos/'.$random.'-'.$atm->idatm.'.'.$extension;
                }

                if($request->hasFile('foto1'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('foto1')->getClientOriginalExtension());
                    $request->file('foto1')->move(public_path().'/edusa',$random.'-'.$atm->idatm.'.'.$extension );
                    $atm->foto1 = $request->get('hiddenUrl').'/edusa/'.$random.'-'.$atm->idatm.'.'.$extension;
                }
                if($request->hasFile('audio1'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('audio1')->getClientOriginalExtension());
                    $request->file('audio1')->move(public_path().'/audios1',$random.'-'.$atm->idatm.'.'.$extension  );
                    $atm->audio1 = $request->get('hiddenUrl').'/audios1/'.$random.'-'.$atm->idatm.'.'.$extension ;
                }

                $atm->save();

                DB::commit();
                return $messages->MessageCorrect('Información de STAND ACTUALIZADO','concurso/lista');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('No se pudo realizar los cambios, consulte con el administrador','concurso/lista');
            }
        }

        $listaEditar= DB::table('concurso')->where('concurso.idatm',$idatm)->select('idatm')->first();

        return view('admin/concurso/editar',['listaEditar' => $listaEditar]);
    }
    public function editarConcurso2(Request $request,Messages $messages, $idatm=null)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();

                $atm =  Tpremio::find($request->get('idatm'));

                $atm->mante= base64_encode($request->get('mante'));
                $atm->video2= $request->get('video2');

                if($request->hasFile('foto2'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('foto2')->getClientOriginalExtension());
                    $request->file('foto2')->move(public_path().'/mante',$random.'-'.$atm->idatm.'.'.$extension );
                    $atm->foto2 = $request->get('hiddenUrl').'/mante/'.$random.'-'.$atm->idatm.'.'.$extension;
                }
                if($request->hasFile('audio2'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('audio2')->getClientOriginalExtension());
                    $request->file('audio2')->move(public_path().'/audios2',$random.'-'.$atm->idatm.'.'.$extension  );
                    $atm->audio2 = $request->get('hiddenUrl').'/audios2/'.$random.'-'.$atm->idatm.'.'.$extension ;
                }

                $atm->save();

                DB::commit();
                return $messages->MessageCorrect('Información de STAND ACTUALIZADO','concurso/lista');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('No se pudo realizar los cambios, consulte con el administrador','concurso/lista');
            }
        }

        $listaEditar= DB::table('concurso')->where('concurso.idatm',$idatm)->first();

        return view('admin/concurso/editar2',['listaEditar' => $listaEditar]);
    }
    public function editarConcurso3(Request $request,Messages $messages, $idatm=null)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
                $atm =  Tpremio::find($request->get('idatm'));
                $atm->gestion= base64_encode($request->get('gestion'));
                $atm->video3= $request->get('video3');

                if($request->hasFile('foto3'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('foto3')->getClientOriginalExtension());
                    $request->file('foto3')->move(public_path().'/gestion',$random.'-'.$atm->idatm.'.'.$extension );
                    $atm->foto3 = $request->get('hiddenUrl').'/gestion/'.$random.'-'.$atm->idatm.'.'.$extension;
                }
                if($request->hasFile('audio3'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('audio3')->getClientOriginalExtension());
                    $request->file('audio3')->move(public_path().'/audios3',$random.'-'.$atm->idatm.'.'.$extension  );
                    $atm->audio3 = $request->get('hiddenUrl').'/audios3/'.$random.'-'.$atm->idatm.'.'.$extension ;
                }

                $atm->save();
                DB::commit();
                return $messages->MessageCorrect('Información de STAND ACTUALIZADO','concurso/lista');
            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('No se pudo realizar los cambios, consulte con el administrador','concurso/lista');
            }
        }

        $listaEditar= DB::table('concurso')->where('concurso.idatm',$idatm)->first();

        return view('admin/concurso/editar3',['listaEditar' => $listaEditar]);
    }

    public function editarConcurso4(Request $request,Messages $messages, $idatm=null)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();

                $atm =  Tpremio::find($request->get('idatm'));

                $atm->web= $request->get('web');
                $atm->face= $request->get('face');

                if($request->hasFile('img'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('img')->getClientOriginalExtension());
                    $request->file('img')->move(public_path().'/logos',$random.'-'.$atm->idatm.'.'.$extension );
                    $atm->img = $request->get('hiddenUrl').'/logos/'.$random.'-'.$atm->idatm.'.'.$extension;
                }

                $atm->save();

                DB::commit();
                return $messages->MessageCorrect('Información de STAND ACTUALIZADO','concurso/lista');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('No se pudo realizar los cambios, consulte con el administrador','concurso/lista');
            }
        }

        $listaEditar= DB::table('concurso')->where('concurso.idatm',$idatm)->first();

        return view('admin/concurso/editar4',['listaEditar' => $listaEditar]);
    }
    public function concurso()
    {
        $munis= DB::table('concurso')
        ->select('idatm','provincia','distrito', 'img','muni','finalista')
        ->get();

        return view('atm/concurso/general',['munis'=>$munis]);
    }
    //   stand virtual individual
    public function stand(Request $request, $idatm)
    {

        $atm=DB::table('concurso')
            ->join('edusa', 'concurso.idatm', '=', 'edusa.idatm')
            ->join('gestion', 'concurso.idatm', '=', 'gestion.idatm')
            ->join('mantenimiento', 'concurso.idatm', '=', 'mantenimiento.idatm')
            ->where('concurso.idatm',  $idatm)
            ->select('edusa.*','gestion.*','mantenimiento.*','concurso.provincia'
            ,'concurso.distrito','concurso.img','concurso.finalista',
            'concurso.portada1','concurso.portada2','concurso.portada3','concurso.web',
            'concurso.face','concurso.muni')->get();

    	return view('atm/concurso/individual',['atm' => $atm]);
    }
// COMUNICADOS
    public function comunicado(Request $request)
    {
        if($request->ajax()){
            $comunicado= Tpublicacion::where('categoria','comunicado')->orderBy('created_at','DESC')->paginate(1);
            return Response::json($comunicado);
        }
        return "HTTP";
    }

    //TEMAS
    public function atm()
    {
        $noticias= Tpublicacion::where('tema','atm')->where('estado','activo')->orderBy('created_at', 'DESC')->paginate(12);

        return view('blog/temas/atm',['noticias'=>$noticias]);
    }
    public function edusa()
    {
        $noticias= Tpublicacion::where('tema','edusa')->where('estado','activo')->orderBy('created_at', 'DESC')->paginate(12);

        return view('blog/temas/edusa',['noticias'=>$noticias]);
    }
    public function viviendas()
    {
        $noticias= Tpublicacion::where('tema','vivienda')->where('estado','activo')->orderBy('created_at', 'DESC')->paginate(6);

        return view('blog/temas/viviendas',['noticias'=>$noticias]);
    }
    public function entrega()
    {
        $entrega= Tpublicacion::where('tema','entrega')->where('estado','activo')->orderBy('created_at', 'DESC')->paginate(6);

        return view('blog/temas/entrega',['entrega'=>$entrega]);
    }
    public function avisos()
    {
        $noticias= Tpublicacion::where('categoria','comunicado')->where('estado','activo')->orderBy('created_at', 'DESC')->paginate(24);

        return view('blog/temas/avisos',['noticias'=>$noticias]);
    }
    public function comursaba()
    {
        $noticias= Tpublicacion::where('tema','comursaba')->where('estado','activo')->orderBy('created_at', 'DESC')->paginate(12);

        return view('blog/temas/comursaba',['noticias'=>$noticias]);
    }
    public function jass()
    {
        $noticias= Tpublicacion::where('tema','jass')->where('estado','activo')->orderBy('created_at', 'DESC')->paginate(12);

        return view('blog/temas/jass',['noticias'=>$noticias]);
    }
    public function planos()
    {
        $planos= Tpublicacion::where('tema','planos')->where('estado','activo')->orderBy('created_at', 'DESC')->paginate(12);

        return view('blog/temas/planos',['planos'=>$planos]);
    }
    //TERMINA TEMAS
    public function listaDocumentos()
    {
        $documento= Tpublicacion::where('categoria','documento')->where('estado','activo')->paginate(8);

        return view('blog/noticias/documentos',['documento'=>$documento]);
    }
    public function verGaleria()
    {   $galeria= Tpublicacion::where('categoria','noticia')->where('estado','activo')->orderBy('created_at', 'DESC')->paginate(40);

        return view('blog/noticias/galeria',compact('galeria'));
    }
    public function wasichakuy()
    {   $wasichakuy= Tpublicacion::where('tema','wasichakuy')->where('estado','activo')->orderBy('created_at', 'DESC')->paginate(6);

        return view('blog/noticias/wasichakuy',compact('wasichakuy'));
    }
    public function edificaciones()
    {   $edificaciones= Tpublicacion::where('tema','edificaciones')->where('estado','activo')->orderBy('created_at', 'DESC')->paginate(6);

        return view('blog/noticias/edificaciones',compact('edificaciones'));
    }
    public function urbanismo()
    {   $urbanismo2021= Tpublicacion::where('tema','urbanismo')->where('estado','activo')->orderBy('created_at', 'DESC')->paginate(6);

        return view('blog/noticias/urbanismo2021',compact('urbanismo2021'));
    }

    public function romas()
    {   $romas= Tpublicacion::where('tema','romas')->where('estado','activo')->orderBy('created_at', 'DESC')->paginate(6);

        return view('blog/noticias/romas',compact('romas'));
    }
    public function popup()
    {   $popup= Tpublicacion::where('categoria','comunicado')->where('estado','activo')->orderBy('created_at', 'DESC')->paginate(1);
        dd($popup);
        return view('blog/app/popup',['popup'=>$popup]);
    }
    public function listaRomas()
    {   $noticias= Tpublicacion::where('tema','romas')->where('estado','activo')->orderBy('created_at', 'DESC')->paginate(6);

        return view('blog/temas/listaromas',compact('noticias'));
    }
    public function directorio()
    {
        $usuarios= Tusuario::where('estado','activo')->orderBy('created_at', 'DESC')->get();

        return view('blog/noticias/directorio',compact('usuarios'));
    }
    public function plan()
    {   $plan= Tpublicacion::where('tema','plan')->where('estado','activo')->orderBy('created_at', 'DESC')->paginate(2);

        return view('blog/noticias/plan',compact('plan'));
    }
    public function documento(Request $request)
    {
        $documento=   Tpublicacion::where('categoria','documento')->where('estado','activo')->orderBy('created_at', 'DESC')->paginate(3);
        return view('blog/noticias/documentos',['documento' => $documento]);
    }
    public function detalle(Request $request, $id)
    {
    	$detalle=Tpublicacion::find($id);
    	return view('blog/noticias.detalle',['detalle' => $detalle]);
    }
    public function institucional()
    {
        return view('blog/noticias/institucional');
    }

    public function verificar(Request $request)
    {
        if($request->ajax()){
            $text = $request->input('atm');

            $datos = DB::table('es_atm')->where('dni',$text)->get();
            return Response::json($datos);
        }else{

            return view('blog/app/verificar');
        }

    }
    public function verificar2(Request $request)
    {
        if($request->ajax()){
            $text = $request->input('ope');

            $datos = DB::table('operadores')->where('nombres', 'LIKE', "%$text%")->get();
            return Response::json($datos);
        }else{
            return view('blog/app/verificar');
        }

    }
    public function verificar3(Request $request)
    {
        if($request->ajax()){
            $text = $request->input('was');

            $datos = DB::table('wasichakuy')->where('nombres', 'LIKE', "%$text%")->get();
            return Response::json($datos);
        }else{
            return view('blog/app/verificar');
        }

    }
    public function documentos()
    {
        return view('blog/noticias/documentos');
    }
    public function contacto()
    {
        return view('blog/blog/noticias/contacto');
    }
    public function oficinas()
    {
        return view('blog/noticias/oficinas');
    }
    public function transparencia()
    {
        return view('blog/noticias/transparencia');
    }
}