<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use App\Helpers\Messages;
use App\Model\Tcsap;
use App\Model\Trsap;

use Redirect;
use DB;
use File;
use Session;
class CsapController extends Controller
{
    public function listaCsap(Request $request)
    {
        $dni=Session::get('t_usuario')[0];
        if($dni != null)
        {
            $listaCsap=DB::table('t_atm')
                        ->join('t_funcion', 't_atm.ubigeo', '=', 't_funcion.ubigeo')
                        ->join('t_sapconsolidado', 't_sapconsolidado.idFuncion', '=', 't_funcion.idFuncion')
                        ->where('t_atm.dniUsuario',  $dni)
                        ->select('t_sapconsolidado.*')
                        ->get();
            $listaRsap=DB::table('t_atm')
            ->join('t_funcion', 't_atm.ubigeo', '=', 't_funcion.ubigeo')
            ->join('t_sapconsolidado', 't_sapconsolidado.idFuncion', '=', 't_funcion.idFuncion')
            ->join('t_sapriesgo', 't_sapriesgo.idSapC', '=', 't_sapconsolidado.idSapC')
            ->where('t_atm.dniUsuario',  $dni)
            ->select('t_sapriesgo.*')
            ->get();

            return view('csap/lista',['listaCsap'=> $listaCsap , 'listaRsap'=>$listaRsap]);
        }
    }


    public function editarCsap(Request $request,Messages $messages,$idSapC=null)
    {
        if($_POST)
        {
            try
            {
                $csap = Tcsap::find($request->get('idSapC'));
                $csap->cpconSap = $request->get('cpconSap');
                $csap->cpsinSap = $request->get('cpsinSap');
                $csap->totalSap = $request->get('totalSap');
                $csap->sisCloracionMejorado = $request->get('sisCloracionMejorado');
                $csap->sisExcretas = $request->get('sisExcretas');
                $csap->cloracionContinua = $request->get('cloracionContinua');
                $csap->romas = $request->get('romas');
                if($request->hasFile('ver1'))
                {
                    $random=rand().time();
                    $extension = strtolower($request->file('ver1')->getClientOriginalExtension());
                    $request->file('ver1')->move(public_path().'/csap',$random.'.'.$extension );

                    $csap->ver1 = $request->get('hiddenUrl').'/csap/'.$random.'.'.$extension;
                }

                if($request->hasFile('ver2'))
                {
                    $random=rand().time();
                    $extension = strtolower($request->file('ver2')->getClientOriginalExtension());
                    $request->file('ver2')->move(public_path().'/csap',$random.'.'.$extension );

                    $csap->ver2 = $request->get('hiddenUrl').'/csap/'.$random.'.'.$extension;
                }

                if($request->hasFile('ver3'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver3')->getClientOriginalExtension());
                    $request->file('ver3')->move(public_path().'/csap',$random.'.'.$extension );

                    $csap->ver3 = $request->get('hiddenUrl').'/csap/'.$random.'.'.$extension;
                }

                if($request->hasFile('ver4'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver4')->getClientOriginalExtension());
                    $request->file('ver4')->move(public_path().'/csap',$random.'.'.$extension );

                    $csap->ver4 = $request->get('hiddenUrl').'/csap/'.$random.'.'.$extension;
                }

                if($request->hasFile('ver5'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver5')->getClientOriginalExtension());
                    $request->file('ver5')->move(public_path().'/csap',$random.'.'.$extension );

                    $csap->ver5 = $request->get('hiddenUrl').'/csap/'.$random.'.'.$extension;
                }

                if($request->hasFile('ver6'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver6')->getClientOriginalExtension());
                    $request->file('ver6')->move(public_path().'/csap',$random.'.'.$extension );

                    $csap->ver6 = $request->get('hiddenUrl').'/csap/'.$random.'.'.$extension;
                }

                if($request->hasFile('ver7'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver7')->getClientOriginalExtension());
                    $request->file('ver7')->move(public_path().'/csap',$random.'.'.$extension );

                    $csap->ver7 = $request->get('hiddenUrl').'/csap/'.$random.'.'.$extension;
                }

                $csap->save();
                DB::commit();

                return $messages->MessageCorrect('Lista de csap actualizada','csap/lista');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('No se pudo realizar los cambios, consulte con el administrador','csap/lista');
            }
        }

        $listaEditar = Tcsap::find($idSapC);
        return view('csap/editar',['listaEditar' => $listaEditar]);
    }

    public function editarRsap(Request $request,Messages $messages,$idSapR=null)
    {
        if($_POST)
        {
            try
            {
                $rsap = Trsap::find($request->get('idSapR'));
                $rsap->numSisRiesgoAgua = $request->get('numSisRiesgoAgua');
                $rsap->numSisRiesgoSane = $request->get('numSisRiesgoSane');

                if($request->hasFile('ver1'))
                {
                    $random=rand().time();
                    $extension = strtolower($request->file('ver1')->getClientOriginalExtension());
                    $request->file('ver1')->move(public_path().'/rsap',$random.'.'.$extension );

                    $rsap->ver1 = $request->get('hiddenUrl').'/rsap/'.$random.'.'.$extension;
                }

                $rsap->save();
                DB::commit();

                return $messages->MessageCorrect('Lista de Rsap actualizada','csap/lista');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('No se pudo realizar los cambios, consulte con el administrador','csap/lista');
            }
        }

        $listaEditar = Trsap::find($idSapR);
        return view('csap/reditar',['listaEditar' => $listaEditar]);
    }
}