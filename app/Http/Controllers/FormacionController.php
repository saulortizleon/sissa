<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use App\Helpers\Messages;
use App\Model\Tformacion;
use App\Model\Tatm;
use Redirect;
use DB;
use File;
use Session;

class FormacionController extends Controller
{
    public function listaFormacion(Request $request)
    {
        $dni=Session::get('t_usuario')[0];
        if($dni != null)
        {
            $listaFormacion=DB::table('t_atm')
                        ->join('t_formacion', 't_atm.ubigeo', '=', 't_formacion.ubigeo')
                        ->where('t_atm.dniUsuario',  $dni)
                        ->select('t_formacion.*')
                        ->get();

            return view('formacion/lista',['listaFormacion'=> $listaFormacion]);
        }
    }
    public function insertarFormacion(Request $request,Messages $messages)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
                $formacion = new Tformacion();

                $formacion->idFormacion = uniqid();
                $formacion->ROF = $request->get('ROF');
                $formacion->MOF = $request->get('MOF');
                $formacion->CAP= $request->get('CAP');
                $formacion->TUPA= $request->get('TUPA');
                $formacion->gradoAcadem= $request->get('gradoAcadem');
                $formacion->especialidad= $request->get('especialidad');
                $formacion->GN= $request->get('GN');
                $formacion->GR= $request->get('GR');
                $formacion->GL= $request->get('GL');
                $formacion->ONG= $request->get('ONG');
                $formacion->EDUSA= $request->get('EDUSA');
                $formacion->AOM= $request->get('AOM');
                $formacion->DL1280= $request->get('DL1280');
                $formacion->PNSR= $request->get('PNSR');
                $formacion->PNSRotros= $request->get('PNSRotros');
                $formacion->planSan= $request->get('planSan');
                $formacion->criterios= $request->get('criterios');
                if($request->hasFile('ver1'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver1')->getClientOriginalExtension());
                    $request->file('ver1')->move(public_path().'/formacion',$random.'.'.$extension );
                    $formacion->ver1 = $request->get('hiddenUrl').'/formacion/'.$random.'.'.$extension;
                }
                if($request->hasFile('ver2'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver2')->getClientOriginalExtension());
                    $request->file('ver2')->move(public_path().'/formacion',$random.'.'.$extension );
                    $formacion->ver2 = $request->get('hiddenUrl').'/formacion/'.$random.'.'.$extension;
                }
                if($request->hasFile('ver3'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver3')->getClientOriginalExtension());
                    $request->file('ver3')->move(public_path().'/formacion',$random.'.'.$extension );
                    $formacion->ver3 = $request->get('hiddenUrl').'/formacion/'.$random.'.'.$extension;
                }
                if($request->hasFile('ver4'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver4')->getClientOriginalExtension());
                    $request->file('ver4')->move(public_path().'/formacion',$random.'.'.$extension );
                    $formacion->ver4 = $request->get('hiddenUrl').'/formacion/'.$random.'.'.$extension;
                }
                if($request->hasFile('ver5'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver5')->getClientOriginalExtension());
                    $request->file('ver5')->move(public_path().'/formacion',$random.'.'.$extension );
                    $formacion->ver5 = $request->get('hiddenUrl').'/formacion/'.$random.'.'.$extension;
                }
                if($request->hasFile('ver6'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver6')->getClientOriginalExtension());
                    $request->file('ver6')->move(public_path().'/formacion',$random.'.'.$extension );
                    $formacion->ver6 = $request->get('hiddenUrl').'/formacion/'.$random.'.'.$extension;
                }
                if($request->hasFile('ver7'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver7')->getClientOriginalExtension());
                    $request->file('ver7')->move(public_path().'/formacion',$random.'.'.$extension );
                    $formacion->ver7 = $request->get('hiddenUrl').'/formacion/'.$random.'.'.$extension;
                }
                if($request->hasFile('ver8'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver8')->getClientOriginalExtension());
                    $request->file('ver8')->move(public_path().'/formacion',$random.'.'.$extension );
                    $formacion->ver8 = $request->get('hiddenUrl').'/formacion/'.$random.'.'.$extension;
                }
                if($request->hasFile('ver9'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver9')->getClientOriginalExtension());
                    $request->file('ver9')->move(public_path().'/formacion',$random.'.'.$extension );
                    $formacion->ver9 = $request->get('hiddenUrl').'/formacion/'.$random.'.'.$extension;
                }
                if($request->hasFile('ver6'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver10')->getClientOriginalExtension());
                    $request->file('ver10')->move(public_path().'/formacion',$random.'.'.$extension );
                    $formacion->ver10 = $request->get('hiddenUrl').'/formacion/'.$random.'.'.$extension;
                }
                if($request->hasFile('ver11'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver11')->getClientOriginalExtension());
                    $request->file('ver11')->move(public_path().'/formacion',$random.'.'.$extension );
                    $formacion->ver11 = $request->get('hiddenUrl').'/formacion/'.$random.'.'.$extension;
                }
                if($request->hasFile('ver12'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver12')->getClientOriginalExtension());
                    $request->file('ver12')->move(public_path().'/formacion',$random.'.'.$extension );
                    $formacion->ver12 = $request->get('hiddenUrl').'/formacion/'.$random.'.'.$extension;
                }
                if($request->hasFile('ver13'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver13')->getClientOriginalExtension());
                    $request->file('ver13')->move(public_path().'/formacion',$random.'.'.$extension );
                    $formacion->ver13 = $request->get('hiddenUrl').'/formacion/'.$random.'.'.$extension;
                }
                $dni=Session::get('t_usuario')[0];
                $ubigeo= DB::table('t_atm')->where('dniUsuario',$dni)->first()->ubigeo;

                $formacion->ubigeo= $ubigeo;
                $formacion->save();
                DB::commit();
                return $messages->MessageCorrect('Lista de formacion actualizada','formacion/lista');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('No se pudo realizar los cambios, consulte con el administrador','formacion/lista');
            }
        }
        return view('formacion/lista');
    }
    public function editarFormacion(Request $request,Messages $messages, $idFormacion=null)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
                $formacion = Tformacion::find($request->get('idFormacion'));
                $formacion->cuentaPerfilCapacitado = $request->get('cuentaPerfilCapacitado');
                $formacion->poiAtm = $request->get('poiAtm');
                $formacion->articulacionSaneam= $request->get('articulacionSaneam');
                $formacion->ordMunCloro= $request->get('ordMunCloro');
                if($request->hasFile('ver1'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver1')->getClientOriginalExtension());
                    $request->file('ver1')->move(public_path().'/formacion',$random.'.'.$extension );
                    $formacion->ver1 = $request->get('hiddenUrl').'/formacion/'.$random.'.'.$extension;
                }
                if($request->hasFile('ver2'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver2')->getClientOriginalExtension());
                    $request->file('ver2')->move(public_path().'/formacion',$random.'.'.$extension );
                    $formacion->ver2 = $request->get('hiddenUrl').'/formacion/'.$random.'.'.$extension;
                }
                if($request->hasFile('ver3'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver3')->getClientOriginalExtension());
                    $request->file('ver3')->move(public_path().'/formacion',$random.'.'.$extension );
                    $formacion->ver3 = $request->get('hiddenUrl').'/formacion/'.$random.'.'.$extension;
                }
                if($request->hasFile('ver4'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver4')->getClientOriginalExtension());
                    $request->file('ver4')->move(public_path().'/formacion',$random.'.'.$extension );
                    $formacion->ver4 = $request->get('hiddenUrl').'/formacion/'.$random.'.'.$extension;
                }
                $dni=Session::get('t_usuario')[0];
                $ubigeo= DB::table('t_atm')->where('dniUsuario',$dni)->first()->ubigeo;

                $formacion->ubigeo= $ubigeo;

                $formacion->save();
                DB::commit();
                return $messages->MessageCorrect('Lista de formacion actualizada','formacion/lista');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('No se pudo realizar los cambios, consulte con el administrador','formacion/lista');
            }
        }
        $listaEditar = Tformacion::find($idFormacion);
        return view('formacion/editar',['listaEditar' => $listaEditar]);
    }
}