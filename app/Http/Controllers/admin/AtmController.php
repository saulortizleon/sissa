<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use App\Helpers\Messages;
use Illuminate\Support\Str as Str;
use App\Model\Taccesos;
use App\Model\Tformacion;
use App\Model\Tusuario;
use App\Model\Tatm;
use App\Model\blog\Tpublicacion;
use App\Model\Ttemas;
use App\Model\Tcategoria;
use App\Model\Tarchivos;
use App\Model\blog\Timg1;
use App\Model\blog\Timg2;
use Redirect;
use DB;
use File;
use Session;

class AdminController extends Controller
{

    public function listaAtm(Request $request)
    {
        $dni=Session::get('t_usuario')[0];
        $temp= DB::table('t_usuario')->where('t_usuario.dniUsuario',$dni)->first();
        $x= $temp->acceso;
        $listaAtm= Tatm::where('acceso',$x)->get();
        return view('admin/listaatm',['listaAtm'=> $listaAtm]);
    }
    public function avance(Request $request)
    {
        $listaAtm= DB::table('t_atm')
        ->join('t_formacion', 't_formacion.ubigeo', '=', 't_atm.ubigeo')
        ->join('t_usuario', 't_usuario.dniUsuario', '=', 't_atm.dniUsuario')
        ->select('*')->get();
        return view('admin/avance/general',['listaAtm'=> $listaAtm]);
    }

    public function listaUsuarios(Request $request)
    {
        $usuario= DB::table('t_usuario')
        ->join('t_accesos', 't_accesos.idusuario', '=', 't_usuario.dniUsuario')
        ->join('t_atm', 't_atm.ubigeo', '=', 't_accesos.ubigeo')
        ->select('*')->get();

        return view('admin/usuario/lista',['usuario'=> $usuario]);
    }
    public function select2(Request $request)
    {
        $distrito=Tatm::pluck('distrito', 'ubigeo')->prepend('selecciona');
        return view('admin/usuario/insertar',['distrito'=> $distrito]);
    }
    public function backup(Request $request)
    {

        return view('admin/backup/ver');
    }
    public function insertarUsuarios(Request $request,SessionManager $sessionManager, Messages $messages)
	{
		if($_POST)
		{
			try
			{
				DB::beginTransaction();
					$validacion=Tusuario::whereRaw('dniUsuario=?', [trim($request->input('dniUsuario'))])->first();

					if ($validacion!=null)
					{
						return $messages->MessageIncorrect('El dni de USUARIO ya existe','usuarios/insertar');

                    }

                    $usuarios=new Tusuario();
                    $usuarios->cargo=$request->input('cargo');

					$usuarios->dniUsuario=$request->input('dniUsuario');
					$usuarios->nombres=$request->input('nombres');
					$usuarios->celular=$request->input('celular');
					$usuarios->estado=$request->input('estado');

                    $usuarios->sexo=$request->input('sexo');
                    $usuarios->correo=$request->input('correo');
                    $usuarios->fechaNacimiento=$request->input('fechaNacimiento');

                    $usuarios->save();

				DB::commit();
			}
			catch(\Exception $ex)
			{
                DB::rollback();
                return $messages->MessageIncorrect('No se pudo realizar los cambios, consulte con el administrador','usuarios/insertar');
			}
        }
        $distritos= DB::table('t_atm')
        ->join('t_accesos', 't_accesos.ubigeo', '=', 't_atm.ubigeo')
        ->where('t_accesos.estado','inactivo')->get();
	return view('admin/usuario/insertar',['distritos'=>$distritos]);
    }

    public function eliminarAccesos(Request $request,SessionManager $sessionManager,$idusuario, Messages $messages)
    {
        DB::table('t_accesos')->where('idusuario', $idusuario)
        ->update(['estado' => 'inactivo']);
        return $messages->MessageCorrect('Se revocó el acceso corrrectamente','admin/usuarios');
    }
    public function editarUsuarios(Request $request,SessionManager $sessionManager, Messages $messages, $idusuario)
	{
		if($_POST)
		{
			try
			{
				DB::beginTransaction();

                    $formacion = Tusuario::find($request->get('idusuario'));
                    $usuarios->cargo=$request->input('cargo');
					$usuarios->dniUsuario=$request->input('dniUsuario');
					$usuarios->nombres=$request->input('nombres');
					$usuarios->celular=$request->input('celular');
					$usuarios->estado=$request->input('estado');
                    $usuarios->sexo=$request->input('sexo');
                    $usuarios->correo=$request->input('correo');
                    $usuarios->fechaNacimiento=$request->input('fechaNacimiento');

                    $usuarios->save();

				DB::commit();

				return $messages->MessageCorrect('Usuario editado correctamente','usuarios/insertar');

			}
			catch(\Exception $ex)
			{
                DB::rollback();
                return $messages->MessageIncorrect('No se pudo realizar los cambios, consulte con el administrador','usuarios/insertar');
			}
        }


        $listaEditar = Tusuario::find($idusuario);

    return view('admin/usuario/editar',['listaEditar' => $listaEditar]);
    }

   
}