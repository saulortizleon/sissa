<?php

namespace App\Http\Controllers;
use App\Model\Tatmver;
use App\Model\Tatm;
use App\Model\Tgestion;
use App\Model\Tmante;
use App\Model\Tedusa;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use App\Model\Tusuario;

use Session;
use App\Helpers\Messages;
use DB;
class AtmController extends Controller
{
    public function lista(Request $request)
    {
        $dni=Session::get('t_usuario')[0];

        if($dni != null)
        {
            $listaAtm=TAtm::where('dniUsuario',$dni)->get();

            return view('atm/lista',['listaAtm'=> $listaAtm]);
        }
    }
    public function editarAtm(Request $request,Messages $messages, $ubigeo=null)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
                $atm =  Tatm::find($request->get('ubigeo'));
                $atm->ordMunicipalRof = $request->get('ordMunicipalRof');
                $atm->munDepende = $request->get('munDepende');
                $temp=$request->get('ubigeo');
                $atm->resolApruebaPerfil= $request->get('resolApruebaPerfil');
                $atm->resolDesignaAtm= $request->get('resolDesignaAtm');
                $atm->atmRof= $request->get('rof');
                $atm->atmMof= $request->get('mof');
                $atm->atmCap= $request->get('cap');
                $atm->atmTupa= $request->get('tupa');

                $atm->equipamiento= $request->get('equipamiento');
                $atm->dniUsuario= Session::get('t_usuario')[0];
                $atm->save();

                $wea=DB::table('t_atm')
                ->join('t_atmver', 't_atm.ubigeo', '=', 't_atmver.ubigeo')
                ->where('t_atmver.ubigeo',$temp)
                ->select('t_atmver.idVerAtm')->first();
                $x=$wea->idVerAtm;

                $verificables =  Tatmver::find($x);

                if($request->hasFile('ver1'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver1')->getClientOriginalExtension());
                    $request->file('ver1')->move(public_path().'/atm',$random.'.'.$extension );
                    $verificables->ver1 = $request->get('hiddenUrl').'/atm/'.$random.'.'.$extension;
                }
                if($request->hasFile('ver2'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver2')->getClientOriginalExtension());
                    $request->file('ver2')->move(public_path().'/atm',$random.'.'.$extension );
                    $verificables->ver2 = $request->get('hiddenUrl').'/atm/'.$random.'.'.$extension;
                }
                if($request->hasFile('ver3'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver3')->getClientOriginalExtension());
                    $request->file('ver3')->move(public_path().'/atm',$random.'.'.$extension );
                    $verificables->ver3 = $request->get('hiddenUrl').'/atm/'.$random.'.'.$extension;
                }
                if($request->hasFile('ver4'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver4')->getClientOriginalExtension());
                    $request->file('ver4')->move(public_path().'/atm',$random.'.'.$extension );
                    $verificables->ver4 = $request->get('hiddenUrl').'/atm/'.$random.'.'.$extension;
                }
                if($request->hasFile('ver5'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver5')->getClientOriginalExtension());
                    $request->file('ver5')->move(public_path().'/atm',$random.'.'.$extension );
                    $verificables->ver5 = $request->get('hiddenUrl').'/atm/'.$random.'.'.$extension;
                }
                $verificables->ubigeo = $request->get('ubigeo');
                $verificables->save();

                DB::commit();
                return $messages->MessageCorrect('Información de ATM actualizada','atm/lista');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('No se pudo realizar los cambios, consulte con el administrador','formacion/lista');
            }
        }
        $temp=$ubigeo;
        $listaEditar = Tatm::find($ubigeo);

        $verificable= Tatmver::where('ubigeo',$temp)->get();

        return view('atm/editar',['listaEditar' => $listaEditar,'verificable' => $verificable]);
    }

    public function listaResponsable(Request $request)
    {
        $dni=Session::get('t_usuario')[0];
        dd($dni);
        $listaUsuario = Tusuario::where('dniUsuario',$dni)->get();
        return view('usuario/lista',['listaUsuario' => $listaUsuario]);
    }
    public function editarResponsable(Request $request, Messages $messages, $dniUsuario=null)
	{
		if($_POST)
		{
			try
			{
				DB::beginTransaction();

                $usuarios = Tusuario::find($request->get('dniUsuario'));

                $usuarios->dniUsuario=$request->input('dniUsuario');
                $usuarios->nombres=$request->input('nombres');
                $usuarios->celular=$request->input('celular');

                $usuarios->sexo=$request->input('sexo');
                $usuarios->correo=$request->input('correo');
                $usuarios->fechaNacimiento=$request->input('fechaNacimiento');

                $usuarios->save();

				DB::commit();

				return $messages->MessageCorrect('Usuario editado correctamente','usuario/perfil');

			}
			catch(\Exception $ex)
			{
                DB::rollback();
                return $messages->MessageIncorrect('No se pudo realizar los cambios, consulte con el administrador','usuario/perfil');
			}
        }

        $listaEditar = Tusuario::find($dniUsuario);

    return view('usuario/editar',['listaEditar' => $listaEditar]);
    }
    //finalistas

    public function IndexAtm()
    {
        return view('index/indexatm');
    }
    public function finalistas()
    {
        $dni=Session::get('t_usuario')[0];
        $listaAtm= DB::table('concurso')->select('idatm','provincia','distrito','finalista')
        ->where('dni',$dni)->where('finalista','S')->get();

        return view('admin/finalistas/lista',['listaAtm'=> $listaAtm]);
    }
    //editar finalistas

    public function editar1(Request $request,Messages $messages, $idatm=null)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
                $id=$request->get('idatm');
                $temp= Tgestion::select('idgestion')->where('idatm',$id)->first();

                $atm =  Tgestion::find($temp->idgestion);


                if($request->hasFile('constanciaAtm'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('constanciaAtm')->getClientOriginalExtension());
                    $request->file('constanciaAtm')->move(public_path().'/constancias',$random.'.'.$extension );
                    $atm->constanciaAtm = $request->get('hiddenUrl').'/constancias/'.$random.'.'.$extension;
                }

                if($request->hasFile('constanciaOperador'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('constanciaOperador')->getClientOriginalExtension());
                    $request->file('constanciaOperador')->move(public_path().'/constancia',$random.'.'.$extension );
                    $atm->constanciaOperador = $request->get('hiddenUrl').'/constancia/'.$random.'.'.$extension;
                }
                if($request->hasFile('reporteDatass'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('reporteDatass')->getClientOriginalExtension());
                    $request->file('reporteDatass')->move(public_path().'/reporte',$random.'.'.$extension  );
                    $atm->reporteDatass = $request->get('hiddenUrl').'/reporte/'.$random.'.'.$extension ;
                }

                if($request->hasFile('constanciaSap'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('constanciaSap')->getClientOriginalExtension());
                    $request->file('constanciaSap')->move(public_path().'/sapc',$random.'.'.$extension  );
                    $atm->constanciaSap = $request->get('hiddenUrl').'/sapc/'.$random.'.'.$extension ;
                }
                if($request->hasFile('POI'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('POI')->getClientOriginalExtension());
                    $request->file('POI')->move(public_path().'/decretos',$random.'.'.$extension  );
                    $atm->POI = $request->get('hiddenUrl').'/decretos/'.$random.'.'.$extension ;
                }
                if($request->hasFile('decretosUrgencia'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('decretosUrgencia')->getClientOriginalExtension());
                    $request->file('decretosUrgencia')->move(public_path().'/decretos',$random.'.'.$extension  );
                    $atm->decretosUrgencia = $request->get('hiddenUrl').'/decretos/'.$random.'.'.$extension ;
                }
                


                $atm->save();

                DB::commit();
                return $messages->MessageCorrect('Información de STAND ACTUALIZADO','finalistas/lista');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('No se pudo realizar los cambios, consulte con el administrador','finalistas/lista');
            }
        }

        $listaEditar= DB::table('gestion')->where('gestion.idatm',$idatm)->first();

        return view('admin/finalistas/editar1',['listaEditar' => $listaEditar]);
    }
    public function editar2(Request $request,Messages $messages, $idatm=null)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
                $id=$request->get('idatm');
                $temp= Tmante::select('idMante')->where('idatm',$id)->first();

                $atm =  Tmante::find($temp->idMante);

                if($request->hasFile('foto1'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('foto1')->getClientOriginalExtension());
                    $request->file('foto1')->move(public_path().'/fotomante1',$random.'.'.$extension );
                    $atm->foto1 = $request->get('hiddenUrl').'/fotomante1/'.$random.'.'.$extension;
                }

                if($request->hasFile('foto2'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('foto2')->getClientOriginalExtension());
                    $request->file('foto2')->move(public_path().'/fotomante2',$random.'.'.$extension );
                    $atm->foto2 = $request->get('hiddenUrl').'/fotomante2/'.$random.'.'.$extension;
                }
                if($request->hasFile('foto3'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('foto3')->getClientOriginalExtension());
                    $request->file('foto3')->move(public_path().'/fotomante3',$random.'.'.$extension  );
                    $atm->foto3 = $request->get('hiddenUrl').'/fotomante3/'.$random.'.'.$extension ;
                }

                if($request->hasFile('foto4'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('foto4')->getClientOriginalExtension());
                    $request->file('foto4')->move(public_path().'/fotomante4',$random.'.'.$extension  );
                    $atm->foto4 = $request->get('hiddenUrl').'/fotomante4/'.$random.'.'.$extension ;
                }
                if($request->hasFile('decretosUrgencia'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('decretosUrgencia')->getClientOriginalExtension());
                    $request->file('decretosUrgencia')->move(public_path().'/decretos',$random.'.'.$extension  );
                    $atm->decretosUrgencia = $request->get('hiddenUrl').'/decretos/'.$random.'.'.$extension ;
                }
                if($request->hasFile('actasSap'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('actasSap')->getClientOriginalExtension());
                    $request->file('actasSap')->move(public_path().'/actassap',$random.'.'.$extension  );
                    $atm->actasSap = $request->get('hiddenUrl').'/actassap/'.$random.'.'.$extension ;
                }
                if($request->hasFile('cargoPOI'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('cargoPOI')->getClientOriginalExtension());
                    $request->file('cargoPOI')->move(public_path().'/cargopoi',$random.'.'.$extension  );
                    $atm->cargoPOI = $request->get('hiddenUrl').'/cargopoi/'.$random.'.'.$extension ;
                }
                if($request->hasFile('fotosSap'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('fotosSap')->getClientOriginalExtension());
                    $request->file('fotosSap')->move(public_path().'/fotosSap',$random.'.'.$extension  );
                    $atm->fotosSap = $request->get('hiddenUrl').'/fotosSap/'.$random.'.'.$extension ;
                }

                $atm->save();

                DB::commit();
                return $messages->MessageCorrect('Información de STAND ACTUALIZADO','finalistas/lista');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('No se pudo realizar los cambios, consulte con el administrador','finalistas/lista');
            }
        }

        $listaEditar= DB::table('mantenimiento')->where('mantenimiento.idatm',$idatm)->first();

        return view('admin/finalistas/editar2',['listaEditar' => $listaEditar]);
    }

    public function editar3(Request $request,Messages $messages, $idatm=null)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
                $id=$request->get('idatm');
                $temp= Tedusa::select('idEdusa')->where('idatm',$id)->first();

                $atm =  Tedusa::find($temp->idEdusa);
                $atm->linkvideo= $request->get('linkvideo');
                $atm->videoEdusa= $request->get('videoEdusa');
                $atm->estrategias= $request->get('estrategias');

                if($request->hasFile('capturaJass'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('capturaJass')->getClientOriginalExtension());
                    $request->file('capturaJass')->move(public_path().'/capturaJass',$random.'.'.$extension );
                    $atm->capturaJass = $request->get('hiddenUrl').'/capturaJass/'.$random.'.'.$extension;
                }

                if($request->hasFile('fichasSup'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('fichasSup')->getClientOriginalExtension());
                    $request->file('fichasSup')->move(public_path().'/fichasSup',$random.'.'.$extension );
                    $atm->fichasSup = $request->get('hiddenUrl').'/fichasSup/'.$random.'.'.$extension;
                }
                if($request->hasFile('jassopm'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('jassopm')->getClientOriginalExtension());
                    $request->file('jassopm')->move(public_path().'/jassopm',$random.'.'.$extension  );
                    $atm->jassopm = $request->get('hiddenUrl').'/jassopm/'.$random.'.'.$extension ;
                }

                if($request->hasFile('jaspoa'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('jaspoa')->getClientOriginalExtension());
                    $request->file('jaspoa')->move(public_path().'/jaspoa',$random.'.'.$extension  );
                    $atm->jaspoa = $request->get('hiddenUrl').'/jaspoa/'.$random.'.'.$extension ;
                }
                if($request->hasFile('videoEdusa'))
                {   $random = Str::random(5);
                    $extension = strtolower($request->file('videoEdusa')->getClientOriginalExtension());
                    $request->file('videoEdusa')->move(public_path().'/videoEdusa',$random.'.'.$extension  );
                    $atm->videoEdusa = $request->get('hiddenUrl').'/videoEdusa/'.$random.'.'.$extension ;
                }

                $atm->save();

                DB::commit();
                return $messages->MessageCorrect('Información de STAND ACTUALIZADO','finalistas/lista');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('No se pudo realizar los cambios, consulte con el administrador','finalistas/lista');
            }
        }

        $listaEditar= DB::table('edusa')->where('edusa.idatm',$idatm)->first();

        return view('admin/finalistas/editar3',['listaEditar' => $listaEditar]);
    }
}