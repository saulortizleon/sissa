<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use App\Model\Tarchivo;
use App\Model\Tsolicitante;
use App\Model\Tdocumento;
use App\Model\Trespuesta;

use Session;
use App\Helpers\Messages;
use DB;
use DateTime;
class ArchivoController extends Controller
{
    public function archivo(Request $request,Messages $messages)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
              
                $archivo = new Tarchivo();  
                
                $archivo->remitente= $request->get('remitente');
               
                $archivo->destinatario= $request->get('destinatario');
                $archivo->numeroDocumento= $request->get('numeroDocumento');
                $archivo->tipoDocumento = $request->get('tipoDocumento');
                $archivo->estadoArchivo= $request->get('estadoArchivo');
                $archivo->observacion= $request->get('observacion');
                $archivo->fechaIngreso= $request->get('fechaIngreso');
                $archivo->asunto= $request->get('asunto');

                $archivo->numeroRegistro= $request->get('numeroRegistro');
                $archivo->folios= $request->get('folios');
                $archivo->proveido= $request->get('proveido');
                if($request->hasFile('documento'))
                {
                    $random=rand().time();
                    $extension = strtolower($request->file('documento')->getClientOriginalExtension());
                    $request->file('documento')->move(public_path().'/archivo',  $archivo->fechaIngreso.'-'. $archivo->proveido.$random.'.'.$extension );

                    $archivo->archivo = $request->get('hiddenUrl').'/archivo/'.  $archivo->fechaIngreso.'-'. $archivo->proveido.$random.'.'.$extension ;
                }

              
                $archivo->save();

                DB::commit();

                return $messages->MessageCorrect('Documento REGISTRADO correctamente','archivo/lista');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('No se pudo enviar el documento, consulte con el administrador','archivo/lista');
            }
        }

 
        $buscarArchivo=trim($request->get('buscarArchivo'));
     
        $listaDocumentos=DB::table('t_archivo')        
        ->where('t_archivo.remitente','like','%'.$buscarArchivo.'%')
        ->orwhere('t_archivo.asunto','like','%'.$buscarArchivo.'%')
        ->orderBy('t_archivo.created_at', 'desc')
        ->paginate(10);
      
        return view('supervision/archivo/lista',['listaDocumentos'=> $listaDocumentos, 'buscarArchivo'=> $buscarArchivo]);

    }
    public function archivoEditar(Request $request,Messages $messages)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();

                $archivo = Tarchivo::find($request->get('idArchivo'));  

                $archivo->remitente= $request->get('remitente');
                $archivo->destinatario= $request->get('destinatario');
                $archivo->numeroDocumento= $request->get('numeroDocumento');
                $archivo->tipoDocumento = $request->get('tipoDocumento');
                $archivo->estadoArchivo= $request->get('estadoArchivo');
                $archivo->observacion= $request->get('observacionEditar');
                $archivo->fechaIngreso= $request->get('fechaIngreso');
                $archivo->asunto= $request->get('asunto');

                $archivo->numeroRegistro= $request->get('numeroRegistro');
                $archivo->folios= $request->get('folios');
                $archivo->proveido= $request->get('proveido');           
             
                if($request->hasFile('archivoEditar'))
                {
                    $random=rand().time();
                    $extension = strtolower($request->file('archivoEditar')->getClientOriginalExtension());
                    $request->file('archivoEditar')->move(public_path().'/archivo',  $archivo->fechaIngreso.'-'. $archivo->proveido.$random.'.'.$extension );

                    $archivo->archivo = $request->get('hiddenUrl').'/archivo/'.  $archivo->fechaIngreso.'-'. $archivo->proveido.$random.'.'.$extension ;
                }                
                
                $archivo->save();

                DB::commit();

                return $messages->MessageCorrect('Documento EDITADO correctamente','archivo/lista');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('No se pudo EDITAR el documento, consulte con el administrador','archivo/lista');
            }
        }
        return view('supervision/archivo/lista');

    }
    public function respuesta(Request $request,Messages $messages)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
                $respuesta = new Trespuesta();
                $respuesta->idRespuesta = uniqid();                
                $respuesta->destinatario= $request->get('destinatarioRpta');
                $respuesta->asuntoRpta= $request->get('asuntoRpta');
                $respuesta->tipoDocumentoRpta= $request->get('tipoDocumentoRpta');
                
                $respuesta->fechaEnvio = $request->get('fechaEnvio');
                $respuesta->numeroRegistroRpta= $request->get('numeroRegistroRpta');
                $respuesta->foliosRpta= $request->get('foliosRpta');
                $respuesta->idArchivo= $request->get('idArchivo');

                if($request->hasFile('documentoRpta'))
                {
                    $random=rand().time();
                    $extension = strtolower($request->file('documentoRpta')->getClientOriginalExtension());
                    $request->file('documentoRpta')->move(public_path().'/respuesta',  $respuesta->fechaEnvio.'-'.$random.'.'.$extension );

                    $respuesta->documentoRpta = $request->get('hiddenUrl').'/respuesta/'.  $respuesta->fechaEnvio.'-'.$random.'.'.$extension ;
                }
          

                $respuesta->save();

                DB::commit();

                return $messages->MessageCorrect('DOCUMENTO respuesta REGISTRADO correctamente','archivo/lista');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('No se pudo enviar el documento, consulte con el administrador','archivo/lista');
            }
        }

      
        $respuesta=DB::table('t_archivo')
        ->join('t_respuesta', 't_respuesta.idArchivo', '=', 't_archivo.idArchivo')
        ->select('*')
        ->paginate(10);
        return view('supervision/respuesta/lista',['respuesta'=> $respuesta]);

    }
    public function respuestaEditar(Request $request,Messages $messages)
    {
        if($_POST)
        {
            try
            {
                DB::beginTransaction();
                $respuesta = Trespuesta::find($request->get('idRespuesta')); 
            
                $respuesta->asuntoRpta= $request->get('asuntoRpta');
                $respuesta->tipoDocumentoRpta= $request->get('tipoDocumentoRpta');                
                $respuesta->fechaEnvio = $request->get('fechaEnvio');
                $respuesta->numeroRegistroRpta= $request->get('numeroRegistroRpta');
                $respuesta->foliosRpta= $request->get('foliosRpta');                

                if($request->hasFile('documentoRpta'))
                {
                    $random=rand().time();
                    $extension = strtolower($request->file('documentoRpta')->getClientOriginalExtension());
                    $request->file('documentoRpta')->move(public_path().'/respuesta',  $respuesta->fechaEnvio.'-'.$random.'.'.$extension );

                    $respuesta->documentoRpta = $request->get('hiddenUrl').'/respuesta/'.  $respuesta->fechaEnvio.'-'.$random.'.'.$extension ;
                }
                
                $respuesta->save();

                DB::commit();

                return $messages->MessageCorrect('DOCUMENTO respuesta EDITADO correctamente','respuesta/lista');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('No se pudo EDITAR el documento, consulte con el administrador','respuesta/lista');
            }
        }

      
        $respuesta=DB::table('t_archivo')
        ->join('t_respuesta', 't_respuesta.idArchivo', '=', 't_archivo.idArchivo')
        ->select('*')
        ->paginate(4);
        return view('supervision/respuesta/lista',['respuesta'=> $respuesta]);
    }
}