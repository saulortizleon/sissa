<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Tformacion;
use Session;
use DB;

class Verificables extends Controller
{
    public function formacion(Request $request,$ubigeo=null)
    {
        $listaEditar=DB::table('t_atm')
                        ->join('t_formacion', 't_atm.ubigeo', '=', 't_formacion.ubigeo')
                        ->where('t_formacion.ubigeo',$ubigeo)
                        ->select('*')
                        ->get();
        return view('admin/verificables/formacion',['listaEditar'=> $listaEditar]);
    }
    public function herramientas(Request $request,$ubigeo=null)
    {
        $listaEditar=DB::table('t_atm')
                        ->join('t_herramienta', 't_atm.ubigeo', '=', 't_herramienta.ubigeo')
                        ->where('t_herramienta.ubigeo',$ubigeo)
                        ->select('*')
                        ->get();
        return view('admin/verificables/herramientas',['listaEditar'=> $listaEditar]);
    }
    public function gestion(Request $request,$ubigeo=null)
    {
        $listaEditar=DB::table('t_atm')
                        ->where('t_atm.ubigeo',$ubigeo)
                        ->select('*')
                        ->get();
        return view('admin/verificables/gestion',['listaEditar'=> $listaEditar]);
    }

}