<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Tusuario;
use App\Helpers\Messages;
use Session;
use DB;
class UsuarioController extends Controller
{
    public function lista(Request $request)
    {
        $dni=Session::get('t_usuario')[0];
        $listaUsuario = Tusuario::orderBy('nombres', 'asc')->where('dniUsuario',$dni)->get();
        return view('usuario/lista',['listaUsuario' => $listaUsuario]);
    }
    public function editarUsuarios(Request $request, Messages $messages, $dniUsuario=null)
	{

		if($_POST)
		{
			try
			{
				DB::beginTransaction();

                    $usuarios = Tusuario::find($request->get('dniUsuario'));
					$usuarios->nombres=$request->input('nombres');
					$usuarios->celular=$request->input('celular');
                    $usuarios->sexo=$request->input('sexo');
                    $usuarios->correo=$request->input('correo');
                    $usuarios->fechaNacimiento=$request->input('fechaNacimiento');

                    $usuarios->save();

				DB::commit();

				return $messages->MessageCorrect('Usuario editado correctamente','usuario/lista');

			}
			catch(\Exception $ex)
			{
                DB::rollback();
                return $messages->MessageIncorrect('No se pudo realizar los cambios, consulte con el administrador','usuario/lista');
			}
        }
        $listaEditar = Tusuario::find($dniUsuario);
    return view('usuario/editar',['listaEditar' => $listaEditar]);
    }
}