<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Session\SessionManager;
use Illuminate\Http\Request;
use App\Model\Tusuario;
use App\Helpers\Messages;

use Session;
use DB;

class LoginController extends Controller
{
    public function IndexLogin()
    {
        return view('admin/login');
    }
    public function actionLogin(Request $request, SessionManager $sessionManager,Messages $messages)
	{
		if($_POST)
        {
			$user = Tusuario::where('dniUsuario',$request->get('dni'))->first();

			$contrasena=$request->input('contrasena');
            if($user != null)
            {
                if($user->contrasena==$contrasena && $user->cargo=='sup')
                {
                    $datos = [$user->dniUsuario , $user->nombres, $user->cargo];
                    $sessionManager->put('t_usuario',$datos);

                    return Redirect::to('supervision');
                }
                if($user->contrasena==$contrasena && $user->cargo=='adm')
                {
                    $datos = [$user->dniUsuario , $user->nombres, $user->cargo];
                    $sessionManager->put('t_usuario',$datos);

                    return Redirect::to('adm');
                }
                if($user->contrasena==$contrasena && $user->cargo=='atm')
                {
                    $datos = [$user->dniUsuario , $user->nombres, $user->cargo];
                    $sessionManager->put('t_usuario',$datos);

                    return Redirect::to('atm-finalista');
                }

            }
            return $messages->MessageIncorrect('Usuario y contraseña incorrectos', 'login');
        }
        return view('login');
	}

    public function actionLogOut(SessionManager $sessionManager)
	{
		$sessionManager->flush();

		return redirect('login');
	}
}
?>