<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use App\Helpers\Messages;
use App\Model\Tapppnsr;

use Redirect;
use DB;
use File;
use Session;
class ApppnsrController extends Controller
{
    public function listaPnsr(Request $request)
    {
        $dni=Session::get('t_usuario')[0];
        if($dni != null)
        {
            $listaPnsr=DB::table('t_atm')
                        ->join('t_funcion', 't_atm.ubigeo', '=', 't_funcion.ubigeo')
                        ->join('t_appaguasane', 't_appaguasane.idFuncion', '=', 't_funcion.idFuncion')
                        ->where('t_atm.dniUsuario',  $dni)
                        ->select('t_appaguasane.*')
                        ->get();

            return view('apppnsr/lista',['listaPnsr'=> $listaPnsr]);
        }
    }
    public function editarApppnsr(Request $request,Messages $messages,$idApp=null)
    {
        if($_POST)
        {
            try
            {
                $apppnsr = Tapppnsr::find($request->get('idApp'));
                $apppnsr->estado1 = $request->get('estado1');
                $apppnsr->estado2 = $request->get('estado2');
                $apppnsr->estado3= $request->get('estado3');
                $apppnsr->estado4= $request->get('estado4');
                $apppnsr->cantidadCpDiag= $request->get('cantidadCpDiag');
                if($request->hasFile('ver1'))
                {
                    $random=rand().time();
                    $extension = strtolower($request->file('ver1')->getClientOriginalExtension());
                    $request->file('ver1')->move(public_path().'/pnsr',$random.'.'.$extension );

                    $apppnsr->ver1 = $request->get('hiddenUrl').'/pnsr/'.$random.'.'.$extension;
                }

                if($request->hasFile('ver2'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver2')->getClientOriginalExtension());
                    $request->file('ver2')->move(public_path().'/pnsr',$random.'.'.$extension );

                    $apppnsr->ver2 = $request->get('hiddenUrl').'/pnsr/'.$random.'.'.$extension;
                }
                if($request->hasFile('ver3'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver3')->getClientOriginalExtension());
                    $request->file('ver3')->move(public_path().'/pnsr',$random.'.'.$extension );

                    $apppnsr->ver3 = $request->get('hiddenUrl').'/pnsr/'.$random.'.'.$extension;
                }
                if($request->hasFile('ver4'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver4')->getClientOriginalExtension());
                    $request->file('ver4')->move(public_path().'/pnsr',$random.'.'.$extension );

                    $apppnsr->ver4 = $request->get('hiddenUrl').'/pnsr/'.$random.'.'.$extension;
                }
                if($request->hasFile('ver5'))
                {   $random=rand().time();
                    $extension = strtolower($request->file('ver5')->getClientOriginalExtension());
                    $request->file('ver5')->move(public_path().'/pnsr',$random.'.'.$extension );

                    $apppnsr->ver5 = $request->get('hiddenUrl').'/pnsr/'.$random.'.'.$extension;
                }
                $apppnsr->save();
                DB::commit();

                return $messages->MessageCorrect('Lista de apppnsr actualizada','pnsr/lista');

            }
            catch(\Exceptio $e)
            {    DB::rollback();
                return $messages->MessageIncorrect('No se pudo realizar los cambios, consulte con el administrador','pnsr/lista');
            }
        }
        $listaEditar = Tapppnsr::find($idApp);
        return view('apppnsr/editar',['listaEditar' => $listaEditar]);
    }
}