<?php
namespace App\Helpers;

use Session;
use Redirect;

class Messages
{
	public function MessageCorrect($message,$route)
    {
		Session::flash('message-correct',$message);
		return Redirect($route);
    }

   public function MessageIncorrect($message,$route)
   {
   		Session::flash('message-error',$message);
		return Redirect($route);
   }
}