<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tusuario extends Model
{
    protected $table='t_usuario';
    protected $primaryKey='dniUsuario';
    public $timestamps=true;
    public $incrementing=false;
    protected $keyType='string';

    public function Tatm()
	{
		return $this->hasMany('App\Model\Tatm', 'dniUsuario');
	}
}