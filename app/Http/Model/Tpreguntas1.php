<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tpreguntas1 extends Model
{
    protected $table='t_preguntas1';
    protected $primaryKey='idPreguntas1';
    public $timestamps=true;
    public $incrementing=false;
    protected $keyType='string';

    public function Tatm()
	{
		return $this->hasMany('App\Model\Tatm', 'ubigeo');
	}
}