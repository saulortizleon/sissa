<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TcentroP extends Model
{
    protected $table='t_centropoblado';
    protected $primaryKey='ubigeo';
    public $timestamps=true;
    public $incrementing=false;
    protected $keyType='string';

    public function Tatm()
	{
		return $this->hasMany('App\Model\Tatm', 'ubigeo');
	}
}