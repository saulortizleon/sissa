<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tpreguntas3 extends Model
{
    protected $table='t_preguntas3';
    protected $primaryKey='idPreguntas3';
    public $timestamps=true;
    public $incrementing=false;
    protected $keyType='string';

    public function Tatm()
	{
		return $this->hasMany('App\Model\Tatm', 'ubigeo');
	}
}