<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tsap extends Model
{
    protected $table='t_herramienta';
    protected $primaryKey='idHerr';
    public $timestamps=true;
    public $incrementing=true;
    protected $keyType='string';

    public function Tatm()
	{
		return $this->hasMany('App\Model\Tatm', 'ubigeo');
	}
}