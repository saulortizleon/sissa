<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tatmver extends Model
{
    protected $table='t_atmver';
    protected $primaryKey='idVerAtm';
    public $timestamps=true;
    public $incrementing=false;
    protected $keyType='string';

    public function Tatm()
    {
    return $this->belongTo('App\Http\Model\Tatm', 'ubigeo');
    }
}