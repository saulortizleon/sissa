<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tformacion extends Model
{
    protected $table='t_formacion';
    protected $primaryKey='idFormacion';
    public $timestamps=true;
    public $incrementing=true;
    protected $keyType='string';

    public function Tatm()
	{
		return $this->hasMany('App\Model\Tatm', 'ubigeo');
	}
}