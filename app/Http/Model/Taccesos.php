<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Taccesos extends Model
{
    protected $table='t_accesos';
    protected $primaryKey='idt_accesos';
    public $timestamps=true;
    public $incrementing=false;
    protected $keyType='string';

    public function Tatm()
	{
		return $this->belongTo('App\Model\Tatm', 'ubigeo');
	}
}