<?php
namespace App\Model\blog;

use Illuminate\Database\Eloquent\Model;

class Timg1 extends Model
{
    protected $table='t_img1';
    protected $primaryKey='idimg1';
    public $timestamps=true;
    public $incrementing=false;
    protected $keyType='string';

    public function Tpublicacion()
    {
      return $this->belongTo('App\Model\Tpublicacion', 'idPublicacion');
  }

}