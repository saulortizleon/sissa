<?php
namespace App\Model\blog;

use Illuminate\Database\Eloquent\Model;

class TPublicacion extends Model
{
    protected $table='t_publicacion';
    protected $primaryKey='idPublicacion';
    public $timestamps=true;
    public $incrementing=true;
    protected $keyType='string';

    public function Tusuario()
	  {
		return $this->belongTo('App\Model\Tusuario', 'dni');
    }
    public function Timgp()
	  {
		return $this->belongTo('App\Model\Timgp', 'idimgp');
    }
    public function Timg1()
	  {
		return $this->belongTo('App\Model\Timg1', 'idimgp');
    }
    public function Timg2()
	  {
		return $this->belongTo('App\Model\Timg2', 'idimgp');
    }
    public function Tdoc()
	  {
		return $this->belongTo('App\Model\Tdoc', 'iddoc');
    }
    public function Tcategoria()
	  {
		return $this->belongTo('App\Model\Tcategoria', 'idCategoria');
    }
}