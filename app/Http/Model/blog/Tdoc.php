<?php
namespace App\Model\blog;

use Illuminate\Database\Eloquent\Model;

class Tdoc extends Model
{
    protected $table='T_documento';
    protected $primaryKey='iddoc';
    public $timestamps=true;
    public $incrementing=false;
    protected $keyType='string';

    public function Tpublicacion()
    {
      return $this->belongTo('App\Model\Tpublicacion', 'idPublicacion');
  }

}