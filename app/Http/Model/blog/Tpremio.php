<?php
namespace App\Model\blog;

use Illuminate\Database\Eloquent\Model;

class Tconcurso extends Model
{
    protected $table='concurso';
    protected $primaryKey='idatm';
    public $timestamps=true;
    public $incrementing=false;
    protected $keyType='string';

}