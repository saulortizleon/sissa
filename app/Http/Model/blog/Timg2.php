<?php
namespace App\Model\blog;

use Illuminate\Database\Eloquent\Model;

class Timg2 extends Model
{
    protected $table='t_img2';
    protected $primaryKey='idimg2';
    public $timestamps=true;
    public $incrementing=false;
    protected $keyType='string';

    public function Tpublicacion()
    {
      return $this->belongTo('App\Model\Tpublicacion', 'idPublicacion');
  }

}