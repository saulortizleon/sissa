-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: sisa
-- ------------------------------------------------------
-- Server version	5.7.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `t_atm`
--

DROP TABLE IF EXISTS `t_atm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_atm` (
  `ubigeo` varchar(11) NOT NULL,
  `provincia` varchar(120) DEFAULT NULL,
  `distrito` varchar(100) DEFAULT NULL,
  `ordMunicipal` varchar(100) DEFAULT NULL,
  `fechaOrdMunicipal` date DEFAULT NULL,
  `depende` varchar(50) DEFAULT NULL,
  `resolucionPerfil` varchar(45) DEFAULT NULL,
  `fechaResPerfil` date DEFAULT NULL,
  `resolucionAlcaldia` varchar(45) DEFAULT NULL,
  `fechaReslAlcaldia` date DEFAULT NULL,
  `condicionLaboral` varchar(45) DEFAULT NULL,
  `otrosCargos` varchar(45) DEFAULT NULL,
  `numCargos` int(11) DEFAULT NULL,
  `miembrosAdicionales` int(11) DEFAULT NULL,
  `numMiembrosAdicionales` char(2) DEFAULT NULL,
  `nroRaContrato` varchar(25) DEFAULT NULL,
  `fechaNroContrato` date DEFAULT NULL,
  `estadoPerfil` varchar(10) DEFAULT NULL,
  `ordenanzaJass` varchar(50) DEFAULT NULL,
  `fechaOrdJass` date DEFAULT NULL,
  `ordenanzaCloro` varchar(50) DEFAULT NULL,
  `fechaOrdenanzaCloro` date DEFAULT NULL,
  `ordenanzaAnemia` varchar(50) DEFAULT NULL,
  `fechaOrdAnemia` date DEFAULT NULL,
  `planSupervision` varchar(50) DEFAULT NULL,
  `otras ordenanzas` varchar(50) DEFAULT NULL,
  `otrasOrdMun` varchar(50) DEFAULT NULL,
  `fechaotrasOrdM` date DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `dniUsuario` char(8) NOT NULL,
  PRIMARY KEY (`ubigeo`),
  KEY `fk_t_atm_t_usuario1` (`dniUsuario`),
  CONSTRAINT `fk_t_atm_t_usuario1` FOREIGN KEY (`dniUsuario`) REFERENCES `t_usuario` (`dniUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_atm`
--

LOCK TABLES `t_atm` WRITE;
/*!40000 ALTER TABLE `t_atm` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_atm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_atmver`
--

DROP TABLE IF EXISTS `t_atmver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_atmver` (
  `idVerAtm` char(13) NOT NULL,
  `ver1` text,
  `ver2` text,
  `ver3` text,
  `ver4` text,
  `ver5` text,
  `ver6` text,
  `ver7` text,
  `ver8` text,
  `ver9` text,
  `ver10` text,
  `ver11` text,
  `ubigeo` varchar(11) NOT NULL,
  PRIMARY KEY (`idVerAtm`),
  KEY `ubigeoAtmxx` (`ubigeo`),
  CONSTRAINT `ubigeoAtmxx` FOREIGN KEY (`ubigeo`) REFERENCES `t_atm` (`ubigeo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_atmver`
--

LOCK TABLES `t_atmver` WRITE;
/*!40000 ALTER TABLE `t_atmver` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_atmver` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_centropoblado`
--

DROP TABLE IF EXISTS `t_centropoblado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_centropoblado` (
  `ubigeo` varchar(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `numPobladores` int(11) DEFAULT NULL,
  `soloServicioAgua` varchar(2) DEFAULT NULL,
  `soloConUBS` int(11) DEFAULT NULL,
  `numSAP` varchar(45) DEFAULT NULL,
  `adminSAS` varchar(45) DEFAULT NULL,
  `atmUbigeo` varchar(11) NOT NULL,
  `OCModelo` char(2) DEFAULT NULL,
  PRIMARY KEY (`ubigeo`),
  KEY `fk_t_centropoblado_t_atm1` (`atmUbigeo`),
  CONSTRAINT `fk_t_centropoblado_t_atm1` FOREIGN KEY (`atmUbigeo`) REFERENCES `t_atm` (`ubigeo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_centropoblado`
--

LOCK TABLES `t_centropoblado` WRITE;
/*!40000 ALTER TABLE `t_centropoblado` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_centropoblado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_formacion`
--

DROP TABLE IF EXISTS `t_formacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_formacion` (
  `idFormacion` char(13) NOT NULL,
  `ROF` varchar(2) DEFAULT NULL,
  `MOF` varchar(2) DEFAULT NULL,
  `CAP` varchar(2) DEFAULT NULL,
  `TUPA` varchar(2) DEFAULT NULL,
  `gradoAcadem` varchar(45) DEFAULT NULL,
  `especialidad` varchar(45) DEFAULT NULL,
  `GN` varchar(2) DEFAULT NULL,
  `GR` varchar(2) DEFAULT NULL,
  `GL` varchar(2) DEFAULT NULL,
  `ONG` varchar(2) DEFAULT NULL,
  `EDUSA` varchar(2) DEFAULT NULL,
  `AOM` varchar(2) DEFAULT NULL,
  `DL1280` varchar(2) DEFAULT NULL,
  `PNSR` varchar(2) DEFAULT NULL,
  `PNSRotros` varchar(2) DEFAULT NULL,
  `planSan` varchar(2) DEFAULT NULL,
  `criterios` varchar(2) DEFAULT NULL,
  `ver1` text,
  `ver2` text,
  `ver3` text,
  `ver4` text,
  `ver5` text,
  `ver6` text,
  `ver7` text,
  `ver8` text,
  `ver9` text,
  `ver10` text,
  `ver11` text,
  `ver12` text,
  `ver13` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `ubigeo` varchar(11) NOT NULL,
  PRIMARY KEY (`idFormacion`),
  KEY `fk_t_formacion_t_atm1` (`ubigeo`),
  CONSTRAINT `fk_t_formacion_t_atm1` FOREIGN KEY (`ubigeo`) REFERENCES `t_atm` (`ubigeo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_formacion`
--

LOCK TABLES `t_formacion` WRITE;
/*!40000 ALTER TABLE `t_formacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_formacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_herramienta`
--

DROP TABLE IF EXISTS `t_herramienta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_herramienta` (
  `idHerr` char(13) NOT NULL,
  `oficinaPropia` char(2) DEFAULT NULL,
  `equipocomputo` char(2) DEFAULT NULL,
  `multimedia` char(2) DEFAULT NULL,
  `estante` char(2) DEFAULT NULL,
  `moto` char(2) DEFAULT NULL,
  `comparadorCloro` char(2) DEFAULT NULL,
  `turbidimetro` char(2) DEFAULT NULL,
  `gps` char(2) DEFAULT NULL,
  `wincha` char(2) DEFAULT NULL,
  `pastillasdpd` char(2) DEFAULT NULL,
  `cronometro` char(2) DEFAULT NULL,
  `mascarilla` char(2) DEFAULT NULL,
  `guantes` char(2) DEFAULT NULL,
  `overol` char(2) DEFAULT NULL,
  `lentesProteccion` char(2) DEFAULT NULL,
  `botasJebe` char(2) DEFAULT NULL,
  `casco` char(2) DEFAULT NULL,
  `ver1` text,
  `ver2` text,
  `ver3` text,
  `ubigeo` varchar(11) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`idHerr`),
  KEY `ubigeo` (`ubigeo`),
  CONSTRAINT `ubigeo` FOREIGN KEY (`ubigeo`) REFERENCES `t_atm` (`ubigeo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_herramienta`
--

LOCK TABLES `t_herramienta` WRITE;
/*!40000 ALTER TABLE `t_herramienta` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_herramienta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_planseg`
--

DROP TABLE IF EXISTS `t_planseg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_planseg` (
  `idplaneg` char(13) NOT NULL,
  `planEDUSA` varchar(45) DEFAULT NULL,
  `avanceEDUSA` varchar(45) DEFAULT NULL,
  `planGS` varchar(45) DEFAULT NULL,
  `avanceGS` varchar(45) DEFAULT NULL,
  `planRiesgos` char(2) DEFAULT NULL,
  `planRptaRapida` char(2) DEFAULT NULL,
  `planAsegurarDisponibilidad` char(2) DEFAULT NULL,
  `ubigeoCP` varchar(11) NOT NULL,
  PRIMARY KEY (`idplaneg`),
  KEY `fk_t_planseg_t_centropoblado1` (`ubigeoCP`),
  CONSTRAINT `fk_t_planseg_t_centropoblado1` FOREIGN KEY (`ubigeoCP`) REFERENCES `t_centropoblado` (`ubigeo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_planseg`
--

LOCK TABLES `t_planseg` WRITE;
/*!40000 ALTER TABLE `t_planseg` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_planseg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_preguntas1`
--

DROP TABLE IF EXISTS `t_preguntas1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_preguntas1` (
  `idPreguntas1` char(13) CHARACTER SET utf8mb4 NOT NULL,
  `cpConHabitantes` int(11) DEFAULT NULL,
  `cpConAyS` int(11) DEFAULT NULL,
  `cpConServicioA` int(11) DEFAULT NULL,
  `cpConUBS` int(11) DEFAULT NULL,
  `totalPrestadoresAgua` int(11) DEFAULT NULL,
  `epsAdmSas` int(11) DEFAULT NULL,
  `admMuniSas` int(11) DEFAULT NULL,
  `totalCp` int(11) DEFAULT NULL,
  `numOES` int(11) DEFAULT NULL,
  `totalSAP` int(11) DEFAULT NULL,
  `SAPoperaNormal` int(11) DEFAULT NULL,
  `SAPlimitado` int(11) DEFAULT NULL,
  `SAPnormal` int(11) DEFAULT NULL,
  `SAPcolapsado` int(11) DEFAULT NULL,
  `OCconLicenciaAgua` int(11) DEFAULT NULL,
  `numSistemasCloracion` int(11) DEFAULT NULL,
  `numSCnormal` int(11) DEFAULT NULL,
  `numSClimitado` int(11) DEFAULT NULL,
  `numSCcolapsado` int(11) DEFAULT NULL,
  `numSisa` int(11) DEFAULT NULL,
  `numSisaNormal` int(11) DEFAULT NULL,
  `numSisaLimitado` int(11) DEFAULT NULL,
  `numSisaColapsados` int(11) DEFAULT NULL,
  `tieneEdusa` char(2) DEFAULT NULL,
  `ubigeo` varchar(11) CHARACTER SET utf8mb4 DEFAULT NULL,
  `ubigeoAtm` varchar(11) NOT NULL,
  PRIMARY KEY (`idPreguntas1`),
  KEY `idx_t_preguntas1_ubigeo` (`ubigeo`),
  KEY `ubigeoAtm` (`ubigeoAtm`),
  CONSTRAINT `ubigeoAtm` FOREIGN KEY (`ubigeoAtm`) REFERENCES `t_atm` (`ubigeo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_preguntas1`
--

LOCK TABLES `t_preguntas1` WRITE;
/*!40000 ALTER TABLE `t_preguntas1` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_preguntas1` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_preguntas2`
--

DROP TABLE IF EXISTS `t_preguntas2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_preguntas2` (
  `idPreguntas2` char(13) NOT NULL,
  `ejecutaPoa` char(2) DEFAULT NULL,
  `numActividadesPoa` int(11) DEFAULT NULL,
  `avancePoa` int(11) DEFAULT NULL,
  `ejecutaPptoPOA` char(2) DEFAULT NULL,
  `presupuestoPOAejecutado` int(11) DEFAULT NULL,
  `avancePptoPOA` int(11) DEFAULT NULL,
  `primerModuloCap` int(11) DEFAULT NULL,
  `segundoModuloCap` int(11) DEFAULT NULL,
  `tercerModuloCap` int(11) DEFAULT NULL,
  `numSesionesAnio` int(11) DEFAULT NULL,
  `numOCCapAnio` int(11) DEFAULT NULL,
  `practicaEDUSA` int(11) DEFAULT NULL,
  `valoraEDUSA` int(11) DEFAULT NULL,
  `sesionesEdUSA` int(11) DEFAULT NULL,
  `numCapEDUSA` int(11) DEFAULT NULL,
  `brindaAsisTecnica` char(2) DEFAULT NULL,
  `numAsisTecTrimestre` int(11) DEFAULT NULL,
  `numOCAsisTec` int(11) DEFAULT NULL,
  `spotRadial` char(2) DEFAULT NULL,
  `pagoCuotaFami` char(2) DEFAULT NULL,
  `cuidadoAP` char(2) DEFAULT NULL,
  `usoCorrectoAP` char(2) DEFAULT NULL,
  `momentosLavadoManos` char(2) DEFAULT NULL,
  `todoUBS` char(2) DEFAULT NULL,
  `manejoRRSS` char(2) DEFAULT NULL,
  `AseguraCloroRural` char(2) DEFAULT NULL,
  `demandaProyectada` int(11) DEFAULT NULL,
  `demandaDiaria` int(11) DEFAULT NULL,
  `fondoRotatorioVenta` char(2) DEFAULT NULL,
  `montoFondo` decimal(12,2) DEFAULT NULL,
  `otrasEstrategias` varchar(80) DEFAULT NULL,
  `ubigeo` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`idPreguntas2`),
  KEY `ubigeoa` (`ubigeo`),
  CONSTRAINT `ubigeoa` FOREIGN KEY (`ubigeo`) REFERENCES `t_atm` (`ubigeo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_preguntas2`
--

LOCK TABLES `t_preguntas2` WRITE;
/*!40000 ALTER TABLE `t_preguntas2` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_preguntas2` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_preguntas3`
--

DROP TABLE IF EXISTS `t_preguntas3`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_preguntas3` (
  `idPreguntas3` char(13) NOT NULL,
  `atmArticulaIE` char(2) DEFAULT NULL,
  `inicial` varchar(50) DEFAULT NULL,
  `primaria` varchar(50) DEFAULT NULL,
  `secundaria` varchar(50) DEFAULT NULL,
  `atmArticulaSA` char(2) DEFAULT NULL,
  `nombreIPRESS` char(100) DEFAULT NULL,
  `reporteIPRESS` text,
  `fechaReporte` datetime DEFAULT NULL,
  `otrasInstPublicas` char(2) DEFAULT NULL,
  `otrasInstPublicas1` varchar(110) DEFAULT NULL,
  `otrasInstPublicas2` varchar(110) DEFAULT NULL,
  `otrasInstPrivadas` char(2) DEFAULT NULL,
  `otrasInstPrivadas1` varchar(110) DEFAULT NULL,
  `mapaRiesgos` varchar(110) DEFAULT NULL,
  `planRespuestaRapida` varchar(110) DEFAULT NULL,
  `planDisponibilidad` varchar(110) DEFAULT NULL,
  `cubiertaProteccionVegetal` varchar(110) DEFAULT NULL,
  `otrasAccionesImpl` varchar(110) DEFAULT NULL,
  `identificacionZona` varchar(110) DEFAULT NULL,
  `areaIdentificada` varchar(110) DEFAULT NULL,
  `cuestionarioAyExSemestre1` char(2) DEFAULT NULL,
  `cuestionarioAyExSemestre2` char(2) DEFAULT NULL,
  `diagAtmAppSemestre1` char(2) DEFAULT NULL,
  `diagAtmAppSemestre2` char(2) DEFAULT NULL,
  `salaSituacionalReporteAppSemestre1` char(2) DEFAULT NULL,
  `salaSituacionalReporteAppSemestre2` char(2) DEFAULT NULL,
  `ubigeo` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`idPreguntas3`),
  KEY `ubigeo_idx` (`ubigeo`),
  CONSTRAINT `ubigeo_fk` FOREIGN KEY (`ubigeo`) REFERENCES `t_atm` (`ubigeo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_preguntas3`
--

LOCK TABLES `t_preguntas3` WRITE;
/*!40000 ALTER TABLE `t_preguntas3` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_preguntas3` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_preguntas4`
--

DROP TABLE IF EXISTS `t_preguntas4`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_preguntas4` (
  `idPreguntas4` int(13) NOT NULL,
  `poiResolucion2019` varchar(70) DEFAULT NULL,
  `poiResolucion2020` varchar(45) DEFAULT NULL,
  `ppto` decimal(10,0) DEFAULT NULL,
  `ppto083` decimal(10,0) DEFAULT NULL,
  `porcentajeGasto` int(11) DEFAULT NULL,
  `anio` varchar(12) DEFAULT NULL,
  `planSupervision` varchar(2) DEFAULT NULL,
  `OCfechaUltimoMes` date DEFAULT NULL,
  `OCfechaUltimo6meses` date DEFAULT NULL,
  `OCfechaUltimoAnio` date DEFAULT NULL,
  `HRfechaUltimoMes` date DEFAULT NULL,
  `HRfechaUltimo6meses` date DEFAULT NULL,
  `HRfechaUltimoAnio` date DEFAULT NULL,
  `ubigeo` varchar(11) NOT NULL,
  PRIMARY KEY (`idPreguntas4`),
  KEY `fk_POI_t_atm1` (`ubigeo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_preguntas4`
--

LOCK TABLES `t_preguntas4` WRITE;
/*!40000 ALTER TABLE `t_preguntas4` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_preguntas4` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_sap`
--

DROP TABLE IF EXISTS `t_sap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_sap` (
  `idSap` varchar(13) NOT NULL,
  `estadoSap` varchar(20) DEFAULT NULL,
  `estadoCloracion` varchar(20) DEFAULT NULL,
  `licenciaAgua` varchar(20) DEFAULT NULL,
  `estadoAlcantarillado` varchar(20) DEFAULT NULL,
  `libroRegistroOC` char(2) DEFAULT NULL,
  `estadoRegistroOC` varchar(10) DEFAULT NULL,
  `fechaRegCloro` varchar(45) DEFAULT NULL,
  `estadoCaracterSAP` varchar(45) DEFAULT NULL,
  `fechaCaracterFA` varchar(45) DEFAULT NULL,
  `cloroAmbitoAsegurado` char(2) DEFAULT NULL,
  `demandaProyectada` int(11) DEFAULT NULL,
  `fondoRotarioVenta` char(2) DEFAULT NULL,
  `montoFondo` decimal(12,2) DEFAULT NULL,
  `otrasEstrategCloro` varchar(45) DEFAULT NULL,
  `cubiertaProtSAP` char(2) DEFAULT NULL,
  `cubiertaVegetal` char(2) DEFAULT NULL,
  `otrasAccionesCubSAP` varchar(45) DEFAULT NULL,
  `lugarIdentificadCubVegSAP` char(2) DEFAULT NULL,
  `ubigeoCP` varchar(11) NOT NULL,
  PRIMARY KEY (`idSap`),
  KEY `fk_t_sap_t_centropoblado1` (`ubigeoCP`),
  CONSTRAINT `fk_t_sap_t_centropoblado1` FOREIGN KEY (`ubigeoCP`) REFERENCES `t_centropoblado` (`ubigeo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_sap`
--

LOCK TABLES `t_sap` WRITE;
/*!40000 ALTER TABLE `t_sap` DISABLE KEYS */;
/*!40000 ALTER TABLE `t_sap` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `t_usuario`
--

DROP TABLE IF EXISTS `t_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `t_usuario` (
  `dniUsuario` char(8) NOT NULL,
  `contrasena` varchar(32) DEFAULT NULL,
  `nombres` varchar(200) DEFAULT NULL,
  `celular` varchar(9) DEFAULT NULL,
  `estado` varchar(15) DEFAULT NULL,
  `cargo` char(3) DEFAULT NULL,
  `sexo` char(1) DEFAULT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `fechaNacimiento` datetime DEFAULT NULL,
  PRIMARY KEY (`dniUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `t_usuario`
--

LOCK TABLES `t_usuario` WRITE;
/*!40000 ALTER TABLE `t_usuario` DISABLE KEYS */;
INSERT INTO `t_usuario` VALUES ('11111111','11111111@2019','GUINA SAMBRANO LEON','930230325','activo','atm',NULL,NULL,NULL),('20579191','20579191@2019','ADRIEL BARBARAN SILVA','986593095','activo','atm',NULL,NULL,NULL),('22222222','22222222@2019','RONAL CCOYCCA QUINTE','914849257','activo','atm',NULL,NULL,NULL),('23858133','23858133@2019','JOSE RAMOS CALCINA','983286105','activo','atm',NULL,NULL,NULL),('30300322','30300322@2019','GRIMALDO ESPINOZA LOPEZ','950317918','activo','atm',NULL,NULL,NULL),('31001954','31001954@2019','WILDO CAITUIRO AGRADA','950019991','activo','atm',NULL,NULL,NULL),('31011032','31011032@2019','JUVENAL HIPOLITO TELLO CARBAJAL','949355296','activo','atm',NULL,NULL,NULL),('31019056','31019056@2019','LUIS MANSILLA TRUJILLO','983789198','activo','atm',NULL,NULL,NULL),('31026732','31026732@2019','GUMERCINDO LEONCIO BRAVO QUISPE','961139080','activo','atm',NULL,NULL,NULL),('31033305','31033305@2019','ESTANISLAO CHANI SEQUEIROS','974932039','activo','atm',NULL,NULL,NULL),('31033691','31033691@2019','ANGELICA VANESSA VALDIVIA RIOS','983610330','activo','atm',NULL,NULL,NULL),('31144145','31144145@2019','FORTUNATO RODRIGUEZ CHIPANA','998738280','activo','atm',NULL,NULL,NULL),('31148086','31148086@2019','LORENZO GONZALES SAMURIN','929737472','activo','atm',NULL,NULL,NULL),('31174100','31174100@2019','ALBINO DIAS PAUCAR','927243437','activo','atm',NULL,NULL,NULL),('31178662','31178662@2019','DONATO LAGO GUIZADO','973224349','activo','atm',NULL,NULL,NULL),('31179206','31179206@2019','PASCUAL VARGAS CCASANI','981904253','activo','atm',NULL,NULL,NULL),('31347878','31347878@2019','YONI ANTESANA CONDORI','914878135','activo','atm',NULL,NULL,NULL),('31352351','31352351@2019','TORIBIO CONTRERAS VELASQUEZ','952139683','activo','atm',NULL,NULL,NULL),('31358401','31358401@2019','WILLBERT NEIRA TORRES','955642217','activo','atm',NULL,NULL,NULL),('31364833','31364833@2019','PASCUAL HUAMANTINGO VILLANUEVA','983657411','activo','atm',NULL,NULL,NULL),('31478567','31478567@2019','LUIS ALBERTO SALAZAR ALTAMIRANO','961264315','activo','atm',NULL,NULL,NULL),('31523289','31523289@2019','MANUEL HUARANCCA CARBAJAL','983193549','activo','atm',NULL,NULL,NULL),('31528427','31528427@2019','DIONICIO QUISPE BERRIO ','979809360','activo','atm',NULL,NULL,NULL),('33333333','33333333@2019','FELIPE ELOY PANIURA','918310064','activo','atm',NULL,NULL,NULL),('40637293','40637293@2019','HUMBERTO GONZALES QUECAÑA','976827950','activo','atm',NULL,NULL,NULL),('40693421','40693421@2019','YOBER MAX ARANDO LLERENA','983940460','activo','atm',NULL,NULL,NULL),('40809113','40809113@2019','LEONIDAS SALGADO SALCEDO','920460771','activo','atm',NULL,NULL,NULL),('40811328','40811328@2019','NAZARIO LEO HUAMANI','983702432','activo','atm',NULL,NULL,NULL),('41278095','41278095@2019','HUBERT GOMEZ SOTO','941005606','activo','atm',NULL,NULL,NULL),('41282353','41282353@2019','CELESTINO CHILINGANO ROJAS','996705709','activo','atm',NULL,NULL,NULL),('41727325','41727325@2019','MIGUEL ANGEL CUSI PEDRASA','990661055','activo','atm',NULL,NULL,NULL),('41794973','41794973@2019','RONALD ISAIAS CARDENAS GAYOSO','952868623','activo','atm',NULL,NULL,NULL),('41835669','41835669@2019','ORLANDO CUADROS CHUCHON','931083428','activo','atm',NULL,NULL,NULL),('41896231','41896231@2019','FREDY RICHAR PINARES QUINO','983916171','activo','fac',NULL,NULL,NULL),('42008154','42008154@2019','GILBERT CASTRO RAMOS','931869391','activo','atm',NULL,NULL,NULL),('42109575','42109575@2019','GUILLERMO TTITO QUISPE','935715137','activo','atm',NULL,NULL,NULL),('42171950','42171950@2019','ISAIAS CAMACHO BATALLANOS','993535836','activo','fac',NULL,NULL,NULL),('42196379','42196379@2019','GRIMALDO PALOMINO LLANOS','929964965','activo','atm',NULL,NULL,NULL),('42340380','42340380@2019','ALDO CHIPANA ENCISO','962384092','activo','atm',NULL,NULL,NULL),('42542306','42542306@2019','JAVIER HERRERA VEGA','952279301','activo','atm',NULL,NULL,NULL),('42585141','42585141@2019','YDA ACOSTUPA TAPIA','976738548','activo','atm',NULL,NULL,NULL),('42754407','42754407@2019','ROSEL TORRES CARBAJAL','983748110','activo','atm',NULL,NULL,NULL),('42787364','42787364@2019','JESUS RIOS RIVERA','948263309','activo','atm',NULL,NULL,NULL),('42859809','42859809@2019','ROMULO CARRASCO CCORAHUA','946629958','activo','atm',NULL,NULL,NULL),('43171120','43171120@2019','FREDY ZEVALLOS QUISPE','991390609','activo','atm',NULL,NULL,NULL),('43602536','43602536@2019','RICHARD LEGUA HUAYANA','978653729','activo','atm',NULL,NULL,NULL),('43827432','43827432@2019','JORGE VARGAS ESPINOZA','976126977','activo','atm',NULL,NULL,NULL),('44601247','44601247@2019','GABRIEL VARGAS QUIÑONES','984135904','activo','atm',NULL,NULL,NULL),('45101869','45101869@2019','PERCY TORRES RODRIGUEZ','986854553','activo','fac',NULL,NULL,NULL),('45103708','45103708@2019','GILMER HUILLCA ROMAN','974593165','activo','fac',NULL,NULL,NULL),('45173050','45173050@2019','IVAN ROJAS SERRANO ','931323244','activo','fac',NULL,NULL,NULL),('45185618','45185618@2019','ROGELIO MALLCCO TORRES','958160637','activo','atm',NULL,NULL,NULL),('45223487','45223487@2019','EDISON RAMOS CARTOLIN','957355846','activo','fac',NULL,NULL,NULL),('45442692','45442692@2019','HUMBERTO MONZON OJEDA','953422165','activo','atm',NULL,NULL,NULL),('45656776','45656776@2019','EDWIN CLIMACO GROVAS LIMA','938268779','activo','atm',NULL,NULL,NULL),('45740205','45740205@2019','ADRIEL CAMERO CONDORI','986593095','activo','atm',NULL,NULL,NULL),('45807253','45807253@2019','MARIA RIOS ZAMORA','950360227','activo','atm',NULL,NULL,NULL),('45944484','45944484@2019','ZANDY PUMACAYO FERREL','989521712','activo','atm',NULL,NULL,NULL),('46061573','46061573@2019','ERMELINDA MENDOZA SERRANO','917651677','activo','atm',NULL,NULL,NULL),('46549374','46549374@2019','RENE LEO PORTILLA','983116440','activo','atm',NULL,NULL,NULL),('46667185','46667185@2019','ALCIDES YUPANQUI ALWAY','946740589','activo','atm',NULL,NULL,NULL),('46685164','46685164@2019','ROSMERY HERRERA CHAHUA','930547973','activo','atm',NULL,NULL,NULL),('46705572','46705572@2019','VERONICA CCOSCCO SOTO','961973712','activo','atm',NULL,NULL,NULL),('46718300','46718300@2019','MIGUEL GONZALES GARCIA','917645182','activo','atm',NULL,NULL,NULL),('47201070','47201070@2019','HECTOR ARANIBAR ABARCA','973763628','activo','atm',NULL,NULL,NULL),('47246155','47246155@2019','MARGOT QUISPE PAUCAR','999912335','activo','atm',NULL,NULL,NULL),('47559654','47559654@2019','FRANKLIN TRUJILLO CONDORI','979530279','activo','atm',NULL,NULL,NULL),('47561652','47561652@2019','EFRAIN CALLER DAVALOS','996188970','activo','fac',NULL,NULL,NULL),('47782505','47782505@2019','DARWIN QUISPE PAHUARA','974542037','activo','atm',NULL,NULL,NULL),('48022301','48022301@2019','ELIAS SALCEDO CALLUCHI','973252722','activo','atm',NULL,NULL,NULL),('48195517','48195517@2019','FRANK ANCCO CCASA','973503257','activo','atm',NULL,NULL,NULL),('48388253','48388253@2019','SIMION HUANACO SOTO','976829717','activo','atm',NULL,NULL,NULL),('48504573','48504573@2019','JESUS ANGEL CAYLLAHUA PAUCAR','926565590','activo','atm',NULL,NULL,NULL),('70081908','70081908@2019','REYDER ARIAS RICRA','942137216','activo','atm',NULL,NULL,NULL),('70160355','70160355@2019','JEFERSON VASQUEZ PALOMINO','993978587','activo','atm',NULL,NULL,NULL),('70337787','70337787@2019','RICHARD QUISPE HUAMAN ','974385591','activo','atm',NULL,NULL,NULL),('70364878','70364878@2019','HENRY ALEX GAMONAL YAÑE','994830444','activo','atm',NULL,NULL,NULL),('70422402','70422402@2019','WANDERLI PANIURA ATAO','985524083','activo','atm',NULL,NULL,NULL),('70497895','70497895@2019','ALAN DEIVER TELLO RAMOS','994752837','activo','atm',NULL,NULL,NULL),('70763433','70763433@2019','ENRIQUE VERA VARGAS ','950967977','activo','atm',NULL,NULL,NULL),('70783808','70783808@2019','LILIANA JUVITA ENCISO CARBAJAL','926457731','activo','atm',NULL,NULL,NULL),('71199031','71199031@2019','GERTRUDES SUNQUILPO LEON','930438318','activo','atm',NULL,NULL,NULL),('71657086','71657086@2019','ROSMERY CASTILLO CHAVEZ','983668571','activo','atm',NULL,NULL,NULL),('71688616','71688616@2019','YOSELIN GARFIAS VILA','932569920','activo','atm',NULL,NULL,NULL),('72026102','72026102@2019','URIETA CORDOVA NIETO','952738670','activo','atm',NULL,NULL,NULL),('72093602','72093602@2019','SAMUEL TAIPE CRUZ','949953808','activo','atm',NULL,NULL,NULL),('72157714','72157714@2019','CIRILO MENDOZA SOTO','974350393','activo','atm',NULL,NULL,NULL),('73576699','73576699@2019','LUIS ALBERTO GARIBAY CCASANI','962242723','activo','atm',NULL,NULL,NULL),('74317617','74317617@2019','OBDULIA ROMAN GARRAFA','925007507','activo','atm',NULL,NULL,NULL),('75394839','75394839','saul ortiz','','activo','adm',NULL,NULL,NULL),('80124093','80124093@2019','PLACIDO SALCEDO MAMANI','913157739','activo','atm',NULL,NULL,NULL),('80597403','80597403@2019','PLACIDO AYALA CARDENAS','918850766','activo','atm',NULL,NULL,NULL);
/*!40000 ALTER TABLE `t_usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-29 17:11:00
