@extends('layouts.templateatm')
@section('section')
    <section class='content-header'>
        <h1> FORTALECIMIENTO DE <small>Areas Tecnicas Municipales</small></h1>
        <ol class='breadcrumb'>
            <li><a href='#'><i class='fa fa-dashboard'></i>Editar </a></li>
            <li class='active'>Formación</li>
        </ol>
    </section>
    <section class='content'>
        <div class='row'>
            <div class='col-xs-12'>
                <div class='box'>
                    <div class='box-body'>
                        <form id="frmFormacion" name="frmFormacion" action="{{url('formacion/editar')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="box box-info">
                                        <div class="box-body">
                                            <input type="hidden" name="idFormacion" value="{{$listaEditar->idFormacion}}">
                                            <div class="form-group">
                                                <label  > Responsable de ATM cuenta con el perfil y esta capacitado
                                                </label>
                                                    <div style="display: {{$listaEditar->ver1!=null ? 'none':''}}">
                                                        <label for="" style="color:#00a65a;">   <span class="label label-success">Subir verificable:</span>  <br> <i class="fa fa-file-archive-o"> Documento escaneado, formato PDF</i> </label>
                                                    <input id="cambiar" type="file"  class="form-control" id="ver1" name="ver1" >

                                                    </div>
                                                  <br>  @if ($listaEditar->ver1!=null)
                                                  <a  href="{{asset($listaEditar->ver1)}}" target="_blank"><i class="fa fa-eye"></i> Ver Verificable</a> |
                                                   <label for="cambiar" style="color:red"><i class="fa fa-pencil"> </i> Cambiar Documento</label>

                                                    @else

                                                    @endif
                                            </div>
                                            <div class="form-group" >
                                                <label > POI ATM(Area Tecnica Municipal) aprobado con resolucion e incorporado en el PIA 2020.</label>
                                                    <input  type="text" value="{{$listaEditar->poiAtm}}"  class="form-control" id="poiAtm"  class="minimal" name="poiAtm" >
                                                    <div style="display: {{$listaEditar->ver2!=null ? 'none':''}}">
                                                        <label for="" style="color:#00a65a;">   <span class="label label-success">Subir verificable:</span>  <br> <i class="fa fa-file-archive-o"> Documento escaneado, formato PDF</i> </label>
                                                    <input id="cambiar" type="file"  class="form-control" id="ver2" name="ver2" >
                                                    </div>
                                                  <br>  @if ($listaEditar->ver2!=null)
                                                  <a  href="{{asset($listaEditar->ver2)}}" target="_blank"><i class="fa fa-eye"></i> Ver Verificable</a> |
                                                   <label for="cambiar" style="color:red"><i class="fa fa-pencil"> </i> Cambiar Documento</label>
                                                    @else
                                                    @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="box box-info">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label for="" > Articulación multisectorial en cogestión  en saneamiento
                                                </label>
                                                <input  type="text" value="{{$listaEditar->articulacionSaneam}}"  class="form-control" id="articulacionSaneam"  class="minimal" name="articulacionSaneam" >
                                                <div style="display: {{$listaEditar->ver3!=null ? 'none':''}}">
                                                    <label for="" style="color:#00a65a;">   <span class="label label-success">Subir verificable:</span>  <br> <i class="fa fa-file-archive-o"> Documento escaneado, formato PDF</i> </label>
                                                <input id="cambiar" type="file"  class="form-control" id="ver3" name="ver3" >
                                                </div>
                                              <br>  @if ($listaEditar->ver3!=null)
                                              <a  href="{{asset($listaEditar->ver3)}}" target="_blank"><i class="fa fa-eye"></i> Ver Verificable</a> |
                                               <label for="cambiar" style="color:red"><i class="fa fa-pencil"> </i> Cambiar Documento</label>
                                                @else
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <label for="">  Ordenanza municipal que aprueba el Protocolo de abastecimiento de cloro.</label>
                                                    <br>
                                                    <input  type="text" value="{{$listaEditar->ordMunCloro}}"  class="form-control" id="ordMunCloro"  class="minimal" name="ordMunCloro" >
                                                    <div style="display: {{$listaEditar->ver4!=null ? 'none':''}}">
                                                        <label for="" style="color:#00a65a;">   <span class="label label-success">Subir verificable:</span>  <br> <i class="fa fa-file-archive-o"> Documento escaneado, formato PDF</i> </label>
                                                    <input id="cambiar" type="file"  class="form-control" id="ver4" name="ver4" >
                                                    </div>
                                                  <br>  @if ($listaEditar->ver4!=null)
                                                  <a  href="{{asset($listaEditar->ver4)}}" target="_blank"><i class="fa fa-eye"></i> Ver Verificable</a> |
                                                   <label for="cambiar" style="color:red"><i class="fa fa-pencil"> </i> Cambiar Documento</label>
                                                    @else
                                                    @endif
                                            </div>
                                        </div>
                                        <div class="box-footer">
                                            <input type="submit" value="Guardar toda la información" class="btn btn-primary btn-sm ">
                                            <a href="{{url('formacion/lista')}}" class="btn btn-danger btn-sm" ><i class="fa fa-arrow-left"></i>  Regresar</a>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script>
$(document).ready(function() {
    $('#frmFormacion').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields:{
            especialidad:{
                validators: {
                    notEmpty: {
                        message: 'La especialidad es requerido'
                    }
                }
            },
            ver1:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
             ver2:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver3:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver4:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver5:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver6:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver7:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver8:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver9:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver10:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver11:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },

        }
    });
});
</script>
@endsection
