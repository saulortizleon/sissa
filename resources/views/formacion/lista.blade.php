@extends('layouts.templateatm')
@section('section')
<section class='content-header'>
    <h1> FORTALECIMIENTO DE <small>Areas Tecnicas Municipales</small></h1>
    <ol class='breadcrumb'>
        <li><a href='#'><i class='fa fa-dashboard'></i>formación</a></li>
        <li class='active'>Lista</li>
    </ol>
</section>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-body table-responsive'>
                    <table id='' class='table table-bordered table-hover'>
                        <thead>
                            <tr>
                                <th style="font-size: 12px;"> Responsable de ATM cuenta con el perfil y esta capacitado
                                </th>
                                <th style="font-size: 12px;"> POI ATM(Area Tecnica Municipal) aprobado con resolucion e incorporado en el PIA 2020.
                                </th>
                                <th style="font-size: 12px;"> Articulacion multisectorial en cogestion  en saneamiento
                                </th>
                                <th style="font-size: 12px;"> Ordenanza municipal que aprueba el Protocolo de abastecimiento de cloro.
                                </th>
                                <th style="font-size: 12px;">Estado de avance </th>
                                <th style="font-size: 12px;"><center>Acciones</center> </th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($listaFormacion as $item)
                            <tr>
                                <td>{{$item->cuentaPerfilCapacitado}}</td>
                                <td>{{$item->poiAtm}}</td>
                                <td>{{$item->articulacionSaneam}}</td>
                                <td>{{$item->ordMunCloro}}</td>
                                <td>
                                    <span class="label label-success" style="font-size:14px">Aprobado</span>
                                </td>
                                <td><button class="btn btn-primary btn-sm"  onclick="actualizar('{{$item->idFormacion}}')"> <i class="fa fa-plus"></i> AGREGAR INFORMACIÓN</button>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="modalFormacion">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <form id="frmFormacion" name="frmFormacion" action="{{url('formacion/agregar')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-md-4">
                              <h4 for="">  <b>Registrar formación ATM</b> </h4>
                            <div class="form-group" style="background-color:#d2cdcd;">
                                 ◘ Documento ROF :
                                    <label>
                                    <input type="radio"  class="minimal" name="ROF" id="ROF" value="SI" checked>
                                    SI
                                    </label>
                                    <label>
                                    <input type="radio"  class="minimal" name="ROF" id="ROF" value="NO">
                                    NO
                                    </label>
                                    <br>
                                    <label for=""> ◘ Subir Verificable : </label>
                                    <input  type="file"  class="form-control" id="ver1"  class="minimal" name="ver1" >
                            </div>
                            <div class="form-group" style="background-color:#d2cdcd;" >
                                    ◘ Documento MOF :
                                    <label>
                                    <input type="radio"  class="minimal" name="MOF" id="MOF" value="SI" checked>
                                    SI
                                    </label>
                                    <label>
                                    <input type="radio"  class="minimal" name="MOF" id="MOF" value="NO">
                                    NO
                                    </label>
                                    <br>
                                    <label for="">◘  Subir Verificable : </label>
                                    <input  type="file"  class="form-control" id="ver2"  class="minimal" name="ver2" >
                            </div>
                           <div class="form-group" style="background-color:#d2cdcd;">
                                ◘ Documento CAP :
                                    <label>
                                    <input type="radio"  class="minimal" name="CAP" id="CAP" value="SI" checked>
                                    SI
                                    </label>
                                    <label>
                                    <input type="radio"  class="minimal" name="CAP" id="CAP" value="NO">
                                    NO
                                    </label>
                                    <br>
                                    <label for="">◘  Subir Verificable : </label>
                                    <input  type="file"  class="form-control" id="ver3"  class="minimal" name="ver3" >
                            </div>
                            <div class="form-group" style="background-color:#d2cdcd;">
                                    ◘ Documento TUPA :
                                    <label>
                                    <input type="radio"  class="minimal" name="TUPA" id="TUPA" value="SI" checked>
                                    SI
                                    </label>
                                    <label>
                                    <input type="radio"  class="minimal" name="TUPA" id="TUPA" value="NO">
                                    NO
                                    </label>
                                    <br>
                                    <label for="">◘  Subir Verificable : </label>
                                    <input  type="file"  class="form-control" id="ver4" name="ver4" >
                            </div>
                            <div class="form-group" style="background-color:#d2cdcd;">
                                    ◘ Grado Académico :
                                <select class="form-control" name="gradoAcadem" id="gradoAcadem">
                                    <option value="bachiller">Bachiller</option>
                                    <option value="titulado">Titulado</option>
                                    <option value="tecnico">Técnico</option>
                                </select>
                                <label for="">◘  Subir Verificable : </label>
                                <input  type="file"  class="form-control" id="ver5" name="ver5" >
                            </div>
                            <div class="form-group" style="background-color:#d2cdcd;">
                                    <label for="description">◘ Especialidad</label>
                                    <input type="text"  class="form-control" id="especialidad" name="especialidad" placeholder="...">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group" style="background-color:#d2cdcd;">
                                    ◘ Capacitación Gob. Nacional:
                                    <label>
                                    <input type="radio"  class="minimal" name="GN" id="GN" value="SI" checked>
                                    SI
                                    </label>
                                    <label>
                                    <input type="radio"  class="minimal" name="GN" id="GN" value="NO" >
                                    NO
                                    </label>
                                    <label for="">◘ Subir Verificable</label>
                                    <input  type="file"  class="form-control" id="ver6"  name="ver6" >
                            </div>
                            <div class="form-group" style="background-color:#d2cdcd;">
                                    ◘ Capacitación Gobierno Regional:
                                <label>
                                <input type="radio"  class="minimal" name="GR" id="GR" value="SI" checked>
                                SI
                                </label>
                                <label>
                                <input type="radio"  class="minimal" name="GR" id="GR" value="NO" >
                                NO
                                </label>
                                <label for=""> ◘ Subir Verificable</label>
                                <input  type="file"  class="form-control" id="ver7"  class="minimal" name="ver7" >
                            </div>
                            <div class="form-group" style="background-color:#d2cdcd;">
                                    ◘ Capacitación Gobierno local:
                                <label>
                                <input type="radio"  class="minimal" name="GL" id="GL" value="SI" checked>
                                SI
                                </label>
                                <label>
                                <input type="radio"  class="minimal" name="GL" id="GL" value="NO" >
                                NO
                                </label>
                                <label for="">◘ Subir Verificable</label>
                                <input  type="file"  class="form-control" id="ver8"  class="minimal" name="ver8" >
                            </div>
                            <div class="form-group" style="background-color:#d2cdcd;">
                                    ◘ Capacitación por alguna ONG:
                                <label>
                                <input type="radio"  class="minimal" name="ONG" id="ONG" value="SI" checked>
                                SI
                                </label>
                                <label>
                                <input type="radio"  class="minimal" name="ONG" id="ONG" value="NO" >
                                NO
                                </label>
                                <label for="">◘ Subir Verificable</label>
                                <input  type="file"  class="form-control" id="ver9"  class="minimal" name="ver9" >
                            </div>
                            <div class="form-group" style="background-color:#d2cdcd;">
                                    ◘ Capacitación por  EDUSA:
                                <label>
                                <input type="radio"  class="minimal" name="EDUSA" id="EDUSA" value="SI" checked>
                                SI
                                </label>
                                <label>
                                <input type="radio"  class="minimal" name="EDUSA" id="EDUSA" value="NO" >
                                NO
                                </label>
                                <label for="">◘ Subir Verificable</label>
                                <input  type="file"  class="form-control" id="ver9"  class="minimal" name="ver9" >
                            </div>
                            <div class="form-group" style="background-color:#d2cdcd;">
                                    ◘ Capacitación por  AOM:
                                <label>
                                <input type="radio"  class="minimal" name="AOM" id="AOM" value="SI" checked>
                                SI
                                </label>
                                <label>
                                <input type="radio"  class="minimal" name="AOM" id="AOM" value="NO" >
                                NO
                                </label>
                                <label for="">◘ Subir Verificable</label>
                                <input  type="file"  class="form-control" id="ver10"  class="minimal" name="ver10" >
                            </div>
                        </div>
                        <div class="col-md-4">

                                <div class="form-group" style="background-color:#d2cdcd;">
                                        ◘ Capacitación por  DL1280:
                                    <label>
                                    <input type="radio"  class="minimal" name="DL1280" id="DL1280" value="SI" checked>
                                    SI
                                    </label>
                                    <label>
                                    <input type="radio"  class="minimal" name="DL1280" id="DL1280" value="NO" >
                                    NO
                                    </label>
                                    <label for="">◘ Subir Verificable</label>
                                    <input  type="file"  class="form-control" id="ver10"  class="minimal" name="ver10" >
                                </div>
                                <div class="form-group" style="background-color:#d2cdcd;">
                                        ◘ Capacitación por  PNSR:
                                    <label>
                                    <input type="radio"  class="minimal" name="PNSR" id="PNSR" value="SI" checked>
                                    SI
                                    </label>
                                    <label>
                                    <input type="radio"  class="minimal" name="PNSR" id="PNSR" value="NO" >
                                    NO
                                    </label>
                                    <label for="">◘ Subir Verificable</label>
                                    <input  type="file"  class="form-control" id="ver10"  class="minimal" name="ver10" >
                                </div>
                                <div class="form-group" style="background-color:#d2cdcd;">
                                        ◘ Capacitación por  PNSR otros:
                                    <label>
                                    <input type="radio"  class="minimal" name="PNSRotros" id="PNSRotros" value="SI" checked>
                                    SI
                                    </label>
                                    <label>
                                    <input type="radio"  class="minimal" name="PNSRotros" id="PNSRotros" value="NO" >
                                    NO
                                    </label>
                                    <label for="">◘ Subir Verificable</label>
                                    <input  type="file"  class="form-control" id="ver11"  class="minimal" name="ver11" >
                                </div>
                                <div class="form-group" style="background-color:#d2cdcd;">
                                        ◘ Plan de saneamiento Nacional
                                    <label>
                                    <input type="radio"  class="minimal" name="planSan" id="planSan" value="SI">
                                    SI
                                    </label>
                                    <label>
                                    <input type="radio"  class="minimal" name="planSan" id="planSan" value="NO" >
                                    NO
                                    </label>
                                    <label for="">◘ Subir Verificable</label>
                                    <input  type="file"  class="form-control" id="ver12"  class="minimal" name="ver12" >
                                </div>
                                <div class="form-group" style="background-color:#d2cdcd;">
                                        ◘ R.M 155-2017 Criterios de Admisibilidad
                                    <label>
                                    <input type="radio"  class="minimal" name="criterios" id="criterios" value="SI"  >
                                    SI
                                    </label>
                                    <label>
                                    <input type="radio"  class="minimal" name="criterios" id="criterios" value="NO">
                                    NO
                                    </label>
                                    <label for="">◘ Subir Verificable</label>
                                    <input  type="file"  class="form-control" id="ver13"  class="minimal" name="ver13" >
                                </div>
                                <div class="form-group">
                                    <label for=""><i>Asegúrese de llenar correctamente los items antes de guardar los cambios!</i> </label>
                                        <input type="submit" value="Guardar" class="btn btn-primary btn-sm">
                                        <button type="button"   class="btn btn-default pull-right" data-dismiss="modal">Cerrar</button>
                                    </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
 <script>
     function actualizar(idFormacion)
    {
    	window.location.href='{{url('formacion/editar')}}/'+idFormacion;
    }
$(document).ready(function() {
    $('#frmFormacion').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields:{
            especialidad:{
                validators: {
                    notEmpty: {
                        message: 'La especialidad es requerido'
                    }
                }
            },
            criterios:{
                validators: {
                    notEmpty: {
                        message: 'El campo es requerido'
                    }
                }
            },
            planSan:{
                validators: {
                    notEmpty: {
                        message: 'El campo es requerido'
                    }
                }
            },
            ver1:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
             ver2:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver3:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver4:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver5:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver6:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver7:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver8:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver9:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver10:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver11:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver12:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver13:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },

        }
    });
});
</script>
@endsection