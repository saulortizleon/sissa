@extends('layouts.templateatm')
@section('section')
    <section class='content-header'>
        <h1>Consolidado de Juntas Administradoras de Servicios de Saneamiento<small>en el distrito</small></h1>
        <ol class='breadcrumb'>
            <li><a href='#'><i class='fa fa-dashboard'></i>Editar </a></li>
            <li class='active'>ATM</li>
        </ol>
    </section>
    <section class='content'>
        <div class='row'>
            <div class='col-xs-12'>
                <form id="frmAtm" name="frmAtm" action="{{url('cjass/editar')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                <div class="row">
                    <div class="col-md-4">
                        <div class="box box-primary">
                            <div class="box-body">
                                <input type="hidden" name="idjass" value="{{$listaEditar->idjass}}">
                                <div class="form-group">
                                    <label>Número total de JASS(Junta Administradora de Servicios de Saneamiento) en el Distrito
                                    </label><br>
                                    <input type="text" value="{{$listaEditar->totalJass}}" class="form-control" id="totalJass" name="totalJass" >
                                    <div style="display: {{$listaEditar->ver1!=null ? 'none':''}}">
                                        <label for="" style="color:#00a65a;">   <span class="label label-success">Subir verificable:</span>  <br> <i class="fa fa-file-archive-o"> Documento escaneado, formato PDF</i> </label>
                                    <input id="cambiar" type="file"  class="form-control" id="ver1" name="ver1" >
                                    </div>
                                    <br>  @if ($listaEditar->ver1!=null)
                                    <a  href="{{asset($listaEditar->ver1)}}" target="_blank"><i class="fa fa-eye"></i> Ver Verificable</a> |
                                    <label for="cambiar" style="color:red"><i class="fa fa-pencil"> </i> Cambiar Documento</label>
                                        @else
                                        @endif
                                    <hr>
                                </div>
                                <div class="form-group" >
                                    <label>Número de JASS(Junta Administradora de Servicios de Saneamiento) Registrados y Reconocidos
                                    </label> <br>
                                    <input type="text" disabled="disabled" value="{{$listaEditar->totalJassReg}}" class="form-control" id="totalJassReg" name="totalJassReg" >
                                       {{-- facilitador --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="form-group" >
                                    <label>N° de CC.PP. (centro poblado)con Padron Nominal de usuarios.
                                    </label><br>
                                    {{-- facilitador --}}
                                      <input type="text" disabled="disabled" value="{{$listaEditar->cpPadronNominal}}" class="form-control" id="cpPadronNominal" name="cpPadronNominal" >
                                </div>
                                <hr>
                                <div class="form-group" >
                                    <label>N° de CC.PP.(centro poblado) Modelo con servicios basicos(Saludable).
                                    </label><br>
                                    <input type="text" disabled="disabled" value="{{$listaEditar->cpPadronNominal}}" class="form-control" id="numCpModelos" name="numCpModelos" >
                                    {{-- facilitador --}}
                                </div>
                                <div class="box-footer">
                                    <input type="submit" value="GUARDAR TODO" class="btn btn-primary btn-sm ">
                                        <a href="{{url('cjass/lista')}}" class="btn btn-danger btn-sm" ><i class="fa fa-arrow-left"></i>  Regresar</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                </form>
            </div>
    </section>
<script>
$(document).ready(function() {

    $("#cargos1").on( "click", function() {
			$('#numCargos1').show(); //muestro mediante id
            $('#numCargos2').show(); //muestro mediante id
            $('#numCargos3').show(); //muestro mediante id
            $('#numCargos4').show(); //muestro mediante id
			$('#ver3').show(); //muestro mediante clase
		 });
    $("#cargos").on( "click", function() {
        $('#numCargos1').hide(); //oculto mediante id
        $('#numCargos2').hide(); //oculto mediante id
        $('#numCargos3').hide(); //oculto mediante id
        $('#numCargos4').hide(); //oculto mediante id
        $('#ver3').hide(); //muestro mediante clase
    });

     $("#miembrosAdicionales1").on( "click", function() {
			$('#numMiembrosAdicionales').show(); //muestro mediante id

		 });
    $("#miembrosAdicionales2").on( "click", function() {
        $('#numMiembrosAdicionales').hide(); //oculto mediante id

    });

    $("#planSupervision1").on( "click", function() {
			$('#divplanSupervision').show(); //muestro mediante id

		 });
    $("#planSupervision2").on( "click", function() {
        $('#divplanSupervision').hide(); //oculto mediante id

    });
    $(function(){
        $('#ocultar').change(function(){
        if(!$(this).prop('checked')){
            $('#otras').hide();
        }else{
            $('#otras').show();
        }
  })
})
    $('#frmAtm').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields:{
            especialidad:{
                validators: {
                    notEmpty: {
                        message: 'La especialidad es requerido'
                    }
                }
            },
            ver1:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
             ver2:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver3:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver4:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver5:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver6:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver7:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver8:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver9:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver10:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver11:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },

        }
    });
});
</script>
@endsection
