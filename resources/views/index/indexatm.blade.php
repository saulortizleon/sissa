@extends('layouts.templateatm')
@section('section')
<section class="content-header">
    <h1>
    <b>PAGINA PRINCIPAL </b>
        <small>ATM</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Bienvenida</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>FINALISTAS </h3>
              <h4> Premio a las buena Gestión del Agua y Saneamiento en la región de Apurímac</h4>
            </div>
            <div class="icon">
              <i class="ion ion-trophy"></i>
            </div>
            <a href="{{url('finalistas/lista')}}" class="small-box-footer"   ><strong> STAND VIRTUAL</strong>  <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>Comunicados</h3>
              <H4>oficiales de la Dirección Regional de Vivienda, Construcción y Saneamiento de Apurímac</H4>
            </div>
            <div class="icon">
              <i class="ion ion-alert"></i>
            </div>
            <a href="{{url('comunicados')}}" class="small-box-footer"  target="_blank"><strong> Más información </strong> <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>Noticias</h3>
              <H4>oficiales de la  Dirección Regional de Vivienda, Construcción y Saneamiento de Apurímac </H4>
            </div>
            <div class="icon">
              <i class="ion ion-pin"></i>
            </div>
            <a href="{{url('noticias')}}" class="small-box-footer"  target="_blank"><strong>Más información </strong><i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
    </div>
    <div class="row">
      <div class="col-md-3 ">
        <div class="box box-default">
          <div class="box-header with-border">
            <center>
            <h1 class="box-title text-primary"> <strong> GOBIERNO REGIONAL DE APURÍMAC <br> GESTIÓN 2019-2022 </strong></h1>
            </center>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="callout ">
<center> <img   src="{{asset('/img/allinkausanapaq.png')}}" style="width: auto !important; height: 150px !important;">
</center>

            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <div class="col-md-3 ">
        <div class="box box-default">
          <div class="box-header with-border">
            <center>
            <h1 class="box-title text-primary"><strong> DIRECCIÓN REGIONAL DE VIVIENDA, CONSTRUCCIÓN Y SANEAMIENTO </strong></h1>
            </center>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="callout ">

              <center>    <img    src="{{asset('/img/vivienda.jpg')}}" style=" width: auto !important; height: 150px !important;   ">
              </center>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <div class="col-md-3 ">
        <div class="box box-default">
          <div class="box-header with-border">
            <center>   <h1 class="box-title text-primary"> <strong>  SISTEMA INTEGRADO DE SANEAMIENTO APURÍMAC - SISA </strong> </h1>   </center>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="callout ">
              <p style="font-size: 17px">Es un REPOSITORIO de INFORMACIÓN que esta dirigido a autoridades,
                 funcionarios, gestores y servidores públicos,  orientados hacia la  adecuada Gestión de los servicios de agua y saneamiento en la Region de Apurímac</p>

            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <div class="col-md-3 ">
        <div class="box box-default">

          <!-- /.box-header -->
          <div class="box-body">


              <ul class="list-group">
                <li class="list-group-item active">SOBRE SISA</li>
                <li class="list-group-item">MANUAL</li>
                <li class="list-group-item">VIDEO TUTORIALES</li>
                <li class="list-group-item">PREGUNTAS FRECUENTES</li>
                <li class="list-group-item">TUTORIAL</li>
                <li class="list-group-item">TUTORIAL</li>
              </ul>

          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div>
    </div>

</section>

@endsection