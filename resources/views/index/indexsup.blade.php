@extends('layouts.templatesup')
@section('section')
<section class="content-header">
    <h1>
    <b>PAGINA PRINCIPAL </b>
        <small>DRVCS APURÍMAC</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">bienvenida</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>Premio </h3>
              </div>
            <div class="icon">
              <i class="ion ion-trophy"></i>
            </div>
            <a href="{{url('concurso/lista')}}" class="small-box-footer"><strong> REGISTRAR ATM</strong>  <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>Avances </h3>
              </div>
            <div class="icon">
              <i class="ion ion-waterdrop"></i>
            </div>
            <a href="#" class="small-box-footer"> <strong>Más información </strong><i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>Avisos</h3>
            </div>
            <div class="icon">
              <i class="ion ion-alert"></i>
            </div>
            <a href="#" class="small-box-footer"><strong> Más información </strong> <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>Noticias</h3>
            </div>
            <div class="icon">
              <i class="ion ion-pin"></i>
            </div>
            <a href="https://www.gob.pe/coronavirus" class="small-box-footer"><strong>Más información </strong><i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
    </div>
    <div class="row">
      <div class="col-md-3 ">
        <div class="box box-default">
          <div class="box-header with-border">
            <center>
            <h1 class="box-title text-primary"> <strong> GOBIERNO REGIONAL DE APURÍMAC <br> GESTIÓN 2019-2022 </strong></h1>
            </center>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="callout ">
<center> <img   src="{{asset('/img/a.png')}}" style="width: auto !important; height: 150px !important;">
</center>

            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <div class="col-md-3 ">
        <div class="box box-default">
          <div class="box-header with-border">
            <center>
            <h1 class="box-title text-primary"><strong> DIRECCIÓN REGIONAL DE VIVIENDA, CONSTRUCCIÓN Y SANEAMIENTO </strong></h1>
            </center>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="callout ">

              <center>    <img    src="{{asset('/img/vivienda.jpg')}}" style=" width: auto !important; height: 150px !important;   ">
              </center>
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <div class="col-md-3 ">
        <div class="box box-default">
          <div class="box-header with-border">
            <center>   <h1 class="box-title text-primary"> <strong>SISA - Sistema Informático Administrativo para la DRVCS</strong> </h1>   </center>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="callout ">              
              <p style="font-size: 17px">
                Es un SISTEMA de INFORMACIÓN con el fin de brindar un adecuado
                seguimiento y control del trámite documentario, y la gestión de archivos
                en la institución.
              </p>              
            </div>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <div class="col-md-3 ">
        <div class="box box-default">

          <!-- /.box-header -->
          <div class="box-body">

              <ul class="list-group">
                <li class="list-group-item">MANUALES</li>
                <li class="list-group-item">VIDEOS</li>
                <li class="list-group-item">PREGUNTAS FRECUENTES</li>
                <li class="list-group-item">TUTORIALES</li>
              </ul>

          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div>
    </div>

</section>

@endsection