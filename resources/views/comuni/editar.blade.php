@extends('layouts.templateatm')
@section('section')
    <section class='content-header'>
        <h1> Difusion y comunicacion<small>en el distrito</small></h1>
        <ol class='breadcrumb'>
            <li><a href='#'><i class='fa fa-dashboard'></i>Editar </a></li>
            <li class='active'>SAP</li>
        </ol>
    </section>
    <section class='content'>
        <div class='row'>
            <div class='col-xs-12'>
                <form id="frmAtm" name="frmAtm" action="{{url('comuni/editar')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                <div class="row">
                    <div class="col-md-4">
                        <div class="box box-primary">
                            <div class="box-body">
                                <input type="hidden" name="idComu" value="{{$listaEditar->idComu}}">
                                <div class="form-group">
                                    <label>Difusión en Pago de cuota familiar :  <br>
                                            <input type="radio"  class="minimal" name="capCuotaF" id="capCuotaF" value="SI" {{$listaEditar->capCuotaF=="SI" ? "checked" : ""}} >
                                            SI

                                            <input type="radio"  class="minimal" name="capCuotaF" id="capCuotaF" value="NO" {{$listaEditar->capCuotaF=="NO" ? "checked" : ""}} >
                                            NO
                                        </label>
                                </div>
                                <div class="form-group">
                                    <label>Cuidado y uso racional del AP. : <br>

                                            <input type="radio"  class="minimal" name="capCuidadoAp" id="capCuidadoAp" value="SI" {{$listaEditar->capCuidadoAp=="SI" ? "checked" : ""}} >
                                            SI

                                            <input type="radio"  class="minimal" name="capCuidadoAp" id="capCuidadoAp" value="NO" {{$listaEditar->capCuidadoAp=="NO" ? "checked" : ""}} >
                                            NO

                                    </label>
                                </div>
                                <div class="form-group">
                                    <label>Correcta manipulación del AP :<br>
                                            <input type="radio"  class="minimal" name="capCorrectoUsoAp" id="capCorrectoUsoAp" value="SI" {{$listaEditar->capCorrectoUsoAp=="SI" ? "checked" : ""}} >
                                            SI

                                            <input type="radio"  class="minimal" name="capCorrectoUsoAp" id="capCorrectoUsoAp" value="NO" {{$listaEditar->capCorrectoUsoAp=="NO" ? "checked" : ""}} >
                                            NO
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>	Momentos y técnica correcta de lavado de manos.<br>
                                            <input type="radio"  class="minimal" name="capLavadoManos" id="capLavadoManos" value="SI" {{$listaEditar->capLavadoManos=="SI" ? "checked" : ""}} >
                                            SI

                                            <input type="radio"  class="minimal" name="capLavadoManos" id="capLavadoManos" value="NO" {{$listaEditar->capLavadoManos=="NO" ? "checked" : ""}} >
                                            NO
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label>	Uso, limpieza y mantenimiento de la UBS.<br>

                                            <input type="radio"  class="minimal" name="capUsoMantUBS" id="capUsoMantUBS" value="SI" {{$listaEditar->capUsoMantUBS=="SI" ? "checked" : ""}} >
                                            SI

                                            <input type="radio"  class="minimal" name="capUsoMantUBS" id="capUsoMantUBS" value="NO" {{$listaEditar->capUsoMantUBS=="NO" ? "checked" : ""}} >
                                            NO
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label> Manejo de RRSS.<br>
                                            <input type="radio"  class="minimal" name="capRRSS" id="capRRSS" value="SI" {{$listaEditar->capRRSS=="SI" ? "checked" : ""}} >
                                            SI

                                            <input type="radio"  class="minimal" name="capRRSS" id="capRRSS" value="NO" {{$listaEditar->capRRSS=="NO" ? "checked" : ""}} >
                                            NO
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-primary">
                            <div class="box-body">

                                <div class="form-group" >
                                    <div style="display: {{$listaEditar->verificable!=null ? 'none':''}}">
                                        <label for="" style="color:#00a65a;">   <span class="label label-success">Subir verificable:</span>  <br> <i class="fa fa-file-archive-o"> Documento escaneado, formato PDF</i> </label>
                                    <input id="cambiar" type="file"  class="form-control" id="verificable" name="verificable" >
                                    </div>
                                    <br>  @if ($listaEditar->verificable!=null)
                                    <a  href="{{asset($listaEditar->verificable)}}" target="_blank"><i class="fa fa-eye"></i> Ver Verificable</a> |
                                    <label for="cambiar" style="color:red"><i class="fa fa-pencil"> </i> Cambiar Documento</label>
                                        @else
                                        @endif
                             </div>

                            <div class="form-group">
                            </div>
                            <div class="form-group">
                                <label> Fecha de Constancia : </label>
                                <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" class="form-control pull-left" id="datepicker"  name="fechaReporte" value="{{$listaEditar->fechaReporte}}"  placeholder="dia/mes/año">
                                </div>
                                <!-- /.input group -->
                              </div>
                            <div class="box-footer">
                                <input type="submit" value="GUARDAR TODO" class="btn btn-primary btn-sm ">
                                    <a href="{{url('comuni/lista')}}" class="btn btn-danger btn-sm" ><i class="fa fa-arrow-left"></i>  Regresar</a>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
    </section>
<script>
$(document).ready(function() {

    $("#cargos1").on( "click", function() {
			$('#numCargos1').show(); //muestro mediante id
            $('#numCargos2').show(); //muestro mediante id
            $('#numCargos3').show(); //muestro mediante id
            $('#numCargos4').show(); //muestro mediante id
			$('#ver3').show(); //muestro mediante clase
		 });
    $("#cargos").on( "click", function() {
        $('#numCargos1').hide(); //oculto mediante id
        $('#numCargos2').hide(); //oculto mediante id
        $('#numCargos3').hide(); //oculto mediante id
        $('#numCargos4').hide(); //oculto mediante id
        $('#ver3').hide(); //muestro mediante clase
    });

     $("#miembrosAdicionales1").on( "click", function() {
			$('#numMiembrosAdicionales').show(); //muestro mediante id

		 });
    $("#miembrosAdicionales2").on( "click", function() {
        $('#numMiembrosAdicionales').hide(); //oculto mediante id

    });

    $("#planSupervision1").on( "click", function() {
			$('#divplanSupervision').show(); //muestro mediante id

		 });
    $("#planSupervision2").on( "click", function() {
        $('#divplanSupervision').hide(); //oculto mediante id

    });
    $(function(){
        $('#ocultar').change(function(){
        if(!$(this).prop('checked')){
            $('#otras').hide();
        }else{
            $('#otras').show();
        }
  })
})
$('#frmAtm').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields:{
            especialidad:{
                validators: {
                    notEmpty: {
                        message: 'La especialidad es requerido'
                    }
                }
            },
            ver1:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
             ver2:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver3:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver4:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver5:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver6:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver7:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver8:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver9:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver10:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver11:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },

        }
    });
});
</script>
@endsection
