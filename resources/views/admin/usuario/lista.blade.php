@extends('layouts.templateadm')
@section('section')
<section class='content-header'>
    <h1> Usuarios<small>Control panel</small></h1>
    <ol class='breadcrumb'>
        <li><a href='#'><i class='fa fa-dashboard'></i>Usuarios</a></li>
        <li class='active'>Lista</li>
    </ol>
</section>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class="box-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group"><label for="">Buscar por Nombre de usuario : </label>
                                <input type="text" id="buscar"  class="form-control" placeholder="escribir aquí..." ></div>
                        </div>
                            <div class="col-sm-6">
                                <a href="{{url('usuarios/insertar')}}" class="btn btn-success btn-sm" title="Nuevo Usuario" style="float: right;"><i class="fa fa-plus"></i> Agregar Nuevo Usuario</a>
                            </div>
                    </div>
                </div>
                <div class='box-body table-responsive'>
                    <table id='tablaUsuarios' class='table table-bordered table-hover'>
                        <thead>
                            <tr>
                                <th>Dni</th>
                                <th>Nombres</th>
                                <th>celular</th>
                                <th>Distrito</th>
                                <th>cargo</th>
                                <th>estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($usuario as $item)
                            <tr>
                                <td>{{$item->dniUsuario}}</td>
                                <td>{{$item->nombres}}</td>
                                <td>{{$item->celular}}</td>
                                <td>{{$item->distrito}}</td>
                                <td>{{$item->cargo =='adm' ? "Administrador":"ATM"}}</td>
                                <td>{{$item->estado}}</td>
                                <td>
                                    <a href="#" onclick="UserEdit('{{$item->dniUsuario}}');" class="btn btn-primary btn-sm"><i class="fa fa-edit"> Editar</i></a>
                                    <a href="#" onclick="UserDelete('{{$item->dniUsuario}}');" class="btn btn-danger  btn-sm"><i class="fa fa-trash-o"></i> Quitar Acceso</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function(){
        $("#buscar").on("keyup", function() {
          var value = $(this).val().toLowerCase();
          $("#tablaUsuarios tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
        });
      });
      function UserDelete(dniUsuario)
      {
          if(confirm('Está seguro de quitar el acceso?'))
          {
              window.location = "{{url('usuario/eliminar')}}/"+dniUsuario;
          }
      }
      function UserEdit(dniUsuario)
      {
          if(confirm('Está seguro de editar el usuario?'))
          {
              window.location = "{{url('usuarios/editar')}}/"+dniUsuario;
          }
      }
</script>
@endsection