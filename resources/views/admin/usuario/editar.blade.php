@extends('layouts.templateadm')
@section('section')
    <section class='content-header'>
        <h1>Gestion Usuarios</h1>
        <ol class='breadcrumb'>
            <li><a href='#'><i class='fa fa-users'></i>Gestión Usuario</a></li>
            <li class='active'>Nuevo Usuarios</li>
        </ol>
    </section>
    <section class='content'>
        <div class='row'>
            <div class='col-xs-12'>
                <div class='box'>
                    <div class='box-body'>
                        <form id="frmUsuarios" name="frmUsuarios" action="{{url('usuarios/editar')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="row">
                                    <div class="col-sm-4 col-md-4">
                                        <div class="box box-success">
                                            <div class="box-body">
                                                <input type="hidden" name="dniUsuario" value="{{$listaEditar->dniUsuario}}">
                                                <div class="form-group">
                                                    <label for="">Nombres y Apellidos</label>
                                                    <input  type="input" class="form-control" id="nombres"  class="minimal" name="nombres" value=" {{$listaEditar->nombres}}" >
                                                </div>
                                                <div class="form-group" >
                                                    <label for="">DNI usuario :</label>
                                                    <input  type="input"  class="form-control" id="dniUsuario"  class="minimal" name="dniUsuario" maxlength="8" disabled="disabled" value="{{$listaEditar->dniUsuario}}">
                                                </div>
                                                <label for="">Contraseña :</label>
                                                <div class="input-group">
                                                    <div class="input-group-addon">
                                                      <i class="fa fa-eye" id="mostrar"></i>
                                                    </div>
                                                    <input type="password" class="form-control" id="contrasena" name="contrasena" value="{{$listaEditar->contrasena}}">
                                                  </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <div class="box box-success">
                                            <div class="box-body">
                                                <div class="form-group" >
                                                    <label for="">  Estado : </label>
                                                    <br>
                                                    <label>
                                                    <input class="minimal" type="radio" name="estado" id="estado" value="activo"  {{$listaEditar->estado=="activo" ? "checked" : ""}}>
                                                    Activo
                                                    </label>
                                                    <label>
                                                    <input class="minimal" type="radio" name="estado" id="estado" value="inactivo" {{$listaEditar->estado=="inactivo" ? "checked" : ""}}>
                                                    Inactivo
                                                    </label>
                                                    </div>
                                                    <div class="form-group">
                                                       <label for=""> Cargo : </label>
                                                   <select class="form-control" name="cargo" id="cargo">
                                                       <option value="atm" {{$listaEditar->cargo=="atm" ? "selected" : ""}}>ATM</option>
                                                       <option value="adm" {{$listaEditar->cargo=="adm" ? "selected" : ""}} >Administrador</option>
                                                       <option value="fac" {{$listaEditar->cargo=="fac" ? "selected" : ""}} >Facilitador</option>
                                                   </select>
                                                </div>
                                                   <div class="form-group" >
                                                    <label for="">Fecha de Nacimiento</label>
                                                    <input  type="date" class="form-control" id="fechaNacimiento"  class="minimal" name="fechaNacimiento" value="{{$listaEditar->fechaNacimiento}}" >
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-md-4">
                                        <div class="box box-success">
                                            <div class="box-body">
                                                    <div class="form-group" >
                                                        <label for="">Distrito :</label>
                                                        <select name="distrito" id="distrito"  class="form-control " style="width: 100%;" disabled>
                                                            <option value="seleccionar">Seleccionar</option>
                                                        </select>
                                                    </div>
                                                   <div class="form-group" >
                                                        <label for="">  Sexo </label>
                                                        <br>
                                                        <label>
                                                        <input class="minimal" type="radio" name="sexo" id="sexo" value="m" checked>
                                                        Masculino
                                                        </label>
                                                        <label>
                                                        <input class="minimal" type="radio" name="sexo" id="sexo" value="f">
                                                        Femenino
                                                        </label>
                                                    </div>
                                                    <div class="form-group" >
                                                        <label for="">Celular</label>
                                                        <input  type="input" class="form-control" id="celular"  class="minimal" name="celular" maxlength="9" >
                                                    </div>
                                                    <div class="form-group" >
                                                        <label for="">Correo electronico</label>
                                                        <input  type="email" class="form-control" id="correo"  class="minimal" name="correo" >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-12">
                                        <div class="form-group">
                                            <label class=""><i>Asegúrese de llenar correctamente los campos antes de guardar los cambios... </i> </label>
                                            <label class=""><b>No olvide subir seguir las indicaciones que están en  color rojo </b> </label>
                                            <input type="submit" value="Guardar" class="btn btn-primary btn-sm " class="form-control">
                                            <a href="{{url('admin/usuarios')}}" class="btn btn-danger btn-sm " class="form-control"><i class="fa fa-arrow-left"></i> Regresar</a>
                                        </div>
                                    </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
    $(document).ready(function() {
        $('#mostrar').click(function(){
        if($(this).hasClass('fa-eye'))
        {
        $('#contrasena').removeAttr('type');
        $('#mostrar').addClass('fa-eye-slash').removeClass('fa-eye');
        }

        else
        {
        //Establecemos el atributo y valor
        $('#contrasena').attr('type','password');
        $('#mostrar').addClass('fa-eye').removeClass('fa-eye-slash');
        }
         });


            $('#frmUsuarios').formValidation({
            framework: 'bootstrap',
            icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
            },
            fields:{
            nombres:{
            validators: {
                notEmpty: {
                    message: 'El nombre  es requerido'
                },
                regexp:{
                    regexp: /[A-Za-z]/,
                    message: 'Debe ingresar solo letras'
                },
            }
            },
            dniUsuario:{
            validators: {
                notEmpty: {
                    message: 'El DNI  es requerido'
                },
                regexp:{
                    regexp: /[0-9]/,
                    message: 'Debe ingresar solo numeros'
                },
                stringLength:{
                    min: 8,
                    max:8,
                    message: 'Debe ingresar 8 digitos'
                }
            }
            },
            contrasena:{
            validators: {
                notEmpty: {
                    message: 'La contraseña es requerido'
                },
            }
            },
            celular:{
            validators: {
                notEmpty: {
                    message: 'El celular es requerido'
                },
                stringLength:{
                        min:9,
                        max:9,
                        message: 'Asegúrese de ingresar 9 dígitos'
                    },
                    regexp:{
                        regexp: /[0-9]/,
                        message: 'Debe ingresar solo numeros'
                    },

            }
            },
            correo:{
                validators: {
                    notEmpty: {
                        message: 'El correo es requerido'
                    },
                    regexp: {
                        regexp: /^(?:[^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*|"[^\n"]+")@(?:[^<>()[\].,;:\s@"]+\.)+[^<>()[\]\.,;:\s@"]{2,63}$/,
                        message: 'Ingrese un correo válido'
                    }
                }
                },
            sexo:{
                validators: {
                    notEmpty: {
                        message: 'El sexo es requerido'
                    },
                }
                },
                estado:{
                    validators: {
                        notEmpty: {
                            message: 'El estado es requerido'
                        },
                    }
                    },
            fechaNacimiento:{
                    validators: {
                        notEmpty: {
                            message: 'El Verificable es requerido'
                        },

                    }
                    },
                    cargo:{
                        validators: {
                            notEmpty: {
                                message: 'El cargo es requerido'
                            },

                        }
                        },
                        distrito:{
                            validators: {
                                notEmpty: {
                                    message: 'El distrito es requerido'
                                },

                            }
                            },
            }
        });
    });
    </script>
@endsection
