@extends('layouts.templateatm')
@section('section')
    <section class='content-header'>
        <h1>CATEGORÍA MANTENIMIENTO  <small>Stand Virtual</small> </h1>
        <ol class='breadcrumb'>
            <li><a href='#'><i class='fa fa-dashboard'></i>Editar </a></li>
            <li class='active'>Categoria MANTENIMIENTO</li>
        </ol>
    </section>
    <section class='content'>
        <div class='row'>
            <div class='col-xs-12'>
                <form id="frmMante" name="frmMante" action="{{url('finalistas/editar2')}}" method="post" enctype="multipart/form-data">
                 {{csrf_field()}}
                <div class="row">
                    <div class="col-md-4">
                        <div class="box box-info">
                            <div class="box-body">
                                <input type="hidden" name="idatm" value="{{$listaEditar->idatm}}">
                            <h3 class="text-primary" >
                              1.1.  El ATM cuenta con JASS WASI MODELO  <br>
                                   <small  class="text-success">  SUBIR CUATRO FOTOGRAFIAS JASS WASI </small>
                                </h5>
                                <div class="form-group">
                                    <label for="foto1"> Imagen 1</label>
                                    <input  type="file"  class="form-control" id="foto1" name="foto1" >
                                    <label for="foto1" style= "color:red;font-weight: bold">
                                        <i class="fa fa-pencil"> </i> Cambiar IMAGEN 1</label>
                                </div>

                                <div class="form-group">
                                    <label for="foto2"> Imagen 2</label>
                                    <input   type="file"  class="form-control" id="foto2" name="foto2" >
                                    <label for="foto2" style= "color:red;font-weight: bold">
                                        <i class="fa fa-pencil"> </i> Cambiar IMAGEN 2</label>
                                </div>
                                <div class="form-group">
                                    <label for="foto3"> Imagen 3</label>
                                    <input  type="file"  class="form-control" id="foto3" name="foto3" >
                                    <label for="foto3" style= "color:red;font-weight: bold">
                                        <i class="fa fa-pencil"> </i> Cambiar IMAGEN 3</label>
                                </div>
                                <div class="form-group">
                                    <label for="foto4"> Imagen 4</label>
                                    <input type="file"  class="form-control" id="foto4" name="foto4" >
                                    <label for="foto4" style= "color:red;font-weight: bold">
                                        <i class="fa fa-pencil"> </i> Cambiar IMAGEN 4</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-info">
                            <div class="box-body">
                                <h3 class="text-primary" >
                                   1.2. INSTALACION DE SISTEMAS DE CLORACION EN EL AÑO 2020 <br>
                                    <small class="text-success"> SUBIR  ACTAS DE INSTALACIÓN  DE SISTEMAS DE CLORACIÓN
                                        EN UN SOLO ARCHIVO PDF
                                         ( También se considera los distritos que cerraron esta brecha)</small>
                                 </h3>
                                 <div class="form-group">

                                     <input  type="file"  class="form-control" id="actasSap" name="actasSap" >
                                     <br>
                                     <label for="actasSap" style= "color:red;font-weight: bold">
                                        <i class="fa fa-pencil"> </i> Cambiar PDF</label>
                                 </div>
                                 <h3 class="text-primary" >
                                     1.3.
                                    POI CUENTA CON PLAN DE MANTENIMIENTO ANUAL <br> <small  class="text-success">
                                         CARGO DE POI PRESENTADO EN FORMATO PDF </small>
                                 </h3>
                                 <div class="form-group">
                                     <input   type="file"  class="form-control" id="cargoPOI" name="cargoPOI" >
                                     <br>
                                     <label for="cargoPOI" style= "color:red;font-weight: bold">
                                        <i class="fa fa-pencil"> </i> Cambiar PDF</label>
                                 </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-info">
                            <div class="box-body">
                                <h3 class="text-primary" >
                                    1.4  SISTEMAS DE AGUA POTABLE INTERVENIDOS EN EL AÑO 2020<br> <small  class="text-success">  2 FOTOGRAFIAS DEL ANTES y
                                        2 FOTOGRAFIAS DEL DESPUES DE CADA INTERVENCION(EN UN ARCHIVO PDF)  </small>
                                 </h3>
                                 <div class="form-group">
                                     <input   type="file"  class="form-control" id="fotosSap" name="fotosSap" >
                                     <br>
                                     <label for="fotosSap" style= "color:red;font-weight: bold">
                                        <i class="fa fa-pencil"> </i> Cambiar PDF</label>
                                 </div>

                                <input class="btn btn-success" type="submit" value="GUARDAR INFORMACIÓN">
                                <a href="{{ URL::previous() }}" class="btn btn-lg btn-danger btn-sm"><span class="fa fa-arrow-left "></span> REGRESAR </a>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
                </form>
            </div>

    </section>
   <script>
$(document).ready(function() {
       $('#frmMante').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields:{
            foto:{
                validators: {
                    file: {
                        extension: 'jpeg,jpg,png',
                        type: 'image/jpeg,image/png',
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'debe subir una IMAGEN jpg o png que pese menos de 5MB'
                    },
                }
            },
            foto1:{
                validators: {
                    file: {
                        extension: 'jpeg,jpg,png',
                        type: 'image/jpeg,image/png',
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'debe subir una IMAGEN jpg o png que pese menos de 5MB'
                    },
                }
            },
            foto2:{
                validators: {
                    file: {
                        extension: 'jpeg,jpg,png',
                        type: 'image/jpeg,image/png',
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'debe subir una IMAGEN jpg o png que pese menos de 5MB'
                    },
                }
            },
            foto3:{
                validators: {
                    file: {
                        extension: 'jpeg,jpg,png',
                        type: 'image/jpeg,image/png',
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'debe subir una IMAGEN jpg o png que pese menos de 5MB'
                    },
                }
            },
            foto4:{
                validators: {
                    file: {
                        extension: 'jpeg,jpg,png',
                        type: 'image/jpeg,image/png',
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'debe subir una IMAGEN jpg o png que pese menos de 5MB'
                    },
                }
            },
            actasSap:{
                validators: {
                    file: {
                        extension: 'pdf',
                        type: 'application/pdf',
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'debe subir un documento PDF que pese menos de 5MB'
                    },
                }
            },
            fotosSap:{
                validators: {
                    file: {
                        extension: 'pdf',
                        type: 'application/pdf',
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'debe subir un documento PDF que pese menos de 5MB'
                    },
                }
            },
            cargoPOI:{
                validators: {
                    file: {
                        extension: 'pdf',
                        type: 'application/pdf',
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'debe subir un documento PDF que pese menos de 5MB'
                    },
                }
            },
        }
    });
});
   </script>
@endsection
