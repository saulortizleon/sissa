@extends('layouts.templateatm')
@section('section')
<section class='content-header'>
    <h1>ACTUALIZACION DE STAND VIRTUAL<small> AREAS TECNICAS MUNICIPALES</small></h1>
    <ol class='breadcrumb'>
        <li><a href='#'><i class='fa fa-dashboard'></i>ATM</a></li>
        <li class='active'>Lista</li>
    </ol>
</section>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class="box-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group"><label for="">Buscar por distrito : </label>
                                <input type="text"  id="myInput" onkeyup="myFunction()"  class="form-control" placeholder="escribir aquí..." ></div>
                        </div>
                            <div class="col-sm-6">

                            </div>
                    </div>
                </div>
                <div class='box-body table-responsive'>
                    <table id='myTable' class='table table-bordered table-hover'>
                        <thead>
                            <tr>
                                <th style="font-size: 16px; color:#367fa9">PROVINCIA</th>
                                <th style="font-size: 16px; color:#367fa9">DISTRITO</th>
                                <th style="font-size: 16px;color:#367fa9"> <i>CATEGORIA</i> GESTIÓN
                                </th>
                                <th style="font-size: 16px;color:#367fa9"> <i>CATEGORIA</i>  MANTENIMIENTO
                                </th>
                                <th style="font-size: 16px;color:#367fa9"> <i>CATEGORIA</i>  EDUSA
                                </th>

                                <th style="font-size: 16px;color:#367fa9"> REVISIÓN </th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($listaAtm as $item)
                            <tr >
                                <td> <b>{{$item->provincia}}</b> </td>
                                <td> <b>{{$item->distrito}}</b> </td>
                                <td> <button class="btn btn-primary btn-sm"  onclick="actualizar1('{{$item->idatm}}')"><i class="fa fa-plus"></i> AGREGAR</button></td>
                                <td><button class="btn btn-warning btn-sm"  onclick="actualizar2('{{$item->idatm}}')"><i class="fa fa-plus"></i> AGREGAR</button></td>
                                <td><button class="btn btn-info btn-sm"  onclick="actualizar3('{{$item->idatm}}')"><i class="fa fa-plus"></i> AGREGAR</button></td>
                                <td>
                                    <a href="{{url('atm')}}/{{$item->idatm}}" target="_blank" class="btn btn-success btn-sm"   ><i class="fa fa-home"></i> VER STAND VIRTUAL</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>


                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function actualizar1(idatm)
    {
    	window.location.href='{{url('finalistas/editar')}}/'+idatm;
    }
    function actualizar2(idatm)
    {
    	window.location.href='{{url('finalistas/editar2')}}/'+idatm;
    }
    function actualizar3(idatm)
    {
    	window.location.href='{{url('finalistas/editar3')}}/'+idatm;
    }

   function myFunction() {
    var input, filter, table, tr, td, i, txtValue;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    table = document.getElementById("myTable");
    tr = table.getElementsByTagName("tr");
    for (i = 0; i < tr.length; i++) {
        td = tr[i].getElementsByTagName("td")[0];
        if (td) {
        txtValue = td.textContent || td.innerText;
        if (txtValue.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
        } else {
            tr[i].style.display = "none";
        }
        }
    }
    }
</script>
@endsection