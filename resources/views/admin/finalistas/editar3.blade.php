@extends('layouts.templateatm')
@section('section')
    <section class='content-header'>
        <h1>CATEGORÍA EDUSA  <small>Stand Virtual</small> </h1>
        <ol class='breadcrumb'>
            <li><a href='#'><i class='fa fa-dashboard'></i>Editar </a></li>
            <li class='active'>Categoria EDUSA</li>
        </ol>
    </section>
    <section class='content'>
        <div class='row'>
            <div class='col-xs-12'>
                <form id="frmEdusa" name="frmEdusa" action="{{url('finalistas/editar3')}}" method="post" enctype="multipart/form-data">
                 {{csrf_field()}}
                <div class="row">
                    <div class="col-md-4">
                        <div class="box box-info">
                            <div class="box-body">
                                <input type="hidden" name="idatm" value="{{$listaEditar->idatm}}">
                            <h3 class="text-primary" >
                                3.1. Elaboración de un spot y/o video con la participación de operadores y otros (Spot y/o Video de Cloración / Desinfección) <br>
                                   <small  class="text-success">  PEGAR ENLACE DEL VIDEO </small>
                                </h3>
                                <div class="form-group">
                                    <label for="foto1"> El video debe ser subido a youtube o facebook</label>
                                    <textarea class="form-control" id="linkvideo" name="linkvideo" rows="2"> {{$listaEditar->linkvideo}}</textarea>
                                </div>

                            <h3 class="text-primary" >
                                3.2. JASS Formalizada  de acuerdo a los lineamientos del PNSR. <br>
                                    <small  class="text-success">  CAPTURA DE PANTALLA DE LOS VERIFICABLES EXISTENTES EN  EL APLICATIVO WEB
                                        EN UN ARCHIVO PDF </small>
                                </h3>
                                <div class="form-group">
                                    <input  type="file"  class="form-control" id="capturaJass" name="capturaJass" >
                                     <br>
                                     <label for="capturaJass" style= "color:red;font-weight: bold">
                                        <i class="fa fa-pencil"> </i> Cambiar PDF</label>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-info">
                            <div class="box-body">
                                <h3 class="text-primary" >
                                    3.3. El ATM realiza la supervisión a la JASS. <br>
                                        <small  class="text-success">   FICHAS DE SUPERVISION
                                            EN UN ARCHIVO PDF </small>
                                    </h3>
                                    <div class="form-group">
                                        <input  type="file"  class="form-control" id="fichasSup" name="fichasSup" >
                                            <br>
                                            <label for="fichasSup" style= "color:red;font-weight: bold">
                                            <i class="fa fa-pencil"> </i> Cambiar DOCUMENTO</label>
                                    </div>

                                <h3 class="text-primary" >
                                   3.4. La JASS realiza Operación y Mantenimiento del sistema de agua potable y eliminación de excretas.<br>
                                    <small class="text-success"> FOTOGRAFIAS DE LAS ACTAS Y 4 FOTOGRAFIAS DE LOS TRABAJOS REALIZADOS
                                        EN UN SOLO ARCHIVO PDF
                                    </small>
                                 </h3>
                                 <div class="form-group">
                                     <input  type="file"  class="form-control" id="jassopm" name="jassopm" >
                                     <br>
                                     <label for="jassopm" style= "color:red;font-weight: bold">
                                        <i class="fa fa-pencil"> </i> Cambiar DOCUMENTO</label>
                                 </div>


                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-info">
                            <div class="box-body">
                                <h3 class="text-primary" >
                                   3.5. La JASS cuenta con plan operativo anual y aprobación de la cuota familiar<br> <small  class="text-success">
                                     FOTOGRAFIAS DEL POA - 2020, JASS WASI EN UN ARCHIVO PDF </small>
                                  </h3>
                                  <div class="form-group">
                                      <input   type="file"  class="form-control" id="jaspoa" name="jaspoa" >
                                      <br>
                                      <label for="jaspoa" style= "color:red;font-weight: bold">
                                         <i class="fa fa-pencil"> </i> Cambiar PDF</label>
                                  </div>
                                <h3 class="text-primary" >
                                   3.6. Videos Enmarcados en educación sanitaria ambiental <br> <small  class="text-success">
                                     SUBIR ENLACE DE  VIDEO</small>
                                 </h3>
                                 <div class="form-group">
                                    <label for="videoEdusa"> El video debe ser subido a youtube o facebook</label>
                                    <textarea class="form-control" id="videoEdusa" name="videoEdusa" rows="2">{{$listaEditar->videoEdusa}}</textarea>
                                </div>

                                </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box box-info">
                            <div class="box-body">
                                <h3 class="text-primary" >
                                    3.7. Estrategias tomadas a  través del área técnica municipal para la sostenibilidad de la cuota familiar.
                                    <br>
                                       <small  class="text-success"> TEXTO DE 500 PALABRAS QUE DESCRIBAN LAS ESTRATEGIAS </small>
                                    </h3>
                                    <div class="form-group">
                                        <textarea class="form-control" id="estrategias" name="estrategias" rows="5"> {{$listaEditar->estrategias}}</textarea>
                                    </div>
                                    <br>
                                    <input class="btn btn-success" type="submit" value="GUARDAR INFORMACIÓN">
                                    <a href="{{ URL::previous() }}" class="btn btn-lg btn-danger btn-sm"><span class="fa fa-arrow-left "></span> REGRESAR </a>

                            </div>
                        </div>
                    </div>
                </div>
                </div>
                </form>
            </div>

    </section>
   <script>
$(document).ready(function() {
       $('#frmEdusa').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields:{
            capturaJass:{
                validators: {
                    file: {
                        extension: 'pdf',
                        type: 'application/pdf',
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'debe subir un documento PDF que pese menos de 5MB'
                    },
                }
            },
            fichasSup:{
                validators: {
                    file: {
                        extension: 'pdf',
                        type: 'application/pdf',
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'debe subir un documento PDF que pese menos de 5MB'
                    },
                }
            },
            jassopm:{
                validators: {
                    file: {
                        extension: 'pdf',
                        type: 'application/pdf',
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'debe subir un documento PDF que pese menos de 5MB'
                    },
                }
            },
            jaspoa:{
                validators: {
                    file: {
                        extension: 'pdf',
                        type: 'application/pdf',
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'debe subir un documento PDF que pese menos de 5MB'
                    },
                }
            },
            estrategias: {
                validators: {
                    stringLength: {
                        max: 500,
                        message: 'EL TEXTO SOLO DEBE CONTENER 500 CARACTERES'
                    }
                }
            },
        }
    });
});
   </script>
@endsection
