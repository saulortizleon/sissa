@extends('layouts.templateatm')
@section('section')
    <section class='content-header'>
        <h1>CATEGORÍA GESTIÓN  <small>Stand Virtual</small> </h1>
        <ol class='breadcrumb'>
            <li><a href='#'><i class='fa fa-dashboard'></i>Editar </a></li>
            <li class='active'>Categoria GESTIÓN</li>
        </ol>
    </section>
    <section class='content'>
        <div class='row'>
            <div class='col-xs-12'>
                <form id="frmGestion" name="frmGestion" action="{{url('finalistas/editar')}}" method="post" enctype="multipart/form-data">
                 {{csrf_field()}}
                <div class="row">
                    <div class="col-md-4">
                        <div class="box box-info">
                            <div class="box-body">
                                <input type="hidden" name="idatm" value="{{$listaEditar->idatm}}">
                            <h3 class="text-primary" >
                                   1.1. Verificable asistencia, Escuela de Saneamiento “YAKU KAWSAY” <br>
                                   <small  class="text-success">  CONSTANCIA FIRMADA POR LA DIRECCIÓN EN FORMATO PDF </small>
                                </h3>
                                <div class="form-group">
                                    <input id="cambiar" type="file"  class="form-control" id="constanciaAtm" name="constanciaAtm" >
                                    <br>
                                    <label for="cambiar" style= "color:red;font-weight: bold">
                                        <i class="fa fa-pencil"> </i> Cambiar PDF</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-info">
                            <div class="box-body">
                                <h3 class="text-primary" >
                                    1.2. Operador rural o gasfitero matriculado, Escuela de Saneamiento "YAKU KAWSAY" <br>
                                    <small class="text-success"> Ficha de MATRICULA Y  ASISTENCIA firmado por la direción en  formato PDF</small>
                                 </h3>
                                 <div class="form-group">
                                     <input id="cambiar1" type="file"  class="form-control" id="constanciaOperador" name="constanciaOperador" >
                                     <br>
                                     <label for="cambiar1" style= "color:red;font-weight: bold">
                                        <i class="fa fa-pencil"> </i> Cambiar PDF</label>
                                 </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="box box-info">
                            <div class="box-body">
                                <h3 class="text-primary" >
                                    1.3. Reporte del aplicativo DATASS <br> <small  class="text-success"> CAPTURAS DE PANTALLAS VALIDADO Y FIRMADO POR EL FACILITADOR EN FORMATO PDF</small>

                                 </h3>
                                 <div class="form-group">
                                     <input id="cambiar2" type="file"  class="form-control" id="reporteDatass" name="reporteDatass" >
                                     <br>
                                     <label for="cambiar2" style= "color:red;font-weight: bold">
                                        <i class="fa fa-pencil"> </i> Cambiar PDF</label>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="box box-info">
                            <div class="box-body">
                                <h3 class="text-primary" >
                                   1.4.  Matriz de agua clorada <br> <small  class="text-success"> CONSTANCIA DE SAP QUE BRINDAN AGUA CLORADA CONTINUAMENTE FIRMADA POR
                                        LA DIRECCIÓN Y  EL SISTEMATIZADOR  EN Formato PDF</small>
                                 </h3>
                                 <div class="form-group">
                                     <input id="cambiar3" type="file"  class="form-control" id="constanciaSap" name="constanciaSap" >
                                     <br>
                                     <label for="cambiar3" style= "color:red;font-weight: bold">
                                        <i class="fa fa-pencil"> </i> Cambiar DOCUMENTO</label>
                                 </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-info">
                            <div class="box-body">
                                <h3 class="text-primary" >
                                  1.5.   Avance de gasto presupuestal – Consulta amigable <br>
                                    <small  class="text-success"> CAPTURA DE PANTALLA DEL AVANCE PRESUPUESTAL
                                        CONSULTA AMIGABLE -  MEF EN Formato PDF</small>

                                 </h3>
                                 <div class="form-group">
                                     <input id="cambiar4" type="file"  class="form-control" id="POI" name="POI" >
                                     <br>
                                     <label for="cambiar4" style= "color:red;font-weight: bold">
                                        <i class="fa fa-pencil"> </i> Cambiar PDF</label>
                                 </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-info">
                            <div class="box-body">
                                <h3 class="text-primary" >
                                  1.6.  ATM Aplica Decretos de Urgencia - Consulta amigable<br>
                                    <small  class="text-success">CAPTURA DE PANTALLA DEL AVANCE PRESUPUESTAL CONSULTA AMIGABLE -  MEF EN Formato IMAGEN</small>
                                 </h3>
                                 <div class="form-group">
                                     <input id="cambiar5" type="file"  class="form-control" id="decretosUrgencia" name="decretosUrgencia" >
                                     <br>
                                     <label for="cambiar5" style= "color:red;font-weight: bold">
                                        <i class="fa fa-pencil"> </i> Cambiar PDF</label>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-body">
                            <input class="btn btn-success" type="submit" value="GUARDAR INFORMACIÓN">
                            <a href="{{ URL::previous() }}" class="btn btn-lg btn-danger btn-sm"><span class="fa fa-arrow-left "></span> REGRESAR </a>
                        </div>
                    </div>
                </div>
                </div>
                </form>
            </div>

    </section>
   <script>
$(document).ready(function() {
       $('#frmGestion').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields:{
            constanciaAtm:{
                validators: {
                    file: {
                        extension: 'pdf',
                        type: 'application/pdf',
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'debe subir un documento PDF que pese menos de 5MB'
                    },
                }
            },
            constanciaOperador:{
                validators: {
                    file: {
                        extension: 'pdf',
                        type: 'application/pdf',
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'debe subir un documento PDF que pese menos de 5MB'
                    },
                }
            },
            reporteDatass:{
                validators: {
                    file: {
                        extension: 'pdf',
                        type: 'application/pdf',
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'debe subir un documento PDF que pese menos de 5MB'
                    },
                }
            },
            constanciaSap:{
                validators: {
                    file: {
                        extension: 'pdf',
                        type: 'application/pdf',
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'debe subir un documento PDF que pese menos de 5MB'
                    },
                }
            },
            POI:{
                validators: {
                    file: {
                        extension: 'pdf',
                        type: 'application/pdf',
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'debe subir un documento PDF que pese menos de 5MB'
                    },
                }
            },
            decretosUrgencia:{
                validators: {
                    file: {
                        extension: 'pdf',
                        type: 'application/pdf',
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'debe subir un documento PDF que pese menos de 5MB'
                    },
                }
            },
        }
    });
});
   </script>
@endsection
