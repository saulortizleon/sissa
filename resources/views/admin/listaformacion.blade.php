@extends('layouts.templateadm')
@section('section')
<section class='content-header'>
    <h1> GESTIÓN DE ATM</h1>
    <ol class='breadcrumb'>
        <li><a href='#'><i class='fa fa-dashboard'></i>ATM</a></li>
        <li class='active'>Lista</li>
    </ol>
</section>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Formación ATM</h3>
                     <button id="agregar" class="btn btn-success btn-sm pull-right btn-sm" data-target="#modalFormacion"  data-toggle="modal" >exportar excel</button></td>
                    </div>
                <div class="box-body">
                    <form action="{{url('centrop/lista')}}" method="get">
                        <div class="input-group">
                            <input type="text" name="search" value="" class="form-control">
                            <span class="input-group-btn">
                                <button class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
                <div class='box-body table-responsive'>
                    <table id='' class='table table-bordered table-hover'>
                        <thead>
                            <tr> <th> Distrito</th>
                                <th> Distrito</th>
                                <th> Responsable ATM</th>
                                <th> Cap. GN </th>
                                <th> Cap. GR</th>
                                <th> Cap. GL</th>
                                <th> ONG</th>
                                <th> OTROS</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($listaFormacion as $item)
                            <tr>
                                <td>{{$item->provincia}}</td>
                                <td>{{$item->distrito}}</td>
                                <td>{{$item->nombres}}</td>
                                <td>{{$item->GN}}</td>
                                <td>{{$item->GR}}</td>
                                <td>{{$item->GL}}</td>
                                <td>{{$item->ONG}}</td>
                                <td>{{$item->EDUSA}}</td>
                                <td><button class="btn btn-primary btn-xs"  onclick="actualizar('{{$item->idFormacion}}')">DESCARGAR VERIFICABLES</button>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection