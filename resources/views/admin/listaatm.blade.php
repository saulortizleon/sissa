@extends('layouts.templateadm')
@section('section')
<section class='content-header'>
    <h1> GESTIÓN DE ATM</h1>
    <ol class='breadcrumb'>
        <li><a href='#'><i class='fa fa-dashboard'></i>ATM</a></li>
        <li class='active'>Lista</li>
    </ol>
</section>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>LISTA DE ATM</h3>
                    <button id="agregar" class="btn btn-success btn-sm pull-right btn-sm"
                    data-target="#modalFormacion"  data-toggle="modal" ><i class="fa fa-file-excel-o"></i> Exportar excel</button></td>
                </div>
                <div class="box-body">
                    <form action="{{url('atm/lista')}}" method="get">
                        <div class="input-group">
                            <input type="text" name="search" value="" class="form-control">
                            <span class="input-group-btn">
                                <button class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
                <div class='box-body table-responsive'>
                    <table id='' class='table table-bordered table-hover'>
                        <thead>
                            <tr>
                                <th>Provincia</th>
                                <th>distrito</th>
                                <th> Institucionalización </th>
                                <th> Fortalecimiento </th>
                                <th> App pnsr </th>
                                <th> Consolidado JASS</th>
                                <th> Consolidad SAP</th>
                                <th> Difusión radial</th>
                                <th> Capacitaciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($listaAtm as $item)
                            <tr>
                                <td>{{$item->provincia}}</td>
                                <td>{{$item->distrito}}</td>
                            <td><button class="btn btn-danger btn-sm"  onclick="formacion('{{$item->ubigeo}}')"><i class="fa fa-eye"></i> Revisar</button>
                            </td>
                            <td><button class="btn btn-primary btn-sm"  onclick="herramientas('{{$item->ubigeo}}')"><i class="fa fa-eye"></i> Revisar</button>
                            </td>
                            <td><button class="btn btn-warning btn-sm"  onclick="gestion('{{$item->ubigeo}}')"><i class="fa fa-eye"></i> Revisar</button>
                            </td>
                            <td><button class="btn btn-info btn-sm"  onclick="gestion('{{$item->ubigeo}}')"><i class="fa fa-eye"></i> Revisar</button>
                            </td>
                            <td><button class="btn btn-success btn-sm"  onclick="gestion('{{$item->ubigeo}}')"><i class="fa fa-eye"></i> Revisar</button>
                            </td>
                            <td><button class="btn btn-secondary btn-sm"  onclick="gestion('{{$item->ubigeo}}')"><i class="fa fa-eye"></i> Revisar</button>
                            </td>
                            <td><button class="btn btn-warning btn-sm"  onclick="gestion('{{$item->ubigeo}}')"><i class="fa fa-eye"></i> Revisar</button>
                            </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    function formacion(ubigeo)
      {
        window.location = "{{url('formacion/ver')}}/"+ubigeo;
      }
    function herramientas(ubigeo)
    {
    window.location = "{{url('herramientas/ver')}}/"+ubigeo;
    }
    function gestion(ubigeo)
    {
      window.location = "{{url('gestion/ver')}}/"+ubigeo;
    }
</script>
@endsection