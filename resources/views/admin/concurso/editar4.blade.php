@extends('layouts.templatesup')
@section('section')
    <section class='content-header'>
        <h1>DATOS MUNICIPALIDAD <small>Stand Virtual</small> </h1>
        <ol class='breadcrumb'>
            <li><a href='#'><i class='fa fa-dashboard'></i>Editar </a></li>
            <li class='active'>DATOS MUNICIPALIDAD</li>
        </ol>
    </section>
    <section class='content'>
        <div class='row'>
            <div class='col-xs-12'>
                <form id="frmAtm" name="frmAtm" action="{{url('concurso/editar4')}}" method="post" enctype="multipart/form-data">
                 {{csrf_field()}}
                <div class="row">
                    <div class="col-md-4">
                        <div class="box box-info">
                            <div class="box-body">
                                <input type="hidden" name="idatm" value="{{$listaEditar->idatm}}">
                                <h4 class="text-primary" >
                                    ENLACE PÁGINA WEB  INSTITUCIONAL
                                </h4>
                                <div class="form-group">
                                    <textarea id="cambiar"  type="textarea"  class="form-control" id="web"
                                    name="web" style="height: 100px;" > {{ $listaEditar->web }} </textarea>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-info">
                            <div class="box-body">
                                <h4 class="text-primary" >
                                    LOGO MUNICIPALIDAD <small>  en formato PNG o JPG:</small>
                                </h4>
                                <div class="form-group">
                                    <input id="cambiar3" type="file"  class="form-control" id="img" name="img" >
                                </div>
                                <div class="form-group">
                                      <label style="margin-left:20px; color:red;font-weight: bold" for="cambiar3" >
                                      <i class="fa fa-pencil"> </i> Cambiar LOGO</label>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-info">
                            <div class="box-body">
                                <h4 class="text-primary" >
                                    ENLACE FACEBOOK DE LA MUNICIPALIDAD
                                </h4>
                                <div class="form-group">
                                    <textarea id="cambiar"  type="textarea"  class="form-control" id="face"
                                    name="face" style="height: 100px;" > {{ $listaEditar->face }} </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-body" >
                                  <input class="btn btn-success" type="submit" value="GUARDAR INFORMACIÓN">
                                 <a href="{{ URL::previous() }}" class="btn btn-lg btn-danger btn-sm"><span class="fa fa-angle-left "></span> REGRESAR </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

    </section>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                height: 600,
                codemirror: { // codemirror options
                    theme: 'monokai'
                  },
                  lang: 'es-ES'

              });
            });
    </script>
@endsection
