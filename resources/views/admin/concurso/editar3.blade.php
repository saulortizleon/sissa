@extends('layouts.templatesup')
@section('section')
    <section class='content-header'>
        <h1>CATEGORÍA GESTIÓN  <small>Stand Virtual</small> </h1>
        <ol class='breadcrumb'>
            <li><a href='#'><i class='fa fa-dashboard'></i>Editar </a></li>
            <li class='active'>Categoria GESTIÓN</li>
        </ol>
    </section>
    <section class='content'>
        <div class='row'>
            <div class='col-xs-12'>
                <form id="frmAtm" name="frmAtm" action="{{url('concurso/editar3')}}" method="post" enctype="multipart/form-data">
                 {{csrf_field()}}
                <div class="row">
                    <div class="col-md-4">
                        <div class="box box-info">
                            <div class="box-body">
                                <input type="hidden" name="idatm" value="{{$listaEditar->idatm}}">
                                <h4 class="text-primary" >
                                    FOTOGRAFIA PRINCIPAL<small>  en formato PNG o JPG :</small>
                                </h4>
                                <div class="form-group">
                                    <input id="cambiar" type="file"  class="form-control" id="foto3" name="foto3" >
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-info">
                            <div class="box-body">
                                <h4 class="text-primary" >
                                   SPOT DE  AUDIO PRINCIPAL <small>  en formato MP3  </small>
                                </h4>
                                <div class="form-group">
                                    <input id="cambiar2" type="file"  class="form-control" id="audio3" name="audio3" >
                                </div>
                                <div class="form-group">
                                    <audio controls>
                                        <source src="{{asset($listaEditar->audio3)}}" type="audio/mp3">

                                      </audio>
                                   <label for="cambiar2" style="margin-top:0px; margin-left:10px;  color:red;font-weight: bold">
                                       <i class="fa fa-pencil"> </i> Cambiar AUDIO</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="box box-info">
                            <div class="box-body">
                                <h4 class="text-primary" >
                                    VIDEO PRINCIPAL <small> inserta link de video de facebook o youtube </small>
                                </h4>
                                <div class="form-group">
                                    <textarea id="cambiar"  type="textarea"  class="form-control" id="video3"
                                    name="video3" style="height: 100px;" > {{ $listaEditar->video3 }} </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-body" >
                                    <div class="box-header">
                                        <h2  class="text-primary" >DETALLAR LAS   <b>ACTIVIDADES REALIZADAS EN EDUSA</b> DURANTE EL AÑO 2020
                                        </h2>
                                    </div>
                             <!-- tools box -->
                             
                                <br>
                                 <input class="btn btn-success" type="submit" value="GUARDAR INFORMACIÓN">
                                 <a href="{{ URL::previous() }}" class="btn btn-lg btn-danger btn-sm"><span class="fa fa-angle-left "></span> REGRESAR </a>

                            </div>
                        </div>
                    </div>
                </form>
            </div>

    </section>
    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                height: 600,
                codemirror: { // codemirror options
                    theme: 'monokai'
                  },
                  lang: 'es-ES'

              });
            });
    </script>
@endsection
