@extends('layouts.templateadm')
@section('section')
<section class='content-header'>
    <h1> MESA DE PARTES VIRTUAL <small>DRVCS</small></h1>
    <ol class='breadcrumb'>
        <li><a href='#'><i class='fa fa-dashboard'></i>MPV</a></li>
        <li class='active'>SOLICITUDES</li>
    </ol>
</section>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class="box-header">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group"><label for="">Buscar por Nombre de usuario : </label>
                                <input type="text" id="buscar"  class="form-control" placeholder="escribir aquí..." ></div>
                        </div>            
                    </div>
                </div>
                <div class='box-body table-responsive'>
                    <table id='tablaUsuarios' class='table table-bordered table-hover'>
                        <thead>
                            <tr>
                                <th>Tipo de Solicitud</th>
                                <th>DNI</th>
                                <th>Nombres</th>
                                <th>celular</th>
                                <th>Correo electrónico</th>
                                <th>documento</th>
                                <th>Acciones</th>              
                            </tr>
                        </thead>
                        <tbody>                      
                            <tr>
                                <td>Solitud de información</td>
                                <td>12345678</td>
                                <td>PRUEBA PRUEBA PRUEBA</td>                         
                                <td>98775588</td>
                                <td>PRUEBA@GMAIL.COM</td>
                                <td> <a href="#" class="btn btn-link btn-sm"><i class="fa fa-eye"> VER DOCUMENTO</i></a>
                                </td>  
                                <td>
                                    <a href="#"  class="btn btn-primary btn-sm"><i class="fa fa-send"> Responder</i></a>
                                    <a href="#"  class="btn btn-success  btn-sm"><i class="fa fa-archive"></i> Archivar</a>
                                </td>
                            </tr>
                
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function(){
        $("#buscar").on("keyup", function() {
          var value = $(this).val().toLowerCase();
          $("#tablaUsuarios tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
        });
      });
      function UserDelete(dniUsuario)
      {
          if(confirm('Está seguro de quitar el acceso?'))
          {
              window.location = "{{url('usuario/eliminar')}}/"+dniUsuario;
          }
      }
      function UserEdit(dniUsuario)
      {
          if(confirm('Está seguro de editar el usuario?'))
          {
              window.location = "{{url('usuarios/editar')}}/"+dniUsuario;
          }
      }
</script>
@endsection