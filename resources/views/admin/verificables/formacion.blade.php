@extends('layouts.templateadm')
@section('section')
    <section class='content-header'>
        <h1> Formación<small>ATM</small></h1>
        <ol class='breadcrumb'>
            <li><a href='#'><i class='fa fa-dashboard'></i>editar </a></li>
            <li class='active'>Formación</li>
        </ol>
    </section>
    <section class='content'>
        <div class='row'>
            <div class='col-xs-12'>
                <div class='box'>
                    <div class='box-body'>
                            <div class="row">
                            <center> Lista de verificables de formacion del <b> Sr. Eugenio Torres Vega  </b>,
                                Responsable ATM del Distrito : <b> Tambobamba</b>
                                <a href="{{url('admin/listaatm')}}" class="btn btn-danger btn-sm" ><i class="fa fa-arrow-left"></i> Regresar</a>
                            </center>
                                        <br>
                                <div class="col-md-4">
                                    <div class="box box-info">
                                        <div class="box-body">

                                    <div class="form-group">
                                             Documento ROF :
                                            <br>

                                            <button class="btn btn-success btn-sm"> <i class="fa fa-download"></i> descargar</button>
                                    </div>
                                    <div class="form-group" >
                                             Documento MOF :
                                            <br>
                                            <button class="btn btn-success btn-sm"> <i class="fa fa-download"></i> descargar</button>
                                    </div>
                                    <div class="form-group">
                                         Documento CAP :
                                            <br>
                                             <button class="btn btn-success btn-sm"> <i class="fa fa-download"></i> descargar</button>
                                    </div>
                                    <div class="form-group">
                                             Documento TUPA :
                                            <br>

                                            <button class="btn btn-success btn-sm"> <i class="fa fa-download"></i> descargar</button>
                                    </div>
                                    <div class="form-group">
                                             Grado Académico :
                                        <br>

                                        <label for=""> no tiene verificable</label>
                                    </div>
                                    <div class="form-group">
                                            <label for="description"> Especialidad</label> <br>
                                            <button class="btn btn-success btn-sm"> <i class="fa fa-download"></i> descargar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                                <div class="col-md-4">
                                        <div class="box box-info">
                                                <div class="box-body">
                                    <div class="form-group">
                                             Capacitación Gob. Nacional:
                                            <br>

                                            <button class="btn btn-success btn-sm"> <i class="fa fa-download"></i> descargar</button>
                                    </div>
                                    <div class="form-group">
                                             Capacitación Gobierno Regional:
                                       <br>

                                        <button class="btn btn-success btn-sm"> <i class="fa fa-download"></i> descargar</button>
                                    </div>
                                    <div class="form-group">
                                             Capacitación Gobierno local:
                                        <br>

                                        <button class="btn btn-success btn-sm"> <i class="fa fa-download"></i> descargar</button>
                                    </div>
                                    <div class="form-group">
                                        <label for=""> no tiene verificable</label>
                                        <br>

                                        <button class="btn btn-success btn-sm"> <i class="fa fa-download"></i> descargar</button>
                                    </div>
                                    <div class="form-group">
                                             Capacitación por  EDUSA:
                                        <br>

                                        <button class="btn btn-success btn-sm"> <i class="fa fa-download"></i> descargar</button>
                                    </div>
                                    <div class="form-group">
                                             Capacitación por  AOM:
                                        <br>

                                        <label for=""> no tiene verificable</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                                <div class="col-md-4">
                                        <div class="box box-info">
                                                <div class="box-body">
                                        <div class="form-group">
                                                 Capacitación por  DL1280:
                                           <br>

                                            <button class="btn btn-success btn-sm"> <i class="fa fa-download"></i> descargar</button>
                                        </div>
                                        <div class="form-group">
                                                 Capacitación por  PNSR:
                                              <br>

                                            <button class="btn btn-success btn-sm"> <i class="fa fa-download"></i> descargar</button>
                                        </div>
                                        <div class="form-group">
                                                 Capacitación por  PNSR otros:
                                            <br>

                                            <button class="btn btn-success btn-sm"> <i class="fa fa-download"></i> descargar</button>
                                        </div>
                                        <div class="form-group">
                                                 Plan de saneamiento Nacional
                                            <br>

                                            <label for=""> no tiene verificable</label>
                                        </div>
                                        <div class="form-group">
                                                 R.M 155-2017 Criterios de Admisibilidad :<br>
                                         <br>

                                            <button class="btn btn-success btn-sm"> <i class="fa fa-download"></i> descargar</button>
                                        </div>

                                </div>
                            </div>
                        </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
