@extends('layouts.templateadm')
@section('section')
    <section class='content-header'>
        <h1> ATM<small>Control panel</small></h1>
        <ol class='breadcrumb'>
            <li><a href='#'><i class='fa fa-dashboard'></i>editar </a></li>
            <li class='active'>ATM</li>
        </ol>
    </section>
    <section class='content'>
        <div class='row'>
            <div class='col-xs-12'>
                <div class='box'>
                    <div class='box-body'>
                            <div class="row">
                                        <center> Lista de verificables de gestion del <b>
                                            Sr. Miguel Lopez  Vega  </b>, Responsable ATM del Distrito : <b>Abancay</b>
                                              <a href="{{ url('admin/listaatm')}}" class="btn btn-danger btn-sm" ><i class="fa fa-arrow-left"></i> Regresar</a>
                                        </center>
                                        <br>
                    <div class="col-md-4">
                        <div class="box box-info">
                            <div class="box-body">
                            <div class="form-group">
                                <label for="ordMunicipal">   N° Ordenanza Municipal</label>
                                <br><button class="btn btn-success btn-sm"><i class="fa fa-download"></i> Descargar </button>
                            <div class="form-group" >
                                <label for="nroRaContrato"> N° RA designación, o N° contrato</label>
                               <br> No tiene Verificable
                            </div>

                            <div class="form-group" >
                                <label for="ordenanzaJass"> Ordenanza que crea el registro de organizaciones comunales (JASS)
                                    </label>
                                    <br><button class="btn btn-success btn-sm"><i class="fa fa-download"></i> Descargar </button>                           </div>

                            <div class="form-group" >
                                <label for="ordenanzaAnemia"> Ordenanza municipal que aprueba lucha contra la anemia
                                    </label>
                                <br><button class="btn btn-success btn-sm"><i class="fa fa-download"></i> Descargar </button>                           </div>
                        </div>
                    </div>
                </div>
                </div>
                <div class="col-md-4">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="resolucionPerfil"> N° Resolución que aprueba perfil ATM</label>
                                 <br><button class="btn btn-success btn-sm"><i class="fa fa-download"></i> Descargar </button>
                               <br> <label for="resolucionAlcaldia">   N° Resolución Alcaldía</label>
                                   <br><button class="btn btn-success btn-sm"><i class="fa fa-download"></i> Descargar </button>
                                <br><label for="depende"> Área a la que depende ATM</label>

                                <br><button class="btn btn-success btn-sm"><i class="fa fa-download"></i> Descargar </button>

                                <div class="form-group" >
                                    <label for="otrasOrdMun"> Otra Ordenanza municipales :
                                        </label>
                                       <br><button class="btn btn-success btn-sm"><i class="fa fa-download"></i> Descargar </button>                                       </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="box box-danger">
                        <div class="box-body">
                            <div class="form-group" >
                                <label for="ordenanzaCloro"> Ordenanza municipal que aprueba el Protocolo de abastecimiento de cloro aprobado :
                                    </label>
                                   <br><button class="btn btn-success btn-sm"><i class="fa fa-download"></i> Descargar </button>                           </div>

                            <div class="form-group" >
                                <label for="">  Condición Laboral : </label>
                                    <br>
                                No tiene verificable
                                <br>
                                <label  id="numCargos1"> otros cargos </label>

                                <br><button class="btn btn-success btn-sm"><i class="fa fa-download"></i> Descargar </button>
                                </label>
                            </div>
                            <div class="form-group" >
                                    <label  id="planSupervision">Plan de supervisión :
                                           </label>
                                           <br><button class="btn btn-success btn-sm"><i class="fa fa-download"></i> Descargar </button>

                            </div>

                    </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</div>
</div>
</div>
</div>
</section>
 @endsection
