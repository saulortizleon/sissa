<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SISA / LOGIN</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('admin/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('admin/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('admin/bower_components/Ionicons/css/ionicons.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('admin/dist/css/AdminLTE.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('admin/plugins/iCheck/square/blue.css')}}">

    <!-- Form validation -->
    <link rel="stylesheet" href="{{asset('formvalidation/formValidation.min.css')}}">

    
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<style type="text/css">
    #login {
        margin: 0;
        width: 100%;
        height: 100vh;
        background: linear-gradient(-45deg, #06418d, #85b9fd, #06418d, #329cff);
        background-size: 400% 400%;
        animation: gradiente 15s ease infinite;
    }
@keyframes gradiente {
	0% {
		background-position: 0% 50%;
	}
	50% {
		background-position: 100% 50%;
	}
	100% {
		background-position: 0% 50%;
	}
}

</style>
<body class="hold-transition login-page"  id="login" >
    <div class="login-box">
        <div class="login-box-body">
         
            <center>
            <img width="25%" align="center" id="logo2" src="{{ asset('/logos/2.png') }}" alt="">
           
            </img>
            <p style="font-size: 20px; font-weight: bold;  color:#00afef;">
                DRVCS APURÍMAC   
            </p> </center>
        <p class="login-box-msg ">
            <CENTER><b style="font-size: 26px; color:red;">
               <i class="fa fa-user"></i> INICIO DE SESIÓN
            </b></CENTER>
            <br>
        <form id="frmLogin" action="{{url('login')}}" method="post">
            {{csrf_field()}}
            <div class="form-group has-feedback">
                <input  type="text"  name="dni" id="dni"  class="form-control input-lg"placeholder="DNI">
            </div>
            <div class="form-group has-feedback">
                 <input type="password" name="contrasena" id="contrasena"  class="form-control input-lg"  placeholder="CONTRASEÑA">
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <input type="submit" value="INGRESAR" class="btn btn-primary form-control input-lg" style="width: 100%; font-size: 18px;">
                </div>
            </div>
            <br>
            @if(Session::has('message-correct'))
              <label class=" input-lg"  style="color:green;"><i class="fa fa-check"></i>{{Session::get('message-correct')}} </label>
            @elseif(Session::has('message-error'))
             <label  class=" input-lg" style="color:red;"><i class="fa fa-exclamation-triangle"></i> {{Session::get('message-error')}}</label>
            @endif

        </form>
        <center> <a style="font-size: 18px" href="#ACCESO">Recuperar contraseña</a> <br>
            <a style="font-size: 18px" href="#ACCESO">Manual de uso</a></center>   

      </div>
    </div>

    <!-- jQuery 3 -->
    <script src="{{asset('admin/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{asset('admin/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- iCheck -->
    <script src="{{asset('admin/plugins/iCheck/icheck.min.js')}}"></script>

    <script src="{{asset('formvalidation/formValidation.min.js')}}"></script>
    <script src="{{asset('formvalidation/bootstrap.validation.min.js')}}"></script>
    <script>
        $(function () {
            $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' /* optional */
            });
        });
    </script>
</body>
</html>