@extends('layouts.templateadm')
@section('section')
    <section class='content-header'>
        <h1> Formación<small>ATM</small></h1>
        <ol class='breadcrumb'>
            <li><a href='#'><i class='fa fa-dashboard'></i>editar </a></li>
            <li class='active'>Copias</li>
        </ol>
    </section>
    <section class='content'>
        <div class='row'>
            <div class='col-xs-12'>
                <div class='box'>
                    <div class='box-body'>
                            <div class="row">
                            <center> Realizar copias de seguridad a la fecha
                                <a href="{{url('admin/listaatm')}}" class="btn btn-danger btn-sm" ><i class="fa fa-arrow-left"></i> Regresar</a>
                            </center>
                            <br>
                                <div class="col-md-4">
                                    <div class="box box-info">
                                        <div class="box-body">
                                            <div class="form-group">
                                                <label>
                                                    <input type="checkbox" checked> Hacer copias de seguridad Autómaticamente
                                                  </label>
                                                  <label for="">Nombre de servidor</label>
                                                  <input  type="text" placeholder="0" id="SAPlimitado" name="prueba"  class="form-control"  class="minimal" value="200.168.1.89" >
                                                  <label for="">Dominio</label>
                                                  <input  type="text" placeholder="0" id="SAPlimitado" name="prueba"  class="form-control"  class="minimal" value="drvcs.regionapurimac.gob.pe">
                                                  <label for="">Fecha</label>
                                                  <input  type="date" placeholder="0" id="SAPlimitado" name="prueba"  class="form-control"  class="minimal">

                                                  <label for="">Base de datos</label>
                                                  <input  type="text" placeholder="0" id="SAPlimitado" name="prueba"  class="form-control"  class="minimal" value="BD_SISA">
                                                  <br>
                                                  <button class="btn btn-success btn-sm">Generar backup</button>
                                              </div>
                                </div>

                            </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
