@extends('layouts.templateblog')
@section('section')

<div class="row">
    <div class="col-lg-9">
        <br>
         <center><h2 class="azul">ENTREGA DE INSUMO CRÍTICOS (HIPOCLORITO DE CALCIO Y dpd01) A CENTROS POBLADOS DE QUINTIL 1 Y 2. 

        </h2> 
        </center>
        <p align="justify" style="font-size: 18px">
            En el marco de la política de reducción de la anemia, desnutrición crónica infantil y acceso a los servicios públicos de calidad y sostenibles, en cumplimiento a los compromisos de gestión asumidos con el FED se realiza la entrega de Insumos Críticos (cloro y pastillas dpd01), para garantizar el consumo de agua clorada.

            </p>  
            <center><h6 class="azul">RESULTADOS 2020
            </h6> 
            </center>
            <p>
            <strong>BENEFECIARIOS:</strong>   23662 habitantes <br>
            <strong>    ALCANCE: </strong>  41 distritos con 161 centros poblados en pobreza y extrema pobreza de quintil 1 y 2.  
            </p>
            
            
            <center><h6 class="azul"> NOTICIAS
            </h6> 
            </center>  
            <div class="row">            
                @foreach($entrega as $item)
                <div class="col-lg-4 col-sm-4 portfolio-item">                  
                    <div class="card my-4">
                        <div class="card-body">
                            <a href="#"><img class="card-img-top" src="{{asset($item->imagen)}}" alt=""></a>
                            <div class="card-body">
                                <h6 class="card-title azul" >
                                <a href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}">{{$item->titulo}}</a>
                                </h6>
                                <p class="card-text" style="font-size:14px;  ">{{$item->subtitulo}}</p>
                                <a class="btn btn-link" href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}">
                                <i class="fa fa-external-link-square"></i> Leer más</a>
                            </div>
                        </div>
                    </div>                  
                </div>
                @endforeach
            </div>
    </div>
    <div class="col-lg-3">
      @include('vp/col2')     
    </div>
</div>
<br><br>
    <style type="text/css">
        #index{ display:none}
    </style>
@endsection