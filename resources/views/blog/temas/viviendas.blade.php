@extends('layouts.templateblog')
@section('section')
<br>
<div class="row">
    <div class="col-lg-9">
        <br>
        <div class="row">
            <div class="col-lg-12">
                    <center><h1 class="azul"> IMPLEMENTACIÓN DE VIVIENDAS RURALES SALUDABLES EN LA REGIÓN DE APURÍMAC  
                    </h1> 
                    </center>
                    <p align="justify" style="font-size: 20px">
                        El Gobierno Regional de Apurímac a través de la Dirección Regional de Vivienda, Construcción y Saneamiento (DRVCS),
                         realizó proyectos pilotos en Educación Sanitaria Ambiental (EDUSA), denominado “Implementación de Viviendas Saludables Rurales”, 
                         como <b>nueva estrategia para lograr calidad y sostenibilidad de los servicios de agua y saneamiento</b>  en el ámbito rural, 
                         promoviendo estilos de vida saludables de 214 familias con cocinas mejoradas, lavaderos con agua caliente, alacenas y 
                         conservadoras ecológicas, en el centro poblado de Nuevo Marcobamba del distrito de Tumay Huaraca, Andahuaylas. 
                         Centros poblados de Patapata y Huichihua del Distrito de Chuquibambilla, Grau; y el centro poblado de Chuñohuacho 
                         de la provincia de Antabamba. </p>
                      <center> <img class="img-fluid" src="{{ asset('img/institucion/proyecto-viviendas-saludables-apurimac.jpg') }}" width="100%" height="auto" >
                      </center> 
                      <p align="justify" style="font-size: 20px">
                        La implementación de viviendas saludables rurales se realizó mediante un convenio tripartito (GORE, Gobiernos local y 
                        comunidad beneficiaria) en 04 centros poblados de la región de Apurímac con un presupuesto total de S/ 705,147.62 beneficiando a un total de 214 familias.
                        Asegurar el consumo de agua clorada  y promover estilos de vida saludable para el adecuado desarrollo infantil temprano en familias con niños menores de 5 años y en etapa escolar.
                        Instalación de cocinas mejoradas a leña, lavaderos con instalación de agua fría y caliente, alacenas y conservadoras ecológicas. Asistencia técnica en el enlucido con barro mejorado de las fachadas y el interior de los ambientes de las cocinas, ordenamiento del predio y elaboración de planes de negocio para el pago de la cuota familiar.
                      </p> 
                      <p align="justify" style="font-size: 20px">
                       <b> Objetivo:</b> Asegurar el consumo de agua clorada  y promover estilos de vida saludable para el adecuado desarrollo infantil temprano 
                        en familias con niños menores de 5 años y en etapa escolar. 
                    </p>
                        <p align="justify" style="font-size: 20px"><b> Actividades: </b>Instalación de cocinas mejoradas a leña, lavaderos con instalación de agua fría y caliente, alacenas y conservadoras ecológicas. Asistencia técnica en el enlucido con barro mejorado de las fachadas y el interior de los ambientes de las cocinas, ordenamiento del predio y elaboración de planes de negocio para el pago de la cuota familiar.

                      </p>
                       <p align="justify" style="font-size: 20px">
                      <center> <i style="font-size: 20px"> La aceptación poblacional del proyecto <b>nos impulsa a seguir trabajando</b>  en la búsqueda de estrategias para la sostenibilidad y mejora de la 
                      calidad de vida del poblador apurimeño.</i> </center>
                     </p>                     
            </div>
 
            <div class="col-lg-12">
            <div class="row">
              @foreach($noticias as $item)
              <div class="col-lg-4 col-sm-4 portfolio-item">                  
                  <div class="card my-4">
                      <div class="card-body">
                          <a href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}" ><img class="card-img-top" src="{{asset($item->imagen)}}" alt="vivienda apurimac" ></a>
                          <div class="card-body">
                              <h6 class="card-title azul" >
                              <a href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}">{{$item->titulo}}</a>
                              </h6>
                              <p class="card-text" style="font-size:14px;  ">{{$item->subtitulo}}</p>
                              <a class="btn btn-link" href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}">
                              <i class="fa fa-external-link-square"></i> Leer más</a>
                          </div>
                      </div>
                  </div>                  
              </div>
              @endforeach
          </div></div>
        </div>
    </div>
    <div class="col-lg-3">
      @include('vp/col2')
     
    </div>
</div>
<br><br>
    <style type="text/css">
        #index{ display:none}
    </style>
@endsection