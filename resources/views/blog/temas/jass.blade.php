@extends('layouts.templateblog')
@section('section')
        <br>
    <center> <h2 class="azul">Juntas Administradoras de Servicios de Agua y Saneamiento</h2></center>
<hr>
    <div class="row">
        @foreach($noticias as $item)
      <div class="col-lg-4 col-sm-6 portfolio-item">
        <div class="card h-100">
            <a href="#"><img class="card-img-top" src="{{asset($item->imagen)}}" alt=""></a>
          <div class="card-body">
            <h6 class="card-title">
                <a href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}">{{$item->titulo}}</a>
            </h6>
            <p class="card-text" style="font-size:14px">{{$item->subtitulo}}</p>

            <a class="btn btn-link" href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}"><i class="fa fa-external-link-square"></i> Leer nota de prensa</a>
              </div>
        </div>
      </div>
      @endforeach
    </div>
    <style type="text/css">
      #index{ display:none; }
  </style>
@endsection