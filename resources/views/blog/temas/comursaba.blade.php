@extends('layouts.templateblog')
@section('section')
<br>
<div class="row">
    <div class="col-lg-9">
        <br>
        <div class="row">
            <div class="col-lg-4">
                    <center> <img class="img-fluid" src="{{ asset('img/institucion/comursaba.jpg') }}" width="100%" height="auto" >
                    </center> <br>
            </div>
            <div class="col-lg-8">
                    <center><h1 class="azul"> COMURSABA  
                    </h1>
                    <h4 class="azul"><i> COMITÉ MULTISECTORIAL REGIONAL DE SANEAMIENTO BÁSICO - APURÍMAC
                    </i>
                    </h4>
                    <h6>Reconocida mediante
                      Resolución Ejecutiva Regional N° 584 – 2012-GR.AP/PR.
                     </h6>
                    </center>
                    <p align="justify" style="font-size: 18px">
                      Nuestras acciones conducen a toda promoción que posibilite el acceso de las poblaciones rurales a servicios 
                      de saneamiento básico dignos, sostenibles y de calidad fruto de estrechar la articulación de los diferentes
                       estamentos estatales de los niveles comunal, local, regional y nacional basados en políticas de inclusión social 
                       vigentes y con ello contribuir a disminuir la pobreza en la región Apurímac. 
                     </p>
                     <p align="justify" style="font-size: 18px">
                      Nuestras acciones conducen a toda promoción que posibilite el acceso de las poblaciones rurales a servicios 
                      de saneamiento básico dignos, sostenibles y de calidad fruto de estrechar la articulación de los diferentes
                       estamentos estatales de los niveles comunal, local, regional y nacional basados en políticas de inclusión social 
                       vigentes y con ello contribuir a disminuir la pobreza en la región Apurímac. 
                     </p> 
            </div>

        </div>
        <div class="row">
            <div class="col-4">
              <div class="list-group" id="list-tab" role="tablist">
                <a class="list-group-item list-group-item-action active"
                id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home"><strong> OBJETIVO GENERAL </strong></a>
                <a class="list-group-item list-group-item-action"
                id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile"><strong>OBJETIVOS ESPECÍFICOS</strong></a>
                <a class="list-group-item list-group-item-action"
                id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages"><strong>PERSPECTIVAS</strong></a>
                <a class="list-group-item list-group-item-action"
                id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings"><strong>INTEGRANTES</strong></a>
                
              </div>
            </div>
            <div class="col-8">
              <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
                  <center>
                    <h3 class="azul">OBJETIVO GENERAL  </h3>
                 </center>
                  <p align="justify" style="font-size: 18px">
                   Constituirse en una red de interesados en fortalecer la incorporación de la gestión del agua y saneamiento
                   bajo la estrategia integral y sostenible en los procesos de desarrollo del ámbito prioritariamente rural del Gobierno Regional 
                   de Apurímac, a través del análisis de problemas comunes, el intercambio de experiencias y conocimientos, el fortalecimiento de 
                   sus propios procesos de gestión y el impulso a iniciativas de cooperación entre los diferentes actores con competencias en materia 
                   de agua y saneamiento; en la perspectiva de aportar a la reducción de EDA y desnutrición infantil, a través de la reducción de las
                    brechas al acceso de agua y saneamiento rural existentes en el ámbito del Gobierno Regional de Apurímac. 
                  </p>
                    </div>
                <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
                 
                  <center><h3 class="azul">OBJETIVOS ESPECÍFICOS</h3></center>  
                  
                   1.	Apoyar y fortalecer procesos de gestión del agua y saneamiento desde los gobiernos locales y del gobierno regional y otras instituciones comprometidas con el saneamiento, facilitando y promoviendo procesos de comunicación y concertación entre los actores sociales involucrados que conduzcan a la toma de decisión para la concreción y aplicación de políticas de gestión de agua y saneamiento rural bajo la estrategia integral y sostenible.
                   <br> <br>  2.	Promover, apoyar y facilitar procesos para la institucionalización e implementación de políticas públicas regionales y  locales en agua y saneamiento bajo estrategias integrales y sostenibles.
                   <br> <br>  3.	promover y fortalecer la articulación entre los sectores con competencias en materia de agua y saneamiento.
                   <br> <br>   4.	Promover, facilitar y apoyar la priorización del agua y saneamiento en las agendas de política del gobierno regional y los gobiernos locales de Apurímac.
                   <br> <br>   5.	Promover, apoyar y facilitar la adopción  de hábitos de higiene saludables para de ese modo aportar en la reducción de enfermedades diarreicas agudas EDAS, de la desnutrición en pro de mejorar la calidad de vida de la población más vulnerable.
                   <br> <br>    6.	Promover el fortalecimiento de capacidades institucionales a través de la difusión/socialización de experiencias, estrategias, herramientas, instrumentos  de intervención en agua y saneamiento, que aporten a la sostenibilidad de las inversiones del sector.
                   <br> <br>   7.	Promover el acceso a información relevante para la toma de decisiones a partir de la formulación del Diagnóstico, Línea de Base y Plan Regional de Saneamiento Básico
                   <br> <br>    8.	Promover la participación y el compromiso del sector público, privado, organismos no gubernamentales,  de la cooperación internacional y de la población organizada en las acciones de Saneamiento Básico que se ejecuten en el ámbito regional.
                   <br> <br>   9.	Propiciar la incorporación de proyectos de Saneamiento Básico en el presupuesto regional,  local e institucional para garantizar su desarrollo
                     
              
                    </div>
                <div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">
                   <center><h3 class="azul">PERSPECTIVAS</h3></center>  
                  <i class="fa fa-tint"></i>         Ser una plataforma de incidencia para la toma de decisiones en agua y saneamiento.

                  <br> <br> <i class="fa fa-tint"></i> Lograr el liderazgo del Gobierno Regional a través de la DRVCS en la gestión de los servicios de Agua y Saneamiento.
                  
                  <br> <br> <i class="fa fa-tint"></i>  Continuar fortaleciendo capacidades y consolidar la participación de los diferentes actores de la COMURSABA.
                  
                  <br> <br> <i class="fa fa-tint"></i>  Lograr el Incremento de inversiones a nivel regional y local para proyectos de agua y saneamiento. 
                  
                  <br> <br> <i class="fa fa-tint"></i>  Contribuir a la reducción de la anemia, DCI y brechas de pobreza.
                </div>
                <div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">En construcción</div>
              </div>
            </div>
        </div>
        <br>
        <center><h6 class="azul"> NOTICIAS
        </h6> 
        </center> 
        <div class="row">
          @foreach($noticias as $item)
          <div class="col-lg-4 col-sm-4 portfolio-item">                  
              <div class="card my-4">
                  <div class="card-body">
                      <a href="#"><img class="card-img-top" src="{{asset($item->imagen)}}" alt=""></a>
                      <div class="card-body">
                          <h6 class="card-title azul" >
                          <a href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}">{{$item->titulo}}</a>
                          </h6>
                          <p class="card-text" style="font-size:14px;  ">{{$item->subtitulo}}</p>
                          <a class="btn btn-link" href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}">
                          <i class="fa fa-external-link-square"></i> Leer más</a>
                      </div>
                  </div>
              </div>                  
          </div>
          @endforeach
      </div>
    </div>
    <div class="col-lg-3">
      @include('vp/col2')      
    </div>
</div>
<br><br>
    <style type="text/css">
        #index{ display:none}
    </style>
@endsection