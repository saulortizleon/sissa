@extends('layouts.templateblog')
@section('section')

<div class="row">
    <div class="col-lg-9">
        <br>
         <center><h2 class="azul">VALORES ARANCELARIOS DE  PLANOS PREDIALES DE LA  REGIÓN DE APURÍMAC
        </h2> 
        </center>
        <p align="justify" style="font-size: 18px">
          La Dirección Regional de Vivienda, Construcción y Saneamiento, en cumplimiento de sus funciones en materia de Vivienda y Urbanismo,
           pone a disposición los valores arancelarios de planos prediales aprobados para el año 2022 de acuerdo a las normas técnicas vigentes, 
           correspondiente a los distritos de Abancay, Anco Huallo, Andahuaylas, Chalhuanca, Chincheros, Curahuasi, San Jeronimo y Talavera.
            </p>  
            <center><h6 class="azul">¿QUÉ ES EL ARANCELAMIENTO?
            </h6> 
            </center>
            <p>
            Es el proceso de la formulación de los valores arancelarios y su aplicación en la valorización arancelaria de terrenos urbanos y
            rústicos en todo el país, proporcionando a los gobiernos provinciales y distritales los instrumentos técnico legales que son 
            utilizados como un componente para determinar el pago del impuesto predial, con una recaudación ordenada de los tributos acorde a 
            las mejoras en obras de infraestructura que se ejecutan anualmente, logrando de este modo un beneficio directo en la revalorización 
            del entorno urbano y consecuentemente de la propiedad privada, generando bienestar y mejor calidad de vida de los administrados.
            <i> Fuente: <a href="https://sites.google.com/vivienda.gob.pe/dgprvu-dudu/pagina_principal/area_de_valores?authuser=0" 
                target="_blank" rel="noopener noreferrer"> MVCS</a></i>    
            </p>
            
            <center><h6 class="azul">  ¿CÓMO ACCEDO A LOS PLANOS PLANOS PREDIALES ARANCELARIOS DE APURÍMAC PARA EL AÑO 2021?
            </h6> 
            </center>        
            <p>
            <br><i class="fa fa-arrow-circle-right"></i> Presentar oficio debidamente firmado por la entidad solicitante, adjuntando la boleta de pago.
            <br><strong> Más información llamar al   (083) 322837  </strong>      
            </p>
            <br> 
            <center><h6 class="azul"> NOTICIAS
            </h6> 
            </center>  
            <div class="row">            
                @foreach($planos as $item)
                <div class="col-lg-4 col-sm-4 portfolio-item">                  
                    <div class="card my-4">
                        <div class="card-body">
                            <a href="#"><img class="card-img-top" src="{{asset($item->imagen)}}" alt=""></a>
                            <div class="card-body">
                                <h6 class="card-title azul" >
                                <a href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}">{{$item->titulo}}</a>
                                </h6>
                                <p class="card-text" style="font-size:14px;  ">{{$item->subtitulo}}</p>
                                <a class="btn btn-link" href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}">
                                <i class="fa fa-external-link-square"></i> Leer más</a>
                            </div>
                        </div>
                    </div>                  
                </div>
                @endforeach
            </div>
    </div>
    <div class="col-lg-3">
      @include('vp/col2')     
    </div>
</div>
<br><br>
    <style type="text/css">
        #index{ display:none}
    </style>
@endsection