@extends('layouts.templateblog')
@section('section')
<br>
<div class="row">
    <div class="col-lg-9">
      <br>
      <div class="row">
          <div class="col-lg-4">
                  <center> <img class="img-fluid" src="{{ asset('img/institucion/escuela-saneamiento-yaku-kawsay.png') }}" width="80%" height="auto" >
                  </center> <br>
          </div>
          
          <div class="col-lg-8">
                  <center><h1 class="azul"><small> Escuela de Saneamiento</small>"Yaku Kawsay"
                  </h1>
                  <h2 class="azul"><i>  Agua para el buen vivir</i>
                  </h2>
                  </center>
                  <p align="justify" style="font-size: 20px">
                    La Dirección Regional de Vivienda, Construcción y Saneamiento de Apurímac en el marco de la política regional y nacional de <b>promover el acceso universal a los servicios de saneamiento con calidad y sostenibles, priorizando la cobertura de las zonas rurales rurales de nuestra región con tecnologías adecuadas para el abastecimiento de agua, saneamiento y tratamiento de aguas residuales; </b>implementa la iniciativa denominada Escuela de Saneamiento “Yaku Kawsay” - Agua para el buen vivir desde el año 2020, con la finalidad de formar personal especializado que dispongan de los conocimientos, habilidades y destrezas necesarias para brindar servicios de agua y saneamiento a satisfacción de los usuarios en el ámbito rural de la región de Apurímac, garantizando de esta manera el acceso a agua clorada para reducir los indicadores de  anemia y desnutrición crónica infantil  en niños menores de 5 años. 
Las clases se desarrollan de manera virtual con la metodología de “clase invertida” en el contexto de la emergencia sanitaria por la COVID-19,  en alianza estratégica con los 84 gobiernos locales de la región,  mediante 4 especialidades técnicas:

                  </p>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-sm-6 portfolio-item">
            <div class="card h-100 text-center">
              
              <div class="card-body bg-light">
                <h4 class="card-title azul">
                1. Especialidad Técnica para responsables de Áreas Técnicas Municipales
                </h4>
                
              </div>
              <a href="#"><img class="card-img-top" src="{{ asset('img/atm.jpg') }}" alt=""></a>
            </div>
          </div>
          <div class="col-lg-6 col-sm-6 portfolio-item">
            <div class="card h-100 text-center">
              
              <div class="card-body bg-light">
                <h4 class="card-title azul">
               2. Especialidad Técnica para Operadores Rurales y Gasfiteros
                </h4>
               
              </div>
              <a href="#"><img class="card-img-top" src="{{ asset('img/gasfiteros.jpg') }}" alt=""></a>
            </div>
          </div>
          <div class="col-lg-6 col-sm-6 portfolio-item">
            <div class="card h-100 text-center">
            
              <div class="card-body bg-light">
                <h4 class="card-title azul">
                  3. Especialidad Técnica para mujeres líderes de  centros poblados
                </h4>
                
              </div>
              <a href="#"><img class="card-img-top" src="{{ asset('img/mujeres-lideres.jpg') }}" alt=""></a>
            </div>
          </div>
          <div class="col-lg-6 col-sm-6 portfolio-item">
            <div class="card h-100 text-center">          
            <div class="card-body bg-light" >
              <h4 class="card-title azul">
                 4. Especialidad Técnica para prestadores de agua y saneamiento (JASS)
              </h4>             
            </div>
            <a href="#"><img class="card-img-top" src="{{ asset('img/institucion/jass-apurimac.jpg') }}" alt=""></a>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-12">
          <div class="card mt-3 tab-card">
              <div class="card-header tab-card-header">
                  <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                      <li class="nav-item" class="active">
                          <a class="nav-link active" id="one-tab" data-toggle="tab"
                           href="#one" role="tab" aria-controls="One" aria-selected="true">
                           <i class="fa fa-tint"></i> <STRONg>Brochure Responsables ATM</STRONg> </a>
                      </li>
                      <li class="nav-item" >
                      <a class="nav-link"  id="two-tab" data-toggle="tab"
                      href="#two" role="tab" aria-controls="Two" aria-selected="false">
                      <i class="fa fa-tint"></i> <STRONG>Brochure Operadores Rurales</STRONG>  </a>
                      </li>
                      <li class="nav-item" >
                        <a class="nav-link"  id="tree-tab" data-toggle="tab"
                        href="#tree" role="tab" aria-controls="Tree" aria-selected="false">
                        <i class="fa fa-tint"></i> <STRONG>Brochure Mujeres Líderes</STRONG>  </a>
                        </li>
                       
                  </ul>
              </div>
              <div class="tab-content" id="myTabContent">
                  <div class="tab-pane fade show active p-3" id="one" role="tabpanel" aria-labelledby="one-tab">
                      <div class="embed-responsive embed-responsive-16by9">
                          <embed class="embed-responsive-item"  src="{{ url('doc/responsables-atm.pdf') }}"
                          type="application/pdf">
                      </div>
                  </div>
                  <div class="tab-pane fade  p-3" id="two" role="tabpanel" aria-labelledby="two-tab" class="tab-pane">
                    <div class="embed-responsive embed-responsive-16by9">
                      <embed class="embed-responsive-item"  src="{{ url('doc/operadores-rurales.pdf') }}"
                      type="application/pdf">
                    </div>
                  </div>
                  <div class="tab-pane fade  p-3" id="tree" role="tabpanel" aria-labelledby="tree-tab" class="tab-pane">
                    <div class="embed-responsive embed-responsive-16by9">
                      <embed class="embed-responsive-item"  src="{{ url('doc/mujeres-lideres.pdf') }}"
                      type="application/pdf">
                    </div>
                  </div>
                  <div class="tab-pane fade  p-3" id="tree" role="tabpanel" aria-labelledby="tree-tab" class="tab-pane">
                   <i>En construcción</i>
                  </div>
                  <div class="tab-pane fade  p-3" id="four" role="tabpanel" aria-labelledby="four-tab" class="tab-pane">
                    <i>En construcción</i>
                   </div>
              </div>
          </div>
      </div>
    </div>
    <br>
    <center><h3 class="azul"> NOTICIAS
    </h3> 
    </center> 
        <div class="row">
          @foreach($escuela as $item)
          <div class="col-lg-4 col-sm-4 portfolio-item">                  
              <div class="card my-4">
                  <div class="card-body">
                      <a href="#"><img class="card-img-top" src="{{asset($item->imagen)}}" alt=""></a>
                      <div class="card-body">
                          <h6 class="card-title azul" >
                          <a href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}">{{$item->titulo}}</a>
                          </h6>
                          <p class="card-text" style="font-size:14px;  ">{{$item->subtitulo}}</p>
                          <a class="btn btn-link" href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}">
                          <i class="fa fa-external-link-square"></i> Leer más</a>
                      </div>
                  </div>
              </div>                  
          </div>
          @endforeach
      </div>
  </div>
    <div class="col-lg-3">
      @include('vp/col2')      
    </div>
</div>
<br>
    <style type="text/css">
        #index{ display:none}
    </style>
@endsection