@extends('layouts.templateblog')
@section('section')
<br>
<div class="row">
  <div class="col-lg-9">
      <br>
      <center><h1 class="azul">VERIFICAR CERTIFICADOS DIGITALES</h1>
      <p style="font-size: 18px">Esta servicio tiene por finalidad proveer un mecanismo en linea para verificar la autenticidad de los certificados virtuales
          emitidos por la <b>Dirección Regional de Vivienda, Construcción y Saneamiento de Apurímac.</b> </p></center>
          <div class="row">
            <div class="col-lg-12">
              <div class="card mt-3 tab-card">
                <div class="card-header tab-card-header">
                  <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                    <li class="nav-item" class="active">
                        <a class="nav-link active" id="one-tab" data-toggle="tab" href="#one"
                        role="tab" aria-controls="One" aria-selected="true"><i class="fa fa-tint"></i> <strong> Responsables ATM  </strong> </a>
                    </li>
                    <li class="nav-item" >
                        <a class="nav-link " id="three-tab" data-toggle="tab" href="#three"
                        role="tab" aria-controls="Three" aria-selected="false"><i class="fa fa-wrench"></i><strong> Operadores Rurales   </strong> </a>
                    </li>
                    <li class="nav-item" >
                      <a class="nav-link"  id="two-tab" data-toggle="tab" href="#two"
                      role="tab" aria-controls="Two" aria-selected="false"><i class="fa fa-home"></i><strong> Webinars Wasichakuy  </strong> </a>
                    </li>
                  </ul>
                </div>
                <div class="tab-content" id="myTabContent">
                  <div class="tab-pane fade show active p-3" id="one" role="tabpanel" aria-labelledby="one-tab">
                    <form action="verificar-certificados" method="get">
                    <div class="row">
                        <div class="col-md-6">
                          <center> <h5 class="azul">Escuela de Saneamiento "Yaku Kawsay" - Agua para el buen vivir  2020</h5>
                            <p>Certificado de la Especialidad Técnica para  Responsables de Áreas Técnicas Municipales</p>
                            <div class="form-group">
                                <strong><label for="dni"> INGRESE DNI</label></strong> </strong>
                                <input type="number" class="form-control form-control-lg" id="atm" name="atm" maxlength="8" minlength="8" >
                            </div>
                            <div class="form-group">
                              <button type="submit" id="submit1" class="btn btn-primary"><b><i class="fa fa-check"></i> Consultar certificado</b> </button>
                              <a type="button" class="btn btn-danger" href="javascript:location.reload()"><i class="fa fa-close"></i> <b>Nueva Consulta</b> </a>
                            </div></center>
                        </div>
                        <div class="col-md-6">
                            <div class="card-body border-right-2">
                              <h6 class="azul"> <i>Resultado :</i>  </h6>

                                <div id="resultadoatm" >

                                </div>

                            </div>
                        </div>
                    </div>
                  </form>
                  </div>
                  <div class="tab-pane fade  p-3" id="two" role="tabpanel" aria-labelledby="two-tab" class="tab-pane active">
                    <div class="row">
                        <div class="col-md-6">
                          <center> <h5 style="color:red; font-weight: bold">Maraton de capacitaciones "Wasichakuy" - Construyendo mi vivienda 2020  </h5>
                            <p>Certificado de participación en los webinars desarrollados</p>
                            <div class="form-group">
                              <strong><label for="dni"> INGRESE NOMBRES Y APELLIDOS</label></strong> </strong>
                              <input type="text" class="form-control form-control-lg" id="was" name="was" style="text-transform:uppercase" >
                            </div>
                            <div class="form-group">
                              <button type="submit" id="submit3" class="btn btn-primary"><b><i class="fa fa-check"></i> Consultar certificado</b> </button>
                              <a type="button" class="btn btn-danger" href="javascript:location.reload()"><i class="fa fa-close"></i> <b>Nueva Consulta</b> </a>
                            </div></center>
                        </div>
                        <div class="col-md-6">
                            <div class="card-body border-right-2">
                              <h6 style="color:red; font-weight: bold"> <i>Resultado :</i>  </h6>
                                <div id="resultadowasi" >

                                </div>

                            </div>
                        </div>
                    </div>
                  </div>
                  <div class="tab-pane fade  p-3" id="three" role="tabpanel" aria-labelledby="three-tab">
                    <form action="verificar-  " method="get">
                    <div class="row">
                        <div class="col-md-6">
                          <center> <h5 style="color:green; font-weight: bold">Escuela de Saneamiento "Yaku Kawsay" - Agua para el buen vivir 2020</h5>
                            <p>Certificado de la Especialidad Técnica para Operadores Rurales y Gasfiteros</p>
                            <div class="form-group">
                                <strong><label for="dni"> INGRESE NOMBRES Y APELLIDOS</label></strong> </strong>
                                <input type="text" class="form-control form-control-lg" id="ope" name="ope" style="text-transform:uppercase" >
                            </div>
                            <div class="form-group">
                              <button type="submit" id="submit2" class="btn btn-success"><b><i class="fa fa-check"></i> Consultar certificado</b> </button>
                              <a type="button" class="btn btn-danger" href="javascript:location.reload()"><i class="fa fa-close"></i> <b>Nueva Consulta</b> </a>
                            </div></center>
                        </div>
                        <div class="col-md-6">
                            <div class="card-body border-right-2">
                              <h6 style="color:green;"> <i>Resultado :</i>  </h6>

                                <div id="resultadoope" >

                                </div>

                            </div>
                        </div>
                    </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
        </div>
  </div>
  <div class="col-lg-3">
    @include('vp/col2')
  </div>

</div>
<style type="text/css">
  #index{ display:none}
</style>

@endsection