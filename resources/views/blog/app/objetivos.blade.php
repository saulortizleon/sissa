@extends('layouts.templateblog')
@section('section')
<br>
<div class="row">
    <div class="col-lg-9">
        <div class="card my-4">
          <div class="card-body">
            <center><h1 class="azul"> OBJETIVOS </h1></center>
            <hr>
            <center><h4 style="color:#182d80 ;">Saneamiento</h4></center>
                <p>
                    <i class="fa fa-tint"></i> Contribuir a ampliar la cobertura y mejorar la calidad y sostenibilidad de los servicios de agua potable, alcantarillado, tratamiento de aguas servidas y disposición de excretas.
                    <br><i class="fa fa-tint"></i> Promover la sostenibilidad de los sistemas, la ampliación de la cobertura y el mejoramiento de la calidad de los servicios de saneamiento en el ámbito urbano y rural.
                </p>

                <center><h4 style="color:#182d80 ;">Vivienda</h4></center>
                <i class="fa fa-child"></i> Promover el mejoramiento de viviendas existentes en el ámbito urbano y rural.
                <br><i class="fa fa-child"></i> Coordinar, formular y/o conducir programas de asistencia técnica para el desarrollo de programas habitacionales dirigidos a las familias de menores recursos.
                <br><i class="fa fa-child"></i> Promover mecanismos de financiamiento para que la población pobre y extremadamente pobre pueda acceder a una vivienda.
                <p>
                    <center><h4 style="color:#182d80 ;"> Construcción</h4></center>

                    <i class="fa fa-wrench"></i> Promover, coordinar, formular y/o conducir programas de asistencia técnica para el desarrollo de programas habitacionales dirigidos a familias de menores recursos.
                    <br><i class="fa fa-wrench"></i> Incentivar la difusión y las capacitaciones tecnológicas con fines de innovación sobre normatividad y sistemas de construcción nuevas.
                    <br><i class="fa fa-wrench"></i> Estimular el estudio y mejoramiento de tecnologías constructivas tradicionales en lo que respecta a condiciones sismo resistente, de estabilización, confort, durabilidad y seguridad.
                    <br>
                </p>
                <center><h4 style="color:#182d80 ;"> Urbanismo</h4></center>

                <i class="fa fa-cog"></i> Contribuir gradualmente a la organización del terreno y consolidación del sistema urbano regional.
                <br><i class="fa fa-cog"></i> Apoyar a la elaboracion del plan de  Acondicionamiento Territorial de la Región de Apurímac en coordinación con los gobiernos locales.
                <br><i class="fa fa-cog"></i> Facilitar el acceso al suelo urbanizado para vivienda desalentando la ocupación informal.
                <br><i class="fa fa-cog"></i> Formular propuestas de acondicionamiento del espacio, zonificación economica - ecológica y programas puntuales de vivienda, equipamiento urbano y productivo para el desarrollo y consolidación del sistema urbano -territorial.
                <br><i class="fa fa-cog"></i> Brindar asistencia técnica a gobiernos locales en gestión de territorio.
                <br><i class="fa fa-cog"></i> Consolidar el proceso de integración física y social de los barrios urbanos marginales.
                <br><i class="fa fa-cog"></i> Identificar, proponer e impulsar; de manera coordinada y concertada, la dotación o complementación de infraestructura urbana.
                </p>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        @include('vp/col2')
    </div>
</div>
<style type="text/css">
    #index{ display:none}
  </style>
@endsection