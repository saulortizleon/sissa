@extends('layouts.templateblog')
@section('section')
<br>
<div class="row">
  <div class="col-lg-9">
      <br>
      <center>  <H1  style="color:#007bff; font-weight: bold;"><i class="fa fa-calculator"></i> YUPASUNCHIS CLOROTA </H1>
      <h6 style="font-weight: unset;">CALCULADOR DE CLORO PARA
        LA DESINFECCIÓN Y CLORACIÓN DE SISTEMAS DE AGUA POTABLE.</h6>   </center>
      <div class="row">
          <div class="col-12">
            <div class="card mt-3 tab-card">
              <div class="card-header tab-card-header">
                <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                  <li class="nav-item">
                      <a class="nav-link" id="one-tab" data-toggle="tab" href="#one"
                      role="tab" aria-controls="One" aria-selected="false">
                      <i class="fa fa-cog"></i> <STRONg> Desinfección</STRONg></a>
                  </li>
                  <li class="nav-item" class="active">
                      <a class="nav-link active"  id="two-tab" data-toggle="tab" href="#two"
                      role="tab" aria-controls="Two" aria-selected="true">
                      <i class="fa fa-tint"></i> <STRONg>Cloración</STRONg></a>
                  </li>
                </ul>
              </div>
              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade  p-3" id="one" role="tabpanel" aria-labelledby="one-tab">
                <div class="col-lg-12">
                    <div class="row">
                    <div class="col-md-6">
                          <div class="form-group col-sm-12">
                      <label for="tipo"> <i>Calcular para :</i>  </label>
                            <select style="font-weight: bolder;font-size:17px;" name="select" id="tipo" class="form-control form-control-sm" required="required"  >
                                <option value="1" selected>       Captación   </option>
                                <option value="2">       Reservorio   </option>
                                <option value="3">       Tuberías   </option>
                                <option value="4">       Cámaras de rompe presión   </option>
                            </select>
                          </div>
                          <form id="frmCap">
                              <div class="form-group col-sm-12">
                                  <label for="caudal">Ingrese  ancho</label>
                                  <input type="number" class="form-control  form-control-sm" id="altura1" placeholder="metros">
                              </div>
                              <div class="form-group col-sm-12">
                                  <label for="pcloro">Ingrese  largo</label>
                                  <input type="number" class="form-control form-control-sm" id="base1" placeholder="metros">
                              </div>
                              <div class="form-group col-sm-12">
                                  <label for="pcloro">Ingrese altura de agua</label>
                                  <input type="number" class="form-control form-control-sm" id="agua1" placeholder="metros">
                                  <br><button type="button" class="btn btn-danger" id="calcular3" class="btn btn-default"> Calcular </button>
                              </div>

                          </form>

                      <div class="form-group col-sm-12" id="radios" >
                          <div class="custom-control custom-radio"  id="controles">
                              <input type="radio" class="custom-control-input" id="rcu" onclick="show1()" name="x"  >
                              <label class="custom-control-label" for="rcu" value="cuadrado" >CIRCULAR</label>
                            </div>
                            <!-- Default checked -->
                            <div class="custom-control custom-radio">
                              <input type="radio" class="custom-control-input" id="rci" onclick="show2()" name="x" checked >
                              <label class="custom-control-label" for="rci" value="circular">CUADRADO</label>
                            </div>
                        </div>
                        <form id="frmRes">
                            <div class="form-group col-sm-8" >
                                <label for="caudal">Ingrese  ancho : </label>
                                <input type="number" class="form-control form-control-sm" id="altura" placeholder="metros">
                            </div>
                            <div class="form-group col-sm-8" >
                                <label for="pcloro">Ingrese  largo : </label>
                                <input type="number" class="form-control form-control-sm" id="base" placeholder="metros">
                            </div>
                          <div class="form-group col-sm-8" >
                                <label for="pcloro">Ingrese altura de agua : </label>
                                <input type="number" class="form-control form-control-sm" id="agua" placeholder="metros">
                                <br><button type="button" class="btn btn-warning" id="calcular1" class="btn btn-default"> Calcular </button>
                            </div>
                        </form>
                        <form id="frmCirculo">
                            <div class="form-group col-sm-8">
                                <label for="caudal">Ingrese Diámetro</label>
                                <input type="number" class="form-control form-control-sm" id="diametro" placeholder="metros">
                            </div>
                            <div class="form-group col-sm-8">
                                <label for="pcloro">Ingrese Altura</label>
                                <input type="number" class="form-control form-control-sm" id="alturaAgua" placeholder="metros">
                              <br> <button type="button" class="btn btn-warning" id="calcular2" class="btn btn-default"> Calcular </button>
                            </div>
                        </form>
                        <form id="frmTub">
                            <div class="form-group col-sm-8" >
                                <label for="alturat">Ingrese  longitud : </label>
                                <input type="number" class="form-control form-control-sm" id="largo" placeholder="en metros">
                            </div>
                            <div class="form-group col-sm-8" >
                                <label for="pcloro">Ingrese  diámetro : </label>
                                <input type="number" class="form-control form-control-sm" id="diat" placeholder="en pulgadas">
                                <br><button type="button" class="btn btn-success" id="calcularTubo" class="btn btn-default"> Calcular </button>
                              </div>
                        </form>
                        <form id="frmCam">
                            <div class="form-group col-sm-12">
                                <label for="caudal">Ingrese  ancho :</label>
                                <input type="number" class="form-control  form-control-sm" id="altura5" placeholder="metros">
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="pcloro">Ingrese  largo :</label>
                                <input type="number" class="form-control form-control-sm" id="base5" placeholder="metros">
                            </div>
                            <div class="form-group col-sm-12">
                                <label for="pcloro">Ingrese altura de agua : </label>
                                <input type="number" class="form-control form-control-sm" id="agua5" placeholder="metros">
                                <br><button type="button" class="btn btn-danger" id="calcular5" class="btn btn-default"> Calcular </button>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                    <div id="result1">
                      <div class="alert alert-warning" role="alert">
                        <span><b>Volumen del reservorio :  </b>  </span><br>  <span id="v1"> </span> litros    <br>
                        <span>o   </span> <span id="m31"> </span>m³  <br>
                        <span><b>Peso de cloro a disolver </b>  </span><br>  <span id="v11"> </span> gramos
                        <span><b>o </b> </span> <span id="kilos1"></span> kilógramos
                    </div>
                    </div>
                    <div id="result2">
                        <div class="alert alert-warning" role="alert">
                        <span><b>Volumen del reservorio circular :</b>  </span> <br> <span id="vm3circular"> </span> litros <br>
                          <span><b>o </b>  </span> <span id="vlitroscircular"> </span> m³   <br>
                        <span><b>Peso de cloro a disolver </b>  </span> <br> <span id="gramoscloro"> </span> gramos
                        <span><b>o </b> </span> <span id="kiloscloro"></span> kilógramos
                        </div>
                    </div>
                      <div id="result3">
                      <div class="alert alert-danger" role="alert">
                        <span><b>Volumen de la captación </b>  </span><br>  <span id="m33"> </span> m³     <br>
                        <span>o   </span> <span id="v3"> </span>  Litros  <br>
                        <span><b>Peso de cloro a disolver </b>  </span><br>  <span id="v33"> </span> gramos
                        <span><b>o </b> </span> <span id="kilos3"></span> kilógramos
                    </div>
                    </div>
                    <div id="result4">
                        <div class="alert alert-success" role="alert">
                          <span><b>Volumen de la tubería: </b>  </span> <br> <span id="vlitrosTubo"> </span> <i>litros</i>    <br>
                          <span>o   </span> <span id="vm3tubo"> </span><i> m³ </i>   <br>
                          <span><b>Peso de cloro a disolver : </b>  </span> <br> <span id="gramosTubo"> </span> <i>gramos</i>
                          <span><b>o </b> </span> <span id="kilosTubo"></span> <i>kilógramos</i>
                      </div>
                      </div>
                      <div id="result5">
                          <div class="alert alert-success" role="alert">
                            <span><b>Volumen cámara de rompe presión :  </b>  </span> <br> <span id="litrosCam"> </span> <i>litros</i>    <br>
                            <span>o   </span> <span id="m3Cam"> </span><i>m³ </i>   <br>
                            <span><b>Peso de cloro a disolver : </b>  </span> <br> <span id="gramosCam"> </span> <i>gramos</i>
                            <span><b>o </b> </span> <span id="kilosCam"></span> <i>kilógramos</i>
                          </div>
                      </div>
                    </div>
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade show active p-3" id="two" role="tabpanel" aria-labelledby="two-tab" class="tab-pane active">
                  <div class="col-lg-12">
                    <div class="row">
                      <div class="col-md-6">
                      <form >
                      <div class="form-group">
                          <label for="caudal"> Caudal de ingreso al reservorio (L/sg):</label>
                          <input type="number" class="form-control" id="caudal" placeholder="L/sg">
                      </div>
                      <div class="form-group">
                          <label for="pcloro">Capacidad de rotoplast (L) : </label>
                          <input type="number" class="form-control" id="cap21" placeholder="Litros">
                      </div>
                      <div class="form-group">
                          <label for="pcloro">Caudal de  Goteo (ml/min) :</label>
                          <input type="number" class="form-control" id="goteo" placeholder="mL" min="1" max="20">
                      </div>
                      <button type="button" class="btn btn-success" id="calcular" class="btn btn-default"> Calcular </button>
                  </form>
                  </div>
                  <div class="col-md-6">
                  <br>
                  <div class="alert alert-success" role="alert">
                          <span><b>Peso de cloro a disolver : </b> <br> </span> <span id="re"></span> gramos <br>
                          <span><b>Duración del tanque Rotoplast : </b><br> </span> <span id="duracion"></span> dias <br>
                        <span><b>Peso de cloro por recarga :</b><br> </span> <span id="recarga"></span> gramos <br>
                        <span><b>o </b> </span> <span id="kilos"></span> kilogramos <br>
                    </div>
                  </div>
                </div>
                </div>
                </div>


              </div>
            </div>
          </div>
      </div>
  </div>
  <div class="col-lg-3">
    @include('vp/col2')
  </div>
</div>
<style type="text/css">
   #index{ display:none}
</style>
@endsection