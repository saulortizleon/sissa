@extends('layouts.templateblog')
@section('section')
<br>
 <div class="row">
    <div class="col-lg-12">
        <br>
        <center><h2 class="azul">MESA DE PARTES VIRTUAL </h2></center>
     <p align="justify" style="font-size: 20px" >Estimados usuarios, para mayor facilidad, se ha puesto a su disposición este formulario,
     que le permitirá el envío de documentos  a la Dirección Regional de Vivienda, Construcción y Saneamiento.
    </p>
    <div class="row">
        <div class="col-12">
            <div class="card mt-3 tab-card">
            <div class="card-header tab-card-header">
                <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                <li class="nav-item" class="active">
                    <a class="nav-link active" id="one-tab" data-toggle="tab" href="#one" role="tab"
                    aria-controls="One" aria-selected="true"><i class="fa fa-send"></i> <STRONg>Enviar documento</STRONg> </a>
                </li>
                <li class="nav-item" >
                    <a class="nav-link"  id="two-tab" data-toggle="tab" href="#two" role="tab"
                    aria-controls="Two" aria-selected="false"><i class="fa fa-check"></i> <STRONG>Estado documento</STRONG>  </a>
                </li>
                </ul>
            </div>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active p-3" id="one" role="tabpanel" aria-labelledby="one-tab">
                <form id="mpv" name="mpv" action="{{url('mpv/insertar')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                <div class="row">
                    <div class="col-sm-12">
                        <div class="content-wrapper">
                        @if(Session::has('message-correct'))
                            <div class="alert alert-success"> <h3> <i class="fa fa-check"></i> {{Session::get('message-correct')}}</h3>
                                <H4 class="azul"><i class="fa fa-envelope"></i>  Revise su correo electrónico, para dar seguimiento a su trámite.</H4> </div>
                        @elseif(Session::has('message-error'))
                            <div class="alert alert-danger"><h3>{{Session::get('message-error')}}</h3></div>
                        @endif
                        </div>
                    </div>
                    <div class="col-sm-6">
                    <div class="alert alert-primary" role="alert">
                        <div class="form-group "> <center> <h5 class="azul"> Datos del solicitante</h5> </center>
                            <p style="font-size: 18px"> • Complete sus datos correctamente. <br>
                            • Los campos del formulario con * deben ser llenados obligatoriamente.</p>
                        </div>
                        <div class="form-group ">
                        <strong><label for="tipoPersona">TIPO DE PERSONA *</label>       </strong>
                            <select name="tipoPersona" id="tipoPersona" class="form-control form-control-lg" required="required"  >
                                <option value="natural">PERSONA NATURAL</option>
                                <option value="juridica">PERSONA JURÍDICA</option>
                            </select>
                        </div>
                        <div class="form-group " id="ocultar1">
                            <strong><label for="ruc"> RUC * </label></strong>
                            <input type="number" class="form-control form-control-lg" id="ruc" name="ruc" maxlength="11" >
                        </div>
                        <div class="form-group " id="ocultar2">
                            <strong><label for="razonSocial"> RAZÓN SOCIAL *</label></strong>
                            <input type="text" class="form-control form-control-lg" id="razonSocial" name="razonSocial" maxlength="50" >
                            </div>
                        <div class="form-group  " id="ocultar4">
                            <strong><label for="dni"> DNI *</label></strong>
                            <input type="number" class="form-control form-control-lg" id="dni" name="dni" maxlength="8">

                            </div>
                            <div class="form-group" id="ocultar5">
                                <strong> <label for="nombres">NOMBRES Y APELLIDOS *</label></strong>
                                <input type="text" class="form-control form-control-lg" id="nombres" name="nombres">
                            </div>
                            <div class="form-group"   >
                            <strong> <label for="direccion">DIRECCIÓN *</label></strong>
                                <input type="text" class="form-control form-control-lg" id="direccion"
                                name="direccion">
                            </div>
                            <div class="form-group  ">
                                <strong> <label for="correo">CORREO ELECTRÓNICO *</label></strong>
                                <input type="text" class="form-control form-control-lg" id="correo"
                                name="correo">
                                </div>
                            <div class="form-group  ">
                                <strong> <label for="cel">NRO. TELEFÓNO *</label></strong>
                                <input type="number" class="form-control form-control-lg" id="cel"
                                name="cel" maxlength="9">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                    <div class="alert alert-info" role="alert">
                        <div class="form-group ">  <center><h5 class="azul"> Datos del documento</h5>  </center>
                            <p style="font-size: 18px"> • Para el documento escaneado el formato debe ser pdf. <br>
                        • El documento escaneado debe tener 15 folios como máximo. </p>
                            </div>
                        <div class="form-group ">
                            <strong>    <label for="tipoDocumento"> TIPO DE DOCUMENTO *</label>       </strong>
                                <select name="tipoDocumento" id="tipoDocumento" class="form-control form-control-lg" required="required"  >
                                    <option value="">Seleccione</option>
                                    <option value="OFICIO">OFICIO</option>
                                    <option value="OFICIO CIRCULAR">OFICIO CIRCULAR</option>
                                    <option value="OFICIO MULTIPLE">OFICIO MULTIPLE</option>
                                    <option value="CARTA">CARTA</option>
                                    <option value="CARTA CIRCULAR">CARTA CIRCULAR</option>
                                    <option value="CARTA DE PRESENTACIÓN">CARTA DE PRESENTACIÓN</option>
                                    <option value="CARTA DE REQUERIMIENTO">CARTA DE REQUERIMIENTO</option>
                                    <option value="CARTA MULTIPLE">CARTA MULTIPLE</option>
                                    <option value="CARTA NOTARIAL">CARTA NOTARIAL</option>
                                    <option value="CÉDULA DE NOTIFICACIÓN<">CÉDULA DE NOTIFICACIÓN</option>
                                    <option value="CONTRATO">CONTRATO</option>
                                    <option value="CONVENIO">CONVENIO</option>
                                    <option value="DOCUMENTO CONFIDENCIAL">DOCUMENTO CONFIDENCIAL</option>
                                    <option value="DOCUMENTO VIA FAX">DOCUMENTO VIA FAX</option>
                                    <option value="DOCUMENTO VIA MAIL">DOCUMENTO VIA MAIL</option>
                                    <option value="EXHORTO">EXHORTO</option>
                                    <option value="INFORME">INFORME</option>
                                    <option value="INFORME CONFIDENCIAL">INFORME CONFIDENCIAL</option>
                                    <option value="INFORME DE SUPERVISIÓN">INFORME DE SUPERVISIÓN</option>
                                    <option value="INFORME TECNICO">INFORME TECNICO</option>
                                    <option value="MEMORANDUM">MEMORANDUM</option>
                                    <option value="MEMORANDUM MULTIPLE">MEMORANDUM MULTIPLE</option>
                                    <option value="MEMORIAL">MEMORIAL</option>
                                    <option value="NOTIFICACIONES JUDICIALES">NOTIFICACIONES JUDICIALES</option>
                                    <option value="PETITORIO">PETITORIO</option>
                                    <option value="PRAES">PRAES</option>
                                    <option value="PROVEIDO">PROVEIDO</option>
                                    <option value="RESOLUCIONES">RESOLUCIONES</option>
                                    <option value="SOLICITUD">SOLICITUD</option>
                                    <option value="ADENDA">ADENDA</option>
                                    <option value="ARCHIVADO">ARCHIVADO</option>
                                    <option value="OTROS">OTROS</option>
                                </select>
                        </div>
                        <div class="form-group">
                            <strong> <label for="numeroDocumento">NRO. DOCUMENTO *</label></strong>
                            <input type="text" class="form-control form-control-lg" id="numeroDocumento" name="numeroDocumento">
                        </div>
                        <div class="form-group">
                            <strong> <label for="asunto">ASUNTO *</label></strong>
                            <textarea class="form-control form-control-lg" rows="3" id="asunto" name="asunto"></textarea>
                        </div>
                        <div class="form-group">
                            <strong> <label for="documento">ADJUNTAR ARCHIVO *</label></strong>
                            <input type="file" class="form-control form-control-lg" id="documento" name="documento" >
                            <small style="font-size: 15px">  <strong>Documento escaneado en formato PDF de 10MB como máximo</strong> <br>
                            </small>
                        </div>
                        <div class="form-group">
                            <strong> <label for="folios">N° FOLIOS *</label></strong>
                            <input type="number" class="form-control form-control-lg" id="folios" name="folios">
                        </div>
                            <input type="submit" value="Enviar Documento" class="btn btn-primary btn-lg ">
                    </div>
                </div>
                </div>
            </form>
            </div>
            <div class="tab-pane fade  p-3" id="two" role="tabpanel" aria-labelledby="two-tab" class="tab-pane active">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <strong><label for="codigo"> INGRESE CODIGO DE INGRESO</label></strong> </strong>
                            <input type="email" class="form-control form-control-lg" id="codigo" name="codigo">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">CONSULTAR</button>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card-body">
                            <label for="">Resultado :</label>
                            <p class="card-text" ><strong>Su trámite se encuentra en EVALUACIÓN </strong> </p>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </div>
        </div>
    </div>
     <br>
     <p align="justify" style="font-size: 18px">
      <i>Esta modalidad de recepción estará activa según la ley de Procedimiento Administrativo general Ley Nro. 27444 y
        el texto único ordenado de la ley de procedimiento administrativo general.</i>
    </p>

     <p align="justify" style="font-size: 18px">
     <i>   La recepción de documentos mediante la mesa de partes virtual será de lunes
         a viernes de 7:30 a 12:45 y de las 14:30 a 16:00 horas.</i>
    </p>
    </div>

</div>
<br>
    <style type="text/css">
        #index{ display:none}
        .help-block{
            color:red;
            font-size: 16px;
            font-weight: bold;
        }
    </style>

    <script>

    $("#tipoPersona").on('change', function(){
            var selectValue = $(this).val();
            switch (selectValue)
            {
                case "juridica":
                    $("#ocultar3").show();
                    $("#ocultar3").hide();
                    $("#ocultar1").show();
                    $("#ocultar2").show();
                    $("#ocultar4").hide();
                    $("#ocultar5").hide();
                break;
                case "natural":
                    $("#ocultar3").show();
                    $("#ocultar1").hide();
                    $("#ocultar2").hide();
                    $("#ocultar4").show();
                    $("#ocultar5").show();
                break;

            }
        }).change();

    $(document).ready(function() {
    $('#mpv').formValidation({
            framework: 'bootstrap',
            icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
            },
            fields:{
                nombres:{
                validators: {
                    notEmpty: {
                        message: 'Los nombres  son requeridos'
                    },
                    regexp:{
                        regexp: /[A-Za-z]/,
                        message: 'Debe ingresar solo letras'
                    },
                }
                },

                tipoDocumento:{
                validators: {
                    notEmpty: {
                        message: 'Elija una opción del tipo de documento'
                    },
                    choice:{
                        message:'Elija una opción'
                    },
                }
                },

                apellidos:{
                    validators: {
                        notEmpty: {
                            message: 'Los apellidos son requeridos'
                        },
                        regexp:{
                            regexp: /[A-Za-z]/,
                            message: 'Debe ingresar solo letras'
                        },
                    }
                    },
                razonSocial:{
                    validators: {
                        notEmpty: {
                            message: 'Los razon social es requerida'
                        },
                        regexp:{
                            regexp: /[A-Za-z]/,
                            message: 'Debe ingresar solo letras'
                        },
                    }
                },
                dni:{
                validators: {
                    notEmpty: {
                        message: 'El DNI  es requerido'
                    },
                    regexp:{
                        regexp: /[0-9]/,
                        message: 'Debe ingresar solo numeros'
                    },
                    stringLength:{
                        min:8,
                        max:8,
                        message: 'Debe ingresar  8 números'
                    }
                }
                },
                ruc:{
                validators: {
                    notEmpty: {
                        message: 'El RUC  es requerido'
                    },
                    regexp:{
                        regexp: /[0-9]/,
                        message: 'Debe ingresar solo números'
                    },
                    stringLength:{
                        min:11,
                        max:11,
                        message: 'Debe ingresar  11 números'
                    }
                }
                },
                cel:{
                validators: {
                    notEmpty: {
                        message: 'El celular es requerido'
                    },
                    stringLength:{
                            min:6,
                            max:9,
                            message: 'Asegúrese de ingresar su N° telefono'
                        }
                    }
                },
                correo:{
                    validators: {
                        notEmpty: {
                            message: 'El correo es requerido'
                        },
                        regexp: {
                            regexp: /^(?:[^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*|"[^\n"]+")@(?:[^<>()[\].,;:\s@"]+\.)+[^<>()[\]\.,;:\s@"]{2,63}$/,
                            message: 'Ingrese un correo válido'
                        }
                    }
                },
                entidad:{
                    validators: {
                        notEmpty: {
                            message: 'La entidad  es requerida'
                        },
                        regexp:{
                            regexp: /[A-Za-z]/,
                            message: 'Debe ingresar solo letras'
                        },
                    }
                },
                asunto:{
                    validators: {
                        notEmpty: {
                            message: 'Los Asunto  es requerido'
                        },
                        regexp:{
                            regexp: /[A-Za-z]/,
                            message: 'Debe ingresar solo letras'
                        }
                    }
                },
                direccion:{
                    validators: {
                        notEmpty: {
                            message: 'La dirección es requerida'
                        },
                    }
                },

                numeroDocumento:{
                    validators: {
                        notEmpty: {
                            message: 'El numero de documento  es requerido'
                        },
                        regexp:{
                        regexp: /[0-9]/,
                        message: 'Debe ingresar solo numeros'
                        },
                    }
                },
                documento:{
                    validators: {
                        file: {
                            extension: 'pdf',
                            maxSize: 10000000,
                            message: 'el archivo no debe pesar mas de 10MB y en formato PDF'
                        },
                        notEmpty: {
                            message: 'El documento  es requerido'
                        }
                    }
                },
                folios:{
                    validators: {
                        notEmpty: {
                            message: 'El numero de folios es requerido'
                        }
                    }
                },
            }
        });
    });
</script>
@endsection