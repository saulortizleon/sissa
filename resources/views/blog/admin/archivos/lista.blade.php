@extends('layout.admin')
@section('section')
<section class='content-header'>
    <h1> GESTIÓN DE Archivos</h1>
    <ol class='breadcrumb'>
        <li><a href='#'><i class='fa fa-dashboard'></i>Administración</a></li>
        <li class='active'>Archivos</li>
    </ol>
</section>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Lista de Archivos</h3>
                    <button class="btn btn-success btn-sm pull-right" data-target="#modalArchivos"  data-toggle="modal" >Agregar archivo</button>
                </div>
                <div class="box-body">
                    <form action="{{url('centrop/lista')}}" method="get">
                        <div class="input-group">
                            <input type="text" name="search" value="" class="form-control">
                            <span class="input-group-btn">
                                <button class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
                <div class='box-body table-responsive'>
                    <table id='' class='table table-bordered table-hover'>
                        <thead>
                            <tr>
                                <th> Nombre</th>                                
                                <th> Categoria</th>
                                <th> Estado</th>
                                <th> Fecha</th>
                                <th> Acciones</th>
                            </tr>
                        </thead>
                        <tbody> 
                        @foreach($archivos as $item)                       
                            <tr>
                                <td>{{$item->nombre}}</td>                                
                                <td>{{$item->categoria}}</td>
                                <td>{{$item->estado}}</td>
                                <td>{{$item->created_at}}</td>
                                <td>                                                               
                                <a title="Editar" href="#" class="btn btn-primary btn-sm" onclick="editarArchivo('{{$item->idDoc}}');"><i class="fa fa-edit"></i></a>
                                 <a title="Eliminar" href="#" class="btn btn-danger btn-sm" onclick="eliminarArchivo('{{$item->idDoc}}');"><i class="fa fa-trash-o"></i></a>
                              
                                </td>
                            </tr>
                        @endforeach                   
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="modalArchivos">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button"   class="btn btn-default pull-right" data-dismiss="modal">Cerrar</button>
                <h4 class="modal-title">Agregar Archivo</h4>
            </div>
            <div class="modal-body">
                <form id="frmAgenda" name="frmAgenda" action="{{url('adm/archivos')}}" method="post" enctype="multipart/form-data">  
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="description">Nombre</label>
                        <input type="text"  class="form-control" id="nombre" name="nombre" >
                    </div>
                    <div class="form-group">
                        <label for="level" >Categoría</label>
                        <select name="categoria" class="form-control" id="categoria">
                            <option  value="acta">Actas</option>
                            <option  value="resolucion">Resoluciones</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile">Archivo</label>
                        <input type="file" id="archivo" name="archivo">
                    </div>
                    <b> Estado :  </b>
                    <div class="form-group">                                                  
                            <label>
                            <input class="minimal" type="radio" name="estado"  value="activo" checked>
                            Activo
                            </label>
                            <label>
                            <input class="minimal" type="radio" name="estado" value="inactivo">
                            Inactivo
                            </label>                                
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Guardar" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script> 
 function eliminarArchivo(idDoc)
	{
		if(confirm('Está seguro de eliminar el conocimiento?'))
		{
			window.location = "{{url('archivo/eliminar')}}/"+idDoc;
		}
	}
</script>
@endsection