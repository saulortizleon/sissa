<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SISA / LOGIN</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{asset('admin/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('admin/bower_components/font-awesome/css/font-awesome.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{asset('admin/bower_components/Ionicons/css/ionicons.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('admin/dist/css/AdminLTE.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('admin/plugins/iCheck/square/blue.css')}}">

    <!-- Form validation -->
    <link rel="stylesheet" href="{{asset('formvalidation/formValidation.min.css')}}">

    <link rel="stylesheet" href="{{asset('user/css/hidemessage.css')}}">
    <script src="{{asset('user/js/hidemessage.js')}}"></script>

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<style type="text/css">
#frmUser .has-error .control-label,
#frmUser .has-error .help-block,
#frmUser .has-error .form-control-feedback {
    color: #F32713;
}

#frmUser .has-success .control-label,
#frmUser .has-success .help-block,
#frmUser .has-success .form-control-feedback {
    color: #18bc9c;
}
</style>
<body class="hold-transition login-page">
   
    <div class="login-box">
      <div class="login-logo">
        <a href="#">Administrador general DRVCS APURIMAC</a>
      </div>
      <!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg"> <b>Iniciar Sesión</b> </p>
        <form id="frmLogin" action="{{url('adm/login')}}" method="post">
            {{csrf_field()}}
            <div class="form-group has-feedback">
                <input type="text" name="dni" id="dni" class="form-control" placeholder="DNI">
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="contrasena" id="contrasena" class="form-control" placeholder="Contraseña">
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <input type="submit" value="Ingresar" class="btn btn-primary form-control" style="width: 100%;">
                </div>
            </div>
            @if(Session::has('message-correct'))
              <label style="color:red;">{{Session::get('message-correct')}} </label>  
            @elseif(Session::has('message-error'))
             <label style="color:red;">{{Session::get('message-error')}}</label>
            @endif 
        </form>
        
      </div>
    </div>

    <!-- jQuery 3 -->
    <script src="{{asset('admin/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="{{asset('admin/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    <!-- iCheck -->
    <script src="{{asset('admin/plugins/iCheck/icheck.min.js')}}"></script>

    <script src="{{asset('formvalidation/formValidation.min.js')}}"></script>
    <script src="{{asset('formvalidation/bootstrap.validation.min.js')}}"></script>
    <script>
        $(function () {
            $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' /* optional */
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $('#frmLogin').formValidation({
                framework: 'bootstrap',
                icon: {
                    valid: 'glyphicon glyphicon-ok',
                    invalid: 'glyphicon glyphicon-remove',
                    validating: 'glyphicon glyphicon-refresh'
                  
                },
                fields: {
                    dni:{
                        validators: {
                            notEmpty: {
                                message: 'El dni es requerido'
                            },
                            regexp: {
                                regexp: /[0-9]/,
                                message: 'Debe ingresar un DNI valido'
                            }
                        }
                    },
                    contrasena:{
                        validators: {
                            notEmpty: {
                                message: 'La contraseña es requerido'
                            }
                        }
                    }
                }
            });
        });
    </script>
</body>
</html>