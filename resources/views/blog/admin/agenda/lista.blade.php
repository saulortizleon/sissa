@extends('layout.admin')
@section('section')
<section class='content-header'>
    <h1> GESTIÓN DE Agenda</h1>
    <ol class='breadcrumb'>
        <li><a href='#'><i class='fa fa-dashboard'></i>Administración</a></li>
        <li class='active'>Agenda</li>
    </ol>
</section>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Lista de Agenda</h3>
                    <button class="btn btn-success btn-sm pull-right" data-target="#modalAgenda"  data-toggle="modal" >Agregar Agenda</button>
                </div>
                <div class="box-body">
                    <form action="{{url('centrop/lista')}}" method="get">
                        <div class="input-group">
                            <input type="text" name="search" value="" class="form-control">
                            <span class="input-group-btn">
                                <button class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
                <div class='box-body table-responsive'>
                    <table id='' class='table table-bordered table-hover'>
                        <thead>
                            <tr>
                                <th> Nombre</th>
                                <th> Fecha</th>
                                <th> Hora</th>
                                <th> Lugar</th>
                                <th> Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($agenda as $item)
                            <tr>
                                <td>{{$item->nombre}}</td>
                                <td>{{$item->fecha}}</td>
                                <td>{{$item->hora}}</td>
                                <td>{{$item->lugar}}</td>
                                <td>
                                 <a title="Editar" href="#" class="btn btn-primary btn-xs" onclick="editarAgenda('{{$item->idAgenda}}');"><i class="fa fa-edit"></i></a>
                                 <a title="Eliminar" href="#" class="btn btn-danger btn-xs" onclick="eliminarAgenda('{{$item->idAgenda}}');"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="modalAgenda">
    <div class="modal-dialog modal-xs">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button"   class="btn btn-default pull-right" data-dismiss="modal">Cerrar</button>
                <h4 class="modal-title">Agregar agenda</h4>
            </div>
            <div class="modal-body">
                <form id="frmAgenda" name="frmAgenda" action="{{url('adm/agenda')}}" method="post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="description">Nombre</label>
                        <input type="text"  class="form-control" id="nombre" name="nombre" >
                    </div>
                    <div class="form-group">
                        <label for="description">Fecha</label>
                        <input type="text"  class="form-control" id="fecha" name="fecha" >
                    </div>
                    <div class="form-group">
                        <label for="description">hora</label>
                        <input type="text"  class="form-control" id="hora" name="hora" >
                    </div>
                    <div class="form-group">
                        <label for="description">lugar</label>
                        <input type="text"  class="form-control" id="lugar" name="lugar" >
                    </div>
                    <b> Estado :  </b>
                    <div class="form-group">
                            <label>
                            <input class="minimal" type="radio" name="estado"  value="activo" checked>
                            Activo
                            </label>
                            <label>
                            <input class="minimal" type="radio" name="estado" value="inactivo">
                            Inactivo
                            </label>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Guardar" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="modalEdit"></div>
<script>
    function editarAgenda(idAgenda)
	{    alert(idAgenda);
		$('#modalEdit').load("{{url('agenda/edit')}}/"+idAgenda);
	}
    function eliminarAgenda(idAgenda)
	{
		if(confirm('Está seguro de eliminar el conocimiento?'))
		{
			window.location = "{{url('agenda/eliminar')}}/"+idAgenda;
		}
	}
</script>
@endsection