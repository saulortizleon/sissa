@extends('layouts.templateadm')
@section('section')
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Agregar publicación
                    </h3>
                    <a type="button" class="btn btn-danger pull-right" href="{{ url('admin/noticias') }}"><i class="fa fa-arrow-left"></i> atras</a>
                    <hr>
                    <form id="frmPublicacion" name="frmPublicacion" action="{{url('noticias/insertar')}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                            <label for="description">Título de la Noticia</label>
                            <input type="text"  class="form-control" id="titulo" name="titulo" required="required">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="description">Subtítulo de la Noticia</label>
                                <input type="text"  class="form-control" id="subtitulo" name="subtitulo"  required="required">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group">
                                    <div class="box">
                                        <div class="box-header">
                                        <h3 class="box-title">Contenido de la noticia
                                        </h3>
                                        <!-- tools box -->
                                        <div class="pull-right box-tools">
                                        </div>
                                        <!-- /. tools -->
                                        </div>
                                        <!-- /.box-header -->
                                        <div class="box-body pad">
                                            <textarea class="textarea" id="contenido" name="contenido" placeholder="Escribir aquí..."
                                                    style="width: 100%; height: 300px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" ></textarea>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="level" >Categoría</label>
                                <select name="categoria" class="form-control" id="categoria">
                                    <option  value="noticia">Nota de prensa</option>
                                    <option  value="comunicado">Comunicado</option>
                                    <option  value="documento">Documento</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="level" >Tema </label>
                                <select name="tema" class="form-control" id="tema">
                                    <option  value="romas">ROMAS</option>
                                    <option  value="atm">ATM</option>
                                    <option  value="wasichakuy">Wasichakuy</option>
                                    <option  value="escuela">Escuela Saneamiento</option>
                                    <option  value="vivienda">Viviendas Saludables</option>
                                    <option  value="jass">JASS</option>
                                    <option  value="urbanismo">Urbanismo</option>
                                    <option  value="convocatoria">Convocatorias</option>
                                    <option  value="participacion">Participaciones</option>
                                    <option  value="capacitacion">Capacitacion</option>
                                    <option  value="fed">FED</option>
                                    <option  value="plan">FED</option>
                                    <option  value="saneamiento">Saneamiento</option>
                                    <option  value="pnsr">PNSR</option>
                                    <option  value="edusa">EDUSA</option>
                                    <option  value="comursaba">COMURSABA</option>
                                    <option  value="techo">Techo propio</option>
                                </select>
                            </div>
                            <b> PORTADA PRINCIPAL, <i style="color: red;" >imagen de 1900x1080: </i>   </b>
                            <div class="form-group">
                                    <label>
                                    <input class="minimal" type="radio" name="portada"  value="SI" >
                                    SI
                                    </label>
                                    <label>
                                    <input class="minimal" type="radio" name="portada" value="NO" checked>
                                    NO
                                    </label>
                                </div>
                            <div class="form-group" id="ocultar1">
                                 <label for="exampleInputFile">Subir Imagen </label>
                                <input type="file" id="imagenp" name="imagenp">
                            </div>
                            <div class="form-group" id="ocultar2">
                                <label for="exampleInputFile">Subir documento en <i style="color: red;">formato PDF</i> :</label>
                                <input type="file" id="archivo" name="archivo">
                            </div>
                            <b> Estado :  </b>
                            <div class="form-group">
                                <label>
                                <input class="minimal" type="radio" name="estado"  value="activo" checked>
                                Activo
                                </label>
                                <label>
                                <input class="minimal" type="radio" name="estado" value="inactivo">
                                Inactivo
                                </label>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Publicar Noticia" class="btn btn-success">
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</section>
<script>
    $(function() {

        $("#categoria").on('change', function() {
          var selectValue = $(this).val();
          switch (selectValue) {

            case "noticia":
              $("#ocultar1").show();
              $("#ocultar2").show();
              $("#ocultar3").show();
              $("#ocultar4").show();
              break;

            case "comunicado":
              $("#ocultar1").show();
              $("#ocultar2").hide();
              $("#ocultar3").hide();
              $("#ocultar4").hide();
              break;

            case "documento":
              $("#ocultar1").hide();
              $("#ocultar2").show();
              $("#ocultar3").hide();
              $("#ocultar4").hide();
              break;

          }

        }).change();

      });
</script>
@endsection