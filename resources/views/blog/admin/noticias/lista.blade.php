@extends('layouts.templateadm')
@section('section')
<section class='content-header'>
    <h1> GESTIÓN DE NOTICIAS</h1>
    <ol class='breadcrumb'>
        <li><a href='#'><i class='fa fa-dashboard'></i>Administración</a></li>
        <li class='active'>Noticias</li>
    </ol>
</section>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class="box-header">
                    <button class="btn btn-success btn-sm" onclick="insertar()"><i class="fa fa-send"></i> Agregar noticia</button>
                    <div class="box-tools">
                      <div class="input-group input-group-sm hidden-sm" style="width: 500px;">
                        <input type="text" name="buscar" id="buscar" class="form-control pull-right" placeholder="Buscar noticia">
                        <div class="input-group-btn">
                          <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                      </div>
                    </div>
                  </div>
                <div class='box-body table-responsive'>
                    <table id="noticias" class='table table-bordered table-hover'>
                        <thead>
                            <tr>
                                <th> Titulo</th>
                                <th> Categoria</th>
                                <th> Fecha publicación</th>
                                <th> Tema</th>
                                <th> Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($publicacion as $item)
                            <tr>
                                <td>{{$item->titulo}}</td>
                                <td>{{$item->categoria}}</td>
                                <td>{{$item->created_at}}</td>
                                <td>{{$item->tema}}</td>
                                <td>
                                <a title="Editar" href="#" class="btn btn-primary btn-xs" onclick="editarNoticia('{{$item->idPublicacion}}');"><i class="fa fa-edit"> Editar</i></a>
                                 <a title="Eliminar" href="#" class="btn btn-danger btn-xs" onclick="eliminarNoticia('{{$item->idPublicacion}}');"><i class="fa fa-trash-o"> Eliminar</i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(document).ready(function(){
        $("#buscar").on("keyup", function() {
          var value = $(this).val().toLowerCase();
          $("#noticias tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
        });
      });
    function insertar()
	{
			window.location = "{{url('noticias/insertar')}}";
    }
    function editarNoticia(idPublicacion)
	{
		if(confirm('Está seguro de editar la publicación?'))
		{
			window.location = "{{url('noticias/editar')}}/"+idPublicacion;
		}
	}
</script>
@endsection