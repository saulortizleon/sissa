<div class="card my-4">
    <ul class="list-group">
            <li class="list-group-item "><a href="{{ url('noticias') }}"><i class="fa fa-list-alt"></i> <b>Noticias</b>  </a> </li>
            <li class="list-group-item "><a href="{{ url('documuentos') }}"><i class="fa fa-file-pdf-o"></i> Documentos</a> </li>
            <li class="list-group-item "><a href="{{ url('comunicados') }}"><i class="fa fa-bullhorn"></i> Comunicados Oficiales</a> </li>
            <li class="list-group-item "><a href="{{ url('directorio') }}"><i class="fa fa-phone-square"></i> Directorio</a> </li>
            <li class="list-group-item "><a href="{{ url('videos') }}"><i class="fa fa-video-camera"></i> Videos</a> </li>
            <li class="list-group-item "><a href="{{ url('galeria') }}"><i class="fa fa-photo"></i> Galería </a> </li>
        </ul>
</div>