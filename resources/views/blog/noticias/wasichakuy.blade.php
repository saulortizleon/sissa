@extends('layouts.templateblog')
@section('section')
<br>
<div class="row">
    <div class="col-lg-9">
        <br>
        <div class="row">
            <div class="col-lg-4">
                    <center> <img class="img-fluid" src="{{ asset('img/wasichakuy.png') }}" width="60%" height="auto" >
                    </center> <br>
            </div>
            <div class="col-lg-8">
                    <center><h1 class="azul"><small> Maratón de Webinars</small>"WASICHAKUY"
                    </h1>
                    <h3 class="azul"><i>  Construyendo mi vivienda</i>
                    </h3>
                    </center>
                    <p align="justify" style="font-size: 20px">
                        Iniciativa del Gobierno Regional de Apurímac a través de la Dirección Regional de Vivienda,
                        Construcción y Saneamiento,  con el objetivo de brindar los conocimiento
                        técnicos necesarios para la autoconstrucción de viviendas en las zonas rurales de la
                         región de Apurímac, en el marco de la emergencia sanitaria COVID-19, con la participación de personal especializado en el
                         área de vivienda y construcción, el cual se desarrolló los años 2020 y 2021  
                          a través de Facebook y Google Meet.
                     </p>
            </div>
        </div>
        <div class="row">
            <div class="col-4">
              <div class="list-group" id="list-tab" role="tablist">
                <a class="list-group-item list-group-item-action active"
                id="modulo2020-list" data-toggle="list" href="#modulo2020" role="tab" aria-controls="home"><strong> Módulos 2020</strong></a>
               
                <a class="list-group-item list-group-item-action"
                id="list-messages-list" data-toggle="list" href="#modulos2021" role="tab" aria-controls="messages"><strong>Módulos 2021</strong></a>

                <a class="list-group-item list-group-item-action"
                id="resultados-list" data-toggle="list" href="#resultados" role="tab" aria-controls="profile"><strong>Resultados</strong></a>
                
              </div>
            </div>
            <div class="col-8">
              <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="modulo2020" role="tabpanel" aria-labelledby="modulo2020-list">
                  
                    <i class="fa fa-arrow-right"></i> CONSTRUCCIÓN DE VIVIENDAS CON ADOBE.
                    <br> <i class="fa fa-arrow-right"></i> CONFORT TÉRMICO EN VIVIENDAS ALTO ANDINAS.
                    <br><i class="fa fa-arrow-right"></i> REPARACIÓN DE VIVIENDAS DE ADOBE.
                    <br> <i class="fa fa-arrow-right"></i> CONSTRUCCIÓN DE VIVIENDAS CON ALBAÑILERÍA CONFINADA.
                    <br> <i class="fa fa-arrow-right"></i> REPARACIÓN DE VIVIENDAS DE ALBAÑILERÍA CONFINADA.
                    <br> <i class="fa fa-arrow-right"></i> ENERGIAS RENOVABLES - INSTALACIÓN DE TERMAS SOLARES
                    <br> <i class="fa fa-arrow-right"></i> IMPLEMENTACIÓN DE CASITAS CALIENTES EN LA REGION DE APURIMAC
                    <br> <i class="fa fa-arrow-right"></i> PROGRAMA TECHO PROPIO - MVCS
                </div>
                <div class="tab-pane fade" id="resultados" role="tabpanel" aria-labelledby="resultados-list">
                  
                  <p align="justify" style="font-size: 20px"> <strong> Más de 180 participantes entre</strong> albañiles, operadores,
                     maestros de obra, estudiantes, técnicos  y profesionales durante el desarrollo de las capacitaciones  
                      del año 2020 y 2021.
                  </p>
                </div>
                <div class="tab-pane fade" id="modulos2021" role="tabpanel" aria-labelledby="modulos2021">
                  <i class="fa fa-arrow-right"></i> ASPECTOS GENERALES PARA LA CONSTRUCCIÓN DE VIVIENDAS
                  <br> <i class="fa fa-arrow-right"></i> CONSIDERACIONES PARA LA CONSTRUCCIÓN DE VIVIENDAS SEGURA
                  <br><i class="fa fa-arrow-right"></i> SEGURIDAD EN LA CONSTRUCCIÓN DE VIVIENDAS
                  <br> <i class="fa fa-arrow-right"></i> LECTURA DE PLANOS 
                  <br> <i class="fa fa-arrow-right"></i> CONTROL DE CALIDAD DE CONCRETO
                  <br> <i class="fa fa-arrow-right"></i> PROCESOS CONSTRUCTIVOS DE UNA VIVIENDA SEGURA
                  <br> <i class="fa fa-arrow-right"></i> CONSTRUCCIÓN DE UNA VIVIENDA SEGURA
                  <br> <i class="fa fa-arrow-right"></i> ADITIVOS PARA CONCRETO
                  <br> <i class="fa fa-arrow-right"></i> CONSTRUCCIÓN DE VIVIENDAS CON ADOBE
                  <br> <i class="fa fa-arrow-right"></i> CONFORT TÉRMICO EN VIVIENDAS
                  <br> <i class="fa fa-arrow-right"></i> INSTALACIONES SANITARIAS EN VIVIENDAS
                  <br> <i class="fa fa-arrow-right"></i> 	INSTALACIONES ELÉCTRICAS EN INTERIORES DE VIVIENDAS
                </div>
                <div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">                  
                </div>
              </div>
            </div>
        </div>

        <br>
        <center><h6 class="azul"> NOTICIAS
        </h6> 
        </center> 
        <div class="row">
          @foreach($wasichakuy as $item)
          <div class="col-lg-4 col-sm-4 portfolio-item">                  
              <div class="card my-4">
                  <div class="card-body">
                      <a href="#"><img class="card-img-top" src="{{asset($item->imagen)}}" alt=""></a>
                      <div class="card-body">
                          <h6 class="card-title azul" >
                          <a href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}">{{$item->titulo}}</a>
                          </h6>
                          <p class="card-text" style="font-size:14px;  ">{{$item->subtitulo}}</p>
                          <a class="btn btn-link" href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}">
                          <i class="fa fa-external-link-square"></i> Leer más</a>
                      </div>
                  </div>
              </div>                  
          </div>
          @endforeach
      </div>
    </div>
    <div class="col-lg-3">
      @include('vp/col2')
     
    </div>
</div>
<br><br>
    <style type="text/css">
        #index{ display:none}
    </style>
@endsection