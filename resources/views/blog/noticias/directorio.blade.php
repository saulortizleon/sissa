@extends('layouts.templateblog')
@section('section')
<br>
<div class="row">
  <div class="col-lg-9">
      <br>
      <center><h1 class="azul">Directorio Institucional 2021</h1>
      <p>Datos de personal nombrado y contratado de la  Dirección Regional de Vivienda, Construcción y Saneamiento de Apurímac </p></center>
      <div class="row">
          <div class="col-lg-12">
            <div class="card mt-3 tab-card">
              <div class="card-header tab-card-header">
                <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                  <li class="nav-item" class="active">
                      <a class="nav-link active" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="One" aria-selected="true">Dirección</a>
                  </li>
                  <li class="nav-item" >
                      <a class="nav-link " id="three-tab" data-toggle="tab" href="#three" role="tab" aria-controls="Three" aria-selected="false">Facilitadores </a>
                  </li>
                  <li class="nav-item" >
                    <a class="nav-link"  id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="Two" aria-selected="false">Áreas Técnicas Municipales</a>
                  </li>
                </ul>
              </div>

              <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active p-3" id="one" role="tabpanel" aria-labelledby="one-tab">
                  <center> <h5>Lista de personal <small>  actualizado al  01 de enero 2021 </small> </h5></center>
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th scope="col">Nombres y Apellidos</th>

                        <th scope="col">Cargo</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($usuarios as $item)
                      <tr>
                        @if ($item->personal == 'planta')
                        <td class="text-uppercase">{{$item->nombres}}  {{$item->apellidos}} </td>

                        <td class="text-uppercase">{{$item->descripcion}}                   </td>
                        @endif
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
                <div class="tab-pane fade  p-3" id="two" role="tabpanel" aria-labelledby="two-tab" class="tab-pane active">
                  <p>No hay registros</p>
                </div>
                <div class="tab-pane fade  p-3" id="three" role="tabpanel" aria-labelledby="three-tab">
                  <center> <h5>Lista de facilitadores <small>  actualizado a  01 de enero 2021</small> </h5></center>
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th scope="col">Nombres y Apellidos</th>

                        <th scope="col">Cargo</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($usuarios as $item)
                      <tr>
                        @if ($item->personal == 'locador')
                        <td class="text-uppercase">{{$item->nombres}}  {{$item->apellidos}} </td>

                        <td class="text-uppercase">{{$item->descripcion}}                   </td>
                        @endif
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>

              </div>
            </div>
          </div>
      </div>
      <br>
  </div>
  <div class="col-lg-3">
    @include('vp/col2')
  </div>

</div>
<style type="text/css">
  #index{ display:none}
</style>
@endsection