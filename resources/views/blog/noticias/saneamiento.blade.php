@extends('layouts.templateblog')
@section('section')
<br>
<div class="row">
    <div class="col-lg-9">
        <br>
            <center><h3 class="azul">Oficina de Construcción y Saneamiento</h3></center>

            <i class="fa fa-check-circle"></i> Formular, coordinar, ejecutar y evaluar los planes y políticas regionales, en materia de construcción, sanemainto y medio ambiente, referidas al sector.
            <br><i class="fa fa-check-circle"></i> Cumplir y cautelar el cumplimiento de la normatividad, en materia de construcción, saneamiento y medio ambiente, en el ámbito de su competencia.
            <br><i class="fa fa-check-circle"></i> Promover y gestar programas, proyectos y estudios de construcción, de infraestructura y saneamiento, ambientalmente equilibrados, en beneficio de la población regional, en coordinación con los Gobiernos Locales.
            <br><i class="fa fa-check-circle"></i> Estimular la participación de la iniciativa de la inversión privada en la generación de la oferta de construcción de ingraestructura y servicios de saneamiento urbano y rural, articulando en el ámbito regional con las instancias vinculadas.
            <br><i class="fa fa-check-circle"></i> Promover el desarrollo de las inversiones en la construcción de infraestructura, de acuerdo a la normatividad vigente.
            <br><i class="fa fa-check-circle"></i> Conducir la elaboración del diagnóstico situacional en materia de saneamiento y sus necesidades en el ámbito regional.
            <br><i class="fa fa-check-circle"></i> Gestionar la identificación de fuentes de financiamiento, provenientes de Cooperación Técnica Internacional, en apoyo a la formulación y ejecución de los proyectos de saneamiento a cargo de los Gobiernos Locales.
            <br><i class="fa fa-check-circle"></i> Proporcionar asistencia técnica especializada a los Gobiernos Locales, en el desarrollo de los proyectos de inversión en materia de saneamiento, cuando así lo soliciten.
            <br><i class="fa fa-check-circle"></i> Coordinar los planes y acciones, previas o posteriores, en casos de desastres, que le corresponde asumir a la Dirección Regional Sectorial, conforme a las disposiciones que le corresponda asumir.
        </p>
        <p>
            <center><h6 class="azul"> Objetivos en Construcción</h6></center>

            <i class="fa fa-wrench"></i> Promover, coordinar, formular y/o conducir programas de asistencia técnica para el desarrollo de programas habitacionales dirigidos a familias de menores recursos.
            <br><i class="fa fa-wrench"></i> Incentivar la difusión y las capacitaciones tecnológicas con fines de innovación sobre normatividad y sistemas de construcción nuevas.
            <br><i class="fa fa-wrench"></i> Estimular el estudio y mejoramiento de tecnologías constructivas tradicionales en lo que respecta a condiciones sismo resistente, de estabilización, confort, durabilidad y seguridad.
            <br>
        </p>
        <center><h6 class="azul"> Objetivos en Saneamiento</h6></center>
        <p>

            <i class="fa fa-tint"></i> Contribuir a ampliar la cobertura y mejorar la calidad y sostenibilidad de los servicios de agua potable, alcantarillado, tratamiento de aguas servidas y disposición de excretas.
            <br><i class="fa fa-tint"></i> Promover la sostenibilidad de los sistemas, la ampliación de la cobertura y el mejoramiento de la calidad de los servicios de saneamiento en el ámbito urbano y rural.
        </p>
    </div>
    <div class="col-lg-3">
        @include('vp/col2')
    </div>
</div>
<br>
<style type="text/css">
 #index{ display:none}
</style>
@endsection