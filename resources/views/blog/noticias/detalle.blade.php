@extends('layouts.templateblog')
@section('section')
<br> 
<div class="row">
<!-- Post Content Column -->
  <div class="col-lg-9">
    <div class="card my-4">
      <div class="card-body">
        <button type="button" class="btn btn-outline-primary btn-sm "><b class="text-uppercase">{{$detalle->tema}}</b></button>
          <button class="btn btn-outline-danger btn-sm"><b class="text-uppercase">{{$detalle->categoria}}</b></button>
          <h2><b>{{ $detalle->titulo }}</b>  </h2>
          <!-- Author -->
          <p class="lead" style="font-size:14px">
            <b><i class="fa fa-send"></i></b>
            <a href="#">Imagen Institucional</a> | <i class="fa fa-calendar"></i> <a href="#"> {{$detalle->created_at->format('Y-m-d')}}
          </a></p>
            <h5>{{ $detalle->subtitulo }}</h5>
          <p style="font-size:14px"> </p>
          @if (!is_null($detalle->imagen))
            <img  class="img-fluid" src="{{asset($detalle->imagen)}}"  >
          @else

          @endif
          <?php
        $orig =$detalle->contenido;

        $a = htmlentities($orig);//convertimos a html

        $b = html_entity_decode($a);

        echo $b;
        ?>
    <br>
      @if (!is_null($detalle->archivo))
      <div class="embed-responsive embed-responsive-16by9">
      <embed class="embed-responsive-item"  src="{{ asset($detalle->archivo) }}"
      type="application/pdf">
    </div>
      @else
        <i>No hay ningún documento adjunto</i>
    @endif


    <br>

      <?php
      $orig =$detalle->embed;
      $a = htmlentities($orig);//convertimos a html

      $b = html_entity_decode($a);
      echo $b;
      ?>
    </div>
    </div>


  </div>
  <div class="col-lg-3">
    @include('vp/col2')
  </div>
<style type="text/css">
  #index{ display:none}
</style>
@endsection