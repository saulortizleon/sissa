@extends('layouts.templateblog')
@section('section')
<br>
<div class="row">
<!-- Post Content Column -->
  <div class="col-lg-9">
      <br>
      <div class="row">
        <div class="col-lg-4">
          <center> <img class="img-fluid" src="{{ asset('img/institucion/romas.png') }}" width="70%" height="auto" ></center>
          <br>
          <center><a type="button" class="btn btn-xs btn-primary btn-lg"
            href="{{url('proyectos-romas')}}">

            <i class="fa fa-eye"></i> <strong>VER PROYECTOS </strong> </a></center>
          <br>
        </div>
        <div class="col-lg-8">
          <center><h1  class="azul">ROMAS APURÍMAC</h1>
          <h4> "REPOSICIÓN, OPERACIÓN Y MANTENIMIENTO DEL SISTEMA DE AGUA Y SANEAMIENTO EN LA REGIÓN DE APURÍMAC"</h4><br>   </center>
          <center>  <p style="font-size:20px;">El objetivo de estos proyectos son regular la labor  durante el proceso de preparación, ejecución física y
                la liquidación de las actividades de Mantenimiento, Reposición, Operación y
                Mantenimiento del Sistema de Agua y Saneamiento en la Región de Apurímac.</p> </center>

        </div>
      </div>
      <br>      <center><h3 class="azul"><i class="fa fa-tint"></i> Finalidad</h3></center>
      <p class="text-justify" style="font-size:20px;">
              Establecer los lineamientos, procedimientos, funciones y obligaciones que garanticen la
              adecuada ejecución de las actividades de mantenimiento, reposición, operación y
              mantenimiento del sistema de agua y saneamiento en la Región Apurímac,
              estableciendo las responsabilidades correspondientes, de acuerdo con las normas y
              procedimientos que regulan la ejecución y control de los proyectos de inversión pública,
              vigente.
      </p>

      <center><h3 class="azul"> <i class="fa fa-tint"></i> Alcance</h3></center>
      <p class="text-justify" style="font-size:20px;">
      La presente Directiva es de aplicación para los órganos estructurales y dependencias que
          conforman el pliego del Gobierno Regional de Apurímac, involucrados en el proceso de
          rehabilitación, Reposición, Operación y Mantenimiento del Sistema de Agua y
          Saneamiento en la Región de Apurímac
      </p>
      <center><h3 class="azul"><i class="fa fa-tint"></i> Base Legal</h3></center>
      <p style="font-size:20px;">
          <i class="fa fa-check-circle"></i>Ley N° 27857, Ley Orgánica de los Gobiernos regionales y sus Modificatorias.
          <br><i class="fa fa-check-circle"></i>Ley N° 27558, Ley marco de la Modernización de la gestión del Estado, y sus
          normas modificatorias (Leyes N° 27842,27852 y 27899).
          <br><i class="fa fa-check-circle"></i> Ley N° 27505, Ley del Canon y sus respectivas modificatorias (Ley N° 28077)
          <br><i class="fa fa-check-circle"></i> Ley N° 30225, Ley de Contrataciones del Estado y su modificatoria Decreto
          legislativo N° 1341.
          <br><i class="fa fa-check-circle"></i> Ley N° 29522, Ley que modifica la Ley N° 27785, ley Orgánica del Sistema
          nacional de Control y de la Contraloría General de la Republica.
          <br><i class="fa fa-check-circle"></i> Resolución de Contraloría General N° 320-2005-C.G, que aprueba las normas de
          Control Interno.
          <br><i class="fa fa-check-circle"></i> Decreto Legislativo No 1280 Ley marco de la gestión y prestación de los servicios
          de saneamiento.
          <br><i class="fa fa-check-circle"></i> Decreto Supremo No 018-2017-VIVIENDA, que aprueba el Plan Nacional de
          Saneamiento 2017 – 2021
          <br><i class="fa fa-check-circle"></i> Ordenanza Regional No 013-2012-GR/APURIMAC/CR, aprobación de aprobar, la
          creación del sistema de información de agua y saneamiento – SIAS Apurímac.
          <br><i class="fa fa-check-circle"></i> Ordenanza Regional Na 013-2012-GR/APURIMAC/CR, aprueba lineamientos de
          política para la Gestión del agua y saneamiento de obligatorio cumplimiento en el
          ámbito del Gobierno Regional de Apurímac para toda intervención que se haga,
          cualquiera fuera el fondo de financiamiento.
          <br><i class="fa fa-check-circle"></i> Resolución Ejecutiva Regional Na 237-2018, Aprueba el Plan Regional de
          Saneamiento.
    </p>
    <div class="row">
      @foreach($romas as $item)
      <div class="col-lg-4 col-sm-4 portfolio-item">                  
          <div class="card my-4">
              <div class="card-body">
                  <a href="#"><img class="card-img-top" src="{{asset($item->imagen)}}" alt=""></a>
                  <div class="card-body">
                      <h6 class="card-title azul" >
                      <a href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}">{{$item->titulo}}</a>
                      </h6>
                      <p class="card-text" style="font-size:14px;  ">{{$item->subtitulo}}</p>
                      <a class="btn btn-link" href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}">
                      <i class="fa fa-external-link-square"></i> Leer más</a>
                  </div>
              </div>
          </div>                  
      </div>
      @endforeach
  </div>
  </div>
  
<!-- Sidebar Widgets Column -->
  <div class="col-lg-3">
  @include('vp/col2')
  
      <br>
</div>

</div>
<style type="text/css">
 #index{ display:none}
</style>
@endsection