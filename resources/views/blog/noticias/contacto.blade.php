@extends('layouts.templateblog')
@section('section')
<br>
<div class="row">
    <div class="col-lg-9">
        <br>
        <center><h1 class="azul"> CONTÁCTENOS </h1>
 <p style="font-size: 18px;">Ubicanos en la <strong>Jr. Lima N° 637 - Abancay,</strong>  llamanos al <strong>(083) 322837 </strong>
    o escribenos al <strong>drvcsapurimac@hotmail.com</strong>
</p></center>
        <div class="row">
            <div class="col-lg-8">
                <center>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3877.35127983
            76028!2d-72.88469228563602!3d-13.636383090422266!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f1
            3.1!3m3!1m2!1s0x916d0308b7b618b7%3A0x413049565202d485!2sDIRECCI%C3%93N%20REGIONAL%20D
            E%20VIVIENDA%2C%20CONSTRUCCI%C3%93N%20Y%20SANEAMIENTO%20APUR%C3%8DMAC!5e0!3m2!1ses-419
            !2spe!4v1614044680479!5m2!1ses-419!2spe" width="600" height="450" style="border:0;"
            allowfullscreen="" loading="lazy"></iframe>
            </center>
            </div>
            <div class="col-lg-4">
                    <form id="contact-form" name="contact-form">
                        <!--Grid row-->
                        <div class="row">

                            <div class="col-md-12">
                                <h4 class="azul">Envianos un mensaje</h4> <br>
                                <div class="md-form mb-0">
                                    <label for="name" style="font-size: 15px;" class="azul">Nombres y Apellidos</label>

                                    <input type="text" id="name" name="name" class="form-control">
                                     </div>
                            </div>
                            <div class="col-md-12">
                                <div class="md-form mb-0">
                                    <label for="email" style="font-size: 15px;" class="azul">Correo Electrónico</label>

                                    <input type="text" id="email" name="email" class="form-control">
                                      </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="md-form mb-0">
                                    <label for="subject" style="font-size: 15px;" class="azul">Asunto</label>

                                    <input type="text" id="subject" name="subject" class="form-control">
                                      </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="md-form">
                                    <label for="message" style="font-size: 15px;" class="azul">Mensaje</label>

                                    <textarea type="text" id="message" name="message" rows="5" class="form-control md-textarea "></textarea>
                                       </div>

                            </div>
                        </div>
                        <!--Grid row-->

                    </form>

                    <div class="text-center text-md-right">
                        <br>
                        <button class="btn btn-primary btn-sm" style="background-color:#182d80 !important;">ENVIAR MENSAJE</button>
                    </div>
                    <br>
                    <div class="status"></div>


            </div>
            <center>

        </div>
    </div>

    <div class="col-lg-3">
        @include('vp/col2')
      </div>
</div>

</div>
<style type="text/css">
     #index{ display:none}
</style>
@endsection