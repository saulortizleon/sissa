@extends('layouts.templateblog')
@section('section')
<br>
<div class="row">
  <div class="col-lg-9">
    <div class="card my-4">
      <div class="card-body">
    <center><h1 class="azul"> FUNCIONES</h1></center>
      <p style="font-size: 18px; text-align: justify;">
         <i class="fa fa-arrow-right"></i> Formular, proponer y evaluar los planes y políticas regionales en materia de vivienda, construcción y saneamiento, en concordancia con los planes de desarrollo de los gobiernos locales y de conformidad con las políticas nacionales y planes sectoriales.
    <br> <i class="fa fa-arrow-right"></i> Promover la ejecución de programas de vivienda urbanos y rurales, canalizando los recursos públicos y privados y la utilización de los terrenos del gobierno regional y materiales de la región, para programas municipales de vivienda.
    <br> <i class="fa fa-arrow-right"></i> Incentivar la participación de promotores privados en los diferentes programas habitacionales, en coordinación con los gobiernos locales.
    <br> <i class="fa fa-arrow-right"></i> Difundir el Plan Nacional de Vivienda y la normativa referida a la edificación de vivienda, así como evaluar su aplicación.
    <br> <i class="fa fa-arrow-right"></i> Ejecutar acciones de promoción, difusión, asistencia técnica, capacitación, investigación científica y tecnológica en materia de construcción y saneamiento.
    <br> <i class="fa fa-arrow-right"></i> Apoyar técnica y financieramente a los gobiernos locales en la prestación de servicios de saneamiento.
    <br> <i class="fa fa-arrow-right"></i> Aprobar los aranceles de los planos prediales con arreglo a las normas técnicas vigentes sobre la materia.

    <br> <i class="fa fa-arrow-right"></i> Asumir la ejecución de los programas de vivienda y saneamiento a solicitud de los gobiernos locales.
    <br> <i class="fa fa-arrow-right"></i> Proponer la actualización del marco normativo relacionado con el ámbito de su competencia.
    <br> <i class="fa fa-arrow-right"></i> Coordinar con los Organismos Públicos y Privados, Comisiones Sectoriales, Multisectoriales y Proyectos Especiales, las actividades vinculadas al ámbito de su competencia.
    <br> <i class="fa fa-arrow-right"></i> Promover la participación del sector privado, en el ámbito de su competencia, para el desarrollo de la construcción de infraestructura y la gestión de los servicios de saneamiento.
    <br> <i class="fa fa-arrow-right"></i> Promover programas de prevención de riesgo de las viviendas, frente a fenómenos naturales, en coordinación con los sectores competentes.
    <br> <i class="fa fa-arrow-right"></i> Generar las condiciones para el acceso a los servicios de saneamiento ambientalmente equilibrados, en los niveles adecuados de calidad y sostenibilidad, especialmente en los sectores de menores recursos económicos.
    <br> <i class="fa fa-arrow-right"></i> Expedir resoluciones directorales en las materias de su competencia.
    <br> <i class="fa fa-arrow-right"></i> Otorgar derechos, a través de autorizaciones, permisos y concesiones de acuerdo con las normas de la materia.
    <br> <i class="fa fa-arrow-right"></i> Otras funciones que le corresponda por dispositivo legal expreso o por disposición del Gobierno Regional del Apurimac.</p>
  </div>
</div>
  </div>
  <div class="col-lg-3">
    @include('vp/col2')
  </div>
</div>

<style type="text/css">
   #index{ display:none}
</style>
@endsection