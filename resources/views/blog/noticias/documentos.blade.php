@extends('layouts.templateblog')
@section('section')
<br>
<div class="row">
    <div class="col-lg-9">
      <br>
      <center>  <h1 class="azul">Documentos Institucionales</<h1></center>
        <br>
      <div class="row">
        <div class="col-lg-4">
          <input class="form-control" id="myInput" type="text" placeholder="Buscar documentos..">
          <br>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
              <select class="form-control" id="sel1">
                <option>Elegir año:</option>
                <option>2021</option>
                <option>2020</option>
                <option>2019</option>
                <option>2018</option>
              </select>
            </div>
        </div>
        <div class="col-lg-4">
          <label class="radio-inline"><input type="radio" name="optradio" checked><strong> Resoluciones Directorales</strong> </label>
          <label class="radio-inline"><input type="radio" name="optradio"><strong> Instrumentos de gestión</strong></label>
             </div>
      </div>
      <table class="table table-bordered">
        <thead class="thead">
          <tr>
            <th scope="col">Nro. Documento</th>
            <th scope="col">Asunto</th>
            <th scope="col">Fecha</th>
            <th scope="col"> Acciones</th>
          </tr>
        </thead>
        @foreach ($documento as $item)
        <tbody>
          <tr>
            <td class="text-uppercase" > {{$item->titulo}}</td>
            <td class="text-uppercase">  </td>
            <td class="text-uppercase">{{$item->created_at}}</td>
            <th scope="row"><a href="{{asset($item->archivo)}}">Descargar</a></th>
          </tr>
        </tbody>
        @endforeach
      </table>
    </div>
    <div class="col-lg-3">
      @include('vp/col2')
    </div>
</div>
<style type="text/css">
  #index{ display:none}
</style>
@endsection