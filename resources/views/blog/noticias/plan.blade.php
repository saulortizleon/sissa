@extends('layouts.templateblog')
@section('section')
<br>
<div class="row">

  <!-- Post Content Column -->
  <div class="col-lg-9">
      <br> 
      <center><h1 class="azul">Plan Regional de Saneamiento  Apurímac (2021 – 2025)</h1></center> <br>     
      <div class="row">
        <div class="col-lg-3 col-sm-3 portfolio-item">           
            <a ><img class="card-img-top" src="{{ asset('img/institucion/plan.jpg') }}" height="auto" width="400"></a>   
            <br><br>
            <center> <a target="_blank" href="{{asset('doc/plan-saneamiento-apurimac-2021-2025.pdf')}}" class="btn btn-lg btn-primary"><b>DESCARGAR</b>  </a>  </center>
        </div>
        <div class="col-lg-9 col-sm-9 portfolio-item">   
          <h6 class="azul">Aprobado y publicado mediante la Resolución Ejecutiva Regional N°526-2020-GR.APURIMAC/GR</h6>    
          <p class="text-justify" > 
            El Gobierno Regional de Apurímac, en cumplimiento a su rol fundamental de promover
            el desarrollo regional de manera integral y sostenible; a través, de la Dirección Regional
            de Vivienda, Construcción y Saneamiento, ha formulado el Plan Regional de
            Saneamiento de Apurímac, 2021-2025; en concordancia con las políticas nacionales,
            sectoriales y los lineamientos para la formulación, aprobación, seguimiento y
            evaluación de los Planes Regionales de Saneamiento del Ministerio de Vivienda,
            Construcción y Saneamiento. <br><br>
    
            La finalidad fundamental de este documento es articular las acciones del gobierno
            central, entidades sectoriales, gobierno regional, gobiernos locales, y entidades
            prestadoras de los servicios de saneamiento, urbanos y rurales; en el único propósito de
            lograr que la población apurimeña tenga acceso universal a los servicios de saneamiento
            de calidad y sostenibles social, económica, técnica y ambientalmente. </p> 
      
          <p class="text-justify">
            El plan contiene diez secciones, se inicia con la sección de Marco general y antecedentes,
            donde se presenta el marco normativo vigente, así como, los fundamentos políticos y
            sociales, que sustentan y explican el carácter trascendental del acceso a los servicios de
            saneamiento. La segunda sección, se refiere a los objetivos del PRS y a manera se síntesis
            se presenta la visión regional del sector saneamiento al 2030. Seguidamente en la
            tercera sección, se desarrolla el diagnostico general del entorno a fin poder
            contextualizar la problemática del sector saneamiento, a nivel regional. 
          </p>     
          
          <p class="text-justify">  
            La sección cuatro, se refiere a las brechas de los servicios de saneamiento, tanto en el ámbito
            urbano como rural, haciendo uso para ello de indicadores de acceso, calidad y
            sostenibilidad de estos servicios. Luego de comprender la situación problemática del
            servicio de saneamiento, con la información de las secciones anteriores; en la quinta
            sección, se establecen las metas que se deben alcanzar durante el horizonte temporal
            del PRS; para tal efecto se hace uso de un conjunto de indicadores estandarizados por
            el sector. 
          </p>  
          <p class="text-justify">
            La sección seis corresponde a la identificación de proyectos, que contribuyan
            al logro de los objetivos del Plan y cabal cumplimiento de las metas propuestas. Para el
            logro de los objetivos y metas, se requiere desarrollar un conjunto de acciones
            específicas, precisamente en la sección siete se plantea las acciones estratégicas
            pertinentes. La sección ocho se refiere a las situaciones contingentes y de emergencia
            que pudieran, suceder durante el periodo del Plan, 2021-2025.
          </p> 
          <p class="text-justify">
            Como todo Plan requiere de recursos económicos y financieros, en la sección nueve se desarrolla el Plan
            Financiero, estimando los recursos disponibles en los tres niveles de gobierno para el
            cierre de brechas. Finalmente se presenta el Seguimiento, Monitoreo y Evaluación del
            Plan que contempla instrumentos como indicadores de cobertura, sostenibilidad,
            eficiencia, articulación y valoración que permitan monitorear y verificar el cumplimiento
            del Plan Regional de Saneamiento. 
          </p>
        </div>
      </div>
 
    <div class="row">
      <div class="col-lg-6 col-sm-6 portfolio-item">
        <div class="card h-100">
          <a ><img class="card-img-top" src="{{ asset('img/institucion/jass.jpg') }}" alt=""></a>
          <div class="card-body">
            <h3 class="card-title azul">
              <a >MISIÓN</a>
            </h3>
            <p class="card-text">Contribuir a mejorar las condiciones de vida de la población, facilitando su acceso
              a una vivienda saludable y a los servicios de saneamiento básico de agua y desague,
              apoyar el crecimiento urbano ordenado de los centro poblados y mejoramiento del medio ambiente,
              así como propiciar la participación de grupos organizados.</p>               
          </div>
        </div>
      </div>
      <div class="col-lg-6 col-sm-6 portfolio-item">
        <div class="card h-100">
          <a ><img class="card-img-top" src="{{ asset('img/institucion/mision.jpg') }}" alt=""></a>
          <div class="card-body">
            <h3 class="card-title azul">
              <a >VISIÓN</a>
            </h3>
            <p class="card-text">Al año 2030 en la Región Apurímac, la población accede a los servicios de saneamiento de calidad ;
              brindados con eficiencia, sostenibilidad económica, social y ambiental.</p>               
          </div>
        </div>
      </div>
      <div class="col-lg-12 col-sm-12 portfolio-item">          
        <center><h3 class="azul">OBJETIVO PRINCIPAL </h3>            
          <p class="card-text">Lograr el acceso universal, a servicios de saneamiento de calidad y sostenibles,
            en la Región Apurímac.</p>    
        </center> 
      </div> 
      <div class="col-lg-6 col-sm-6 portfolio-item">          
          <a ><img class="card-img-top" src="{{ asset('img/institucion/objetivo.jpg') }}" alt=""></a> 
      </div>
      <div class="col-lg-6 col-sm-6 portfolio-item">      
            <div class="list-group">                                    
              <h4 class="card-title azul">
                  OBJETIVOS ESPECÍFICOS
              </h4>
              <a  class="list-group-item list-group-item-action "><i class="fa fa-tint"></i> Atender a la población sin acceso a los servicios y de
                manera prioritaria a la de escasos recursos. </a>
              <a  class="list-group-item list-group-item-action "><i class="fa fa-tint"></i> Garantizar la generación de recursos económicos y su
                uso eficiente por parte de los prestadores.</a>
              <a  class="list-group-item list-group-item-action "><i class="fa fa-tint"></i> Desarrollar y fortalecer la capacidad de gestión de los
                prestadores.</a>
              <a  class="list-group-item list-group-item-action  "><i class="fa fa-tint"></i> Desarrollar proyectos de saneamiento sostenibles, con
                eficiencia técnica, administrativa, económica y
                financiera</a>
              <a  class="list-group-item list-group-item-action  "><i class="fa fa-tint"></i> Consolidar el rol rector del MVCS y fortalecer la
                  articulación con los actores involucrados en el sector saneamiento.</a>
              <a  class="list-group-item list-group-item-action  "><i class="fa fa-tint"></i> Desarrollar una cultura ciudadana de valoración de los
                    servicios de saneamiento.</a>
            </div>           
          
        
      </div>
    </div>      
  </div>   
  <div class="col-lg-3">
    @include('vp/col2')
  </div>
</div>
<style type="text/css">
 #index{ display:none}
</style>
@endsection