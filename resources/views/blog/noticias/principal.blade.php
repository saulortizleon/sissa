@extends('layouts.templateblog')
@section('section')
<br>
<div class="row">
    <div class="col-md-9" id="ocultar">
            <div class="card my-4" style="position: absolute;    top: 0.0em;
            left: 8;
            width: 39em;
            height: 22.4em;">
                <div id="carouselExampleControls" class="carousel slide carousel-fade" data-ride="carousel">
                    <div class="carousel-inner" role="listbox">
                    @foreach( $publicacion as $item )
                        <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
                        <a href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}"> <img style="position: absolute;    top: 0.0em;
                            left: 8;
                            width: 39em;
                            height: 22.4em;" src="{{asset($item->imagen)}}"> </a>
                                <div class="carousel-caption" style="
                                    text-transform: uppercase;  top: 10%;
                                        bottom: auto;
                                        -webkit-transform: translate(0, -50%);
                                        -ms-transform: translate(0, -50%);
                                        transform: translate(0, -50%);
                                        background-size: 20% auto;
                                    " >
                                    <div style="background-color: rgba(0, 123, 255, 0.4);"  > 
                                        <a href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}" 
                                            style="text-decoration:none; ">
                                            <h5 class="text-uppercase font-weight-bold" 
                                            style="color:white" >{{ $item->titulo }}</h5></a>
                                    </div>
                                </div>
                        </div>
                    @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    <div class="col-md-3">
            <div class="card my-4">
                <div class="btn-group-vertical">
                    <a  class="btn btn-primary" href="http://pnsr.vivienda.gob.pe/portal/"><i class="fa fa-tint"></i> PNSR</a>
                    <a class="btn btn-danger" href="{{ url('plan-regional-saneamiento-apurimac') }}"  style="color:white"><i class="fa fa-bookmark"></i> Plan Regional de Saneamiento</a>
                    <a class="btn btn-primary" href="https://www.mivivienda.com.pe/portalweb/usuario-busca-viviendas/pagina.aspx?idpage=30" style="color:white"> <i class="fa fa-cog"></i> Techo Propio</a>
                  </div>
            </div>
            <div class="card my-4">
                <ul class="list-group">
                        <li class="list-group-item "><a href="{{ url('romas') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Proyectos ROMAS</a> </li>
                        <li class="list-group-item "><a href="{{ url('vivienda-saludable') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Viviendas Saludables</a></li>
                        <li class="list-group-item "><a href="{{ url('atm') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> Capacitaciones</a> </li>
                        <li class="list-group-item "><a href="{{ url('comursaba') }}" style="font-size:13.5px;"><i class="fa fa-tint"></i> COMURSABA</a> </li>
                    </ul>
            </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 col-sm-3 portfolio-item">
        <div class="d-flex justify-content-start">
            <div class="image-container">
                <img src="{{asset('img/atm.png')}}" id="imgProfile" style="width: 200px; height: 150px" class="img-thumbnail" />
                <div class="middle">
                    <a  class="btn btn-primary" href="{{url('sisa')}}"> Ingresar</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-sm-3 portfolio-item">
        <div class="d-flex justify-content-start">
            <div class="image-container">
                <img src="{{asset('img/cloro.png')}}" id="imgProfile" style="width: 200px; height: 150px" class="img-thumbnail" />
                <div class="middle">
                    <a  class="btn btn-primary" href="{{url('calculador-de-cloro')}}"> Ingresar</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-sm-3 portfolio-item">
        <div class="d-flex justify-content-start">
            <div class="image-container">
                <img src="{{asset('img/cap.png')}}" id="imgProfile" style="width: 200px; height: 150px" class="img-thumbnail" />
                <div class="middle">
                    <a  class="btn btn-primary" href=""> Ingresar</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-sm-3 portfolio-item">
        <div class="d-flex justify-content-start">
            <div class="image-container">
                <img src="{{asset('img/vivienda-saludable.png')}}" id="imgProfile" style="width: 200px; height: 150px" class="img-thumbnail" />
                <div class="middle">
                    <a  class="btn btn-primary" href=""> Ingresar</a>
                </div>
            </div>
        </div>
    </div>
</div>
  <center><h6 style="color:#007bff;"><i class="fa fa-refresh"></i> Otros Servicios Online</h6></center>
<div class="row">
    <div class="col-lg-3 col-sm-3 mb-3">
        <a href="{{url('atm-apurimac')}}" class="btn btn-primary btn-sm" style="color:white;"><i class="fa fa-bar-chart"></i>Todo sobre ATM</a>
    </div>
    <div class="col-lg-3 col-sm-3 mb-3">
        <a class="btn btn-primary btn-sm" style="color:white;"><i class="fa fa-paper-plane"></i>  Trámites Online</a>
    </div>
    <div class="col-lg-3 col-sm-3 mb-3">
        <a href="" class="btn btn-primary btn-sm" style="color:white;"><i class="fa fa-flask  "></i>Todo sobre JASS</a>
    </div>
    <div class="col-lg-3 col-sm-3 mb-3">
        <a class="btn btn-primary btn-sm" style="color:white;"><i class="fa fa-book"></i> Todo sobre Urbanismo</a>
    </div>
  </div>
  <center><h6 style="color:#007bff;"><i class="fa fa-newspaper-o"></i> Notas de Prensa</h6></center>
<div class="row">
    @foreach($publicacion as $item)
        <div class="col-lg-4 col-sm-4 portfolio-item" >
            <div class="card h-100">
                <a href="#"><img class="card-img-top" src="{{asset($item->imagen)}}" alt=""></a>
                <div class="card-body">
                    <h6 class="card-title">
                    <a href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}">{{$item->titulo}}</a>
                    </h6>
                    <p class="card-text" style="font-size:14px">{{$item->subtitulo}}</p>
                    <a class="btn btn-link" href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}"><i class="fa fa-external-link-square"></i> Leer nota de prensa</a>

                </div>
            </div>
        </div>
    @endforeach
</div>
<center> <h6 style="color:green"><i class="fa fa-file-text"></i> Documentos Publicados</h6> </center>
<div class="row">
        @foreach($documento as $item)
        <div class="col-lg-4 col-sm-4 portfolio-item">
            <div class="card h-100">
                <div class="card-body text-success">
                    <h6 class="card-title">  <?php $resultado = substr($item->subtitulo, 0,60);
                    echo $resultado	?></h6>
                    <p class="card-text"><?php $resultado = substr($item->titulo, 0,120);
                    echo $resultado.'...'	?>	</p>
                    <a href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}"> <i class="fa fa-download"></i> Descargar</a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
@endsection