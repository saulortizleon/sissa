@extends('layouts.templateblog')
@section('section')
<br>
<div class="row">
<div class="col-md-12">
    <br>
    <center><h2 class="azul">Galería de imágenes</h2></center>
<br>
    <div class="row">

     @foreach($galeria as $item)
    <div class="col-lg-3 col-sm-3 portfolio-item">
      <div class="card h-100">
          <a href="#"><img class="card-img-top" src="{{asset($item->imagen)}}" alt=""></a>
        <div class="card-body">
          <p>{{ $item->titulo }}  <a type="button" class="btn btn-xs btn-outline-primary" href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}"><i class="fa fa-eye"></i> Ver más</a></p>

                 </div>
      </div>
    </div>

    @endforeach

  </div>
</div>

</div>
<style type="text/css">
 #index{ display:none}
</style>
@endsection
