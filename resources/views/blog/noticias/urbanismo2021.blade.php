@extends('layouts.templateblog')
@section('section')
<br>
<div class="row">
    <div class="col-lg-9">
        <br>
        <div class="row">
            <div class="col-lg-4">
                    <center> <img class="img-fluid" src="{{ asset('img/urbanismo-edificaciones.png') }}" width="80%" height="auto" >
                    </center> <br>
                    <div class="alert alert-warning" role="alert">
                      <center>  <h4 class="azul">Objetivo</h4>   </center>
                      <p align="center" style="font-size: 20px">
                        Promover  el cumplimiento de los lineamientos técnicos de las  Normas del Reglamento 
                        Nacional de Edificaciones a nivel regional y nacional para los empleadores
                         y trabajadores de la actividad pública y privada.
                      </p>
                    </div>
            </div>
            <div class="col-lg-8">
                    <center><h1 class="azul"><small> Capacitaciones en</small> URBANISMO
                    </h1>
                    <h3 class="azul"><i>  Normas del Reglamento Nacional de Edificaciones (RNE) 2021
                    </i>
                    </h3>
                    <div class="alert alert-primary" role="alert">
                     <h5> En cumplimiento de la  Ley Orgánica de Gobiernos Regionales N° 27867</h5>
                    </div>
                     
                    </center>
                    <p align="justify" style="font-size: 20px">
                      El Gobierno Regional de Apurímac mediante la Dirección Regional de  Vivienda,
                      Construcción y Saneamiento en cumplimiento a sus funciones de promover y regular
                      el adecuado desarrollo urbano de la región de acuerdo a la Ley Orgánica N° 27867, 
                      reinicia las capacitaciones virtuales, en esta ocasión en materia de urbanismo y 
                      edificaciones, con el primer módulo denominado: “Norma Técnica en Edificaciones
                      G.050 Seguridad durante la construcción”  dirigido a profesionales de Ingeniería Civil 
                      ,Arquitectura, estudiantes, técnicos  y público en general, con el objetivo de promover 
                      el cumplimiento de los lineamientos técnicos, las cuales se deben aplicar a nivel regional y
                      nacional para los empleadores y trabajadores de la actividad pública y privada, de tal manera 
                      que estas se desarrollen sin accidentes de trabajo, ni causen enfermedades ocupacionales.
                    </p>
                 
            </div>
        </div>
        <div class="row">
            <div class="col-4">
              <div class="list-group" id="list-tab" role="tablist">
                <a class="list-group-item list-group-item-action active"
                id="modulo2020-list" data-toggle="list" href="#modulo2020" role="tab" aria-controls="home"><strong> Módulos 2021</strong></a>
                
                <a class="list-group-item list-group-item-action"
                id="resultados-list" data-toggle="list" href="#resultados" role="tab" aria-controls="profile"><strong>Resultados</strong></a>
                
              </div>
            </div>
            <div class="col-8">
              <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="modulo2020" role="tabpanel" aria-labelledby="modulo2020-list">
                  
                    <i class="fa fa-arrow-right"></i>  NTP G.050 SEGURIDAD DURANTE LA CONSTRUCCIÓN	
                    <br> <i class="fa fa-arrow-right"></i> NTP G.50 SEGURIDAD DURANTE LA CONSTRUCCION
                    <br><i class="fa fa-arrow-right"></i> CLASE 03: LLENADO DE FICHAS NTP G.050    SEGURIDAD DURANTE LA CONSTRUCCIÓN 
                    <br> <i class="fa fa-arrow-right"></i> 	 NTP G.010 - CONDICIONES GENERALES DISEÑO
                    <br> <i class="fa fa-arrow-right"></i> NTP G.010 - CONDICIONES GENERALES DISEÑO
                    <br> <i class="fa fa-arrow-right"></i>  NTP A.020 VIVIENDA	
                    <br> <i class="fa fa-arrow-right"></i> NTP A.070 COMERCIO
                    <br> <i class="fa fa-arrow-right"></i> NTP E.030 - DISEÑO SISMORRESISTENTE
                    
                </div>
                <div class="tab-pane fade" id="resultados" role="tabpanel" aria-labelledby="resultados-list">
                  
                  <p align="justify" style="font-size: 20px"> 32 profesionales de Ingeniería Civil, 08 profesionales de Arquitectura, 41 técnicos y estudiantes CAPACITADOS.

                  </p>
                </div>
          
              </div>
            </div>
        </div>

        <br>
        <center><h6 class="azul"> NOTICIAS
        </h6> 
        </center> 
        <div class="row">
          @foreach($urbanismo2021 as $item)
          <div class="col-lg-4 col-sm-4 portfolio-item">                  
              <div class="card my-4">
                  <div class="card-body">
                      <a href="#"><img class="card-img-top" src="{{asset($item->imagen)}}" alt=""></a>
                      <div class="card-body">
                          <h6 class="card-title azul" >
                          <a href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}">{{$item->titulo}}</a>
                          </h6>
                          <p class="card-text" style="font-size:14px;  ">{{$item->subtitulo}}</p>
                          <a class="btn btn-link" href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}">
                          <i class="fa fa-external-link-square"></i> Leer más</a>
                      </div>
                  </div>
              </div>                  
          </div>
          @endforeach
      </div>
    </div>
    <div class="col-lg-3">
      @include('vp/col2')
     
    </div>
</div>
<br><br>
    <style type="text/css">
        #index{ display:none}
    </style>
@endsection