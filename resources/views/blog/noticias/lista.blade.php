@extends('layouts.templateblog')
@section('section')
        <br>
        <br>
    <div class="row">
        @foreach($noticias as $item)
      <div class="col-lg-4 col-sm-6 portfolio-item">
        <div class="card h-100">
            <a href="#"><img class="card-img-top" src="{{asset($item->imagen)}}" alt=""></a>
          <div class="card-body">
            <h6 class="card-title">
                <a href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}">{{$item->titulo}}</a>
            </h6>
            <p class="card-text" style="font-size:14px">{{$item->subtitulo}}</p>

            <a class="btn btn-link" href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}"><i class="fa fa-external-link-square"></i> Leer nota de prensa</a>
              </div>
        </div>
      </div>
      @endforeach
    </div>
    <nav aria-label="Page navigation example">
      <ul class="pagination justify-content-center">
        <li class="page-item disabled">
          <a class="page-link" href="#" tabindex="-1">Antes</a>
        </li>
        <li class="page-item"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item">
          <a class="page-link" href="#">Siguiente</a>
        </li>
      </ul>
    </nav>
    <style type="text/css">
      #index{ display:none}
  </style>
@endsection