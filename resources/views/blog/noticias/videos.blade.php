@extends('layouts.templateblog')
@section('section')
<br>
<div class="row">

<!-- Post Content Column -->
  <div class="col-lg-9">
      <br>
      <center><h5 class="azul">Videos Institucionales </h5></center>
      <p><i class="fa fa-folder-open"></i> Explicación del sistema de cloración por goteo mejorado, durante el VII ENCUENTRO DE JASS APURIMAC 2019</p>
      <div class="embed-responsive embed-responsive-16by9">
      <iframe  class="embed-responsive-item" width="525" height="315" src="https://www.youtube.com/embed/vURc_Xkv-Dk"
      frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

      </div>
    <br>   <p><i class="fa fa-folder-open"></i> VII encuentro JASS APURIMAC, realizado en la ciudad de Abancay los días 12 y 13 de noviembre del 2019</p>
    <div class="embed-responsive embed-responsive-16by9">
      <iframe class="embed-responsive-item" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fdrvcsapurimacoficial%2Fvideos%2F1642341372557156%2F&show_text=0&width=525"
      width="525" height="315" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
      </div>

  </div>
  <div class="col-lg-3">
    @include('vp/col2')
  </div>
</div>
<br>
<style type="text/css">
  #index{ display:none}
</style>

@endsection