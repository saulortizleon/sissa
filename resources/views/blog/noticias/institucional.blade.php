@extends('layouts.templateblog')
@section('section')
<br>
<div class="row">
  <div class="col-lg-9">
  <br>
    <center><h2 class="azul">DIRECCIÓN REGIONAL DE VIVIENDA, CONSTRUCCIÓN Y SANEAMIENTO APURÍMAC</h2></center>
        <p style="font-size:18px;  text-align: center;">
          Es un órgano desconcentrado de la Gerencia Regional de Infraestructura, del Gobierno Regional de Apurímac, que tiene personería 
           jurídica de derecho público; tiene a su cargo las funciones específicas en materia de vivienda y saneamiento, 
           señaladas en el artículo 58° de la Ley Orgánica de Gobiernos Regionales.
        </p>

  <div class="card my-4">    
    <div class="card-body">
      <div class="row"  >        
        <div class="col-lg-3">
          <img class="img-fluid" src="{{ asset('img/institucion/mision.jpg') }}"  >
        </div>      
        <div class="col-lg-9">      
          <center><h3 class="azul">Misión</h3></center>
          <p style="font-size:18px;  text-align: center;">Contribuir a mejorar las condiciones de vida de la población,
          facilitando su acceso a una vivienda saludable y a los servicios
            de saneamiento básico de agua y desagüe, apoyar el crecimiento
            urbano ordenado de los centros poblados y mejoramiento del Medio
            Ambiente, así como propiciar la participación de grupos organizados.
            </p>
          </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-lg-3">
          <img class="img-fluid" src="{{ asset('img/institucion/vision.jpg') }}"  >
        </div>
        <div class="col-lg-9">
          <center><h3  class="azul">Visión</h3></center>
          <p style="font-size:18px;  text-align: center;">La Región Apurímac cuenta con un
          territorio ordenado, centros de población
          competitivos y sostenibles, así como los servicios básicos
          de saneamiento básico de agua y desagüe que brindan condiciones
          para el mejoramiento continuo de la calidad de vida de la población.</p>
          </div>
      </div>
      </div>
    </div>
  </div>
  <div class="col-lg-3">
    @include('vp/col2')
  </div>
</div>
<style type="text/css">
   #index{ display:none}
</style>
@endsection