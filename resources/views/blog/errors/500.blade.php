@extends('layout.templateblog')
@section('section')
<div class="row">


<div class="col-lg-8">
          <br>
      <div class="error-template">
            <h1>
                Oops!</h1>
            <h2>
                500 Not Found</h2>
            <div class="error-details">
                lo sentimos, ocurrió un error, <small>La página aun se encuentra en construcción</small>
            </div>
            <div class="error-actions">
                <a href="{{ url('/') }}" class="btn btn-primary btn-sm"><span class="fa fa-home"> Ir a inicio</span></a>
                <a href="{{ url('contacto') }}" class="btn btn-danger btn-sm"><span class="fa fa-envelope"></span> Contactar </a>
            </div>
        </div>
</div>
<div class="col-md-4">

<!-- Search Widget -->
<div class="card my-4">
  <h5 class="card-header">Buscar</h5>
  <div class="card-body">
    <div class="input-group">
      <input type="text" class="form-control" placeholder="escribir...">
      <span class="input-group-btn">
        <button class="btn btn-secondary" type="button">ir!</button>
      </span>
    </div>
  </div>
</div>

<!-- Categories Widget -->
<div class="card my-4">
  <h5 class="card-header">Todo sobre</h5>
  <div class="card-body">
    <div class="row">
      <div class="col-lg-6">
        <ul class="list-unstyled mb-0">
          <li>
            <a href="#">ATM</a>
          </li>
          <li>
            <a href="#">JASS</a>
          </li>
          <li>
            <a href="#">Actividades</a>
          </li>
        </ul>
      </div>
      <div class="col-lg-6">
        <ul class="list-unstyled mb-0">
          <li>
            <a href="#">Capacitaciones</a>
          </li>
          <li>
            <a href="#">Trámites</a>
          </li>
          <li>
            <a href="#">Directorio</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>

<!-- Side Widget -->
<div class="card my-4">
  <h5 class="card-header">Sobre</h5>
  <div class="card-body">
   <button class="btn-primary">Proyectos</button>
   <button class="btn-danger">Prensa</button>
   <button class="btn-info">Avisos</button>
  </div>
</div>

</div>
</div>
@endsection