@extends('layouts.templateatm')
@section('section')
<section class='content-header'>
    <h1> GESTIÓN DE CENTROS POBLADOS</h1>
    <ol class='breadcrumb'>
        <li><a href='#'><i class='fa fa-dashboard'></i>ATM</a></li>
        <li class='active'>Centros Poblados</li>
    </ol>
</section>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class="box-body"> 
                    <div class="row">
                        <div class="col-md-4" >                   
                        <div class="form-group">
                            <label for="">Buscar por Nombre Centro Poblado : </label>
                            <input type="text" id="buscar"  class="form-control"> 
                        </div> 
                        </div> 
                        <div class="col-md-4" >  
                            <div class="input-group">
                                <label for="">  </label>                                
                            </div>
                        </div>
                    </div>                 
                </div>  
                <div class='box-body table-responsive'>
                    <table id='frmCp' class='table table-bordered table-hover'>
                        <thead>
                            <tr>
                                
                                <th> Centro Poblado </th>
                                <th> N° SA</th>
                                <th> Jerarquía</th>
                                <th> Adm Agua y San.</th>
                                <th> N° EPS</th>
                                <th> N° SAS</th>
                                <th> N° OES</th>
                                <th> N° UBS</th>
                                <th> N° SA</th>
                                <th> Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($listaCentroP as $item)
                            <tr>                               
                                <td>{{$item->nombre}}</td>
                                <td>{{$item->soloConUBS}}</td>
                                <td>{{$item->soloConUBS}}</td>  
                                <td>{{$item->soloConUBS}}</td> 
                                <td>{{$item->soloConUBS}}</td> 
                                <td>{{$item->soloConUBS}}</td>  
                                <td>{{$item->soloConUBS}}</td> 
                                <td>{{$item->soloConUBS}}</td> 
                                <td>{{$item->soloConUBS}}</td> 
                                <td><button class="btn btn-primary btn-xs"  onclick="actualizar('{{$item->ubigeo}}')">AGREGAR VERIFICABLES</button> 
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>  
 <script> 

$(document).ready(function(){
  $("#buscar").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#frmCp tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});

function actualizar(ubigeo)
    {   
    	window.location.href='{{url('centrop/editar')}}/'+ubigeo;
    }
$(document).ready(function() {
    $('#frmEditCP').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields:{ 
            description:{
                validators: {
                    notEmpty: {
                        message: 'La Descripción requerida'
                    }
                }
            },
             level:{
                validators: {
                    notEmpty: {
                        message: 'Seleccione un nivel'
                    }
                }
            }
        }
    });
});
</script>
@endsection