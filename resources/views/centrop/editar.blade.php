@extends('layouts.templateatm')
@section('section')
    <section class='content-header'>
        <h1> Centro poblado<small>Control panel</small></h1>
        <ol class='breadcrumb'>
            <li><a href='#'><i class='fa fa-dashboard'></i>editar </a></li>
            <li class='active'>Centro poblado</li>
        </ol>
    </section>
<section class='content'>
        <div class='row'>
            <div class='col-xs-12'>
                <div class='box'>
                    <div class='box-body'>  
                        <form id="frmFormacion" name="frmFormacion" action="{{url('formacion/editar')}}" method="post" enctype="multipart/form-data">  
                            {{csrf_field()}}
                            <div class="row">                        
                                <div class="col-md-4">
                                        <input type="hidden" name="idFormacion" value="{{$listaEditar->idFormacion}}"> 
                                    <div class="form-group" style="background-color:#d2cdcd;">
                                            ◘ Documento ROF :                                                                
                                            <label>
                                            <input type="radio"  class="minimal" name="ROF" id="ROF" value="SI" {{$listaEditar->ROF=="SI" ? "checked" : ""}} >
                                            SI
                                            </label>
                                            <label>
                                            <input type="radio"  class="minimal" name="ROF" id="ROF" value="NO" {{$listaEditar->ROF=="NO" ? "checked" : ""}} >
                                            NO
                                            </label> 
                                            <br>  
                                            <label for=""> ◘ Subir Verificable : </label> 
                                            <input  type="file"  class="form-control" id="ver1"  class="minimal" name="ver1" >                               
                                    </div>  
                                    <div class="form-group" style="background-color:#d2cdcd;" >
                                            ◘ Documento MOF :                                                            
                                            <label>
                                            <input type="radio"  class="minimal" name="MOF" id="MOF" value="SI" {{$listaEditar->MOF=="SI" ? "checked" : ""}} >
                                            SI
                                            </label>
                                            <label>
                                            <input type="radio"  class="minimal" name="MOF" id="MOF" value="NO" {{$listaEditar->MOF=="NO" ? "checked" : ""}} >
                                            NO
                                            </label>
                                            <br> 
                                            <label for="">◘  Subir Verificable : </label>   
                                            <input  type="file"  class="form-control" id="ver2"  class="minimal" name="ver2" >     
                                    </div> 
                                    <div class="form-group" style="background-color:#d2cdcd;">
                                        ◘ Documento CAP :                                                           
                                            <label>
                                            <input type="radio"  class="minimal" name="CAP" id="CAP" value="SI" {{$listaEditar->CAP=="SI" ? "checked" : ""}} >
                                            SI
                                            </label>
                                            <label>
                                            <input type="radio"  class="minimal" name="CAP" id="CAP" value="NO" {{$listaEditar->CAP=="NO" ? "checked" : ""}} >
                                            NO
                                            </label>
                                            <br> 
                                            <label for="">◘  Subir Verificable : </label>  
                                            <input  type="file"  class="form-control" id="ver3"  class="minimal" name="ver3" >                                                             
                                    </div> 
                                    <div class="form-group" style="background-color:#d2cdcd;">
                                            ◘ Documento TUPA :                                                           
                                            <label>
                                            <input type="radio"  class="minimal" name="TUPA" id="TUPA" value="SI" {{$listaEditar->TUPA=="SI" ? "checked" : ""}} >
                                            SI
                                            </label>
                                            <label>
                                            <input type="radio"  class="minimal" name="TUPA" id="TUPA" value="NO" {{$listaEditar->TUPA=="NO" ? "checked" : ""}} >
                                            NO
                                            </label>
                                            <br> 
                                            <label for="">◘  Subir Verificable : </label>   
                                            <input  type="file"  class="form-control" id="ver4" name="ver4" >                                
                                    </div>                          
                                    <div class="form-group" style="background-color:#d2cdcd;">
                                            ◘ Grado Académico : 
                                        <select class="form-control" name="gradoAcadem" id="gradoAcadem">
                                            <option value="bachiller" {{$listaEditar->ROF=="bachiller" ? "selected" : ""}} >Bachiller</option>
                                            <option value="titulado" {{$listaEditar->ROF=="titulado" ? "selected" : ""}} >Titulado</option>
                                            <option value="tecnico" {{$listaEditar->ROF=="tecnico" ? "selected" : ""}} >Técnico</option>
                                        </select>
                                        <label for="">◘  Subir Verificable : </label>  
                                        <input  type="file"  class="form-control" id="ver5" name="ver5" >
                                    </div>
                                    <div class="form-group" style="background-color:#d2cdcd;">
                                            <label for="description">◘ Especialidad</label>
                                            <input type="text"  class="form-control" id="especialidad" name="especialidad" placeholder="..." value="{{$listaEditar->especialidad}}" >
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <div class="form-group" style="background-color:#d2cdcd;">
                                            ◘ Capacitación Gob. Nacional:                
                                            <label>
                                            <input type="radio"  class="minimal" name="GN" id="GN" value="SI" {{$listaEditar->GN=="SI" ? "checked" : ""}} >
                                            SI
                                            </label>
                                            <label>
                                            <input type="radio"  class="minimal" name="GN" id="GN" value="NO" {{$listaEditar->GN=="NO" ? "checked" : ""}} >
                                            NO
                                            </label>   
                                            <label for="">◘ Subir Verificable</label> 
                                            <input  type="file"  class="form-control" id="ver6"  name="ver6" >                               
                                    </div>                                     
                                    <div class="form-group" style="background-color:#d2cdcd;">
                                            ◘ Capacitación Gobierno Regional:                 
                                        <label>
                                        <input type="radio"  class="minimal" name="GR" id="GR" value="SI" {{$listaEditar->GR=="SI" ? "checked" : ""}} >
                                        SI
                                        </label>
                                        <label>
                                        <input type="radio"  class="minimal" name="GR" id="GR" value="NO" {{$listaEditar->GR=="NO" ? "checked" : ""}} >
                                        NO
                                        </label>
                                        <label for=""> ◘ Subir Verificable</label> 
                                        <input  type="file"  class="form-control" id="ver7"  class="minimal" name="ver7" > 
                                    </div>
                                    <div class="form-group" style="background-color:#d2cdcd;">
                                            ◘ Capacitación Gobierno local:                 
                                        <label>
                                        <input type="radio"  class="minimal" name="GL" id="GL" value="SI" {{$listaEditar->GL=="SI" ? "checked" : ""}} >
                                        SI
                                        </label>
                                        <label>
                                        <input type="radio"  class="minimal" name="GL" id="GL" value="NO" {{$listaEditar->GL=="NO" ? "checked" : ""}} >
                                        NO
                                        </label>   
                                        <label for="">◘ Subir Verificable</label> 
                                        <input  type="file"  class="form-control" id="ver8"  class="minimal" name="ver8"  > 
                                    </div> 
                                    <div class="form-group" style="background-color:#d2cdcd;">
                                            ◘ Capacitación por alguna ONG:                 
                                        <label>
                                        <input type="radio"  class="minimal" name="ONG" id="ONG" value="SI" {{$listaEditar->ONG=="SI" ? "checked" : ""}} >
                                        SI
                                        </label>
                                        <label>
                                        <input type="radio"  class="minimal" name="ONG" id="ONG" value="NO" {{$listaEditar->ONG=="NO" ? "checked" : ""}} >
                                        NO
                                        </label>   
                                        <label for="">◘ Subir Verificable</label> 
                                        <input  type="file"  class="form-control" id="ver9"  class="minimal" name="ver9" > 
                                    </div> 
                                    <div class="form-group" style="background-color:#d2cdcd;">
                                            ◘ Capacitación por  EDUSA:                 
                                        <label>
                                        <input type="radio"  class="minimal" name="EDUSA" id="EDUSA" value="SI" {{$listaEditar->EDUSA=="SI" ? "checked" : ""}} >
                                        SI
                                        </label>
                                        <label>
                                        <input type="radio"  class="minimal" name="EDUSA" id="EDUSA" value="NO" {{$listaEditar->EDUSA=="NO" ? "checked" : ""}}  >
                                        NO
                                        </label>   
                                        <label for="">◘ Subir Verificable</label> 
                                        <input  type="file"  class="form-control" id="ver9"  class="minimal" name="ver9"  > 
                                    </div> 
                                    <div class="form-group" style="background-color:#d2cdcd;">
                                            ◘ Capacitación por  AOM:                 
                                        <label>
                                        <input type="radio"  class="minimal" name="AOM" id="AOM" value="SI" {{$listaEditar->AOM=="SI" ? "checked" : ""}} >
                                        SI
                                        </label>
                                        <label>
                                        <input type="radio"  class="minimal" name="AOM" id="AOM" value="NO" {{$listaEditar->AOM=="NO" ? "checked" : ""}}  >
                                        NO
                                        </label>   
                                        <label for="">◘ Subir Verificable</label> 
                                        <input  type="file"  class="form-control" id="ver10"  class="minimal" name="ver10" > 
                                    </div> 
                                </div>
                                <div class="col-md-4">

                                        <div class="form-group" style="background-color:#d2cdcd;">
                                                ◘ Capacitación por  DL1280:                 
                                            <label>
                                            <input type="radio"  class="minimal" name="DL1280" id="DL1280" value="SI" {{$listaEditar->DL1280=="SI" ? "checked" : ""}} >
                                            SI
                                            </label>
                                            <label>
                                            <input type="radio"  class="minimal" name="DL1280" id="DL1280" value="NO" {{$listaEditar->DL1280=="NO" ? "checked" : ""}} >
                                            NO
                                            </label>   
                                            <label for="">◘ Subir Verificable</label> 
                                            <input  type="file"  class="form-control" id="ver10"  class="minimal" name="ver10" > 
                                        </div> 
                                        <div class="form-group" style="background-color:#d2cdcd;">
                                                ◘ Capacitación por  PNSR:                 
                                            <label>
                                            <input type="radio"  class="minimal" name="PNSR" id="PNSR" value="SI" {{$listaEditar->PNSR=="SI" ? "checked" : ""}} >
                                            SI
                                            </label>
                                            <label>
                                            <input type="radio"  class="minimal" name="PNSR" id="PNSR" value="NO" {{$listaEditar->PNSR=="NO" ? "checked" : ""}} >
                                            NO
                                            </label>   <br>
                                            <label for="">◘ Subir Verificable</label> 
                                            <input  type="file"  class="form-control" id="ver10"  class="minimal" name="ver10" > 
                                        </div>
                                        <div class="form-group" style="background-color:#d2cdcd;">
                                                ◘ Capacitación por  PNSR otros:                 
                                            <label>
                                            <input type="radio"  class="minimal" name="PNSRotros" id="PNSRotros" value="SI" {{$listaEditar->PNSRotros=="SI" ? "checked" : ""}} >
                                            SI
                                            </label>
                                            <label>
                                            <input type="radio"  class="minimal" name="PNSRotros" id="PNSRotros" value="NO" {{$listaEditar->PNSRotros=="NO" ? "checked" : ""}} >
                                            NO
                                            </label>   
                                            <label for="">◘ Subir Verificable</label> 
                                            <input  type="file"  class="form-control" id="ver11"  class="minimal" name="ver11" > 
                                        </div>
                                        <div class="form-group" style="background-color:#d2cdcd;">
                                                ◘ Plan de saneamiento Nacional               
                                            <label>
                                            <input type="radio"  class="minimal" name="planSan" id="planSan" value="SI" {{$listaEditar->planSan=="SI" ? "checked" : ""}} >
                                            SI
                                            </label>
                                            <label>
                                            <input type="radio"  class="minimal" name="planSan" id="planSan" value="NO" {{$listaEditar->planSan=="NO" ? "checked" : ""}} >
                                            NO
                                            </label>   
                                            <label for="">◘ Subir Verificable</label> 
                                            <input  type="file"  class="form-control" id="ver12"  class="minimal" name="ver12" > 
                                        </div>
                                        <div class="form-group" style="background-color:#d2cdcd;">
                                                ◘ R.M 155-2017 Criterios de Admisibilidad            
                                            <label>
                                            <input type="radio"  class="minimal" name="criterios" id="criterios" value="SI" {{$listaEditar->criterios=="SI" ? "checked" : ""}} >
                                            SI
                                            </label>
                                            <label>
                                            <input type="radio"  class="minimal" name="criterios" id="criterios" value="NO" {{$listaEditar->criterios=="NO" ? "checked" : ""}} >
                                            NO
                                            </label>   
                                            <label for="">◘ Subir Verificable</label> 
                                            <input  type="file"  class="form-control" id="ver13"  class="minimal" name="ver13" > 
                                        </div>
                                        <div class="form-group">  
                                            <label for=""><i>Asegúrese de llenar correctamente los items antes de guardar los cambios</i> </label>  
                                                <input type="submit" value="Guardar" class="btn btn-primary btn-sm">
                                                <a href="{{url('formacion/lista')}}" class="btn btn-danger btn-sm" >Regresar</a>
                                            </div>  
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script>
$(document).ready(function() {
    $('#frmFormacion').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields:{ 
            especialidad:{
                validators: {
                    notEmpty: {
                        message: 'La especialidad es requerido'
                    }
                }
            },
            ver1:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
             ver2:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver3:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver4:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver5:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver6:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver7:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver8:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver9:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver10:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver11:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },

        }
    });
});
</script>
@endsection
