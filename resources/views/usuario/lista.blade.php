@extends('layouts.templatesup')
@section('section')
<section class='content-header'>
    <h1>Gestión datos Responsable <small>Area Tecnica Municipal</small> </h1>
    <ol class='breadcrumb'>
        <li><a href='#'><i class='fa fa-dashboard'></i>Gestion ATM</a></li>
        <li class='active'>Lista</li>
    </ol>
</section>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-body table-responsive'>
                    <table id='' class='table table-bordered table-hover'>
                        <thead>
                            <tr>
                                <th>Dni</th>
                                <th>Nombres</th>
                                <th>Celular</th>
                                <th>Correo</th>
                                <th>Fecha Nac.</th>
                                <th>Cargo</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($listaUsuario as $item)
                            <tr>
                                <td>{{$item->dniUsuario}}</td>
                                <td>{{$item->nombres}}</td>
                                <td>{{$item->celular}}</td>
                                <td>{{$item->correo}}</td>
                                <td>{{$item->fechaNacimiento}}</td>
                                <td>{{$item->cargo}}</td>
                                <td>{{$item->estado }}</td>
                                <td>
                                    <a href="#" onclick="actualizar('{{$item->dniUsuario}}');" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> Editar Información</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    function actualizar(dniUsuario)
    {
    	window.location.href='{{url('usuario/editar')}}/'+dniUsuario;
    }
</script>
@endsection