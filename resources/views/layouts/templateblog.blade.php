@include('layouts/head')
 
<body>

    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v5.0&appId=332640044163509&autoLogAppEvents=1"></script>
    
    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-primary fixed-top"  role="navigation" style="height: 100px; background-color : #182d80">    
        <div class="container-fluid" style="background-color : #182d80">
                <a class="navbar-brand pull-left" href="{{ url('/') }}"> <img  src="{{asset('logos/drvcs-apurimac.png')}}" width="250"></a>
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive" style="background-color : #182d80">
                <ul class="nav navbar-nav navbar-left ml-auto" style="align-content: center; !important"  style="background-color : #182d80">
                    <li class="nav-item" >
                        <a class="nav-link" href="{{url('/')}}" style="font-size: 1.16em !important;"><strong> Inicio</strong></a>
                    </li>
                    <li class="nav-item dropdown px-2.0">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" 
                        aria-haspopup="true" aria-expanded="false" style="font-size: 1.08em !important;"> 
                            <i class="fa fa-university"></i>  <strong> Institución </strong>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio" style="background-color: #00afef">
                            <a class="dropdown-item" href="{{url('nuestra-institucion')}}"  style=" font-size: 1.18em !important;">  <i class="fa fa-users"></i> <strong>Nosotros</strong> </strong></a>
                            <a class="dropdown-item" href="{{url('funciones')}}"  style=" font-size: 1.18em !important;"><i class="fa fa-cog"></i> <strong>¿Qué hacemos?</strong></a>
                            <a class="dropdown-item" href="{{url('objetivos')}}"  style=" font-size: 1.18em !important;"><i class="fa fa-check"></i> <strong>Objetivos</strong></a>
                            <a class="dropdown-item" href="{{url('plan-regional-saneamiento')}}"  style=" font-size: 1.18em !important;"><i class="fa fa-refresh"></i><strong> Políticas y Planes</strong></a>
                            <a class="dropdown-item" href="{{url('directorio-institucional')}}"  style=" font-size: 1.18em !important;"><i class="fa fa-fax"></i><strong> Directorio Institucional</strong></a>

                        </div>
                    </li>
                    <li class="nav-item dropdown px-2.0" >
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" font-size: 1.18em !important;>
                            <i class="fa fa-cogs"></i>  <strong>Áreas</strong>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                            <a class="dropdown-item" href="{{ url('vivienda-y-urbanismo') }}" style=" font-size: 1.18em !important;"><i class="fa fa-home"></i> <strong>Vivienda y Urbanismo</strong></a>
                            <a class="dropdown-item" href="{{ url('construccion-y-saneamiento') }}" style=" font-size: 1.18em !important;"><i class="fa fa-tint"></i> <strong>Construcción y Saneamiento</strong></a>
                        </div>
                    </li>
                    <li class="nav-item dropdown px-2.0" >
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" font-size: 1.18em !important;>
                            <i class="fa fa-line-chart"></i>  <strong>Actividades   </strong>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                            <a class="dropdown-item" href="{{url('wasichakuy')}}"  style=" font-size: 1.18em !important;"><strong>Wasichakuy</strong></a>
                            <a class="dropdown-item" href="{{url('escuela-saneamiento')}}"  style=" font-size: 1.18em !important;"><strong>Yaku Kawsay</strong></a>                 
                            <a class="dropdown-item" href="{{url('viviendas-saludables')}}"  style=" font-size: 1.18em !important;"><strong> Educación Sanitaria</strong></a>
                            <a class="dropdown-item" href="{{url('romas')}}"  style=" font-size: 1.18em !important;"><strong> ROMAS</strong></a>
                            <a class="dropdown-item" href="{{url('fed')}}"  style=" font-size: 1.18em !important;"><strong> FED</strong></a>
                            <a class="dropdown-item" href="{{url('edificaciones')}}"  style=" font-size: 1.18em !important;"><strong> Edificaciones</strong></a>
                            <a class="dropdown-item" href="{{url('comursaba')}}"  style=" font-size: 1.18em !important;"><strong>COMURSABA</strong></a>
                            <a class="dropdown-item" href="{{url('urbanismo')}}"  style=" font-size: 1.18em !important;"><strong> Urbanismo</strong></a>
                        </div>
                    </li>

                    <li class="nav-item dropdown px-2.0" >
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-newspaper-o"></i>   <strong>Noticias   </strong>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                            <a class="dropdown-item" href="{{url('comunicados')}}"  style=" font-size: 1.18em !important;"><strong> Comunicados </strong></a>
                            <a class="dropdown-item" href="{{url('noticias')}}"  style=" font-size: 1.18em !important;"><strong>  Notas de Prensa</strong></a>
                            <a class="dropdown-item" href="{{url('videos')}}"  style=" font-size: 1.18em !important;"><strong>  Videos </strong></a>
                            <a class="dropdown-item" href="{{url('galeria-fotos')}}"  style=" font-size: 1.18em !important;"><strong> Fotos </strong></a>
                        </div>
                    </li>
                    <li class="nav-item dropdown px-2.0">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownBlog" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-search"></i>    <strong>Servicios</strong>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownBlog">
                            <a class="dropdown-item" href="{{url('calculador-de-cloro')}}"  style=" font-size: 1.18em !important;"><strong>Calculador de Cloro</strong> </a>
                            <a class="dropdown-item" href="{{url('mpv')}}"  style=" font-size: 1.18em !important;"> <strong>Mesa de partes </strong></a>
                            <a class="dropdown-item" href="{{url('planos-prediales')}}"  style=" font-size: 1.18em !important;"><strong>Valores Arancelarios</strong></a>
                            <a class="dropdown-item" href="{{url('documentos-institucionales')}}"  style=" font-size: 1.18em !important;"> <strong> Documentos</strong></a>
                            <a class="dropdown-item" href="{{url('verificar-certificados')}}"  style=" font-size: 1.18em !important;"><strong> Certificados </strong></a>                        
                            <a class="dropdown-item" href="{{url('contactenos')}}" style=" font-size: 1.18em !important;" >  <i class="fa fa-phone"></i> <strong>Contáctanos</strong></a>
                            <a class="dropdown-item" href="{{url('login')}}" style=" font-size: 1.18em !important;" >  <i class="fa fa-user-circle"></i> <strong>Iniciar Sesión</strong></a>

                        </div>
                    </li>
                   
                 
                </ul> 
                <ul class="nav navbar-nav navbar-center px-2.0">
                    <li class="nav-item" >
                      <center><a  class="nav-link disabled" href="{{ url('/') }}"> <img   src="{{asset('img/a.png')}}" width="80"></a> </center>  
                    </li>
                </ul> 
            </div>
        </div>
    </nav>
    
    @yield('header')
    <!-- Page Content -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <br>
                @yield('section')
                @include('vp/barra1')
            </div>
                @include('blog/app/popup')
        </div>
  </div>
  @include('layouts/footer')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" rel="stylesheet" />
    <script src="{{asset('js/aliados.js')}}"></script>
    <script src="{{asset('admin/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('admin/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('admin/bower_components/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('formvalidation/formValidation.min.js')}}"></script>
    <script src="{{asset('formvalidation/bootstrap.validation.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script src="https://apis.google.com/js/platform.js"></script>

    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v4.0&appId=332640044163509&autoLogAppEvents=1"></script>
    <script>

    $("#submit1").click(function(e){
        e.preventDefault();
        var text = $('#atm').val();
        $.ajax({
            type    : 'GET',
            url     :  'verificar-certificados',
            data    : {'atm':text},
            dataType: 'json',
            beforeSend:function(data){ //  la wea que precarga
             $('#resultadoatm').html('<i>Cargando...</i>');
            },
            success: function (result) {//si va todo bien equis de
                var x =(JSON.stringify(result));//que fuee
                // $('#resultadoatm').html(x);
                if( x=== "[]")
                {
                    $('#resultadoatm').html("<h3><i> No tienes ningún registro... </i> </h3>");
                    setTimeout(function() {
                        $('#resultadoatm').fadeOut('slow');
                            }, 100000);
                }
                else{
                    for(var i = 0; i < result.length; i++) {
                      var d = result[i];
                      $('#resultadoatm').html('<h5> <b> Distrito:</b> </h5>' + d.DISTRITO + '<br>'+ '<h5> <b>Nombres:</b> </h5> '
                      + d.NOMBRES  + '<br>'+ '<h5> <b>Apellidos:</b> </h5> ' + d.APELLIDOS + '<br>'+ '<h5> <b>N° de Certificado: </b></h5> ' + d.CODIGO+ '<br>'
                      +'<h5> <b>Año académico :</b></h5>  '+'2020' );
                      setTimeout(function() {
                        $('#resultadoatm').fadeOut('slow');
                            }, 100000);
                    };
                }
                }
            });
    });
    $("#submit2").click(function(e){
        e.preventDefault();
        var text = $('#ope').val();
        $.ajax({
            type    : 'GET',
            url     :  'verificar-certificados2',
            data    : {'ope':text},
            dataType: 'json',
            beforeSend:function(data){ //  la wea que precarga
             $('#resultadoope').html('<i>Cargando...</i>');
            },
            success: function (result) {//si va todo bien equis de
                var x =(JSON.stringify(result));//que fuee
                // $('#resultadoatm').html(x);
                if( x=== "[]")
                {
                    $('#resultadoope').html("<h3><i> No tienes ningún registro... </i> </h3>");
                    setTimeout(function() {
                        $('#resultadoope').fadeOut('slow');
                            }, 100000);
                }
                else{
                    for(var i = 0; i < result.length; i++) {
                      var d = result[i];
                      $('#resultadoope').html('<h5> <b> Distrito:</b> </h5>' + d.DISTRITO + '<br>'+ '<h5> <b>Nombres:</b> </h5> '
                      + d.NOMBRES  +'<br>'+ '<h5> <b>N° de Certificado: </b></h5> ' + d.CODIGO+ '<br>'
                      +'<h5> <b>Año académico :</b></h5>  '+'2020' );
                      setTimeout(function() {
                        $('#resultadoope').fadeOut('slow');
                            }, 100000);
                    };
                }
                }
            });
    });
    $("#submit3").click(function(e){
        e.preventDefault();
        var text = $('#was').val();
        $.ajax({
            type    : 'GET',
            url     :  'verificar-certificados3',
            data    : {'was':text},
            dataType: 'json',
            beforeSend:function(data){ //  la wea que precarga
             $('#resultadowasi').html('<i>Cargando...</i>');
            },
            success: function (result) {//si va todo bien equis de
                var x =(JSON.stringify(result));//que fuee
                // $('#resultadoatm').html(x);
                if( x=== "[]")
                {
                    $('#resultadowasi').html("<h3><i> No tienes ningún registro... </i> </h3>");
                    setTimeout(function() {
                        $('#resultadowasi').fadeOut('slow');
                            }, 100000);
                }
                else{
                    for(var i = 0; i < result.length; i++) {
                      var d = result[i];
                      $('#resultadowasi').html( '<h5> <b>Nombres:</b> </h5> '
                      + d.NOMBRES  +'<br>'+ '<h5> <b>N° de Certificado: </b></h5> ' + d.CODIGO+ '<br>'
                      +'<h5> <b>Participación:</b></h5>  '+' 28 octubre - 16 Diciembre 2020' );
                      setTimeout(function() {
                        $('#resultadowasi').fadeOut('slow');
                            }, 100000);
                    };
                }
                }
            });
    });
        $('#mainCarousel').carousel({
            interval: 3500
        });
            var loc = window.location.pathname;

          if(loc=="/")
          {
                $('#myModal').modal('show');
                setTimeout(function(){
                    $('#myModal').modal('hide')
                  }, 10000);
            }
            else
            {
                $('#myModal').modal('hide')
            }

            $.ajax({
                url: 'comunicado',
                type:'get',
                success: function(respuesta) {

                var listaAviso = $("#aviso");

                $.each(respuesta.data, function(index, elemento) {
                    var a=elemento.imagen;
                    var x="http://drvcs.regionapurimac.gob.pe/";
                    listaAviso.append(
                        '<img src="'+ x + a + '"'+  'class="'+'img-fluid"'  + '"'+'>'
                    );

                    });
                    var boton = $("#boton");
                    $.each(respuesta.data, function(index, elemento) {
                    var x="http://drvcs.regionapurimac.gob.pe/noticias/detalle/";
                    var y= "Leer Comunicado";
                        boton.append(
                            '<h6 class="'+'modal-title">' + elemento.titulo + '</h6>' +
                  '<a class="'+'btn btn-primary btn-sm"'+' '+' href="'+ x + elemento.idPublicacion+'">'
                      +'<i class="'+'fa fa-hand-o-right">'+'</i>'+' '+y+ '</a>'

                        );
                        });
                },
                error: function() {
                console.log("No se ha podido obtener la información");
                }
            });
          $(document).ready(function() {
            $('#calcular').click(function(){
                var caudal = parseFloat($('#caudal').val(), 10);
                var cap = parseFloat($('#cap21').val(), 10);
                var goteo = parseFloat($('#goteo').val(), 10);
                var r;
                var gastodiario;
              console.log(caudal);
              console.log(cap);
              console.log(goteo);

              gastodiario= parseFloat((goteo *(1000)) * 1440   ) ;//litros por dia

                var v =parseFloat(caudal) * 86400;
                var resultado= parseFloat(v) * 1.0 / 700;//kilos

              //duracion tanque
                var r = gastodiario/1000000;

                var duracion=cap/r;
                var s=duracion.toFixed();
                var f=resultado.toFixed(2);
                var p;
                p= s*f;
                var rr;
                rr=p.toFixed();

                var kilos;
                kilos= rr/1000;
              // document.getElementById('dias').value=s;
                document.getElementById('duracion').innerHTML = s;
                document.getElementById('re').innerHTML = f;
                document.getElementById('recarga').innerHTML = rr;
                document.getElementById('kilos').innerHTML = kilos;
                });
            });


      /* $('input[type=checkbox]').on('click', function () {
            if ($(this).prev().text()==="Captacion") {
                    $(this).prev().text('Reservorio')
                    $('#cu').show();
                    $('#result1').show();
                        $('#result3').hide();
                    $('#controles').show();
                $('#cap').hide();
            } else {
            $(this).prev().text('Captacion')
                    $('#ci').hide();
                    $('#cu').hide();
                $('#result1').hide();
                $('#result2').hide();
            $('#result3').show();
            $('#controles').hide();
                $('#cap').show();
            }
        });*/
        $("#ci").hide();
        $("#result2").hide();
        $("#result1").hide();
        $("#frmTub").hide();
        $(function() {
            $("#tipo").on('change', function() {
            var selectValue = $(this).val();
            switch (selectValue) {

                case "1":
                    $("#frmCap").show();
                    $("#frmRes").hide();
                    $("#frmCam").hide();
                    $("#frmTub").hide();
                    $("#frmCirculo").hide();
                    $("#result2").hide();
                    $("#result1").hide();
                    $("#result3").show();
                    $("#radios").hide();
                    $("#result4").hide();
                    $("#result5").hide();
                break;

                case "2":

                    $("#frmCap").hide();
                    $("#frmRes").show();
                    $("#frmCam").hide();
                    $("#frmTub").hide();
                    $("#radios").show();
                    $("#result1").show();
                    $("#result3").hide();
                    $("#result4").hide();
                    $("#result5").hide();
                break;

                case "3":
                $("#frmTub").show();
                $("#frmCap").hide();
                $("#frmCam").hide();
                $("#frmRes").hide();
                $("#frmCirculo").hide();
                $("#radios").hide();
                $("#result1").hide();
                $("#result3").hide();
                $("#result2").hide();
                $("#result4").show();
                $("#result5").hide();
                break;

                case "4":
                $("#frmTub").hide();
                $("#frmCam").show();
                $("#frmCap").hide();
                $("#frmRes").hide();
                $("#frmCirculo").hide();
                $("#radios").hide();
                $("#result1").hide();
                $("#result3").hide();
                $("#result2").hide();
                $("#result4").hide();
                $("#result5").show();
            break;
            }
            }).change();

        });
        function show1(){
            $("#frmCirculo").show();
            $("#frmRes").hide();
            $("#result2").show();
            $("#result1").hide();
            $("#result3").hide();
            }
            function show2(){
            $("#frmCirculo").hide();
            $("#frmRes").show();
            $("#result2").hide();
            $("#result1").show();
            $("#result3").hide();
            }


        $(document).ready(function() {
        $('#calcular3').click(function(){
            var altura1 = parseFloat($('#altura1').val(), 10);
            var base1 = parseFloat($('#base1').val(), 10);
            var agua1 = parseFloat($('#agua1').val(), 10);

            var v;
            v= parseFloat(altura1 * base1 * agua1  * 1000  );
            var p;
            p=(v*175)/(700);//gramos
                var r;
            r=v.toFixed();
            var ee;
            ee= (v/1000).toFixed(2);
                var hh;
            hh= (p/1000).toFixed(2);

            document.getElementById('v33').innerHTML = p;
            document.getElementById('v3').innerHTML = r;
                document.getElementById('m33').innerHTML = ee;
                document.getElementById('kilos3').innerHTML = hh;

            });
        });

        $(document).ready(function() {
        $('#calcular1').click(function(){
            var altura = parseFloat($('#altura').val(), 10);
            var base = parseFloat($('#base').val(), 10);
            var agua = parseFloat($('#agua').val(), 10);

            var v;
            v= parseFloat(altura * base * agua  * 1000  );//litros por dia
            var p;
            p=((v*50)/(700)).toFixed(2);//gramos
                var r;
            r=v.toFixed(2);
            var ee;
            ee= (v/1000).toFixed(2);
                var hh;
            hh= (p/1000).toFixed(2);

            document.getElementById('v11').innerHTML = p;
            document.getElementById('v1').innerHTML = r;
                document.getElementById('m31').innerHTML = ee;
                document.getElementById('kilos1').innerHTML = hh;

            });
        });
        $(document).ready(function() {
        $('#calcular2').click(function(){
        var diametro = parseFloat($('#diametro').val(), 10);
        var alturaAgua = parseFloat($('#alturaAgua').val(), 10);

        var v;
            v= parseFloat(((Math.PI) * Math.pow(diametro,2) ) * alturaAgua)/4 ;//m3  vaaaaaaaaaa
        var vdecimal
        vdecimal=v.toFixed(4);
        var litros
        litros=v*1000//litros
        var reduccionLitros
        reduccionLitros= litros.toFixed(4); //vaaaaaaaa

        p=(litros*50)/(700);//cloro en gramos
        var gramos;

        gramos=p.toFixed(4);//gramos reducido vaaaaaa

        var kilos;
        kilos= (gramos/1000);//kilos
        var kilosReducido;
        kilosReducido=kilos.toFixed(4);//vaaaaa
        document.getElementById('vm3circular').innerHTML = vdecimal;
        document.getElementById('vlitroscircular').innerHTML = reduccionLitros;
        document.getElementById('gramoscloro').innerHTML = gramos;
        document.getElementById('kiloscloro').innerHTML = kilosReducido;
        });
        });

        $(document).ready(function() {
        $('#calcularTubo').click(function(){
            var diametro = parseFloat($('#diat').val(), 10);
            var largo = parseFloat($('#largo').val(), 10);
            var diametrosMetros;
            diametrosMetros= diametro/39.37;

            var v;
            v= parseFloat(((Math.PI * Math.pow(diametrosMetros,2))/4) * largo );// metros cubicos

            //volumen a litros
            var vlitros;
            vlitros=(v*1000).toFixed(2);
            p=(vlitros*50)/(700);//gramos de cloro

            var gr;
            gr=p.toFixed(2);

            var m3;
                m3=v.toFixed(5);

            var kk;
            kk= (p/1000).toFixed(5);

            document.getElementById('gramosTubo').innerHTML = gr;
            document.getElementById('vm3tubo').innerHTML =m3;
                document.getElementById('kilosTubo').innerHTML = kk;
                document.getElementById('vlitrosTubo').innerHTML = vlitros;
            });
        });
        $(document).ready(function() {
        $('#calcular5').click(function(){
            var altura = parseFloat($('#altura5').val(), 10);
            var base = parseFloat($('#base5').val(), 10);
            var agua = parseFloat($('#agua5').val(), 10);

            var v;
            v= parseFloat((altura * base * agua ) * 1000  );// volumen en litros
            var p;
            p=((v*175)/(700)).toFixed(2);// cloro en gramos
                var r;
            r=v.toFixed(2);//reduccion decimales litros
            var ee;
            ee= (v/1000).toFixed(2);// m3
                var hh;
            hh= (p/1000).toFixed(2);//kilos

            document.getElementById('litrosCam').innerHTML = r;
            document.getElementById('gramosCam').innerHTML = p;
                document.getElementById('m3Cam').innerHTML = ee;
                document.getElementById('kilosCam').innerHTML = hh;

            });
        });

    </script>
</body>

</html>