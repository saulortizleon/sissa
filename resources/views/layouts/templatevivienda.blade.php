<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SISA | Apurímac</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{asset('admin/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('admin/bower_components/bootstrap/dist/css/AdminLTE.css')}}">
  <link rel="stylesheet" href="{{asset('admin/bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('admin/bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('admin/dist/css/AdminLTE.css')}}">
  <link rel="stylesheet" href="{{asset('admin/dist/css/AdminLTE.min.css')}}">

  <link rel="stylesheet" href="{{asset('admin/dist/css/skins/_all-skins.min.css')}}">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{asset('admin/bower_components/morris.js/morris.css')}}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{asset('admin/bower_components/jvectormap/jquery-jvectormap.css')}}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{asset('admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('admin/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{asset('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
  <!-- Form validation -->
  <link rel="stylesheet" href="{{asset('formvalidation/formValidation.min.css')}}">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
   <link rel="stylesheet" href="{{asset('selectMultiple/css/select2.min.css')}}">

   <!-- jQuery 3 -->
  <script src="{{asset('admin/bower_components/jquery/dist/jquery.min.js')}}"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="{{asset('admin/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
  <script src="{{asset('selectMultiple/js/select2.min.js')}}"></script>

</head>
<style> .btn-tertiary {
    color: #555;
    padding: 0;
    line-height: 40px;
    width: 300px;
    margin: auto;
    display: block;
    border: 2px solid #555;
    &:hover,
      &:focus {
        color: lighten(#555, 20%);
        border-color: lighten(#555, 20%);
      }
  }

  /* input file style */

  .input-file {
    width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;
    + .js-labelFile {
      overflow: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;
      padding: 0 10px;
      cursor: pointer;
      .icon:before {
        content: "\f001";
      }
      &.has-file {
        .icon:before {

          content: "\f00c";
          color: #5AAC7B;
        }
      }
    }
  }
</style>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>SI</b>SA</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>SISA</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        Sistema Integrado de Saneamiento Apurímac
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu" >
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{asset('/img/usuario.png')}}" class="user-image" alt="Perfil">
              <span class="hidden-xs">{{Session::get('t_usuario')[1]}}</span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-body bg-primary">
                <div class="row">
               <center>
                 <p class="text-uppercase"> Representante <b>ATM</b> 2020</p> </center>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="{{url('usuario/perfil')}}" class="btn btn-success btn-flat">Mi perfil </a>
                </div>
                <div class="pull-right">
                  <a href="{{url('logout')}}" class="btn btn-danger btn-flat">Salir</a>
                </div>
              </li>
            </ul>
          </li>

        </ul>
      </div>
    </nav>
  </header>
  <!-- menucito maz na-->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">

      <li class="active treeview">
          <a href="#">
            <i class="fa fa-users"></i> <span>Matriz ATM</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="{{request()->is('atm/lista') ? 'active treeview': ''}}"><a href="{{url('atm/lista')}}"><i class="fa fa-arrow-circle-right"></i> Institucionalización</a></li>
            <li class="{{request()->is('formacion/lista') ? 'active treeview': ''}}"><a href="{{url('formacion/lista')}}"><i class="fa fa-arrow-circle-right"></i> Fortalecimiento</a></li>
            <li class="{{request()->is('funcion/lista') ? 'active treeview': ''}}"><a href="{{url('funcion/lista')}}"><i class="fa fa-arrow-circle-right"></i> Funciones</a></li>
            <li class="{{request()->is('pnsr/lista') ? 'active treeview': ''}}"><a href="{{url('pnsr/lista')}}"><i class="fa fa-arrow-circle-right"></i> App Pnsr Diágnosticos</a></li>
            <li class="{{request()->is('cjass/lista') ? 'active treeview': ''}}"><a href="{{url('cjass/lista')}}"><i class="fa fa-arrow-circle-right"></i> Consolidado JASS</a></li>
            <li class="{{request()->is('csap/lista') ? 'active treeview': ''}}"><a href="{{url('csap/lista')}}"><i class="fa fa-arrow-circle-right"></i> Consolidado SAP</a></li>
            <li class="{{request()->is('comuni/lista') ? 'active treeview': ''}}"><a href="{{url('comuni/lista')}}"><i class="fa fa-arrow-circle-right"></i> Comunicacion</a></li>
            <li class="{{request()->is('capac/lista') ? 'active treeview': ''}}"><a href="{{url('capac/lista')}}"><i class="fa fa-arrow-circle-right"></i> Capacitaciones</a></li>

          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-tag"></i>
            <span>JASS</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li class="{{request()->is('centrop/lista') ? 'active treeview': ''}}"><a href="{{url('centrop/lista')}}"><i class="fa fa-circle-o"></i> Centros Poblados</a></li>
            <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Gestión</a></li>
            <li><a href="pages/layout/fixed.html"><i class="fa fa-circle-o"></i> Fixed</a></li>
            <li><a href="pages/layout/collapsed-sidebar.html"><i class="fa fa-circle-o"></i> Collapsed Sidebar</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-home"></i>
            <span>EDUSA</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li class="{{request()->is('centrop/lista') ? 'active treeview': ''}}"><a href="{{url('centrop/lista')}}"><i class="fa fa-circle-o"></i> Centros Poblados</a></li>
            <li><a href="pages/layout/boxed.html"><i class="fa fa-circle-o"></i> Gestión</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>Mi perfil</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{url('usuario/perfil')}}"><i class="fa fa-circle-o"></i> Gestión</a></li>
          </ul>
        </li>
        <li class="treeview">
            <a href="#">
              <i class="fa fa-book"></i>
              <span>Manuales</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{url('usuario/lista')}}"><i class="fa fa-circle-o"></i> Descargar</a></li>
              <li><a href="{{url('usuario/lista')}}"><i class="fa fa-circle-o"></i> Pregutas comunes</a></li>
            </ul>
          </li>
          <li class="treeview">
              <a href="#">
                <i class="fa fa-bar-chart"></i>
                <span>Avances</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{url('usuario/lista')}}"><i class="fa fa-circle-o"></i> General</a></li>
                <li><a href="{{url('usuario/lista')}}"><i class="fa fa-circle-o"></i> Por partes</a></li>
              </ul>
            </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    @if(Session::has('message-correct'))
        <div class="alert alert-success">{{Session::get('message-correct')}}</div>
    @elseif(Session::has('message-error'))
        <div class="alert alert-danger">{{Session::get('message-error')}}</div>
    @endif
    @yield('section')
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2019 <a href="https://drvcsapurimac.gob.pe">DRVCS Apurímac</a>.
    </strong> Todos los derechos reservados.
  </footer>

  <div class="control-sidebar-bg"></div>
</div>

<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('admin/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- Morris.js charts -->
<script src="{{asset('admin/bower_components/raphael/raphael.min.js')}}"></script>
<script src="{{asset('admin/bower_components/morris.js/morris.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('admin/bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('admin/bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{asset('admin/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->

<script src="{{asset('admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>

<script type="text/javascript" src="{{asset('admin/bower_components/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js')}}" charset="UTF-8"></script>

<script src="{{asset('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('admin/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('admin/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('admin/dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('admin/dist/js/demo.js')}}"></script>
<script src="{{asset('formvalidation/formValidation.min.js')}}"></script>
<script src="{{asset('formvalidation/bootstrap.validation.min.js')}}"></script>
<!-- page script -->
<script>

  $('#datepicker').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
    todayHighlight: true,
    language: 'es',
    orientation: 'bottom auto',
    todayBtn: 'linked',
    todayHighlight: true
    });
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })

  var url = window.location;

// for sidebar menu entirely but not cover treeview
$('ul.sidebar-menu a').filter(function() {
	 return this.href == url;
}).parent().addClass('active');
//Date picker

// for treeview
$('ul.treeview-menu a').filter(function() {
	 return this.href == url;
}).parentsUntil(".sidebar-menu > .treeview-menu").addClass('active');
  $(document).ready(function() {
$('.input-file').each(function() {
  var $input = $(this),
      $label = $input.next('.js-labelFile'),
      labelVal = $label.html();

 $input.on('change', function(element) {
    var fileName = '';
    if (element.target.value) fileName = element.target.value.split('\\').pop();
    fileName ? $label.addClass('has-file').find('.js-fileName').html(fileName) : $label.removeClass('has-file').html(labelVal);
 });
});
});
</script>
</body>
</html>