<footer class="text-white" id="pie" 
style="background-color:#172664;   
position: absolute;
right: 0; 
left: 0; 
text-align: center; "  >
    <div class="container-fluid">
      <!-- Section: Social media -->
      <br>
      <section class="mb-12 text-center">
        <!-- Facebook -->
        <a href="https://www.facebook.com/drvcsapurimacoficial" target="_blank"    class="btn btn-outline-light btn-floating m-2"   role="button"
          ><i class="fa fa-facebook"></i></a>

        <!-- Twitter -->
        <a href="https://twitter.com/drvcsapurimac" target="_blank"  class="btn btn-outline-light btn-floating m-2"   role="button"
          ><i class="fa fa-twitter"></i
        ></a>


        <a href="https://www.linkedin.com/in/drvcs-apurímac/"  target="_blank" class="btn btn-outline-light btn-floating m-2"   role="button"
          ><i class="fa fa-linkedin"></i
        ></a>

        <!-- Instagram -->
        <a href="" target="_blank"  class="btn btn-outline-light btn-floating m-2"   role="button"
          ><i class="fa fa-instagram"></i
        ></a>

        <!-- Linkedin -->
        <a class="btn btn-outline-light btn-floating m-2" href="https://www.youtube.com/channel/UCLD1KB8wVWFNUYKWZLhRkTA" role="button"
          ><i class="fa fa-youtube"></i
        ></a>

      </section>
      <!-- Section: Social media -->
      <br>
      <!-- Section: Form -->
      <section class="">
        <form action="">
          <!--Grid row-->
          <div class="row d-flex justify-content-center">
            <!--Grid column-->
            <div class="col-auto">
              <p class="pt-2" style="color:rgb(11, 218, 255)">
                <strong>Recibe noticias en tu e-mail</strong>
              </p>
            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-md-5 col-12">
              <!-- Email input -->
              <div class="form-outline form-white mb-4">
                <input type="email" id="form5Example2" class="form-control" />
                <label class="form-label" for="form5Example2"><i>Ingresa tu correo electrónico</i> </label>
              </div>
            </div>
            <!--Grid column-->

            <!--Grid column-->
            <div class="col-auto">
              <!-- Submit button -->
              <button type="submit" class="btn btn-primary mb-4">
                Suscríbete
              </button>
            </div>
            <!--Grid column-->
          </div>
          <!--Grid row-->
        </form>
      </section>
      <section>
        <div class="row text-center">
          <!--Grid column-->
          <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
            <h5 class="text-uppercase" style="color:yellow">DRVCS APURÍMAC</h5>
            <ul class="list-unstyled mb-0">
              <li>
                <a href="{{url('nuestra-institucion')}}" class="text-white"><strong> Nosotros</strong></a>
              </li>
              <li>
                <a href="{{url('funciones')}}" class="text-white"><strong> ¿Qué hacemos?</strong></a>
              </li>
              <li>
                <a href="{{url('plan-regional-saneamiento')}}" class="text-white"><strong> Políticas y Planes</strong></a>
              </li>
               <br>
            </ul>
          </div>
          <!--Grid column-->

          <!--Grid column-->
          <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
            <h5 class="text-uppercase" style="color:yellow">Áreas Funcionales</h5>

            <ul class="list-unstyled mb-0">
              <li>
                <a href="{{ url('vivienda-y-urbanismo') }}" class="text-white"><strong> Vivienda y Urbanismo</strong></a>
              </li>
              <li>
                <a href="{{ url('construccion-y-saneamiento') }}" class="text-white"><strong> Construcción y Saneamiento</strong></a>
              </li>
             
              <br>
            </ul>
          </div>
          <!--Grid column-->
          <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
            <h5 class="text-uppercase" style="color:yellow">Servicios en Línea</h5>

            <ul class="list-unstyled mb-0">
              <li>
                <a href="{{url('calculador-de-cloro')}}" class="text-white"><strong> Yupasunchis Clorota</strong></a>
              </li>
              <li>
                <a href="{{url('verificar-certificados')}}" class="text-white"><strong> Verificar Certificados</strong></a>
              </li>
              <li>
                <a href="{{url('mpv')}}" class="text-white"><strong> Mesa de partes virtual</strong></a>
              </li>
              <br>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 mb-4 mb-md-0">
            <h5 class="text-uppercase" style="color:yellow">Contacto </h5>

            <ul class="list-unstyled mb-0">
              <li>
                <a href="{{url('contactenos')}}" class="text-white">  <i  class="fa fa-map-marker "></i><strong> Jr. Lima 637, Abancay  </strong> </a>
              </li>
              <li>
                <a href="{{url('contactenos')}}" class="text-white">  <i style="color:white;" class="fa fa-envelope  "></i><strong> drvcsapurimac@hotmail.com </strong>  </a>
              </li>
              <li>
                <a href="{{url('contactenos')}}" class="text-white">  <i style="color:white;" class="fa fa-phone "></i> <strong> (083) 322837 </strong> </a>
              </li>
              <br>
            </ul>
          </div>
          <!--Grid column-->

          <!--Grid column-->


        </div>

      </section>
      <br>
    </div>

    <div class="text-center p-3 bg-primary">
      © Copyright 2022.
      <a class="text-white" href="https://drvcs.regionapurimac.gob.pe/"> <strong>Dirección Regional de Vivienda, Construcción y Saneamiento de Apurímac</strong> </a>
    </div>

</footer>