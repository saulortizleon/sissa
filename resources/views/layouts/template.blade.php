<!DOCTYPE HTML>
<html lang="en">
<style type="text/css">
    .container {
    margin-top: 20px;
}

/* Carousel Styles */
.carousel-indicators .active {
    background-color: #2980b9;
}

.carousel-inner img {
    width: 100%;
    height: 400px
}

.carousel-control {
    width: 0;
}

.carousel-control.left,
.carousel-control.right {
    opacity: .7;
    filter: alpha(opacity=50);
    background-image: none;
    background-repeat: no-repeat;
    text-shadow: none;
}

.carousel-control.left span {
    padding: 2px;
}

.carousel-control.right span {
    padding: 2px;
}

.carousel-control .glyphicon-chevron-left,
.carousel-control .glyphicon-chevron-right,
.carousel-control .icon-prev,
.carousel-control .icon-next {
    position: absolute;
    top: 50%;
    z-index: 5;
    display: inline-block;
}

.carousel-control .glyphicon-chevron-left,
.carousel-control .icon-prev {
    left: 0;
}

.carousel-control .glyphicon-chevron-right,
.carousel-control .icon-next {
    right: 0;
}

.carousel-control.left span,
.carousel-control.right span {

}

.carousel-control.left span:hover,
.carousel-control.right span:hover {
    opacity: .5;
    filter: alpha(opacity=10);
}

/* Carousel Header Styles */
.header-text {
    position: absolute;
    top: 20%;
    left: 1.8%;
    right: auto;
    width: 96.66666666666666%;
    color: #fff;
}

.header-text h2 {
    font-size: 40px;
    color: white; text-shadow: black 0.1em 0.1em 0.2em
}
.header-text h3 {
    font-size: 10px;
    color: white; text-shadow: black 0.1em 0.1em 0.2em
}

.header-text h2 span {
    position: absolute;left: 15px;top: 15px;
    background-color: #f36f36;
    padding: 10px;
}

.header-text h3 span {
    background-color: #1bac91;
    padding: 12px;

}

.btn-min-block {
    min-width: 170px;
    line-height: 100px;
}

.btn-theme {
    color: #fff;
    background-color: transparent;
    border: 2px solid #fff;
    margin-right: 15px;
}

.btn-theme:hover {
    color: #000;
    background-color: #fff;
    border-color: #fff;
}
</style>
<head>
	<meta charset="utf-8">
	<title>SVS</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link href="{{asset('user/css/bootstrap-responsive.css')}}" rel="stylesheet">
	<link href="{{asset('user/css/style.css')}}" rel="stylesheet">
	<link href="{{asset('user/color/default.css')}}" rel="stylesheet">
	<link rel="shortcut icon" href="{{asset('user/img/favicon.ico')}}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- Messages -->
	<link rel="stylesheet" href="{{asset('user/css/hidemessage.css')}}">
	<script src="{{asset('user/js/hidemessage.js')}}"></script>
</head>
<body>
	@if(Session::has('contact'))
  		<input type="hidden" id="contact" value="{{Session::get('contact')}}">
	@endif
	<div id="modal"></div>
	<div class="navbar-wrapper">
		<div class="navbar navbar-inverse navbar-fixed-top" style="background-color: #D2D6D6;">
			<div class="navbar-inner">
				<div class="container">
				 <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
					</a>
					<h1 class="brand"><a href="{{url('/')}}"><img src="{{asset('user/img/svs.png')}}" height="70" width="70" > <span >SVS</span></a> </h1>

					<nav class="pull-right nav-collapse collapse">
						<ul id="menu-main" class="nav">
							<li><a title="Nosotros" href="{{url('#about')}}">Nosotros</a></li>
							<li><a title="Servicios" href="{{url('#services')}}">Servicios</a></li>
							<li><a title="Trabajos" href="{{url('#works')}}">Trabajos</a></li>
							<li><a title="Contactos" id="cont" href="{{url('#contact')}}">Contáctenos</a></li>
                  		    @if(Session::has('user'))
                  		    	<li>
                  		    		<a href="{{url('user/profile')}}"><span class="fa fa-user-circle" ></span> {{Session::get('user')[1]}}</a>
	                  		    </li>
	                  		    <li>
	                  		    	<a href="{{url('logout')}}"><span class="fa fa-close" style="color:#1bac91;"></span> <b style="color:#1bac91;">SALIR</b> </a>
	                  		    </li>
	                  		@else
	                  			<li>
									<a title="login" href="{{url('login')}}" class="btn-theme"><i class="fa fa-user"></i> Ingresar</a>
								</li>
								<li>
									<a title="registro" href="{{url('register')}}" class="btn-sm-theme"><i class="fa fa-plus"></i> Crear cuenta</a>
	                  		    </li>
                  		    @endif
						</ul>
					</nav>
				</div>
			</div>
			@if(Session::has('message-correct'))
		        <div class="message-correct">{{Session::get('message-correct')}}</div>
		    @elseif(Session::has('message-error'))
		        <div class="message-error">{{Session::get('message-error')}}</div>
		    @endif
		</div>
	</div>
	@yield('section')
	<footer>
		<div class="container">
			<div class="row">
				<div class="span6 offset3">
					<ul class="social-networks">
						<li><a href="#"><i class="icon-circled icon-bgdark icon-instagram icon-2x"></i></a></li>
						<li><a href="#"><i class="icon-circled icon-bgdark icon-twitter icon-2x"></i></a></li>
						<li><a href="#"><i class="icon-circled icon-bgdark icon-dribbble icon-2x"></i></a></li>
						<li><a href="#"><i class="icon-circled icon-bgdark icon-pinterest icon-2x"></i></a></li>
					</ul>
					<p class="copyright">
						&copy; DRVCS APURIMAC 2020 Todos los derechos reservados.
						<div class="credits">
							<a href="svs.com">drvcs apurimac</a>
						</div>
					</p>
				</div>
			</div>
		</div>
	</footer>
	<a href="#" class="scrollup"><i class="icon-angle-up icon-square icon-bgdark icon-2x"></i></a>
	<script src="{{asset('user/js/jquery.js')}}"></script>
	<script src="{{asset('user/js/jquery.scrollTo.js')}}"></script>
	<script src="{{asset('user/js/jquery.nav.js')}}"></script>
	<script src="{{asset('user/js/jquery.localScroll.js')}}"></script>
	<script src="{{asset('user/js/bootstrap.js')}}"></script>
	<script src="{{asset('user/js/jquery.prettyPhoto.js')}}"></script>
	<script src="{{asset('user/js/isotope.js')}}"></script>
	<script src="{{asset('user/js/jquery.flexslider.js')}}"></script>
	<script src="{{asset('user/js/inview.js')}}"></script>
	<script src="{{asset('user/js/animate.js')}}"></script>
	<script src="{{asset('user/js/custom.js')}}"></script>
</body>
</html>
<script>
	window.onload = function()
  	{
        HideMessage();

    	$('#'+$('#contact').val()).trigger("click");
  	};
</script>