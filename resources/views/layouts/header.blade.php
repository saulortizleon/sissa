@extends('layouts.templateblog')
@section('header')
<header id="vp">
    <div id="mainCarousel"   class="carousel slide" data-ride="carousel">
      	<div class="carousel-inner" role="listbox"> 
			@foreach( $publicacion as $item )
			<div class="carousel-item  {{ $loop->first ? 'active' : '' }}"
				style="background-image: url('{{asset($item->imagen) }}');">
				<div class="carousel-caption" style="">
					<a href="{{url('noticias/detalle')}}/{{$item->idPublicacion}}">
					<h1 style="color:white; font-size:1.6em; clear:both; margin-bottom:33px; line-height:2em; margin-top:-10px; overflow:visible; ">
					<span style=" position:relative; background:#182d80;						
						color:#fff;
						
						display: inline-flex;
						line-height:200%; font-weight: bold;border-radius:
						 30px;padding: 12px; white-space:pre-wrap; " >{{ $item->titulo }}</span>						 
					</h1>
 
					</a>
				</div>
			</div>
			@endforeach
		</div>
		<a class="carousel-control-prev" href="#mainCarousel" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		  </a>
		  <a class="carousel-control-next" href="#mainCarousel" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		  </a>
    </div>

</header>
 
@endsection