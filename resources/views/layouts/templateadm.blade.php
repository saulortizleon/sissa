<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>DRVCS | Apurímac</title>
  <!-- Tell the browser to be responsive to scresen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{asset('admin/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('admin/bower_components/bootstrap/dist/css/AdminLTE.css')}}">
  <link rel="stylesheet" href="{{asset('admin/bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('admin/bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('admin/dist/css/AdminLTE.css')}}">
  <link rel="stylesheet" href="{{asset('admin/dist/css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{asset('admin/dist/css/skins/_all-skins.min.css')}}">
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{asset('admin/bower_components/morris.js/morris.css')}}">
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{asset('admin/bower_components/jvectormap/jquery-jvectormap.css')}}">
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{asset('admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('admin/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{asset('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
  <!-- Form validation -->
  <link rel="stylesheet" href="{{asset('formvalidation/formValidation.min.css')}}">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
   <link rel="stylesheet" href="{{asset('selectMultiple/css/select2.min.css')}}">

   <!-- jQuery 3 -->
  <script src="{{asset('admin/bower_components/jquery/dist/jquery.min.js')}}"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="{{asset('admin/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
  <link rel="stylesheet" href="{{asset('select2-develop/dist/css/select2.min.css')}}">
  <script src="{{asset('select2-develop/dist/js/select2.min.js')}}"></script>

</head>
<style>

  .input-file {
    width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;
    + .js-labelFile {
      overflow: hidden;
      text-overflow: ellipsis;
      white-space: nowrap;
      padding: 0 10px;
      cursor: pointer;
      .icon:before {
        //font-awesome
        content: "\f093";
      }
      &.has-file {
        .icon:before {
          //font-awesome
          content: "\f00c";
          color: #5AAC7B;
        }
      }
    }
  }
</style>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="{{ url('adm')}}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>SI</b>SA</span>
      <span class="logo-lg"><b>SISA</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        Sistema Integrado de Saneamiento Apurímac
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu" >
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{asset('/img/user.png')}}" class="user-image" alt="Perfil">
              <span class="hidden-xs">{{Session::get('t_usuario')[1]}}</span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-body bg-primary">
                <div class="row">
               <center>   {{Session::get('t_usuario')[2]=='adm' ? "Administrador":"ATM"}} : </b>
                 <p class="text-uppercase"> {{Session::get('t_usuario')[1]}}</p> </center>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-success btn-flat">Mi perfil </a>
                </div>
                <div class="pull-right">
                  <a href="{{url('logout')}}" class="btn btn-danger btn-flat">Salir</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Menu Principal</li>
         
        <li class="active treeview">
          <a href="#">
              <i class="fa fa-file"></i> <span>Gestión</span>
              <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
          <ul class="treeview-menu">
              <li class="active"><a href="{{url('admin/noticias')}}"><i class="fa fa-circle-o"></i> Notas de prensa</a></li>
          </ul>
          <ul class="treeview-menu">
              <li class="active"><a href="{{url('adm/mpv')}}"><i class="fa fa-circle-o"></i> Mesa de partes Virtual</a></li>
          </ul>
      </li>
      <li class=" active treeview">
        <a href="#">
          <i class="fa fa-user"></i>
          <span>PERFIL</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="#"><i class="fa fa-pencil"></i> Mis datos</a></li>
        </ul>
          
      </li>
      <li class="active treeview">
        <a href="#">
          <i class="fa fa-university"></i> <span>CONCURSO VIRTUAL ATM   </span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li>
            <a href="{{url('concurso/lista')}}"  ><i class="fa fa-plus"></i>Inscripción</a></li>
        </ul>
      </li>
      <li class="active treeview">
        <a href="#">
          <i class="fa fa-university"></i> <span>ARCHIVO  </span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li>
            <a href="{{url('archivo/lista')}}"  ><i class="fa fa-plus"></i>Lista documentos</a></li>
        </ul>
      </li>
      <li class="active treeview">
        <a href="#">
          <i class="fa fa-university"></i> <span>TRAMITE DOC.</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li>
            <a href="{{url('archivo/lista')}}"  ><i class="fa fa-plus"></i>Seguimiento</a></li>
        </ul>
      </li>
      <li class="active treeview">
        <a href="#">
          <i class="fa fa-university"></i> <span>MESA DE PARTES</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li>
            <a href="{{url('archivo/lista')}}"  ><i class="fa fa-plus"></i>Lista documentos</a>
          </li>
          <li>
            <a href="{{url('archivo/lista')}}"  ><i class="fa fa-plus"></i>Registrar</a>
          </li>
        </ul>
      </li>
      <li class="active treeview">
        <a href="#">
          <i class="fa fa-university"></i> <span>AJUSTES </span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li>
            <a href="{{url('lista/usuario')}}"  ><i class="fa fa-plus"></i>Trabajadores</a>
          </li>
        </ul>
        <ul class="treeview-menu">
          <li>
            <a href="{{url('lista/areas')}}"  ><i class="fa fa-plus"></i>Áreas</a>
          </li>
        </ul>
        <ul class="treeview-menu">
          <li>
            <a href="{{url('lista/areas')}}"  ><i class="fa fa-plus"></i>Responsables ATM</a>
          </li>
        </ul>
      </li>
      <li class="active treeview">
        <a href="#">
          <i class="fa fa-university"></i> <span>YAKU KAWSAY</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li>
            <a href="https://classroom.google.com/u/2/h" target="_blank"><i class="fa fa-tint"></i>Mis aulas virtuales</a></li>
          <li  >
            <a href="{{url('yakukawsay/calendario')}}" target="_blank"><i class="fa fa-tint"></i>Calendario de clases</a>
          </li>
        </ul>
      </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    @if(Session::has('message-correct'))
        <div class="alert alert-success">{{Session::get('message-correct')}}</div>
    @elseif(Session::has('message-error'))
        <div class="alert alert-danger">{{Session::get('message-error')}}</div>
    @endif
    @yield('section')
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2019 <a href="https://drvcsapurimac.gob.pe">DRVCS Apurímac</a>.
    </strong> Todos los derechos reservados.
  </footer>
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('admin/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- DataTables -->
<script src="{{asset('admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<!-- Morris.js charts -->
<script src="{{asset('admin/bower_components/raphael/raphael.min.js')}}"></script>
<script src="{{asset('admin/bower_components/morris.js/morris.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('admin/bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('admin/bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{asset('admin/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{asset('admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{asset('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
<!-- Slimscroll -->
<script src="{{asset('admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('admin/bower_components/fastclick/lib/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('admin/dist/js/adminlte.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('admin/dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('admin/dist/js/demo.js')}}"></script>
<script src="{{asset('formvalidation/formValidation.min.js')}}"></script>
<script src="{{asset('formvalidation/bootstrap.validation.min.js')}}"></script>
<!-- page script -->
<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>