<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="La Dirección Regional de Vivienda, Construcción
    y Saneamiento de Apurímac, es un órgano desconcentrado de la Gerencia Regional de Infraestructura,
     del Gobierno Regional de Apurímac, que tiene personería jurídica de derecho público;
     tiene a su cargo las funciones específicas en materia de vivienda y
     saneamiento, señaladas en el artículo 58° de la Ley Orgánica de Gobiernos Regionales.">
    <meta name="author" content="Dirección Regional de Vivienda, Construcción
    y Saneamiento de Apurímac">
    <meta name="keywords" content="drvcs apurimac, agua de calidad,cloracion de agua,
     viviendas saludables, edusa,romas, escuela de saneamiento,yaku kawsay, vivienda Apurimac,
    saneamiento, ATM, JASS, Dirección Regional de Vivienda, Construcción
    y Saneamiento de Apurímac">
    <meta name="copyright" content="Dirección Regional de Vivienda, Construcción
    y Saneamiento de Apurímac">
    <a hreflang="es">
    <title>Dirección Regional de Vivienda, Construcción y Saneamiento Apurimac</title>
    <script src="{{asset('admin/vendor/jquery/jquery.min.js')}}"></script>
    <link href="{{asset('admin/vendor/bootstrap/css/bootstrap.css')}}" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="{{asset('css/layout.css')}}" rel="stylesheet">
    <link href="{{asset('css/aliados.css')}}" rel="stylesheet">
    <link href="{{asset('css/butons.css')}}" rel="stylesheet">
    <link href="{{asset('admin/dist/css/modern-business.css')}}" rel="stylesheet">
    <link href="{{asset('admin/dist/css/skins/_all-skins.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('formvalidation/formValidation.min.css')}}">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <script type="application/ld+json">
        {
          "@context": "https://schema.org",
          "@type": "GovernmentOrganization",
          "name": "Dirección Regional de Vivienda Construcción y Saneamiento",
          "alternateName": "DRVCS APURIMAC",
          "url": "http://drvcs.regionapurimac.gob.pe",
          "logo": "http://drvcs.regionapurimac.gob.pe/layout/images/drvcs.png",
          "contactPoint": {
            "@type": "ContactPoint",
            "telephone": "(083) 322837",
            "contactType": "billing support",
            "areaServed": "PE",
            "availableLanguage": "es"
          },
          "sameAs": "https://www.facebook.com/drvcsapurimacoficial"
        }
    </script>
</head>