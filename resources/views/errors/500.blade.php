@extends('layouts.templateblog')
@section('section')
<div class="row">


<div class="col-lg-8">
          <br>
      <div class="error-template">
            <h1>
                Oops!</h1>
            <h2>
                500 Not Found</h2>
            <div class="error-details">
                lo sentimos, ocurrió un error, <small>La página aun se encuentra en construcción</small>
            </div>
            <div class="error-actions">
                <a href="{{ url('/') }}" class="btn btn-primary btn-sm"><span class="fa fa-home"> Ir a inicio</span></a>
                <a href="{{ url('contacto') }}" class="btn btn-danger btn-sm"><span class="fa fa-envelope"></span> Contactar </a>
            </div>
        </div>
</div>
</div>
@endsection