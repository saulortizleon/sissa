@extends('layouts.templateblog')
@section('section')
<div class="row">
<div class="col-lg-12">
          <br>
      <div class="error-template">
       <center>
        <img class="img-fluid"  src="{{asset('img/404.jpg')}}" width="700">
      
      <div class="error-details">
     <small>La página aún se encuentra en construcción</small>
      </div>

       <br>
            <div class="error-actions">
                <a href="{{ url('/') }}" class="btn btn-lg btn-primary btn-lg"><span class="fa fa-home"> Página Principal</span></a>
                <a href="{{ URL::previous() }}" class="btn btn-lg btn-danger btn-lg"><span class="fa fa-angle-left "></span> Regresar </a>
            </div>
            <br>
        </div>
      </center>
</div>

<style type="text/css">
    #index{ display:none}
  </style>
</div>
@endsection