@extends('layouts.templateatm')
@section('section')
<section class='content-header'>
    <h1> Actualización del diagnóstico de ATM <small>en el aplicativo web PNSR</small></h1>
    <ol class='breadcrumb'>
        <li><a href='#'><i class='fa fa-dashboard'></i>App</a></li>
        <li class='active'>Lista</li>

    </ol>
</section>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-body table-responsive'>
                    <table id='' class='table table-bordered table-hover'>
                        <thead>
                            <tr>
                                <th style="font-size: 12px;">Actualización del diagnóstico de ATM(PNSR) al primer trimestre
                                </th>
                                <th style="font-size: 12px;"> Actualización del diagnóstico de ATM(PNSR) al segundo trimestre
                                </th>
                                <th style="font-size: 12px;"> Actualización del diagnóstico de ATM(PNSR) al tercer trimestre
                                </th>
                                <th style="font-size: 12px;"> Actualización del diagnóstico de ATM(PNSR) al cuarto trimestre
                                </th>
                                <th style="font-size: 12px;"> Número de centros poblados con diagnóstico de abastecimiento
                                    de agua y disposición sanitaria de excretas
                                </th>
                                <th style="font-size: 12px;"><center>Acciones</center> </th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($listaPnsr as $item)
                            <tr >
                                <td>{{$item->estado1}}</td>
                                <td>{{$item->estado2}}</td>
                                <td>{{$item->estado3}}</td>
                                <td>{{$item->estado4}}</td>
                                <td>{{$item->cantidadCpDiag}}</td>
                                <td>
                                    <button class="btn btn-success btn-sm"  onclick="actualizar('{{$item->idApp}}')"><i class="fa fa-plus"></i> AGREGAR INFORMACIÓN ATM</button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function actualizar(idApp)
    {
    	window.location.href='{{url('pnsr/editar')}}/'+idApp;
    }
    function actualizarPregunta1(ubigeo)
    {
    	window.location.href='{{url('preguntas1/editar')}}/'+ubigeo;
    }
    function actualizarPregunta2(ubigeo)
    {
    	window.location.href='{{url('preguntas2/editar')}}/'+ubigeo;
    }
    function actualizarPregunta3(ubigeo)
    {
    	window.location.href='{{url('preguntas3/editar')}}/'+ubigeo;
    }
    function actualizarPregunta4(ubigeo)
    {
    	window.location.href='{{url('preguntas4/editar')}}/'+ubigeo;
    }
$(document).ready(function() {
    $('#frmAtm').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields:{
            ver1:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
             ver2:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver3:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver4:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            }
        }
    });
});
</script>
@endsection