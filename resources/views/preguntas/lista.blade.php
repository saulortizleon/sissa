@extends('layouts.templateatm')
@section('section')
<section class='content-header'>
    <h1> Cuestionarios de diagnostico ATM 2019</h1>
    <ol class='breadcrumb'>
        <li><a href='#'><i class='fa fa-dashboard'></i>Cuestionarios</a></li>
        <li class='active'>lista</li>
    </ol>
</section>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                    @foreach($listaAtm as $item)
                    <div class="box-header with-border">
                        <h5> <b>Ubigeo :</b> {{$item->ubigeo}}<b> Provincia :</b> {{$item->provincia}}<b> Distrito : </b>{{$item->distrito}}</h5>
                    </div>
                <div class='box-body'>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="box box-primary">
                                <div class="box-body">
                                    <button class="btn btn-primary btn-sm"  onclick="actualizarPregunta1('{{$item->ubigeo}}')"><i class="fa fa-pencil"> </i> PRIMER CUESTIONARIO</button>
                                    <hr>
                                    <label for=""> <span class="label label-primary"> 1</span> Sistemas de agua potable y sistemas de cloración en el distrito</label>
                                    <label for=""><span class="label label-primary"> 2</span> Centros poblados rurales con población del distrito </label>
                                    <label for=""><span class="label label-primary"> 3</span> ATM con planes de capacitación y asistencia técnica</label>
                                    <label for=""><span class="label label-primary"> 4</span> Libro de registro de organizaciones comunales</label>
                                    <label for=""><span class="label label-primary"> 5</span> Registro de hogares rurales a ser capacitados en marco del plan de capacitaciónde EDUSAA - PP0083</label>
                                   <hr>
                                   <h5>Avance : <b>10%</b> </h5>
                                    <div class="progress">
                                            <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%">
                                              <span class="sr-only">70% Complete (danger)</span>
                                            </div>
                                          </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                            <div class="box box-danger">
                                    <div class="box-body">
                                            <button class="btn btn-danger btn-sm"  onclick="actualizarPregunta2('{{$item->ubigeo}}')"><i class="fa fa-pencil"> </i> SEGUNDO CUESTIONARIO</button>
                                            <hr>
                                            <label for=""><span class="label label-danger"> 1</span>  Ejecución de actividades y presupuesto del POA</label>
                                            <label for=""><span class="label label-danger"> 2</span>  ATM capacita a las OC en gestión de los Servicios de Saneamiento, EDUSA y hogares Rurales</label>
                                            <label for=""><span class="label label-danger"> 3</span>  ATM cumple en hacer spot radial para la difusión a nivel distrital en :</label>
                                            <label for=""><span class="label label-danger"> 4</span>  INFORMACIÓN CLORO</label>
                                            <hr>
                                            <h5>Avance : <b>40%</b> </h5>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                                    <span class="sr-only">40% Complete (danger)</span>
                                                </div>
                                             </div>
                                    </div>
                                </div>
                            </div>
                    <div class="col-sm-3">
                        <div class="box box-warning">
                                <div class="box-body">
                                    <button class="btn btn-warning btn-sm"  onclick="actualizarPregunta3('{{$item->ubigeo}}')"><i class="fa fa-pencil"> </i> TERCER CUESTIONARIO</button>
                                    <hr>
                                    <label for=""><span class="label label-warning"> 1</span>  ATM articula con otras instituciones públicas y/o privadas</label>
                                    <label for=""><span class="label label-warning"> 2</span>  GESTION INTEGRADA DE RECURSOS HIDRICOS Y GESTION DE RIESGOS</label>
                                    <label for=""><span class="label label-warning"> 3</span>  GESTION DE LA INFORMACION DE APLICATIVOS DEL MVCS</label>
                                    <hr>
                                    <h5>Avance : <b>70%</b> </h5>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 70%">
                                            <span class="sr-only">70% Complete (warning)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                                <div class="box box-info">
                                        <div class="box-body">
                                            <button class="btn btn-info btn-sm"  onclick="actualizarPregunta4('{{$item->ubigeo}}')"><i class="fa fa-pencil"> </i> CUARTO CUESTIONARIO</button>
                                            <hr>
                                            <label for=""><span class="label label-info"> 1</span> Gestión POI ATM 2019 y 2020</label>
                                            <label for=""><span class="label label-info"> 2</span> Gestión  fichas de seguimiento y evaluación </label>
                                            <label for=""><span class="label label-info"> 3</span> Registro de monitoreo de cloro residual y Caracterizacion de fuentes de agua</label>
                                            <hr>
                                            <h5>Avance : <b>50%</b> </h5>
                                            <div class="progress">
                                                    <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 50%">
                                                        <span class="sr-only">50% Complete (warning)</span>
                                                    </div>
                                                </div>
                                        </div>
                                    </div>
                                </div>
@endforeach
                </div>
            </div>
            </div>
        </div>
    </div>
</section>

<script>
    function actualizar(ubigeo)
    {
    	window.location.href='{{url('atm/editar')}}/'+ubigeo;
    }
    function actualizarPregunta1(ubigeo)
    {
    	window.location.href='{{url('preguntas1/editar')}}/'+ubigeo;
    }
    function actualizarPregunta2(ubigeo)
    {
    	window.location.href='{{url('preguntas2/editar')}}/'+ubigeo;
    }
    function actualizarPregunta3(ubigeo)
    {
    	window.location.href='{{url('preguntas3/editar')}}/'+ubigeo;
    }
    function actualizarPregunta4(ubigeo)
    {
    	window.location.href='{{url('preguntas4/editar')}}/'+ubigeo;
    }
$(document).ready(function() {
    $('#frmAtm').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields:{
            ver1:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
             ver2:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver3:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver4:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            }
        }
    });
});
</script>
@endsection