@extends('layouts.templatesup')
@section('section')
<section class='content-header'>
    <h1> DOCUMENTO RESPUESTA</h1>
    <ol class='breadcrumb'>
        <li><a href='#'><i class='fa fa-dashboard'></i>ARCHIVO CENTRAL</a></li>
        <li class='active'>Documentos con respuesta</li>
    </ol>
</section>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class="box-header">
                    <button class="btn btn-success btn-lg"  >

                        <i class="fa fa-file-excel-o"></i> EXPORTAR A EXCEL</button>

                    <div class="box-tools">
                        <br>
                      <div class="input-group input-group-sm hidden-sm" style="width: 500px;">
                        <input type="text" name="buscar" id="buscar" class="form-control input-lg pull-right" placeholder="Buscar por Asunto y Remitente" >
                        <div class="input-group-btn">
                          <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>

                      </div>

                    </div>
                </div>
                <div class='box-body table-responsive'>
                    <table id="noticias" class='table table-bordered table-striped'>
                        <thead>
                            <tr>
                                <th> ID</th>
                                <th> REMITENTE</th>
                                <th> ASUNTO </th>
                                <th> TRAMITE</th>
                                <th> TIPO</th>
                                <th> ARCHIVO</th>
                                <th> ASUNTO RPTA </th>
                                <th> ENVÍO</th>
                                <th> TIPO</th>
                                <th> ARCHIVO</th>
                                <th> ACCIONES</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($respuesta as $item)
                            <tr>
                                <td>{{$item->idRespuesta}}</td>
                                <td>{{$item->remitente}}</td>

                                <td>{{$item->asunto}}</td>
                                <td>{{$item->fechaIngreso}}</td>
                                <td>{{$item->tipoDocumento}}</td>
                                <td>
                                    <a title="revisar" href="{{asset($item->archivo)}}" class="btn btn-link btn-sm" target="_blank"
                                    ><i class="fa fa-eye" > ARCHIVO</i></a>
                                </td>
                                <td>{{$item->asuntoRpta}}</td>
                                <td>{{$item->fechaEnvio}}</td>
                                <td>{{$item->tipoDocumentoRpta}}</td>
                                <td>
                                    <a title="revisar" href="{{asset($item->documentoRpta)}}" class="btn btn-link btn-sm" target="_blank"
                                    ><i class="fa fa-eye" > RESPUESTA</i></a>
                                </td>
                                <td>
                                    <a title="Editar"  href="#" class="btn btn-primary btn-sm" data-toggle="modal"
                                    data-target="#editarRpta" 
                                    
                                    data-id="{{$item->idRespuesta}}"
                                    data-asunto="{{$item->asuntoRpta}}"
                                    data-remite="{{$item->remitente}}"
                                    data-destinatario="{{$item->destinatario}}"                                     
                                    data-tipo="{{$item->tipoDocumentoRpta}}"                                     
                                    data-fecha="{{$item->fechaEnvio}}"
                                    data-numero="{{$item->numeroRegistroRpta}}"
                                    data-folios="{{$item->foliosRpta}}"
                                    data-documento="{{$item->documentoRpta}}"                                    
                                    data-idar="{{$item->idArchivo}}"
                                     ><i class="fa fa-edit"> EDITAR</i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                    {{ $respuesta->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
 
    @include('supervision/respuesta/editar')
</section>

<script>
   $('#editarRpta').on('show.bs.modal', function (event) {
            
            var button = $(event.relatedTarget)
            var idRespuesta = button.data('id')            
            var remitenteRpta = button.data('remite')
            var destinatarioRpta = button.data('destinatario')
            var asuntoRpta = button.data('asunto')           
            var tipoDocumentoRpta = button.data('tipo')
            var fechaEnvio = button.data('fecha')            
            var numeroRegistroRpta = button.data('numero')
            var foliosRpta = button.data('folios')
            var idArchivo = button.data('idArchivo')
            var modal = $(this)

            modal.find('.modal-body #idRespuesta').val(idRespuesta);
            modal.find('.modal-body #remitenteRpta').val(remitenteRpta); 
            modal.find('.modal-body #destinatarioRpta').val(destinatarioRpta); 
            modal.find('.modal-body #asuntoRpta').val(asuntoRpta); 
            modal.find('.modal-body #tipoDocumentoRpta').val(tipoDocumentoRpta); 
             
            modal.find('.modal-body #fechaEnvio').val(fechaEnvio); 
             modal.find('.modal-body #numeroRegistroRpta').val(numeroRegistroRpta); 
            modal.find('.modal-body #foliosRpta').val(foliosRpta); 
            modal.find('.modal-body #idArchivo').val(idArchivo); 
          
    })
</script>
@endsection