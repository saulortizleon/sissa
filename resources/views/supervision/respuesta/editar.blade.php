<div class="modal fade bd-example-modal-lg" id="editarRpta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"> <i class="fa fa-times"style="font-size:35px;color:red" ></i></span></button>
                    <h3 class="modal-title"><i class="fa fa-file"></i> RESPUESTA DEL DOCUMENTO</h3>
            </div>
            <div class="modal-body">
                <div class='row'>
                    <div class='col-xs-12'>
                    <form id="frmDocumentoRpta" name="frmDocumentoRpta" action="{{url('respuesta/editar')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="box box-info">
                                    <div class="box-body">
                                        <h4 class="text-primary">
                                            DETALLES DEL DOCUMENTO RESPUESTA
                                        </h4>
                                        <div class="form-group">
                                            <input type="hidden" for="idRespuesta" id="idRespuesta" name="idRespuesta" readonly="readonly" class="form-control"  >
                                        </div>
                                        <div class="form-group">
                                            <input type="hidden" for="idArchivo" id="idArchivo" name="idArchivo" readonly="readonly" class="form-control"  >
                                        </div>
                                        <div class="form-group">
                                            <label>NOMBRE DEL REMITENTE</label>
                                            <input type="text"  id="remitenteRpta" value="" name="remitenteRpta" readonly="readonly" class="form-control input-lg" >
                                        </div>
                                        <div class="form-group">
                                            <label>NOMBRE DEL DESTINATARIO</label>
                                            <input type="text"   id="destinatarioRpta" name="destinatarioRpta" readonly="readonly" class="form-control input-lg" >
                                        </div>
                                        <div class="form-group">
                                            <label>ASUNTO DEL DOCUMENTO</label>
                                            <input type="text"  for="asuntoRpta" id="asuntoRpta" name="asuntoRpta"  class="form-control input-lg" >
                                        </div>
                                        <div class="form-group">
                                            <strong>    <label> TIPO DE DOCUMENTO </label>       </strong>
                                                <select name="tipoDocumentoRpta" id="tipoDocumentoRpta" class="form-control input-lg form-control input-lg-lg" required="required"  >
                                                    <option value="">Seleccione</option>
                                                    <option value="OFICIO">OFICIO</option>
                                                    <option value="OFICIO CIRCULAR">OFICIO CIRCULAR</option>
                                                    <option value="OFICIO MULTIPLE">OFICIO MULTIPLE</option>
                                                    <option value="CARTA">CARTA</option>
                                                    <option value="CONTRATO">CONTRATO</option>
                                                    <option value="CONVENIO">CONVENIO</option>
                                                    <option value="RESOLUCIONES">RESOLUCIONES</option>
                                                    <option value="CARTA CIRCULAR">CARTA CIRCULAR</option>
                                                    <option value="INFORME">INFORME</option>
                                                    <option value="INFORME CONFIDENCIAL">INFORME CONFIDENCIAL</option>
                                                    <option value="INFORME DE SUPERVISIÓN">INFORME DE SUPERVISIÓN</option>
                                                    <option value="INFORME TECNICO">INFORME TECNICO</option>
                                                    <option value="CARTA DE PRESENTACIÓN">CARTA DE PRESENTACIÓN</option>
                                                    <option value="CARTA DE REQUERIMIENTO">CARTA DE REQUERIMIENTO</option>
                                                    <option value="CARTA MULTIPLE">CARTA MULTIPLE</option>
                                                    <option value="CARTA NOTARIAL">CARTA NOTARIAL</option>
                                                    <option value="CÉDULA DE NOTIFICACIÓN<">CÉDULA DE NOTIFICACIÓN</option>
                                                    <option value="DOCUMENTO CONFIDENCIAL">DOCUMENTO CONFIDENCIAL</option>
                                                    <option value="DOCUMENTO VIA FAX">DOCUMENTO VIA FAX</option>
                                                    <option value="DOCUMENTO VIA MAIL">DOCUMENTO VIA MAIL</option>
                                                    <option value="EXHORTO">EXHORTO</option>
                                                    <option value="MEMORANDUM">MEMORANDUM</option>
                                                    <option value="MEMORANDUM MULTIPLE">MEMORANDUM MULTIPLE</option>
                                                    <option value="MEMORIAL">MEMORIAL</option>
                                                    <option value="NOTIFICACIONES JUDICIALES">NOTIFICACIONES JUDICIALES</option>
                                                    <option value="PETITORIO">PETITORIO</option>
                                                    <option value="PRAES">PRAES</option>
                                                    <option value="PROVEIDO">PROVEIDO</option>
                                                    <option value="SOLICITUD">SOLICITUD</option>
                                                    <option value="ADENDA">ADENDA</option>
                                                    <option value="ARCHIVADO">ARCHIVADO</option>
                                                    <option value="OTROS">OTROS</option>
                                                </select>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="box box-info">
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="fechaEnvio"> FECHA ENVÍO DEL DOCUMENTO RESPUESTA</label>
                                            <input type="date" required   id="fechaEnvio" name="fechaEnvio"   class="form-control input-lg"  pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" title="Enter a date in this formart YYYY-MM-DD">
                                        </div>
                                        <div class="form-group">
                                            <label for="numeroRegistroRpta"  >NUMERO DEL DOCUMENTO DE RESPUESTA</label>
                                            <input type="text" id="numeroRegistroRpta" name="numeroRegistroRpta"  class="form-control input-lg"  >
                                        </div>
                                        <div class="form-group">
                                            <label for="foliosRpta" >FOLIOS DEL DOCUMENTO RESPUESTA</label>
                                            <input type="text"  id="foliosRpta" name="foliosRpta"  class="form-control input-lg"  >
                                        </div>
                                        <div class="form-group">
                                            <a href="">  <strong> <label style="color:red" for="documentoRpta"> <i class=" fa fa-file" ></i> CAMBIAR DOCUMENTO ESCANEADO </label></strong></a> 
                                               <input type="file" class="form-control input-lg form-control input-lg-lg" id="documentoRpta" name="documentoRpta" style="display:none" >
                                           </div>
                                        <hr>
                                        <div class="form-group">
                                        <button class="btn btn-success btn-lg" type="submit"    > <i class="fa fa-floppy-disk"></i>  GUARDAR</button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>