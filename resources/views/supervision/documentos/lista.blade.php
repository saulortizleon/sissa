@extends('layouts.templatesup')
@section('section')
<section class='content-header'>
    <h1> GESTIÓN DE DOCUMENTOS ONLINE</h1>
    <ol class='breadcrumb'>
        <li><a href='#'><i class='fa fa-dashboard'></i>DOCUMENTOS ONLINE</a></li>
        <li class='active'>LISTA</li>
    </ol>
</section>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class="box-header">
                    <button class="btn btn-danger btn-lg" onclick="insertar()">
                        <i class="fa fa-plus"></i> AGREGAR</button>
                    <div class="box-tools">
                        <form action="{{url('buscar/documento')}}" method="get"  enctype="multipart/form-data">
                            <div class="input-group input-group-lg hidden-sm" style="width: 600px; height:20px;">
                                <input type="text" name="buscar" id="buscar" value="{{$buscar}}"
                                class="form-control pull-right" placeholder="Buscar por Nombre o Asunto">
                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class='box-body table-responsive'>
                    <table id="documento" class="table table-bordered table-hover">
                        <thead>
                            <tr>

                                <th> TIPO</th>
                                <th> N°</th>
                                <th> REMITE</th>
                                <th> ASUNTO</th>
                                <th> ARCHIVO</th>
                                <th> FOLIOS</th>
                                <th> ESTADO</th>
                                <th> FECHA DE ENVÍO</th>
                                <th> Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (count($listaDocumentos)<=0)
                            <tr>
                                <td colspan="10"> <h3> <i class="fa fa-times"></i> No se encontraron resultados</h3> </td>
                            </tr>
                            @else

                        @foreach($listaDocumentos as $item)
                            <tr>

                                <td>{{$item->tipoDocumento}}</td>
                                <td>{{$item->nroDocumento}}</td>
                                <td>{{$item->nombres}} {{$item->razonSocial}}</td>
                                <td>{{$item->asunto}}</td>
                                <td>   <a title="revisar" href="#" class="btn btn-link btn-sm" data-toggle="modal" data-target="#revisar"
                                   ><i class="fa fa-eye" > REVISAR</i></a>
                                </td>
                                <td>{{$item->nroFolios}}</td>
                                <td> <span class="label label-info"> {{$item->estado}} </span></td>
                                <td>{{$item->created_at}}</td>
                                <td>
                                 <button class="btn btn-info" data-id="{{$item->idDocumento}}"
                                     data-tipo="{{$item->tipoDocumento}}"
                                     data-nombres="{{$item->nombres}}"
                                     data-ruc="{{$item->ruc}}"
                                     data-dni="{{$item->dni}}"
                                     data-razon="{{$item->razonSocial}}"
                                     data-asunto="{{$item->asunto}}"
                                     data-archivo="{{$item->archivo}}"
                                     data-folios="{{$item->nroFolios}}"
                                     data-fecha="{{$item->created_at}}"
                                     data-estado="{{$item->estado}}"
                                     data-telefono="{{$item->telefono}}"
                                     data-correo="{{$item->correoElectronico}}"
                                     data-direccion="{{$item->direccion}}"
                                     data-telefono="{{$item->telefono}}"
                                     data-nro="{{$item->nroDocumento}}"
                                     data-toggle="modal"
                                      data-target="#generar">
                                      <i class="fa fa-envelope" > RECIBIR</i></button>
                                </td>
                            </tr>
                        @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
    <div id="revisar" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" >
        <div class="modal-content"  >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"> <i class="fa fa-times"style="font-size:35px;color:red" ></i></span></button>
                    <h3 class="modal-title"><i class="fa fa-check"></i> REVISAR DOCUMENTO</h3>
            </div>
                <div class="modal-body">
                <embed     src="{{ asset($item->archivo) }}"
                    type="application/pdf" frameborder="0" width="100%" height="800px">
                </div>
        </div>
        </div>
    </div>

    <div class="modal fade bd-example-modal-lg" id="generar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"> <i class="fa fa-times"style="font-size:35px;color:red" ></i></span></button>
                        <h3 class="modal-title"><i class="fa fa-check"></i> RECIBIR DOCUMENTO</h3>
                </div>
                <div class="modal-body">
                    <div class='row'>
                        <div class='col-xs-12'>
                        <form id="frmDocumento" name="frmDocumento" action="{{url('documento/insertar')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="box box-info">
                                        <div class="box-body">

                                            <h4 class="text-primary" >
                                            DETALLES DEL SOLICITANTE
                                            </h4>
                                            <div class="form-group">
                                                <label for="id">ID DOCUMENTO</label>
                                                <input type="hidden" for="id" id="id" name="idDocumento" readonly="readonly" class="form-control"  >
                                            </div>
                                            <div class="form-group">
                                                <label for="nombres">NOMBRES</label>
                                                <input type="text" for="nombres" id="nombres" readonly="readonly" class="form-control"  >
                                            </div>
                                            <div class="form-group">
                                                <label for="razon">RAZON SOCIAL</label>
                                                <input type="text" for="razon" id="razon" readonly="readonly" class="form-control"  >
                                            </div>
                                            <div class="form-group">
                                                <label for="ruc">RUC</label>
                                                <input type="text" for="ruc" id="ruc" readonly="readonly" class="form-control"  >
                                            </div>
                                            <div class="form-group">
                                                <label for="dni">DNI</label>
                                                <input type="text" for="dni" id="dni" readonly="readonly" class="form-control"  >
                                            </div>
                                            <div class="form-group">
                                                <label for="direccion">DIRECCIÓN</label>
                                                <input type="text" for="direccion" id="direccion" readonly="readonly" class="form-control"  >
                                            </div>
                                            <div class="form-group">
                                                <label for="correo">CORREO</label>
                                                <input type="text" for="correo" id="correo" readonly="readonly" class="form-control"  >
                                            </div>
                                            <div class="form-group">
                                                <label for="telefono">TELEFONO</label>
                                                <input type="text" for="telefono" id="telefono" readonly="readonly" class="form-control"  >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="box box-info">
                                        <div class="box-body">
                                            <h4 class="text-primary" >
                                                DETALLES DEL DOCUMENTO
                                            </h4>
                                            <div class="form-group">
                                                <label for="estado">ESTADO</label>
                                                <input type="text" for="estado" id="estado" readonly="readonly" class="form-control"  >
                                            </div>
                                            <div class="form-group">
                                                <label for="tipo">TIPO</label>
                                                <input type="text" for="tipo" id="tipo" readonly="readonly" class="form-control"  >
                                            </div>
                                            <div class="form-group">
                                                <label for="nro">N° DOCUMENTO</label>
                                                <input type="text" for="nro" id="nro" readonly="readonly" class="form-control"  >
                                            </div>
                                            <div class="form-group">
                                                <label for="asunto">ASUNTO</label>
                                                <input type="text" for="asunto" id="asunto" readonly="readonly" class="form-control"  >
                                            </div>

                                            <div class="form-group">
                                                <label for="folios">FOLIOS</label>
                                                <input type="text" for="folios" id="folios" readonly="readonly" class="form-control"  >
                                            </div>
                                            <div class="form-group">
                                                <label for="fecha">FECHA DE ENVIO</label>
                                                <input type="text" for="fecha" id="fecha" readonly="readonly" class="form-control"  >
                                            </div>
                                            <hr>
                                            <h4 class="text-primary">
                                                OBSERVACIONES
                                            </h4>
                                            <div class="form-group">
                                                <div class="radio">
                                                    <label style="font-size : 18px; font-weight: bold;" >
                                                        <input type="radio" style="height:15px; width:15px; vertical-align: middle;"
                                                        name="optionsRadios" id="optionsRadios1" value="recibir" checked>
                                                        Recibir documento
                                                    </label>
                                                    </div>
                                                    <div class="radio" style="font-size : 18px; font-weight: bold;" >
                                                    <label style="color:red">
                                                        <input type="radio" style="height:15px; width:15px; vertical-align: middle;"
                                                        name="optionsRadios" id="optionsRadios2" value="observar" >
                                                            Observar documento
                                                    </label>
                                                    </div>
                                            </div>
                                            <div class="form-group">
                                                <textarea id="observaciones" name="observaciones" class="form-control" rows="3" placeholder="Escribir"></textarea>
                                            </div>
                                            <div class="form-group">
                                            <button class="btn btn-success btn-lg" type="submit"    > <i class="fa fa-floppy-disk"></i>  GUARDAR</button>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="d-flex justify-content-center">
        {{ $listaDocumentos->links() }}
    </div>
    </div>

</div>
</div>

</div>
</section>
<script>
        $('#generar').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')
            var tipo = button.data('tipo')
            var estado = button.data('estado')
            var nro = button.data('nro')
            var asunto = button.data('asunto')
            var ruc = button.data('ruc')
            var dni = button.data('dni')

            var folios = button.data('folios')
            var fecha = button.data('fecha')
            var nombres = button.data('nombres')
            var razon = button.data('razon')
            var direccion = button.data('direccion')
            var correo = button.data('correo')
            var telefono = button.data('telefono')
            var modal = $(this)
            modal.find('.modal-body #id').val(id);
            modal.find('.modal-body #tipo').val(tipo);
            modal.find('.modal-body #estado').val(estado);
            modal.find('.modal-body #nro').val(nro);
            modal.find('.modal-body #asunto').val(asunto);

            modal.find('.modal-body #folios').val(folios);
            modal.find('.modal-body #fecha').val(fecha);
            modal.find('.modal-body #nombres').val(nombres);
            modal.find('.modal-body #razon').val(razon);
            modal.find('.modal-body #direccion').val(direccion);
            modal.find('.modal-body #direccion').val(direccion);
            modal.find('.modal-body #correo').val(correo);
            modal.find('.modal-body #telefono').val(telefono);
            modal.find('.modal-body #ruc').val(ruc);
            modal.find('.modal-body #dni').val(dni);
        })

    $("#observaciones").css("display", "none");
    $("#tituloObservaciones").css("display", "none");

    $("#optionsRadios1").click(function(evento) {
        $("#observaciones").css("display", "none");
        $("#tituloObservaciones").css("display", "none");
    });

    $("#optionsRadios2").click(function(evento) {
        $("#observaciones").css("display", "block");
        $("#tituloObservaciones").css("display", "block");
    });
</script>
@endsection