@extends('layouts.templatesup')
@section('section')
    <section class='content-header'>
        <h1>Calendario de actividades</h1>
        <ol class='breadcrumb'>
            <li><a href='#'><i class='fa fa-dashboard'></i>editar </a></li>
            <li class='active'>Formación</li>
        </ol>
    </section>
    <section class='content'>
        <div class='row'>
            <div class='col-xs-12'>
                <div class='box'>
                    <div class='box-body'>
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe src="https://calendar.google.com/calendar/b/1/embed?height=600&amp;wkst=1&amp;bgcolor=%23ffffff&amp;ctz=America%2FLima&amp;src=ZHJ2Y3NhcHVyaW1hY0BnbWFpbC5jb20&amp;src=YWRkcmVzc2Jvb2sjY29udGFjdHNAZ3JvdXAudi5jYWxlbmRhci5nb29nbGUuY29t&amp;src=Y2xhc3Nyb29tMTA1NDU0NzU2NzQzOTk0MDM2Mjg5QGdyb3VwLmNhbGVuZGFyLmdvb2dsZS5jb20&amp;src=Y2xhc3Nyb29tMTE4MTA5NTA2MTgzODcxMTk2MTM4QGdyb3VwLmNhbGVuZGFyLmdvb2dsZS5jb20&amp;src=Y2xhc3Nyb29tMTAxNDgzNTY3MDY4OTIxMjMxMjM0QGdyb3VwLmNhbGVuZGFyLmdvb2dsZS5jb20&amp;src=ZXMucGUjaG9saWRheUBncm91cC52LmNhbGVuZGFyLmdvb2dsZS5jb20&amp;color=%23039BE5&amp;color=%2333B679&amp;color=%23007b83&amp;color=%230047a8&amp;color=%230047a8&amp;color=%230B8043&amp;showTitle=0" style="border:solid 1px #777" width="800" height="600" frameborder="0" scrolling="no"></iframe>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
