@extends('layouts.templatesup')
@section('section')
<section class='content-header'>
    <h1> GESTIÓN DE EXPEDIENTES</h1>
    <ol class='breadcrumb'>
        <li><a href='#'><i class='fa fa-dashboard'></i>Administración</a></li>
        <li class='active'>Expedientes</li>
    </ol>
</section>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class="box-header">
                    <button class="btn btn-success btn-lg" onclick="insertar()">
                        <i class="fa fa-send"></i> Nuevo Expediente</button>
                    <div class="box-tools">
                      <div class="input-group input-group-sm hidden-sm" style="width: 500px;">
                        <input type="text" name="buscar" id="buscar" class="form-control pull-right" placeholder="BUSCAR ARCHIVO">
                        <div class="input-group-btn">
                          <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                        </div>
                      </div>
                    </div>
                  </div>
                <div class='box-body table-responsive'>
                    <table id="noticias" class='table table-bordered table-hover'>
                        <thead>
                            <tr>
                                <th> N°</th>
                                <th> Proveido</th>                                
                                <th> Estado</th>                                
                                <th> Observaciones</th>
                                <th> Tipo</th>
                                <th> Asunto</th>
                                <th> Documento</th>
                                <th> Entidad / Persona</th>
                                <th> Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($listaDocumentos as $item)
                            <tr>
                                <td>{{$item->nroTramite}}</td>
                                <td>{{$item->created_at}}</td>
                                <td>{{$item->estado}}</td>
                                <td>{{$item->observaciones}}</td>
                                <td>{{$item->tipo}}</td>
                                <td>{{$item->asunto}}</td>
                                <td>{{$item->archivo}}</td>
                                <td>{{$item->nombres}} {{$item->razonSocial}}</td>
                                <td>

                                <a title="Editar" href="#" class="btn btn-primary btn-sm" 
                                onclick="editar('{{$item->nroTramite}}');"><i class="fa fa-edit"> Editar</i></a>

                                 <a title="Eliminar" href="#" class="btn btn-danger btn-sm" 
                                 onclick="eliminarNoticia('{{$item->nroTramite}}');"><i class="fa fa-trash-o"> Eliminar</i></a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
            </div>
        </div>
     
    </div>
    {{ $listaDocumentos->links() }}
</section>

<script>
    $(document).ready(function(){
        $("#buscar").on("keyup", function() {
          var value = $(this).val().toLowerCase();
          $("#noticias tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
          });
        });
      });
    function insertar()
	{
			window.location = "{{url('noticias/insertar')}}";
    }
    function editar(nroTramite)
	{
		if(confirm('Está seguro de editar la publicación?'))
		{
			window.location = "{{url('noticias/editar')}}/"+nroTramite;
		}
	}
</script>
@endsection