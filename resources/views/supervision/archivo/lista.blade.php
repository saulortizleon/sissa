@extends('layouts.templatesup')
@section('section')
<section class='content-header'>
    <h1> GESTIÓN DE ARCHIVOS</h1>
    <ol class='breadcrumb'>
        <li><a href='#'><i class='fa fa-dashboard'></i>Administración</a></li>
        <li class='active'>Archivo</li>
    </ol>
</section>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class="box-header">
                    <button class="btn btn-success btn-lg" data-toggle="modal"
                    data-target="#archivo">
                        <i class="fa fa-files-o"></i> Nuevo Registro</button>
                        <div class="box-tools">
                            <form action="{{url('buscar/archivo')}}" method="get"  enctype="multipart/form-data">
                                <div class="input-group input-group-lg hidden-sm" style="width: 600px; height:20px;">
                                    <input type="text" name="buscarArchivo" id="buscarArchivo" value="{{$buscarArchivo}}"
                                    class="form-control pull-right" placeholder="Buscar por Remitente o Asunto">
                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                </div>
                <div class='box-body table-responsive'>
                    <table id="noticias" class='table table-bordered table-striped'>
                        <thead>
                            <tr>
                              
                                <th> REMITENTE</th>
                             
                                <th> TIPO </th>
                                <th> N° DOCUMENTO </th>
                                <th > ASUNTO</th>
                                <th> ESTADO</th>
                                <th > OBSERVACION</th>
                                <th> FECHA TRÁMITE</th>
                                <th> N° EXPEDIENTE</th>
                                <th> FOLIOS</th>
                                <th> ARCHIVO</th>
                                <th> ACCIONES</th>
                            </tr>
                        </thead>
                        <tbody>
                             @if (count($listaDocumentos)<=0)
                            <tr>
                                <td colspan="10"> <h3> <i class="fa fa-times"></i> No se encontraron resultados</h3> </td>
                            </tr>
                            @else
                            @endif
                        @foreach($listaDocumentos as $item)
                            <tr>
                                
                                <td style="color:rgb(17, 107, 171)">{{$item->remitente}}</td>
                                
                                <td>{{$item->tipoDocumento}}</td>
                                <td>{{$item->numeroDocumento}}</td>
                                <td>{{$item->asunto}}</td>
                                <td>{{$item->estadoArchivo}}</td>
                                <td style="color:rgb(231, 42, 42)">{{$item->observacion}}</td>
                                <td>{{$item->fechaIngreso}}</td>
                                <td>{{$item->numeroRegistro}}</td>
                                <td>{{$item->folios}}</td>
                                <td>
                                    <a title="revisar" href="{{asset($item->archivo)}}" class="btn btn-link btn-sm" target="_blank"
                                    ><i class="fa fa-eye" > REVISAR</i></a>
                                </td>
                                <td>
                                    <a title="Editar"  href="#" class="btn btn-primary btn-sm" data-toggle="modal"
                                    data-target="#editar" 
                                    
                                    data-id="{{$item->idArchivo}}"
                                    data-remitente="{{$item->remitente}}"
                                    data-destinatario="{{$item->destinatario}}"
                                    data-numero="{{$item->numeroDocumento}}"
                                    data-tipo="{{$item->tipoDocumento}}"
                                    data-estado="{{$item->estadoArchivo}}"
                                    data-observacion="{{$item->observacion}}"
                                    data-fecha="{{$item->fechaIngreso}}"
                                    data-asunto="{{$item->asunto}}"
                                    data-numero2="{{$item->numeroRegistro}}"
                                    data-folios="{{$item->folios}}"
                                    data-proveido="{{$item->proveido}}"
                                    data-archivo="{{$item->archivo}}"
                                    
                                    data-observacion="{{$item->observacion}}"
                                     ><i class="fa fa-edit"> EDITAR</i></a>

                                    <br> <br>
                                    <a title="Respuesta" href="#" class="btn btn-warning btn-sm"
                                     data-toggle="modal"  data-target="#respuesta"  
                                    
                                     data-id="{{$item->idArchivo}}"
                                     data-remite="{{$item->remitente}}">
                                    
                                     <i class="fa fa-plus"> RESPUESTA</i></a>

                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                    {{ $listaDocumentos->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
    @include('supervision/archivo/insertar')
    @include('supervision/archivo/respuesta')
    @include('supervision/archivo/editar')
</section>

<style type="text/css">
    #index{ display:none}
    .help-block{
        color:red;
        font-size: 15px;
        font-weight: bold;
    }
    .form-group.has-error label
    {
        color:#333;
    }
    .has-error .form-control input-lg-feedback{
        display: none;
    }
</style>
<script>
 
    // items a modal respuesta
    $('#respuesta').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var id = button.data('id')           
            var remite = button.data('remite')
   
            var modal = $(this)

            modal.find('.modal-body #idArchivo').val(id);
            modal.find('.modal-body #remitenteRpta').val(remite);
            
    })
    //editar archivo
 
    $("#editar1").click(function(evento) {
        $('#editar1').attr('checked', true);   
        $("#observacionEditar").css("display", "none");
        if( $(this).is(':checked') ) alert("checked");
      
    });

    $("#editar2").click(function(evento) {
        $('#editar2').attr('checked', true);   
        $("#observacionEditar").css("display", "block");
        if( $(this).is(':checked') ) alert("checked");
    });
 

    $('#editar').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget)
        var idArchivo = button.data('id')
        var remitente = button.data('remitente')
        var destinatario = button.data('destinatario')
        var numeroDocumento = button.data('numero')
        var tipoDocumento = button.data('tipo')
        var estadoArchivo = button.data('estado')
        var observacion = button.data('observacion')
        var fechaIngreso = button.data('fechaIngreso')
        var numeroRegistro = button.data('numero2')
        var asunto = button.data('asunto')
        var fecha = button.data('fecha')
        var folios = button.data('folios')
        var proveido = button.data('proveido')
        var archivo = button.data('archivo')
        var observacionEditar = button.data('observacion')
        var modal = $(this)

        modal.find('.modal-body #idArchivo').val(idArchivo);
        modal.find('.modal-body #remitente').val(remitente);
        modal.find('.modal-body #destinatario').val(destinatario);
        modal.find('.modal-body #numeroDocumento').val(numeroDocumento);
        modal.find('.modal-body #tipoDocumento').val(tipoDocumento);
        modal.find('.modal-body #estadoArchivo').val(estadoArchivo);
        modal.find('.modal-body #observacion').val(observacion);
        modal.find('.modal-body #fechaIngreso').val(fecha);
        modal.find('.modal-body #asunto').val(asunto);
        modal.find('.modal-body #numeroRegistro').val(numeroRegistro);
        modal.find('.modal-body #folios').val(folios);
        modal.find('.modal-body #proveido').val(proveido);
        modal.find('.modal-body #archivo').val(archivo);
        modal.find('.modal-body #observacionEditar').val(observacionEditar);
        
        if(estadoArchivo=="archivado"){
            $('#editar1').attr('checked', true);                
            $("#observacionEditar").css("display", "none");    
        }
        else{
            
            $('#editar1').attr('checked', false);   
            $("#observacionEditar").css("display", "block");   
        }

        if(estadoArchivo=="observado"){
               
                $('#editar2').attr('checked', true);
              
                $("#observacionEditar").css("display", "block");  
        }
        else{                  
            $('#editar2').attr('checked', false);         
            $("#observacionEditar").css("display", "none");    
        }
    })
      // items modal insertar archivo
      $("#observacion").css("display", "none");
    
    $("#x").click(function(evento) {
        $("#observacion").css("display", "none");
        $("#tituloObservaciones").css("display", "none");
    });

    $("#y").click(function(evento) {
        $("#observacion").css("display", "block");
        $("#tituloObservaciones").css("display", "block");
    });
    
    //respuesta
    $("#observacionRpta").css("display", "none");
    $("#no").click(function(evento) {
        $("#observacionRpta").css("display", "none");
        $("#tituloObservaciones").css("display", "none");
    });

    $("#si").click(function(evento) {
        $("#observacionRpta").css("display", "block");
        $("#tituloObservaciones").css("display", "block");
    });
    


 $(document).ready(function() {
    $('#frmDocumento').formValidation({
    framework: 'bootstrap',
    icon: {
    valid: 'glyphicon glyphicon-ok',
    invalid: 'glyphicon glyphicon-remove',
    validating: 'glyphicon glyphicon-refresh'
    },
    fields:{
        remitente:{
        validators: {
            notEmpty: {
                message: 'Los nombres  del remitente son requeridos'
            },

        }
        },
        destinatario:{
        validators: {
            notEmpty: {
                message: 'Los nombres  del destinatario son requeridos'
            },

        }
        },
        asunto:{
        validators: {
            notEmpty: {
                message: 'Los nombres  del asunto son requeridos'
            },

        }
        },
        documento:{
            validators: {
                file: {
                    extension: 'pdf',
                    message: 'el archivo debe tener formato PDF'
                },
                notEmpty: {
                    message: 'El documento  es requerido'
                }
            }
        },
        respuesta:{
            validators: {
                file: {
                    extension: 'pdf',
                    message: 'el archivo debe tener formato PDF'
                },
                notEmpty: {
                    message: 'El documento  es requerido'
                }
            }
        },
        tipoDocumento:{
        validators: {
            notEmpty: {
                message: 'Elija una opción del tipo de documento'
            },

        }
        },
        tipoRespuesta:{
        validators: {
            notEmpty: {
                message: 'Elija una opción del tipo de documento'
            },

        }
        },
        numeroDocumento:{
        validators: {
            notEmpty: {
                message: 'El número de documento  es requerido'
            },
            regexp:{
                regexp: /[0-9]/,
                message: 'Debe ingresar solo numeros'
            }
        }
        },
        numeroRegistro:{
            validators: {
                notEmpty: {
                    message: 'El número de recepción  es requerido'
                },
                regexp:{
                    regexp: /[0-9]/,
                    message: 'Debe ingresar solo numeros'
                }
            }
            },
        folios:{
            validators: {
                notEmpty: {
                    message: 'La cantidad de folios  es requerido'
                },
                regexp:{
                    regexp: /[0-9]/,
                    message: 'Debe ingresar solo numeros'
                }
            }
        },
        proveido:{
            validators: {
                notEmpty: {
                    message: 'Elija una opción de área proveido '
                },

            }
            },
            fechaIngreso:{
                validators: {
                    notEmpty: {
                        message: 'Elija la fecha de recepción del documento'
                    },

                }
                },

    }
    });
 });

 $(document).ready(function() {
    $('#frmDocumentoRpta').formValidation({
    framework: 'bootstrap',
    icon: {
    valid: 'glyphicon glyphicon-ok',
    invalid: 'glyphicon glyphicon-remove',
    validating: 'glyphicon glyphicon-refresh'
    },
    fields:{

        asuntoRpta:{
        validators: {
            notEmpty: {
                message: 'Los nombres  del asunto son requeridos'
            },

        }
        },
        documentoRpta:{
            validators: {
                file: {
                    extension: 'pdf',
                    message: 'el archivo debe tener formato PDF'
                },
                notEmpty: {
                    message: 'El documento  es requerido'
                }
            }
        },
        tipoDocumentoRpta:{
        validators: {
            notEmpty: {
                message: 'Elija una opción del tipo de documento'
            },

        }
        },

        numeroDocumentoRpta:{
        validators: {
            notEmpty: {
                message: 'El número de documento  es requerido'
            },
            regexp:{
                regexp: /[0-9]/,
                message: 'Debe ingresar solo numeros'
            }
        }
        },
        numeroRegistroRpta:{
            validators: {
                notEmpty: {
                    message: 'El número de recepción  es requerido'
                },
                regexp:{
                    regexp: /[0-9]/,
                    message: 'Debe ingresar solo numeros'
                }
            }
            },
        foliosRpta:{
            validators: {
                notEmpty: {
                    message: 'La cantidad de folios  es requerido'
                },
                regexp:{
                    regexp: /[0-9]/,
                    message: 'Debe ingresar solo numeros'
                }
            }
        },

        fechaEnvio:{
            validators: {
                notEmpty: {
                    message: 'Elija la fecha de recepción del documento'
                },

            }
            },

    }
    });
 });
</script>
@endsection