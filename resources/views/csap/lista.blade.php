@extends('layouts.templateatm')
@section('section')
<section class='content-header'>
    <h1> Sistemas de Agua potable  <small>en el distrito</small></h1>
    <ol class='breadcrumb'>
        <li><a href='#'><i class='fa fa-dashboard'></i>SAP</a></li>
        <li class='active'>Lista</li>
    </ol>
</section>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-body table-responsive'>
                    <table id='' class='table table-bordered table-hover'>
                        <thead>
                            <tr>
                                <th style="font-size: 12px;">N° de CCPP(centros poblados) abastecidos por SAP(sistema de agua potable).
                               </th>
                                <th style="font-size: 12px;"> N° centros poblados sin abastecimiento de SAP(sistemas de agua potable)
                               </th>
                                <th style="font-size: 12px;"> Numero total de sistemas de agua potable en el distrito
                               </th>
                                <th style="font-size: 12px;"> Numero de SAP(Sistemas de agua potable) que tienen instalado de sistemas mejorados de cloracion 2020.
                                </th>
                                <th style="font-size: 12px;"> Numero de CC.PP.(centros poblados) Que consumen agua clorada en forma continua.
                                </th>
                                <th style="font-size: 12px;"> Numero de sistemas intervenidos por programas de reposicion y mantenimiento
                                </th>
                                <th style="font-size: 12px;"> Numero total de sistemas de eliminacion de excretas.
                                </th>
                                <th style="font-size: 12px;"><center>Acciones</center> </th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($listaCsap as $item)
                            <tr >
                                <td>{{$item->cpconSap}}</td>
                                <td>{{$item->cpsinSap}}</td>
                                <td>{{$item->totalSap}}</td>
                                <td>{{$item->sisCloracionMejorado}}</td>
                                <td>{{$item->cloracionContinua}}</td>
                                <td>{{$item->romas}}</td>
                                <td>{{$item->sisExcretas}}</td>
                                <td>
                                    <button class="btn btn-success btn-sm"  onclick="actualizar('{{$item->idSapC}}')"><i class="fa fa-plus"></i> AGREGAR INFORMACIÓN ATM</button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class='col-xs-12'>
            <h3 style="position: relative;
            padding: 0px 5px 0 0px;"> Riesgos en Sistemas de Agua potable  <small>en el distrito</small></h3>
            <div class='box'>
                <div class='box-body table-responsive'>
                    <table id='' class='table table-bordered table-hover'>
                        <thead>
                            <tr>
                                <th style="font-size: 12px;">Cantidad de sistemas agua en riesgo.
                               </th>
                                <th style="font-size: 12px;"> Cantidad de sistemas saneamiento en riesgo.
                               </th>
                                <th style="font-size: 12px;"> Constancia + fotos
                               </th>
                            </th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($listaRsap as $item)
                            <tr >
                                <td>{{$item->numSisRiesgoAgua}}</td>
                                <td>{{$item->numSisRiesgoSane}}</td>
                                <td>{{$item->ver1}}</td>
                                <td>
                                    <button class="btn btn-success btn-sm"  onclick="actualizar('{{$item->idSapR}}')"><i class="fa fa-plus"></i> AGREGAR INFORMACIÓN ATM</button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function actualizar(idSapR)
    {
    	window.location.href='{{url('csap/reditar')}}/'+idSapR;
    }
    function actualizar(idSapC)
    {
    	window.location.href='{{url('csap/editar')}}/'+idSapC;
    }
    function actualizarPregunta1(ubigeo)
    {
    	window.location.href='{{url('preguntas1/editar')}}/'+ubigeo;
    }
    function actualizarPregunta2(ubigeo)
    {
    	window.location.href='{{url('preguntas2/editar')}}/'+ubigeo;
    }
    function actualizarPregunta3(ubigeo)
    {
    	window.location.href='{{url('preguntas3/editar')}}/'+ubigeo;
    }
    function actualizarPregunta4(ubigeo)
    {
    	window.location.href='{{url('preguntas4/editar')}}/'+ubigeo;
    }
$(document).ready(function() {
    $('#frmAtm').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields:{
            ver1:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
             ver2:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver3:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver4:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            }
        }
    });
});
</script>
@endsection