@extends('layouts.templateatm')
@section('section')
    <section class='content-header'>
        <h1> Sistemas de Agua potable  <small>en el distrito</small></h1>
        <ol class='breadcrumb'>
            <li><a href='#'><i class='fa fa-dashboard'></i>Editar </a></li>
            <li class='active'>SAP</li>
        </ol>
    </section>
    <section class='content'>
        <div class='row'>
            <div class='col-xs-12'>
                <form id="frmAtm" name="frmAtm" action="{{url('csap/editar')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                <div class="row">
                    <div class="col-md-4">
                        <div class="box box-primary">
                            <div class="box-body">
                                <input type="hidden" name="idSapC" value="{{$listaEditar->idSapC}}">
                                <div class="form-group">
                                    <label>N° de CCPP(centros poblados) abastecidos por SAP(sistema de agua potable).
                                    </label><br>
                                    <input type="text" value="{{$listaEditar->cpconSap}}" class="form-control" id="cpconSap" name="cpconSap" >
                                    <div style="display: {{$listaEditar->ver1!=null ? 'none':''}}">
                                        <label for="" style="color:#00a65a;">   <span class="label label-success">Subir verificable:</span>  <br> <i class="fa fa-file-archive-o"> Documento escaneado, formato PDF</i> </label>
                                    <input id="cambiar" type="file"  class="form-control" id="ver1" name="ver1" >
                                    </div>
                                    <br>  @if ($listaEditar->ver1!=null)
                                    <a  href="{{asset($listaEditar->ver1)}}" target="_blank"><i class="fa fa-eye"></i> Ver Verificable</a> |
                                    <label for="cambiar" style="color:red"><i class="fa fa-pencil"> </i> Cambiar Documento</label>
                                        @else
                                        @endif

                                </div>
                                <div class="form-group" >
                                    <label>N° centros poblados sin abastecimiento de SAP(sistemas de agua potable)
                                    </label> <br>
                                    <input type="text" value="{{$listaEditar->cpsinSap}}" class="form-control" id="cpsinSap" name="cpsinSap" >
                                    <div style="display: {{$listaEditar->ver2!=null ? 'none':''}}">
                                        <label for="" style="color:#00a65a;">   <span class="label label-success">Subir verificable:</span>  <br> <i class="fa fa-file-archive-o"> Documento escaneado, formato PDF</i> </label>
                                    <input id="cambiar" type="file"  class="form-control" id="ver2" name="ver2" >
                                    </div>
                                    <br>  @if ($listaEditar->ver2!=null)
                                    <a  href="{{asset($listaEditar->ver2)}}" target="_blank"><i class="fa fa-eye"></i> Ver Verificable</a> |
                                    <label for="cambiar" style="color:red"><i class="fa fa-pencil"> </i> Cambiar Documento</label>
                                        @else
                                        @endif
                                </div>
                                <div class="form-group" >
                                    <label> Numero total de sistemas de agua potable en el distrito
                                    </label> <br>
                                    <input type="text" value="{{$listaEditar->totalSap}}" class="form-control" id="totalSap" name="totalSap" >
                                    <div style="display: {{$listaEditar->ver3!=null ? 'none':''}}">
                                        <label for="" style="color:#00a65a;">   <span class="label label-success">Subir verificable:</span>  <br> <i class="fa fa-file-archive-o"> Documento escaneado, formato PDF</i> </label>
                                    <input id="cambiar" type="file"  class="form-control" id="ver3" name="ver3" >
                                    </div>
                                    <br>  @if ($listaEditar->ver3!=null)
                                    <a  href="{{asset($listaEditar->ver3)}}" target="_blank"><i class="fa fa-eye"></i> Ver Verificable</a> |
                                    <label for="cambiar" style="color:red"><i class="fa fa-pencil"> </i> Cambiar Documento</label>
                                        @else
                                        @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="form-group" >
                                    <label>Numero de SAP(Sistemas de agua potable) que tienen instalado de sistemas mejorados de cloracion 2020.
                                    </label><br>
                                    {{-- facilitador --}}
                                      <input type="text" value="{{$listaEditar->sisCloracionMejorado}}" class="form-control" id="sisCloracionMejorado" name="sisCloracionMejorado" >
                                      <div style="display: {{$listaEditar->ver4!=null ? 'none':''}}">
                                        <label for="" style="color:#00a65a;">   <span class="label label-success">Subir verificable:</span>  <br> <i class="fa fa-file-archive-o"> Documento escaneado, formato PDF</i> </label>
                                    <input id="cambiar" type="file"  class="form-control" id="ver4" name="ver4" >
                                    </div>
                                    <br>  @if ($listaEditar->ver4!=null)
                                    <a  href="{{asset($listaEditar->ver4)}}" target="_blank"><i class="fa fa-eye"></i> Ver Verificable</a> |
                                    <label for="cambiar" style="color:red"><i class="fa fa-pencil"> </i> Cambiar Documento</label>
                                        @else
                                        @endif
                                    </div>
                                <hr>
                                <div class="form-group" >
                                    <label>Numero de sistemas intervenidos por programas de reposicion y mantenimiento
                                    </label><br>
                                    <input type="text" value="{{$listaEditar->romas}}" class="form-control" id="romas" name="romas" >

                                    <div style="display: {{$listaEditar->ver5!=null ? 'none':''}}">
                                        <label for="" style="color:#00a65a;">   <span class="label label-success">Subir verificable:</span>  <br> <i class="fa fa-file-archive-o"> Documento escaneado, formato PDF</i> </label>
                                    <input id="cambiar" type="file"  class="form-control" id="ver5" name="ver5" >
                                    </div>
                                    <br>  @if ($listaEditar->ver5!=null)
                                    <a  href="{{asset($listaEditar->ver5)}}" target="_blank"><i class="fa fa-eye"></i> Ver Verificable</a> |
                                    <label for="cambiar" style="color:red"><i class="fa fa-pencil"> </i> Cambiar Documento</label>
                                        @else
                                        @endif
                                </div>

                                <div class="form-group" >
                                    <label>Numero total de sistemas de eliminacion de excretas.
                                    </label><br>
                                      <input type="text" value="{{$listaEditar->sisExcretas}}" class="form-control" id="sisExcretas" name="sisExcretas" >
                                      <br>
                                      <div style="display: {{$listaEditar->ver6!=null ? 'none':''}}">
                                        <label for="" style="color:#00a65a;">   <span class="label label-success">Subir verificable:</span>  <br> <i class="fa fa-file-archive-o"> Documento escaneado, formato PDF</i> </label>
                                    <input id="cambiar" type="file"  class="form-control" id="ver6" name="ver6" >
                                    </div>
                                    <br>  @if ($listaEditar->ver6!=null)
                                    <a  href="{{asset($listaEditar->ver6)}}" target="_blank"><i class="fa fa-eye"></i> Ver Verificable</a> |
                                    <label for="cambiar" style="color:red"><i class="fa fa-pencil"> </i> Cambiar Documento</label>
                                        @else
                                        @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="form-group" >
                                    <label>Numero de CC.PP.(centros poblados) Que consumen agua clorada en forma continua
                                    </label><br>
                                    <input type="text" value="{{$listaEditar->cloracionContinua}}" class="form-control" id="cloracionContinua" name="cloracionContinua" >
                                    <div style="display: {{$listaEditar->ver7!=null ? 'none':''}}">
                                        <label for="" style="color:#00a65a;">   <span class="label label-success">Subir verificable:</span>  <br> <i class="fa fa-file-archive-o"> Documento escaneado, formato PDF</i> </label>
                                    <input id="cambiar" type="file"  class="form-control" id="ver7" name="ver7" >
                                    </div>
                                    <br>  @if ($listaEditar->ver7!=null)
                                    <a  href="{{asset($listaEditar->ver7)}}" target="_blank"><i class="fa fa-eye"></i> Ver Verificable</a> |
                                    <label for="cambiar" style="color:red"><i class="fa fa-pencil"> </i> Cambiar Documento</label>
                                        @else
                                        @endif
                                </div>
                                <div class="box-footer">
                                    <input type="submit" value="GUARDAR TODO" class="btn btn-primary btn-sm ">
                                        <a href="{{url('csap/lista')}}" class="btn btn-danger btn-sm" ><i class="fa fa-arrow-left"></i>  Regresar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
    </section>
<script>
$(document).ready(function() {

    $("#cargos1").on( "click", function() {
			$('#numCargos1').show(); //muestro mediante id
            $('#numCargos2').show(); //muestro mediante id
            $('#numCargos3').show(); //muestro mediante id
            $('#numCargos4').show(); //muestro mediante id
			$('#ver3').show(); //muestro mediante clase
		 });
    $("#cargos").on( "click", function() {
        $('#numCargos1').hide(); //oculto mediante id
        $('#numCargos2').hide(); //oculto mediante id
        $('#numCargos3').hide(); //oculto mediante id
        $('#numCargos4').hide(); //oculto mediante id
        $('#ver3').hide(); //muestro mediante clase
    });

     $("#miembrosAdicionales1").on( "click", function() {
			$('#numMiembrosAdicionales').show(); //muestro mediante id

		 });
    $("#miembrosAdicionales2").on( "click", function() {
        $('#numMiembrosAdicionales').hide(); //oculto mediante id

    });

    $("#planSupervision1").on( "click", function() {
			$('#divplanSupervision').show(); //muestro mediante id

		 });
    $("#planSupervision2").on( "click", function() {
        $('#divplanSupervision').hide(); //oculto mediante id

    });
    $(function(){
        $('#ocultar').change(function(){
        if(!$(this).prop('checked')){
            $('#otras').hide();
        }else{
            $('#otras').show();
        }
  })
})
    $('#frmAtm').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields:{
            especialidad:{
                validators: {
                    notEmpty: {
                        message: 'La especialidad es requerido'
                    }
                }
            },
            ver1:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
             ver2:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver3:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver4:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver5:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver6:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver7:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver8:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver9:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver10:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver11:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },

        }
    });
});
</script>
@endsection
