@extends('layouts.templateatm')
@section('section')
<section class='content-header'>
    <h1> GESTIÓN DE ATM</h1>
    <ol class='breadcrumb'>
        <li><a href='#'><i class='fa fa-dashboard'></i>ATM</a></li>
        <li class='active'>fichas</li>
    </ol>
</section>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Planes de capacitación y asistencia tecnica</h3>
                    <a href="{{url('register')}}" class="btn btn-success" title="Nuevo Usuario" style="float: right;"><i class="fa fa-plus"></i></a>
                </div>
                <div class="box-body">
                    <form action="{{url('centrop/lista')}}" method="get">
                        <div class="input-group">
                            <input type="text" name="search" value="" class="form-control">
                            <span class="input-group-btn">
                                <button class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
                <div class='box-body table-responsive'>
                    <table id='' class='table table-bordered table-hover'>
                        <thead>
                            <tr>
                                <th> Plan EDUSA</th>
                                <th> Avance</th>
                                <th> Plan GS</th>
                                <th> Avance</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($listaPlan as $item)
                            <tr>
                                <td>{{$item->planEDUSA}} %</td>
                                <td>{{$item->avanceEDUSA}}</td>
                                <td>{{$item->planGS}}</td>    
                                <td>{{$item->avanceGS}}</td> 
                                <td><button class="btn btn-success btn-sm pull-right btn-sm" data-target="#modalAtm"  data-toggle="modal" ><i class="fa fa-plus"></i></button></td>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection