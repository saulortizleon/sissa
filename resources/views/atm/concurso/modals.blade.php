<div class="modal fade bd-example-modal-lg" id="andahuaylas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel" style=" color: #182d80 ; ">DISTRITOS DE LA <small></small> PROVINCIA DE ANDAHUAYLAS</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            @foreach ($munis as $item)
            @if ($item->provincia == 'ANDAHUAYLAS')
            <a class="btn btn-primary shadow-lg p-2 mb-3 bg-white rounded" target="_blank" style="color:white; padding: 10px; margin-top:10px ; background-color:#182d80 !important;
            border-color:#182d80 !important;"  href="{{url('atm')}}/{{$item->idatm}}">
                 @if (!empty($item->img))
                 <img class="rounded-circle" src="{{url($item->img)}}"      width="60" height="auto"  />
                 @else
                 <img class="rounded-circle" src="{{ asset('img/virtual/m.jpg') }}"    width="60" height="auto"  />
                 @endif

                 @if (!empty($item->finalista))
                 <i class="fa fa-hand-o-up"></i>  <b>{{$item->distrito}} </b>
                 <span class="badge badge-danger">Finalista</span>
                 @else
                 <i class="fa fa-hand-o-up"></i>  <b>{{$item->distrito}} </b>
                 @endif
            </a>
            @endif
            @endforeach
        </div>
        <div class="modal-footer">
          <p class="azul">"Esta pandemia no frenará la adecuada gestión de los servicios de agua y saneamiento en la región de Apurímac"</p>
        </div>
        </div>
    </div>
</div>
<div class="modal fade bd-example-modal-lg" id="abancay" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel" style=" color: #182d80 ; ">DISTRITOS DE LA <small></small> PROVINCIA DE ABANCAY</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            @foreach ($munis as $item)
            @if ($item->provincia == 'ABANCAY')
            <a class="btn btn-primary shadow-lg p-2 mb-3 bg-white rounded" target="_blank" style="color:white; padding: 10px; margin-top:10px ; background-color:#182d80 !important;
            border-color:#182d80 !important;"  href="{{url('atm')}}/{{$item->idatm}}">
                 @if (!empty($item->img))
                <img class="rounded-circle" src="{{url($item->img)}}"      width="60" height="auto"  />
                @else
                <img class="rounded-circle" src="{{ asset('img/virtual/m.jpg') }}"    width="60" height="auto"  />
                @endif
                @if (!empty($item->finalista))
                <i class="fa fa-hand-o-up"></i>  <b>{{$item->distrito}} </b>
                <span class="badge badge-danger">Finalista</span>
                @else
                <i class="fa fa-hand-o-up"></i>  <b>{{$item->distrito}} </b>
                @endif

            </a>
            @endif
            @endforeach
        </div>
        <div class="modal-footer">
          <p class="azul">"Esta pandemia no frenará la adecuada gestión de los servicios de agua y saneamiento en la región de Apurímac"</p>
        </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="antabamba" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel" style=" color: #182d80 ; ">DISTRITOS DE LA <small></small> PROVINCIA DE ANTABAMBA</h3>
            <button type="button" class="btn btn-danger close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            @foreach ($munis as $item)
            @if ($item->provincia == 'ANTABAMBA')
            <a class="btn btn-primary shadow-lg p-2 mb-3 bg-white rounded" target="_blank" style="color:white; padding: 10px; margin-top:10px ; background-color:#182d80 !important;
            border-color:#182d80 !important;"  href="{{url('atm')}}/{{$item->idatm}}">
                @if (!empty($item->img))
                <img class="rounded-circle" src="{{url($item->img)}}"      width="60" height="auto"  />
                @else
                <img class="rounded-circle" src="{{ asset('img/virtual/m.jpg') }}"    width="60" height="auto"  />
                @endif
                @if (!empty($item->finalista))
                <i class="fa fa-hand-o-up"></i>  <b>{{$item->distrito}} </b>
                <span class="badge badge-danger">Finalista</span>
                @else
                <i class="fa fa-hand-o-up"></i>  <b>{{$item->distrito}} </b>
                @endif
            </a>
            @endif
            @endforeach
        </div>
        <div class="modal-footer">
          <p class="azul">"Esta pandemia no frenará la adecuada gestión de los servicios de agua y saneamiento en la región de Apurímac"</p>
        </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="cotabambas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel" style=" color: #182d80 ; ">DISTRITOS DE LA <small></small> PROVINCIA DE COTABAMBAS</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            @foreach ($munis as $item)
            @if ($item->provincia == 'COTABAMBAS')
            <a class="btn btn-primary shadow-lg p-2 mb-3 bg-white rounded" target="_blank" style="color:white; padding: 10px; margin-top:10px ; background-color:#182d80 !important;
            border-color:#182d80 !important;"  href="{{url('atm')}}/{{$item->idatm}}">
                @if (!empty($item->img))
                <img class="rounded-circle" src="{{url($item->img)}}"      width="60" height="auto"  />
                @else
                <img class="rounded-circle" src="{{ asset('img/virtual/m.jpg') }}"    width="60" height="auto"  />
                @endif
                @if (!empty($item->finalista))
                <i class="fa fa-hand-o-up"></i>  <b>{{$item->distrito}} </b>
                <span class="badge badge-danger">Finalista</span>
                @else
                <i class="fa fa-hand-o-up"></i>  <b>{{$item->distrito}} </b>
                @endif
            </a>
            @endif
            @endforeach
        </div>
        <div class="modal-footer">
          <p class="azul">"Esta pandemia no frenará la adecuada gestión de los servicios de agua y saneamiento en la región de Apurímac"</p>
        </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="grau" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel" style=" color: #182d80 ; ">DISTRITOS DE LA <small></small> PROVINCIA DE GRAU</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            @foreach ($munis as $item)
            @if ($item->provincia == 'GRAU')
            <a class="btn btn-primary shadow-lg p-2 mb-3 bg-white rounded" target="_blank" style="color:white; padding: 10px; margin-top:10px ; background-color:#182d80 !important;
            border-color:#182d80 !important;"  href="{{url('atm')}}/{{$item->idatm}}">

            @if (!empty($item->img))
            <img class="rounded-circle" src="{{url($item->img)}}"      width="60" height="auto"  />
            @else
            <img class="rounded-circle" src="{{ asset('img/virtual/m.jpg') }}"    width="60" height="auto"  />
            @endif
            @if (!empty($item->finalista))
            <i class="fa fa-hand-o-up"></i>  <b>{{$item->distrito}} </b>
            <span class="badge badge-danger">Finalista</span>
            @else
            <i class="fa fa-hand-o-up"></i>  <b>{{$item->distrito}} </b>
            @endif

            </a>
            @endif
            @endforeach
        </div>
        <div class="modal-footer">
          <p class="azul">"Esta pandemia no frenará la adecuada gestión de los servicios de agua y saneamiento en la región de Apurímac"</p>
        </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="aymaraes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel" style=" color: #182d80 ; ">DISTRITOS DE LA <small></small> PROVINCIA DE AYMARAES</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            @foreach ($munis as $item)
            @if ($item->provincia == 'AYMARAES')
            <a class="btn btn-primary shadow-lg p-2 mb-3 bg-white rounded" target="_blank" style="color:white; padding: 10px; margin-top:10px ; background-color:#182d80 !important;
            border-color:#182d80 !important;"  href="{{url('atm')}}/{{$item->idatm}}">

           @if (!empty($item->img))
            <img class="rounded-circle" src="{{url($item->img)}}"      width="60" height="auto"  />
            @else
            <img class="rounded-circle" src="{{ asset('img/virtual/m.jpg') }}"    width="60" height="auto"  />
            @endif
            @if (!empty($item->finalista))
            <i class="fa fa-hand-o-up"></i>  <b>{{$item->distrito}} </b>
            <span class="badge badge-danger">Finalista</span>
            @else
            <i class="fa fa-hand-o-up"></i>  <b>{{$item->distrito}} </b>
            @endif

            </a>
            @endif
            @endforeach
        </div>
        <div class="modal-footer">
          <p class="azul">"Esta pandemia no frenará la adecuada gestión de los servicios de agua y saneamiento en la región de Apurímac"</p>
        </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="chincheros" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title" id="exampleModalLabel" style=" color: #182d80 ; ">DISTRITOS DE LA <small></small> PROVINCIA DE CHINCHEROS</h3>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            @foreach ($munis as $item)
            @if ($item->provincia == 'CHINCHEROS')
            <a class="btn btn-primary shadow-lg p-2 mb-3 bg-white rounded" target="_blank" style="color:white; padding: 10px; margin-top:10px ; background-color:#182d80 !important;
            border-color:#182d80 !important;"  href="{{url('atm')}}/{{$item->idatm}}">

                @if (!empty($item->img))
                <img class="rounded-circle" src="{{url($item->img)}}"      width="60" height="auto"  />
                @else
                <img class="rounded-circle" src="{{ asset('img/virtual/m.jpg') }}"    width="60" height="auto"  />
                @endif
                @if (!empty($item->finalista))
                <i class="fa fa-hand-o-up"></i>  <b>{{$item->distrito}} </b>
                <span class="badge badge-danger">Finalista</span>
                @else
                <i class="fa fa-hand-o-up"></i>  <b>{{$item->distrito}} </b>
                @endif
            </a>
            @endif
            @endforeach
        </div>
        <div class="modal-footer">
          <p class="azul">"Esta pandemia no frenará la adecuada gestión de los servicios de agua y saneamiento en la región de Apurímac"</p>
        </div>
        </div>
    </div>
</div>