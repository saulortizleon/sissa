<div id="actasSap" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" >
    <div class="modal-content"  >
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">VERIFICABLE</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            @if (!empty($stand->actasSap))
            <embed    src="{{ asset($stand->actasSap) }}"
                type="application/pdf" frameborder="0" width="100%" height="800px">
            @else
              <i style="color:red"> NO SUBIÓ VERIFICABLE</i>
            @endif
          </div>           
    </div>
  </div>
</div>

<div id="cargoPOI" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" >
      <div class="modal-content"  >
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">VERIFICABLE</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
            @if (!empty($stand->cargoPOI))
              <embed    src="{{ asset($stand->cargoPOI) }}"
                  type="application/pdf" frameborder="0" width="100%" height="800px">
            @else
              <i style="color:red"> NO SUBIÓ VERIFICABLE</i>
            @endif
            </div>           
      </div>
    </div>
  </div>

  <div id="fotosSap" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" >
      <div class="modal-content"  >
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">VERIFICABLE</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              @if (!empty($stand->fotosSap))
              <embed    src="{{ asset($stand->fotosSap) }}"
                  type="application/pdf" frameborder="0" width="100%" height="800px">
              @else
              <i style="color:red"> NO SUBIÓ VERIFICABLE</i>
              @endif
            </div>           
      </div>
    </div>
  </div>

   