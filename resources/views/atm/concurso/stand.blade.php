<div class="modal fade bd-example-modal-lg" id="stand1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document" >
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="exampleModalLabel" style=" color: #182d80 ; "> 
              Categoría:  Educación Sanitaria Ambiental </h5>
            <button type="button" class="btn" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body"> 
               
            <div class="col-lg-12 col-sm-6 portfolio-item">
                <h5 class="azul"> 3.1. Elaboración de un spot y/o video con la participación
                    de operadores y otros (desinfección,cloración)</h5>      
              <center>            
                  
                    <?php
                        $orig =$stand->linkvideo;
                
                        $a = htmlentities($orig);//convertimos a html
                
                        $b = html_entity_decode($a);
                
                        echo $b;
                    ?>
              </center>             
            </div>
            <div class="col-lg-12 col-sm-6 portfolio-item">
                <h5 class="azul"> 3.2. JASS Formalizada  de acuerdo a los lineamientos del PNSR.</h5>   
                 
                <center>           
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#capturaJass">
                  REVISAR VERIFICABLE</button>
                </center> 
            </div>
            <div class="col-lg-12 col-sm-6 portfolio-item">
                <h5 class="azul"> 3.3. El ATM realiza la supervisión a la JASS</h5>   
                 
                <center>           
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#fichasSup">
                  REVISAR VERIFICABLE</button>
                </center> 
            </div>
            <div class="col-lg-12 col-sm-6 portfolio-item">
                <h5 class="azul"> 3.4. La JASS realiza Operación y Mantenimiento del sistema de agua potable y eliminación de excretas.</h5>  
                <center>           
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#jassopm">
                  REVISAR VERIFICABLE</button>
                </center> 
            </div>
            <div class="col-lg-12 col-sm-6 portfolio-item">               
                <h5 class="azul"> 3.5. La JASS cuenta con plan operativo anual y aprobación de la cuota familiar</h5>  
                <center>       
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#jaspoa">
                  REVISAR VERIFICABLE</button>
                </center> 
            </div>
            <div class="col-lg-12 col-sm-6 portfolio-item">
                 <h5 class="azul"> 3.6. Videos Enmarcados en educación sanitaria ambiental</h5>      
                 <center>
                    <?php
                        $orig =$stand->videoEdusa;                
                        $a = htmlentities($orig);//convertimos a html                
                        $b = html_entity_decode($a);                
                        echo $b;
                    ?>
              </center>             
            </div>   
            <div class="col-lg-12 col-sm-6 portfolio-item">               
                <h5 class="azul"> 3.7. Estrategias tomadas a  través del área técnica municipal 
                    para la sostenibilidad de la cuota familiar.  </h5>  
                <center>       
                <p class="text-justify" style="font-size:18px">
                    {{$stand->estrategias}}
                </p>
            </div>   
        </div>
        
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="stand2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style=" color: #182d80 ; ">Categoría: Mantenimiento de Sistemas de Agua y Saneamiento</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class ="row">
                    <div class="col-lg-12 col-sm-6 portfolio-item">
                        <h5 class="azul"> 2.1. El ATM cuenta con JASS WASI MODELO  </h5>   
                        <div class ="row">                                
                            <div class="col-lg-6 col-sm-6 portfolio-item"> 
                                    <a href="#"><img class="card-img-top" src="{{asset($stand->foto1)}}" >
                                    </a>                                         
                            </div>
                            <div class="col-lg-6 col-sm-6 portfolio-item">                        
                                    <a href="#"><img class="card-img-top" src="{{asset($stand->foto2)}}" >
                                    </a>                                     
                            </div>
                            <div class="col-lg-6 col-sm-6 portfolio-item">
                                <div class="card h-100">
                                    <a href="#"><img class="card-img-top" src="{{asset($stand->foto3)}}" >
                                    </a> 
                                </div>                  
                            </div>
                            <div class="col-lg-6 col-sm-6 portfolio-item">
                                <div class="card h-100">
                                    <a href="#"><img class="card-img-top" src="{{asset($stand->foto4)}}" >
                                    </a> 
                                </div>                  
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-sm-6 portfolio-item">
                    <h5 class="azul"> 2.2. INSTALACION DE SISTEMAS DE CLORACION EN EL AÑO 2020</h5>  
                    <center>           
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#actasSap">
                    REVISAR VERIFICABLE</button>
                    </center> 
                </div>
                <div class="col-lg-12 col-sm-6 portfolio-item">
                    <h5 class="azul"> 2.3. POI CUENTA CON PLAN DE MANTENIMIENTO ANUAL</h5>   
                    <center>           
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#cargoPOI">
                    REVISAR VERIFICABLE</button>
                    </center> 
                </div>
                <div class="col-lg-12 col-sm-6 portfolio-item">
                    <h5 class="azul"> 2.4. SAP INTERVENIDOS EN EL AÑO 2020</h5>   
                    <center>           
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#fotosSap">
                    REVISAR VERIFICABLE</button>
                    </center> 
                </div>
            </div>       
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="stand3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel" style=" color: #182d80 ; ">Categoría:  Gestión de gobiernos locales  </h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="col-lg-12 col-sm-6 portfolio-item">
                <h5 class="azul"> 1.1. PARTICIPACIÓN EN LA ESCUELA YAKU KAWSAY - 1° ESPECIALIDAD TÉCNICA PARA  ATM</h5>  
                <center>           
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#constanciaAtm">
                REVISAR VERIFICABLE</button>
                </center> 
            </div>
            <div class="col-lg-12 col-sm-6 portfolio-item">
                <h5 class="azul"> 1.2. PARTICIPACIÓN DE OPERADORES EN LA ESCUELA YAKU KAWSAY - 
                    2° ESPECIALIDAD TÉCNICA PARA  OPERADORES RURALES Y GASFITEROS</h5>  
                <center>           
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#constanciaOperador">
                REVISAR VERIFICABLE</button>
                </center> 
            </div>
            <div class="col-lg-12 col-sm-6 portfolio-item">
                <h5 class="azul"> 1.3. REPORTE DE ACTUALIZACIÓN DE DATASS </h5>  
                <center>           
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#reporteDatass">
                REVISAR VERIFICABLE</button>
                </center> 
            </div>
            <div class="col-lg-12 col-sm-6 portfolio-item">
                <h5 class="azul"> 1.4. CONSTANCIA DE SAP QUE BRINDAN AGUA CLORADA CONTINUAMENTE </h5>  
                <center>           
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#constanciaSap">
                REVISAR VERIFICABLE</button>
                </center> 
            </div>
            <div class="col-lg-12 col-sm-6 portfolio-item">
                <h5 class="azul"> 1.5. ATM EJECUTA SU POI 2020 </h5>  
                <center>           
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#POI">
                REVISAR VERIFICABLE</button>
                </center> 
            </div>
            <div class="col-lg-12 col-sm-6 portfolio-item">
                <h5 class="azul"> 1.6. GOBIERNO LOCAL MEJORA LA GESTION EN AGUA Y SANEAMIENTO DURANTE EMERGENCIA SANITARIA</h5>  
                <center>           
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#decretosUrgencia">
                REVISAR VERIFICABLE</button>
                </center> 
            </div>
        </div>
       
        </div>
    </div>
</div>