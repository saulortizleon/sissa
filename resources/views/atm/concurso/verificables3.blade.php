<div id="constanciaAtm" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" >
    <div class="modal-content"  >
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">VERIFICABLE</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            @if (!empty($stand->constanciaAtm))
            <embed    src="{{ asset($stand->constanciaAtm)}}"
                type="application/pdf" frameborder="0" width="100%" height="800px">
            @else
                <i style="color:red"> NO SUBIÓ VERIFICABLE</i>
            @endif
          </div>           
    </div>
  </div>
</div>

<div id="constanciaOperador" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" >
      <div class="modal-content"  >
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">VERIFICABLE</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              @if (!empty($stand->constanciaOperador))
              <embed    src="{{ asset($stand->constanciaOperador)}}"
                  type="application/pdf" frameborder="0" width="100%" height="800px">
              @else
                  <i style="color:red"> NO SUBIÓ VERIFICABLE</i>
              @endif
            </div>           
      </div>
    </div>
  </div>

  <div id="reporteDatass" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" >
      <div class="modal-content "  >
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">VERIFICABLE</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              @if (!empty($stand->reporteDatass))
              <embed    src="{{ asset($stand->reporteDatass) }}"
                  type="application/pdf" frameborder="0" width="100%" height="800px">
              @else
                  <i style="color:red"> NO SUBIÓ VERIFICABLE</i>
              @endif
            </div>           
      </div>
    </div>
  </div>

  <div id="constanciaSap" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" >
      <div class="modal-content"  >
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">VERIFICABLE</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              @if (!empty($stand->constanciaSap))
              <embed    src="{{ asset($stand->constanciaSap) }}"
                  type="application/pdf" frameborder="0" width="100%" height="800px">
              @else
                  <i style="color:red"> NO SUBIÓ VERIFICABLE</i>
              @endif
            </div>           
      </div>
    </div>
  </div> 
  <div id="POI" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" >
      <div class="modal-content"  >
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">VERIFICABLE</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              @if (!empty($stand->POI))
              <embed    src="{{ asset($stand->POI) }}"
                  type="application/pdf" frameborder="0" width="100%" height="800px">
              @else
                  <i style="color:red"> NO SUBIÓ VERIFICABLE</i>
              @endif
            </div>           
      </div>
    </div>
  </div> 
  <div id="decretosUrgencia" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" >
      <div class="modal-content"  >
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">VERIFICABLE</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              @if (!empty($stand->decretosUrgencia))
              <embed    src="{{ asset($stand->decretosUrgencia) }}"
                  type="application/pdf" frameborder="0" width="100%" height="800px">
              @else
                  <i style="color:red"> NO SUBIÓ VERIFICABLE</i>
              @endif
            </div>           
      </div>
    </div>
  </div> 