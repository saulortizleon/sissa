<div id="capturaJass" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" >
    <div class="modal-content"  >
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">VERIFICABLE</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            @if (!empty($stand->capturaJass))
            <embed    src="{{ asset($stand->capturaJass) }}"
                type="application/pdf" frameborder="0" width="100%" height="800px">
            @else
              <i style="color:red"> NO SUBIÓ VERIFICABLE</i>
            @endif
          </div>           
    </div>
  </div>
</div>

<div id="fichasSup" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" >
      <div class="modal-content"  >
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">VERIFICABLE</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              @if (!empty($stand->fichasSup))
              <embed    src="{{ asset($stand->fichasSup) }}"
                  type="application/pdf" frameborder="0" width="100%" height="800px">
              @else
                  <i style="color:red"> NO SUBIÓ VERIFICABLE</i>
              @endif
            </div>           
      </div>
    </div>
  </div>

  <div id="jassopm" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" >
      <div class="modal-content"  >
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">VERIFICABLE</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              @if (!empty($stand->jassopm))
              <embed    src="{{ asset($stand->jassopm) }}"
                  type="application/pdf" frameborder="0" width="100%" height="800px">
              @else
                  <i style="color:red"> NO SUBIÓ VERIFICABLE</i>
              @endif
            </div>           
      </div>
    </div>
  </div>

  <div id="jaspoa" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" >
      <div class="modal-content"  >
          <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">VERIFICABLE</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              @if (!empty($stand->jaspoa))
              <embed    src="{{ asset($stand->jaspoa) }}"
                  type="application/pdf" frameborder="0" width="100%" height="800px">
              @else
                  <i style="color:red"> NO SUBIÓ VERIFICABLE</i>
              @endif
            </div>           
      </div>
    </div>
  </div> 