@extends('layouts.templateblog')
@section('section')
@foreach($atm as $stand)
  <div class="row" style="    display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  margin-right: -80px;
  margin-left: -80px;
  margin-top: -10px;">
    <div class="col-lg-12"  >
      <img  img src="{{url('img/virtual/stand.webp')}}" class="img-fluid" alt="Responsive image"  style="position: relative;">

      @if (!empty($stand->portada1))
      <a data-toggle="modal" data-target="#stand1"  >    <img id="edusa1" class="img-fluid"  src="{{url($stand->portada1)}}"   style="
          position:absolute;
          object-fit: cover;
          top: 46.8%;
          width: 14.5%;
          height: 17.8%;
          left: 57.5%;
          background-size:cover;
          max-width: 28%;
          padding: 0;
          "
        > </a>
      @else
      <a data-toggle="modal" data-target="#stand1"  >  <img id="edusa2" class="img-fluid"  src="{{url('img/virtual/x.jpg')}}"   style="
            position:absolute;
            object-fit: cover;
            top: 46.8%;
            width: 14.5%;
            height: 17.8%;
            left: 57.5%;
            background-size:cover;
            max-width: 28%;
            padding: 0;
            "
          > </a>
          @endif

          <button id="edusa3"  type="button"  class="btn btn-xs btn-danger" data-toggle="modal" data-target="#stand1" style="
            position:absolute;
            top: 47.5%;
            left: 69.7%;
            width:2%;
            height: 2.8%;
            max-width: 33%;
            padding: 0;
            font-size: calc(0.15rem + 0.30vw);
            font-weight:bold;
            "
          >
          <i class="fa fa-2x fa-plus-circle"  >  </i>
          </button>

          @if (!empty($stand->portada2))
              <img id="sap1" src="{{url($stand->portada2)}}"   style="
                position:absolute;
                object-fit: cover;
                top: 31%;
                left: 31.8%;
                width:17.315%;
                height:23.95%;
                max-width: 28%;
                padding: 0;
                background-size:contain;
                clip-path: polygon(54% 0%, 76% 25%, 76% 76%, 54% 100%, 31% 76%, 31% 25%);
              "  >
            @else
                <img id="sap2" src="{{url('img/virtual/x.jpg')}}"   style="
                position:absolute;
                object-fit: cover;
                top: 31%;
                left: 31.8%;
                width:17.315%;
                height:23.95%;
                max-width: 28%;
                padding: 0;
                background-size:contain;
                clip-path: polygon(54% 0%, 76% 25%, 76% 76%, 54% 100%, 31% 76%, 31% 25%);
                "  >
            @endif
            <button id="sap3"  type="button"  class="btn btn-xs btn-danger" data-toggle="modal" data-target="#stand2" style="
              position:absolute;
              top: 60.5%;
              left: 43.5%;
              width:2%;
              height: 2.8%;
              max-width: 33%;
              padding: 0;
              font-size: calc(0.15rem + 0.30vw);
              font-weight:bold;

              "
            >
              <i class="fa fa-2x fa-plus-circle"  >  </i>
            </button>


            @if (!empty($stand->portada3))
            <img id="gestion1" src="{{url($stand->portada3)}}"
            style="
                position:absolute;
                object-fit: cover;
                top: 32%;
                left: 15.89%;
                width:17.315%;
                height:23.95%;
                max-width: 28%;
                padding: 0;
                background-size:contain;
                clip-path: polygon(54% 0%, 76% 25%, 76% 76%, 54% 100%, 31% 76%, 31% 25%); "
            >
            @else
              <img id="gestion2" src="{{url('img/virtual/x.jpg')}}"
            style="
                position:absolute;
                object-fit: cover;
                top: 32%;
                left: 15.89%;
                width:17.315%;
                height:23.95%;
                max-width: 28%;
                padding: 0;
                background-size:contain;
                clip-path: polygon(54% 0%, 76% 25%, 76% 76%, 54% 100%, 31% 76%, 31% 25%); "
            >
          @endif
          <button id="gestion3" type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#stand3" style="
            position:absolute;
            top: 60.5%;
            left: 27.5%;
            width:2%;
            height: 2.5%;
            max-width: 33%;
            padding: 0;
            font-size: calc(0.15rem + 0.30vw);
            font-weight:
            "
          >
          <i class="fa fa-2x fa-plus-circle">  </i>
          </button>


          @if (!empty($stand->img))
            <img id="logo" class="img-fluid"  src="{{url($stand->img)}}"   alt="logo muni" style="
                position:absolute;
                object-fit: cover;
                top: 5%;
                width: 5%;
                height: auto;
                left: 37.8%;
                background-size:contain;
                max-width: 28%;
                padding: 0;
              "  >
            @else
            <img id="logo" class="img-fluid"  src="{{url('img/virtual/x.jpg')}}"  alt="logo muni" style="
              position:absolute;
              object-fit: cover;
              top: 5%;
              width: 5%;
              height: auto;
              left: 37.8%;
              background-size:contain;
              max-width: 28%;
              padding: 0;
              "
            >
          @endif
            <h6 style="
              font-weight: bold;
              text-align: center;
              position:absolute;
              object-fit: cover;
              top: 8%;
              left: 44.8%;
              width:17.315%;
              height:auto;
              max-width: 28%;
              padding: 0;
              color:#2a9690;
              "
            >
            {{$stand->muni}}
            </h6>

          <a id="web1"  type="button"  class="btn btn-xs btn-danger" target="_blank"  href="{{asset($stand->face)}}"  style="
              position:absolute;
              top: 15.5%;
              left: 58.7%;
              width:1.4%;
              height: 2.5%;
              max-width: 2%;
              padding: 0;
              font-size: calc(0.13rem + 0.30vw);
              color:white
              "
            >
          <i class="fa fa-2x fa-facebook-square">  </i>
          </a>
          <a id="web2"  type="button"  class="btn btn-xs btn-danger" target="_blank"   href="{{asset($stand->web)}}" style="
            position:absolute;
            top: 15.5%;
            left: 60.7%;
            width:1.4%;
            height: 2.5%;
            max-width: 2%;
            padding: 0;
            font-size: calc(0.13rem + 0.30vw);
            color:white
            "
          >
        <span class="fa fa-2x fa-hand-pointer-o">  </span>
        </a>


          <a id="trans1"  type="button"  class="btn btn-xs btn-danger" target="_blank"  href="https://www.facebook.com/drvcsapurimacoficial/videos/"  style="
            position:absolute;
            top: 76.5%;
            left: 53.7%;
            width:6%;
            height: 2.5%;
            max-width: 6%;
            padding: 0;
            background-color: Transparent;
            background-repeat:no-repeat;
            border: none;
            cursor:pointer;
            overflow: hidden;
            outline:none;
            color:transparent;
            "    >
      VIDEOS DRVCS APURIMAC
        </a>
      <a id="trans2"  type="button"  class="btn btn-xs btn-danger" target="_blank"  href="https://www.facebook.com/drvcsapurimacoficial/photos/"  style="
        position:absolute;
        top: 76.2%;
        left: 37.7%;
        width:5%;
        height: 2.8%;
        max-width: 5%;
        padding: 0;
        background-color: Transparent;
        background-repeat:no-repeat;
        border: none;
        cursor:pointer;
        overflow: hidden;
        outline:none;
        color:transparent;
        "    >
      FOTOS DRVCS APURIMAC
      </a>
      <audio controls autoplay  frameborder="8" style="
        position: absolute;
        top: 7.8%;
        left: 12%;
        filter: sepia(20%) saturate(70%) grayscale(1) contrast(99%) invert(12%);
        width: 140px;
        height: 30px;
        padding: 0;
        font-size: calc(0.13rem + 0.30vw);
        color: white;
        audio::-webkit-media-controls-panel

      "> 
      </audio>
    </div>
  </div>
@endforeach
<!-- Modal -->
@include('atm/concurso/stand')
@include('atm/concurso/verificables')
@include('atm/concurso/verificables2')
@include('atm/concurso/verificables3')
<script>
  $(function() {
    $('.carousel-3d-basic').mdbCarousel3d();
    $('.carousel-3d-controls').mdbCarousel3d();
    $('.carousel-3d-vertical').mdbCarousel3d({
      vertical: true
    });
    $('.carousel-3d-autoplay-off').mdbCarousel3d({
      autoplay: false
    });
  });
</script>
<style type="text/css">
    #index{ display:none}
    #aliados1{ display:none}
    #aliados2{ display:none}
    #tituloAliados{ display:none}

</style>
@endsection