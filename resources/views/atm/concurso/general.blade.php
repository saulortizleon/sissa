@extends('layouts.templateblog')
@section('section')
<div class="row" style="
  display: flex;
  -ms-flex-wrap: wrap;
  flex-wrap: wrap;
  margin-right: -70px;
  margin-left: -70px;
  margin-top: -70px;">
  <div class="col-lg-12" >
   <img  img src="{{asset('img/virtual/principal2.webp')}}" class="img-fluid" alt="Responsive image"  style="position: relative;">

 <button type="button" class="btn-lg btn-primary" data-toggle="modal" data-target="#chincheros" style="
   position:absolute;
   background-color: #faf5a6 !important;
   border-color:#faf5a6 !important;
   top: 19%;
   left: 40%;
   width:6.5%;
   height: 3.5%;
   max-width: 33%;
   padding: 0;
   font-size: calc(0.40rem + 0.30vw);
   font-weight: bold;
   color: #182d80 ;">
<span class="fa fa-angle-right " style="font-weight: bold"></span> CHINCHEROS
 </button>

 <button type="button" class="btn-lg btn-primary" data-toggle="modal" data-target="#andahuaylas" style="
   position:absolute;
   background-color:#fcc546!important;
   border-color:#faf5a6 !important;
   top: 29%;
   left: 40.3%;
   width:7%;
   height: 3.5%;
   max-width: 33%;
   padding: 0;
   font-size: calc(0.40rem + 0.30vw);
   font-weight: bold;
   color: #182d80 ;">
<span class="fa fa-angle-right " style="font-weight: bold"></span> ANDAHUAYLAS
 </button>

 <button type="button" class="btn-lg btn-primary" data-toggle="modal" data-target="#aymaraes" style="
   position:absolute;
   background-color:#fdc795 !important;
   border-color:#fdc795 !important;
   color: #182d80 ;
   top: 40%;
   left: 40.8%;
   width:6.5%;
   height: 3.5%;
   max-width: 33%;
   padding: 0;
   font-size: calc(0.40rem + 0.30vw);
   font-weight: bold;">
<span class="fa fa-angle-right " style="font-weight: bold"></span> AYMARAES
 </button>

<button type="button" class="btn-lg btn-primary" data-toggle="modal" data-target="#grau" style="
position:absolute;
background-color:#cbdd8a !important;
border-color:#cbdd8a  !important;
color: #182d80 ;
top: 20%;
left: 60.5%;
width:6%;
height: 3.5%;
max-width: 33%;
padding: 0;
font-size: calc(0.40rem + 0.30vw);
font-weight: bold;">
<span class="fa fa-angle-right " style="font-weight: bold"></span>  GRAU
</button>
<button type="button" class="btn-lg btn-primary" data-toggle="modal" data-target="#cotabambas" style="
background-color:#c6e2c6 !important;
border-color: #c6e2c6 !important ;
color: #182d80 ;
position:absolute;
top: 32%;
left: 63.3%;
width:6%;
height: 3.5%;
max-width: 33%;
padding: 0;
font-size: calc(0.40rem + 0.30vw);
font-weight: bold;">
 <span class="fa fa-angle-right " style="font-weight: bold"></span> COTABAMBAS
</button>
<button type="button" class="btn-lg btn-primary" data-toggle="modal" data-target="#abancay" style="
position:absolute;
background-color:#f7a2a1 !important;
border-color: #f7a2a1 !important ;
color: #182d80 ;
top: 17%;
left: 52%;
width:5.8%;
height: 3.5%;
max-width: 33%;
padding: 0;
font-size: calc(0.40rem + 0.30vw);
font-weight: bold;">
 <span class="fa fa-angle-right " style="font-weight: bold"></span> ABANCAY
</button>

<button type="button" class="btn-lg btn-primary" data-toggle="modal" data-target="#antabamba" style="
position:absolute;
background-color:#95d0ec !important;
border-color: #95d0ec !important ;
color: #182d80 ;
top: 41.5%;
left: 57%;
width:7%;
height: 3.5%;
max-width: 33%;
padding: 0;
font-size: calc(0.40rem + 0.30vw);
font-weight: bold;">
 <span class="fa fa-angle-right " style="font-weight: bold"></span> ANTABAMBA
</button>

<div class="col-md-3" style="position:absolute;  top: 76.5%;
left: 65%;">
 <iframe width="50%" height="110" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/1176965863&color=%23ff5500&auto_play=true&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true">
</iframe>

  </div>
</div>

<style type="text/css">
    #index{ display:none}
</style>
@include('atm/concurso/modals')
@endsection