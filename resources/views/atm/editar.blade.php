@extends('layouts.templateatm')
@section('section')
    <section class='content-header'>
        <h1> INSTITUCIONALIZACIÓN <small>Area Tecnica Municipal</small></h1>
        <ol class='breadcrumb'>
            <li><a href='#'><i class='fa fa-dashboard'></i>Editar </a></li>
            <li class='active'>ATM</li>
        </ol>
    </section>
    <section class='content'>
        <div class='row'>
            <div class='col-xs-12'>
                <form id="frmAtm" name="frmAtm" action="{{url('atm/editar')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                <div class="row">
                    <div class="col-md-4">
                        <div class="box box-info">
                            <div class="box-body">
                                @foreach ($verificable as $item)
                                <input type="hidden" name="ubigeo" value="{{$listaEditar->ubigeo}}">
                                <div class="form-group">

                                    <label for="ordMunicipalRof">   Ordenanza que crea o incorpora las funciones en el ROF
                                    </label>
                                    <input type="text"  class="form-control" id="ordMunicipalRof" name="ordMunicipalRof" value="{{$listaEditar->ordMunicipalRof}}">
                                    <div style="display: {{$item->ver1!=null ? 'none':''}}">
                                        <label for="" style="color:#00a65a;">   <span class="label label-success">Subir verificable:</span>  <br> <i class="fa fa-file-archive-o"> Documento escaneado, formato PDF</i> </label>
                                    <input id="cambiar" type="file"  class="form-control" id="ver1" name="ver1" >

                                    </div>
                                  <br>  @if ($item->ver1!=null)
                                  <a  href="{{asset($item->ver1)}}" target="_blank"><i class="fa fa-eye"></i> Ver Verificable</a> |
                                   <label for="cambiar" style="color:red"><i class="fa fa-pencil"> </i> Cambiar Documento</label>
                                    @else

                                    @endif


                                </div>
                                <div class="form-group" >
                                    <label for="munDepende"> Nombre del area o unidad del cual  depende el ATM(Area Tecnica Municipal) en la actualidad según la ordenanza de creacion.
                                    </label>
                                    <input type="text"  class="form-control" id="munDepende" name="munDepende" value="{{$listaEditar->munDepende}}" >

                                    <div style="display: {{$item->ver2!=null ? 'none':''}}">
                                        <label for="" style="color:#00a65a;">   <span class="label label-success">Subir verificable:</span>  <br> <i class="fa fa-file-archive-o"> Documento escaneado, formato PDF</i> </label>
                                    <input id="cambiar" type="file"  class="form-control" id="ver2" name="ver2" >

                                    </div>
                                  <br>  @if ($item->ver2!=null)
                                  <a  href="{{asset($item->ver2)}}" target="_blank"><i class="fa fa-eye"></i> Ver Verificable</a> |
                                   <label for="cambiar" style="color:red"><i class="fa fa-pencil"> </i> Cambiar Documento</label>

                                    @else

                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-primary">
                            <div class="box-body">
                                <div class="form-group" >
                                    <label for="resolApruebaPerfil">  Resolucion que aprueba el perfil de puesto del responsable de la ATM(Area Tecnica Municipal).
                                    </label>
                                    <input type="text"  class="form-control" id="resolApruebaPerfil" name="resolApruebaPerfil" value="{{$listaEditar->resolApruebaPerfil}}" >

                                    <div style="display: {{$item->ver3!=null ? 'none':''}}">
                                        <label for="" style="color:#00a65a;">   <span class="label label-success">Subir verificable:</span>  <br> <i class="fa fa-file-archive-o"> Documento escaneado, formato PDF</i> </label>
                                    <input id="cambiar" type="file"  class="form-control" id="ver3" name="ver3" >

                                    </div>
                                  <br>  @if ($item->ver3!=null)
                                  <a  href="{{asset($item->ver3)}}" target="_blank"><i class="fa fa-eye"></i> Ver Verificable</a> |
                                   <label for="cambiar" style="color:red"><i class="fa fa-pencil"> </i> Cambiar Documento</label>

                                    @else

                                    @endif
                                </div>

                                <div class="form-group" >
                                    <label for="resolDesignaAtm"> Resolucion que designa al responsable de la ATM(Area Tecnica Municipal)
                                    </label>
                                    <input type="text"  class="form-control" id="resolDesignaAtm" name="resolDesignaAtm" value="{{$listaEditar->resolDesignaAtm}}" >
                                    <div style="display: {{$item->ver4!=null ? 'none':''}}">
                                        <label for="" style="color:#00a65a;">   <span class="label label-success">Subir verificable:</span>  <br> <i class="fa fa-file-archive-o"> Documento escaneado, formato PDF</i> </label>
                                    <input id="cambiar" type="file"  class="form-control" id="ver4" name="ver4" >

                                    </div>
                                  <br>  @if ($item->ver4!=null)
                                  <a  href="{{asset($item->ver4)}}" target="_blank"><i class="fa fa-eye"></i> Ver Verificable</a> |
                                   <label for="cambiar" style="color:red"><i class="fa fa-pencil"> </i> Cambiar Documento</label>

                                    @else

                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box box-danger">
                            <div class="box-body">
                                <div class="form-group" >
                                    <label for="ordenanzaCloro">ATM(Area Tecnica Municipal) incorporada en los Documentos de gestion :
                                    </label>
                                    <label>ROF :
                                        <input type="radio"  class="minimal" name="rof" id="rof" value="SI" {{$listaEditar->atmRof=="SI" ? "checked" : ""}} >
                                            SI
                                        <input type="radio"  class="minimal" name="rof" id="rof" value="NO" {{$listaEditar->atmRof=="NO" ? "checked" : ""}} >
                                            NO
                                    </label>
                                    <br>
                                    <label>MOF /MPP :
                                        <input type="radio"  class="minimal" name="mof" id="mof" value="SI" {{$listaEditar->atmMof=="SI" ? "checked" : ""}} >
                                        SI

                                        <input type="radio"  class="minimal" name="mof" id="mof" value="NO" {{$listaEditar->atmMof=="NO" ? "checked" : ""}} >
                                        NO

                                    </label>
                                    <br>
                                    <label >CAP :
                                        <input type="radio"  class="minimal" name="cap" id="cap" value="SI" {{$listaEditar->atmCap=="SI" ? "checked" : ""}} >
                                        SI

                                        <input type="radio"  class="minimal" name="cap" id="cap" value="NO" {{$listaEditar->atmCap=="NO" ? "checked" : ""}} >
                                        NO

                                    </label>
                                    <br>
                                    <label>TUPA :
                                        <input type="radio"  class="minimal" name="tupa" id="tupa" value="SI" {{$listaEditar->atmTupa=="SI" ? "checked" : ""}} >
                                        SI

                                        <input type="radio"  class="minimal" name="tupa" id="tupa" value="NO" {{$listaEditar->atmTupa=="NO" ? "checked" : ""}} >
                                        NO
                                    </label>
                                </div>
                                <div class="form-group" >
                                    <label for="equipamiento"> Oficina implementada con mobiliario y equipos basicos
                                    </label>
                                    <div style="display: {{$item->ver5!=null ? 'none':''}}">
                                        <label for="" style="color:#00a65a;">   <span class="label label-success">Subir verificable:</span>  <br> <i class="fa fa-file-archive-o"> Documento escaneado, formato PDF</i> </label>
                                    <input id="cambiar" type="file"  class="form-control" id="ver5" name="ver5" >

                                    </div>
                                  <br>  @if ($item->ver5!=null)
                                  <a  href="{{asset($item->ver5)}}" target="_blank"><i class="fa fa-eye"></i> Ver Verificable</a> |
                                   <label for="cambiar" style="color:red"><i class="fa fa-pencil"> </i> Cambiar Documento</label>

                                    @else

                                    @endif
                                </div>
                                @endforeach
                                <div class="box-footer">
                                        <input type="submit" value="Guardar toda la información" class="btn btn-primary btn-sm ">
                                        <a href="{{url('atm/lista')}}" class="btn btn-danger btn-sm" ><i class="fa fa-arrow-left"></i>  Regresar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>

    </section>
<script>
$(document).ready(function() {

    $("#cargos1").on( "click", function() {
			$('#numCargos1').show(); //muestro mediante id
            $('#numCargos2').show(); //muestro mediante id
            $('#numCargos3').show(); //muestro mediante id
            $('#numCargos4').show(); //muestro mediante id
			$('#ver3').show(); //muestro mediante clase
		 });
    $("#cargos").on( "click", function() {
        $('#numCargos1').hide(); //oculto mediante id
        $('#numCargos2').hide(); //oculto mediante id
        $('#numCargos3').hide(); //oculto mediante id
        $('#numCargos4').hide(); //oculto mediante id
        $('#ver3').hide(); //muestro mediante clase
    });

     $("#miembrosAdicionales1").on( "click", function() {
			$('#numMiembrosAdicionales').show(); //muestro mediante id

		 });
    $("#miembrosAdicionales2").on( "click", function() {
        $('#numMiembrosAdicionales').hide(); //oculto mediante id

    });

    $("#planSupervision1").on( "click", function() {
			$('#divplanSupervision').show(); //muestro mediante id

		 });
    $("#planSupervision2").on( "click", function() {
        $('#divplanSupervision').hide(); //oculto mediante id

    });
    $(function(){
        $('#ocultar').change(function(){
        if(!$(this).prop('checked')){
            $('#otras').hide();
        }else{
            $('#otras').show();
        }
  })
})
    $('#frmAtm').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields:{
            especialidad:{
                validators: {
                    notEmpty: {
                        message: 'La especialidad es requerido'
                    }
                }
            },
            ver1:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
             ver2:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver3:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver4:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver5:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver6:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver7:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver8:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver9:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver10:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver11:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },

        }
    });
});
</script>
@endsection
