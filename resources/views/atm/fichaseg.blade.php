@extends('layouts.templateatm')
@section('section')
<section class='content-header'>
    <h1> GESTIÓN DE ATM</h1>
    <ol class='breadcrumb'>
        <li><a href='#'><i class='fa fa-dashboard'></i>ATM</a></li>
        <li class='active'>fichas</li>
    </ol>
</section>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-header'>
                    <h3 class='box-title'>Fichas de seguimiento y evaluación en Hogares rurales y org. comunales</h3>
                    <a href="{{url('register')}}" class="btn btn-success" title="Nuevo Usuario" style="float: right;"><i class="fa fa-plus"></i></a>
                </div>
                <div class="box-body">
                    <form action="{{url('centrop/lista')}}" method="get">
                        <div class="input-group">
                            <input type="text" name="search" value="" class="form-control">
                            <span class="input-group-btn">
                                <button class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </form>
                </div>
                <div class='box-body table-responsive'>
                    <table id='' class='table table-bordered table-hover'>
                        <thead>
                            <tr>
                                <th> # Ultimo mes OC</th>
                                <th> # Ultimos 6 meses OC</th>
                                <th> # Ultimo mes OC</th>
                                <th> # Ultimo mes HR</th>
                                <th> # Ultimo 6 meses HR</th>
                                <th> # Ultimo año</th>
                                <th> Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($listaFichaSeg as $item)
                            <tr>
                                <td>{{$item->ultimomesOC}}</td>
                                <td>{{$item->ultimo6mesesOC}}</td>
                                <td>{{$item->ultimoanioOC}}</td>
                                <td>{{$item->ultimomesHR}}</td>
                                <td>{{$item->ultimo6mesesHR}}</td>
                                <td>{{$item->ultimoanioHR}}</td>  
  
                                <td>
                                    <a href="#" onclick="UserEdit('{{$item->ubigeo}}');" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i></a>
                                    <a href="#" onclick="UserDelete('{{$item->ubigeo }}');" class=""></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection