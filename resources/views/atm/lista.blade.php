@extends('layouts.templateatm')
@section('section')
<section class='content-header'>
    <h1> INSTITUCIONALIZACION  <small>AREA TECNICA MUNICIPAL</small></h1>
    <ol class='breadcrumb'>
        <li><a href='#'><i class='fa fa-dashboard'></i>ATM</a></li>
        <li class='active'>Lista</li>

    </ol>
</section>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-body table-responsive'>
                    <table id='' class='table table-bordered table-hover'>
                        <thead>
                            <tr>
                                <th>Distrito</th>
                                <th style="font-size: 12px;"> Ordenanza que crea o incorpora las funciones en el ROF
                                </th>
                                <th style="font-size: 12px;"> Nombre del area o unidad del cual  depende el ATM en la actualidad según la ordenanza de creacion.
                                </th>
                                <th style="font-size: 12px;"> Resolucion que aprueba el perfil de puesto del responsable de la ATM.
                                </th>
                                <th style="font-size: 12px;">  Resolucion que designa al responsable de la ATM
                                </th>

                                <th style="font-size: 12px;">  Oficina implementada con mobiliario y equipos basicos
                                </th>
                                <th style="font-size: 12px;">Estado de avance </th>
                                <th style="font-size: 12px;"><center>Acciones</center> </th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($listaAtm as $item)
                            <tr >
                                <td>{{$item->distrito}}</td>
                                <td>{{$item->ordMunicipalRof}}</td>
                                <td>{{$item->munDepende}}</td>
                                <td>{{$item->resolApruebaPerfil}}</td>
                                <td>{{$item->resolDesignaAtm}}</td>
                                <td>{{$item->equipamiento}}</td>
                                <td> <span class="label label-warning" style="font-size:14px">Observado</span></td>
                                <td>
                                    <button class="btn btn-primary btn-sm"  onclick="actualizar('{{$item->ubigeo}}')"><i class="fa fa-plus"></i> AGREGAR INFORMACIÓN</button>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>


                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function actualizar(ubigeo)
    {
    	window.location.href='{{url('atm/editar')}}/'+ubigeo;
    }
    function actualizarPregunta1(ubigeo)
    {
    	window.location.href='{{url('preguntas1/editar')}}/'+ubigeo;
    }
    function actualizarPregunta2(ubigeo)
    {
    	window.location.href='{{url('preguntas2/editar')}}/'+ubigeo;
    }
    function actualizarPregunta3(ubigeo)
    {
    	window.location.href='{{url('preguntas3/editar')}}/'+ubigeo;
    }
    function actualizarPregunta4(ubigeo)
    {
    	window.location.href='{{url('preguntas4/editar')}}/'+ubigeo;
    }
$(document).ready(function() {
    $('#frmAtm').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields:{
            ver1:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
             ver2:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver3:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver4:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            }
        }
    });
});
</script>
@endsection