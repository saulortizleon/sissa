@extends('layouts.templateatm')
@section('section')
<section class='content-header'>
    <h1> Formación  ATM</h1>
    <ol class='breadcrumb'>
        <li><a href='#'><i class='fa fa-dashboard'></i>formación</a></li>
        <li class='active'>Lista</li>
    </ol>
</section>
<section class='content'>
    <div class='row'>
        <div class='col-xs-12'>
            <div class='box'>
                <div class='box-body table-responsive'>
                    <table id='' class='table table-bordered table-hover'>
                        <thead>
                            <tr>
                                <th style="font-size: 12px"> Cantidad de trabajadores del ATM(Area Tecnica Municipal).
                                </th>
                                <th style="font-size: 12px"> ATM(Area Tecnica Municipal)Cuenta con sala Situacional Implementada
                                </th>
                                <th style="font-size: 12px">  Ordenanza  Municipal que crea el registro de organizaciones comunales (Junta Administradora de Servicios de Sanemiento).
                               </th>
                                <th style="font-size: 12px">  Utilización del Libro de registro de JASS(Junta Administradora de Servicios de Saneamiento).
                                </th>
                                <th>Estado de avance </th>
                           <th>Funciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($listaFuncion as $item)
                            <tr>
                                <td>{{$item->cantidadTrabaj}}</td>
                                <td>{{$item->estadoSalaSitua}}</td>
                                <td>{{$item->ordMunjass}}</td>
                                <td>{{$item->libroJass}}</td>
                                <td>

                                       <span class="label label-danger" style="font-size:14px">Desaprobado</span>

                                </td>

                                <td><button class="btn btn-primary btn-sm"  onclick="actualizar('{{$item->idFuncion}}')"> <i class="fa fa-plus"></i> AGREGAR INFORMACIÓN</button>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
 <script>
     function actualizar(idFuncion)
    {
    	window.location.href='{{url('funcion/editar')}}/'+idFuncion;
    }
$(document).ready(function() {
    $('#frmFormacion').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields:{
            especialidad:{
                validators: {
                    notEmpty: {
                        message: 'La especialidad es requerido'
                    }
                }
            },
            criterios:{
                validators: {
                    notEmpty: {
                        message: 'El campo es requerido'
                    }
                }
            },
            planSan:{
                validators: {
                    notEmpty: {
                        message: 'El campo es requerido'
                    }
                }
            },
            ver1:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
             ver2:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver3:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver4:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver5:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver6:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver7:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver8:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver9:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver10:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver11:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver12:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },
            ver13:{
                validators: {
                    file: {
                        maxSize: 5242880 ,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 5MB'
                    },
                }
            },

        }
    });
});
</script>
@endsection