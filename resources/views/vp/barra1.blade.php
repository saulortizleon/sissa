<div id="index">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 portfolio-item">
                <div class="card h-100 bg-primary">
                    <a href="{{url('concurso-atm-apurimac')}}"><img class="card-img-top" src="{{asset('img/concurso.jpg')}}" alt=""></a>
                    <div class="card-body">
                    <h5 class="card-title">
                        <a href="{{url('concurso-atm-apurimac')}}" style=" color: white;">I Concurso Virtual
                            “Servicios de Calidad y Sostenibles Apurímac 2021”
                            </a>
                    </h5>
                    <button  class="btn btn-danger" target="_blank" href="{{url('concurso-atm-apurimac')}}" disabled>
                        <i class="fa fa-desktop"> </i> <b>PASEO VIRTUAL</b>  </button>
                    
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 portfolio-item">
                <div class="card h-100 bg-primary">
                <a href="{{url('escuela-saneamiento')}}"><img class="card-img-top" src="{{asset('img/yaku-kawsay.jpeg')}}" alt=""></a>
                <div class="card-body">
                    <h5 class="card-title">
                    <a href="{{url('escuela-saneamiento')}}" style=" color: white;">Escuela de Saneamiento "Yaku Kawsay" - Agua para el buen vivir</a>
                    </h5>
                    <a  class="btn btn-success" target="_blank" href="{{url('escuela-saneamiento')}}">
                         <i class="fa fa-hand-o-up"></i><b> MÁS INFORMACIÓN</b> </a>
                </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 portfolio-item">
                <div class="card h-100 bg-primary">
                    <a href="{{url('urbanismo-edificaciones')}}"><img class="card-img-top" src="{{asset('img/edificaciones-urbanismo.jpg')}}" alt=""></a>
                    <div class="card-body">
                    <h5 class="card-title">
                        <a href="{{url('urbanismo-edificaciones')}}" style=" color: white;"> Capacitaciones virtuales en Edificaciones y Urbanismo</a>
                    </h5>
                    <a  class="btn btn-danger" target="_blank" href="{{url('urbanismo-edificaciones')}}">
                        <i class="fa fa-hand-o-up"></i><b> MÁS INFORMACIÓN</b> </a>
                    </div>
                </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 portfolio-item">
                        <div class="card h-100 bg-primary">
                        <a href="{{url('politicas-planes')}}"><img class="card-img-top" src="{{asset('img/drvcs-apurimac.jpg')}}" alt=""></a>
                        <div class="card-body">
                            <h5 class="card-title">
                            <a style=" color: white;" href="{{url('politicas-planes')}}">Políticas y planes regionales DRVCS APURÍMAC</a>
                            </h5>
                            <a  class="btn btn-warning" target="_blank" href="{{url('politicas-planes')}}">
                                 <i class="fa fa-check"></i> <b> REVISAR</b> </a>

                            </div>
                        </div>
                    </div>
        </div>
       
        <center><h2  class="azul">Proyectos</h2>
        </center>
        <br>
        <div class="row">
            <div class="col-lg-6   col-sm-6 portfolio-item">
                <div class="card h-100">
                <a href="#"><img class="card-img-top" src="{{asset('img/romas-apurimac.jpg')}}" alt=""></a>
                <div class="card-body">
                    <h4 class="card-title">
                    <a href="#" class="azul">Mantenimientos de Sistemas de Agua Potable en la Región de Apurímac</a>
                    </h4>
                    <a  class="btn btn-outline-danger btn-lg" href="{{url('romas')}}"> <i class="fa fa-hand-o-up"></i> MÁS INFORMACIÓN</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6   col-sm-6 portfolio-item">
                <div class="card h-100">
                <a href="#"><img class="card-img-top" src="{{asset('img/viviendas-saludables-apurimac.jpg')}}" alt=""></a>
                <div class="card-body">
                    <h4 class="card-title">
                    <a href="#" class="azul">Implementación de Viviendas Saludables en la Región de Apurímac  </a>
                    </h4>
                    <a  class="btn btn-outline-danger btn-lg" href="{{url('viviendas-saludables')}}"> <i class="fa fa-hand-o-up"></i> MÁS INFORMACIÓN</a>

                </div>
                </div>
            </div>
        </div>
        <center><h2 class="azul">Servicios online</h2>
        </center>
        <br>
        <div class="row" style="margin: auto; max-width: 1450px">
            <div class="col-lg-2 col-sm-2 portfolio-item">
                <div class="d-flex justify-content-start">
                    <div class="image-container">
                        <a  class="btn btn-info" href="{{url('concurso-atm-apurimac')}}">   <img  src="{{asset('img/concurso.png')}}" id="imgProfile" style="width: auto; height: auto" class="img-thumbnail" />
                        
                          </a>
                         
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-2 portfolio-item">
                <div class="d-flex justify-content-start">
                    <div class="image-container">
                        <a  class="btn btn-info" href="{{url('calculador-de-cloro')}}">  <img src="{{asset('img/2.png')}}" id="imgProfile" style="width: auto; height: auto" class="img-thumbnail" />
                      
                         </a>
                  
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-2 portfolio-item">
                <div class="d-flex justify-content-start">
                    <div class="image-container">
                        <a  class="btn btn-info" href="{{ url('escuela-saneamiento') }}">
                            <img src="{{asset('img/6.png')}}" id="imgProfile" style="width: auto; height: auto" class="img-thumbnail" />                   
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-2 portfolio-item">
                <div class="d-flex justify-content-start">
                    <div class="image-container">
                        <a  class="btn btn-info" href="{{url('mpv')}}">   <img src="{{asset('img/5.png')}}" id="imgProfile" style="width: auto; height: auto" class="img-thumbnail" />
                        </a>                     
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-2 portfolio-item">
                <div class="d-flex justify-content-start">
                    <div class="image-container">
                        <a  class="btn btn-info" href="{{url('directorio-institucional')}}"> <img src="{{asset('img/directorio.png')}}" id="imgProfile" style="width: auto; height: auto" class="img-thumbnail" />
                        </a>                      
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-sm-2 portfolio-item">
                <div class="d-flex justify-content-start">
                    <div class="image-container">
                        <a  class="btn btn-info" href="{{url('verificar-certificados')}}"> 
                        <img src="{{asset('img/verificar.png')}}" id="imgProfile" style="width: auto; height: auto" class="img-thumbnail" />
                          </a>                        
                    </div>
                </div>
            </div>
        </div>

        <center>  <h3 class="azul">Enlaces  de interés</h3>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="btn-group flex-wrap" role="group" aria-label="Basic example">
                    <a  class="btn-lg  btn-outline-secondary" target="_blank" href="{{url('capacitaciones')}}"><i class="fa fa-2x fa-mortar-board"> </i><strong> CAPACITACIONES</strong></a>
                    <a  class="btn-lg  btn-outline-secondary" target="_blank" href="{{url('compromisos-pnsr')}}"><i class="fa fa-2x fa-gear"> </i><strong> PNSR</strong></a>
                    <a  class="btn-lg  btn-outline-secondary" target="_blank" href="{{url('compromisos-fed')}}"><i class="fa fa-2x fa-child"> </i><strong> FED</strong></a>
                    <a  class="btn-lg  btn-outline-secondary" target="_blank" href="{{url('concurso-atm-apurimac')}}"><i class="fa fa-2x fa-trophy"> </i><strong> CONCURSOS</strong></a>
                    <a  class="btn-lg  btn-outline-secondary" target="_blank" href="{{url('plan-regional-saneamiento')}}"><i class="fa fa-2x fa-folder-open"> </i><strong> PLANES</strong>     </a>
                    <a  class="btn-lg  btn-outline-secondary" target="_blank" href="{{url('saneamiento')}}"><i class="fa fa-2x fa-tint"> </i><strong> SANEAMIENTO</strong>     </a>
                    <a  class="btn-lg  btn-outline-secondary" target="_blank" href="{{url('vivienda')}}"><i class="fa fa-2x fa-home"> </i><strong> VIVIENDA</strong>     </a>
                    <a  class="btn-lg  btn-outline-secondary" target="_blank" href="{{url('romas')}}"><i class="fa fa-2x fa-handshake-o"> </i><strong> ROMAS</strong>     </a>
                    <a class="btn-lg   btn-outline-secondary" target="_blank" href="{{url('atm-apurimac')}}" ><i class="fa fa-2x fa-users"> </i> <strong> ATM</strong> </a>
                    <a class="btn-lg   btn-outline-secondary" target="_blank" href="{{url('planos-prediales')}}" ><i class="fa fa-2x fa-paperclip"> </i>  <strong> PLANOS</strong> </a>
                    <a class="btn-lg   btn-outline-secondary" target="_blank" href="{{url('jass')}}" ><i class="fa fa-2x fa-wrench"> </i>  <strong>JASS</strong>  </a>
                    <a class="btn-lg   btn-outline-secondary" target="_blank" href="{{url('comursaba')}}" > <i class="fa fa-2x fa-chain"> </i> <strong>COMURSABA</strong></a>
                    <a class="btn-lg   btn-outline-secondary" target="_blank" href="https://datass.vivienda.gob.pe/" ><i class="fa fa-2x fa-laptop"> </i>  <strong>DATASS  </strong> </a>
                    <a class="btn-lg   btn-outline-secondary" target="_blank" href="https://www.gob.pe/coronavirus" ><i class="fa fa-2x fa-medkit"> </i> <strong>COVID-19 </strong>  </a>
                    <a class="btn-lg   btn-outline-secondary" target="_blank" href="https://www.mef.gob.pe/es/?option=com_content&language=es-ES&Itemid=101547&lang=es-ES&view=article&id=2221" ><i class="fa fa-2x fa-check"> </i> <strong>META PI  </strong>  </a>
                </div>
            </div>
        </div>
        </center>
        <br>
        <center>  <h3 class="azul">Redes Sociales</h3>
            <br>
            <div class="row">
                <div class="col-lg-4 col-sm-3 portfolio-item">
                    <a class="twitter-timeline" data-lang="es" data-width="400" data-height="400" href="https://twitter.com/drvcsapurimac?ref_src=twsrc%5Etfw">
                        Tweets by drvcsapurimac</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
              </div>
            <div class="col-lg-4 col-sm-3 portfolio-item">
                <div class="fb-page" data-href="https://www.facebook.com/drvcsapurimacoficial/" data-tabs="timeline" data-width="400" data-height="400"
                data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                <blockquote cite="https://www.facebook.com/drvcsapurimacoficial/" class="fb-xfbml-parse-ignore">
                    <a href="https://www.facebook.com/drvcsapurimacoficial/"> Dirección Regional de Vivienda, Construcción y Saneamiento Apurímac</a>
                </blockquote></div>
            </div>
            <div class="col-lg-4 col-sm-3 portfolio-item">
                <div class="g-ytsubscribe" data-channelid="UCaMjj_qdhraJmOIuWzsZfBA" data-layout="full" data-count="default"></div>
                    <br>
                    <iframe width="400" height="350" src="https://www.youtube.com/embed/upYQVoRWMeg"
                    frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
            </center>

        <center>  <h3 class="azul">Nuestros aliados</h3>  </center>
        <div class="row">
            <div class="col-md-12">
                <section class="logo-carousel slider" data-arrows="true">
                <div class="slide" target="_blank"><img src="https://drvcs.regionapurimac.gob.pe/img/region.png"></div>
                <div class="slide" target="_blank"><img src="{{asset('img/estado/allin.png')}}"  style="width: 45%;
                    height: auto;   display: block;
  margin-left: auto;
  margin-right: auto; "></div>
                <div class="slide" target="_blank"><img src="https://drvcs.regionapurimac.gob.pe/img/fed.png"></div>
                <div class="slide" target="_blank"><img src="{{asset('img/estado/pnsr.png')}}"  style="width: 90%;
                    height: auto;"></div>
                <div class="slide" target="_blank"><img src="https://drvcs.regionapurimac.gob.pe/img/diresa.png"></div>
                <div class="slide" target="_blank"><img src="{{asset('img/drea.png')}}"    style="width: 60%;
                    height: auto;   display: block;
  margin-left: auto;
  margin-right: auto;" ></div>
                <div class="slide" target="_blank"><img src="{{asset('img/estado/3.png')}}"  style="width: 90%;
                    height: auto;"></div>

             <div class="slide" target="_blank"><img src="https://drvcs.regionapurimac.gob.pe/logos/vivienda.png"></div>
                <div class="slide" target="_blank"><img src="{{asset('img/estado/pais.png')}}" style="width: 75%;
                    height: auto;"></div>
                <div class="slide" target="_blank"><img src="https://drvcs.regionapurimac.gob.pe/img/ana.png"  style="width: 90%;
                    height: auto;   display: block;
  margin-left: auto;
  margin-right: auto;"></div>


                <div class="slide" target="_blank"><img src="{{asset('img/estado/desarrollo.png')}}" style="width: 88%;
                    height: auto;"></div>
                <div class="slide" target="_blank"><img src="{{asset('img/estado/gl.png')}}" style="width: 88%;
                    height: auto;"></div>
                </section>
            </div>
        </div>
</div>