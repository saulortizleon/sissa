<div class="card my-4 bg-primary">
    <div class="card-body">
        <center>
            <img id="avatar" src="{{asset('img/eva.jpg')}}" alt="">  <br>
             <b style="font-size:15px; color:#ffffff" >Ing. Evangelina Guadalupe López Contreras</b>
             <label style="font-size:13px; color:#ebecee;   font-weight: bold;" > <i> Directora Regional
                de Vivienda Construcción y Saneamiento Apurímac</i> </label>
             <a class="btn btn-danger" href="{{url('calendario')}}">    <i class="fa fa-calendar" aria-hidden="true"></i> Calendario de Actividades</a>
            </center>
    </div>
</div>
<div class="card my-4">
    <ul class="list-group"  >
            <li class="list-group-item list-group-item-primary " style="background-color: #182d80" >
                <a style="color:white" href="{{ url('noticias') }}">
            <i class="fa fa-list-ol"></i> <b> Notas de prensa</b> </a> <span class="badge badge-success badge-pill">Actualizado</span> </li>

            <li style="background-color: #182d80"  class="list-group-item"><a  style="color:white" href="{{ url('comunicados') }}">
                <i class="fa fa-bullhorn"></i> <b> Comunicados oficiales</b> </a> </li>

            <li style="background-color: #182d80"  class="list-group-item"><a style="color:white"  href="{{ url('documentos-institucionales') }}">
                <i class="fa fa-file"></i> <b> Documentos publicados</b> </a> </li>

            <li style="background-color: #182d80"  class="list-group-item "><a style="color:white"  href="{{ url('directorio-institucional') }}">
                <i class="fa fa-phone-square"></i><b> Directorio Institucional</b> </a> </li>

            <li style="background-color: #182d80"  class="list-group-item "><a style="color:white"  href="{{ url('galeria-fotos') }}">
                <i class="fa fa-photo"></i><b> Galería de fotos</b> </a> </li>

            <li style="background-color: #182d80"  class="list-group-item "><a style="color:white" href="{{ url('verificar-certificados') }}">
                <i class="fa fa-check-circle"></i><b> Verificar Certificados </b></a> <span class="badge badge-warning badge-pill">Nuevo</span></li>

            <li style="background-color: #182d80"  class="list-group-item "><a style="color:white"  href="{{ url('calculador-de-cloro') }}"><i class="fa fa-calculator">
                </i><b> Yupasunchis Clorota </b> </a> </li>
    </ul>
</div>
<div class="card my-4">

    <iframe width="100%" height="500" scrolling="no" frameborder="no" 
    allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/1176965863&color=%2324a9c4&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true">
</iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: 
nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,
Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/drvcs-apurimac" 
title="Vivienda Apurímac" target="_blank" style="color: #cccccc; text-decoration: none;">Vivienda Apurímac</a> · 
<a href="https://soundcloud.com/drvcs-apurimac/sets/audios-institucionales" title="AUDIOS INSTITUCIONALES"
 target="_blank" style="color: #cccccc; text-decoration: none;">AUDIOS INSTITUCIONALES</a></div>
    
</div>


