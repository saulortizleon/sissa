@extends('layouts.templateatm')
@section('section')
    <section class='content-header'>
        <h1>
            <section class='content-header'>
                <h1> CAPACITACIONES <small>en el distrito</small></h1>
                <ol class='breadcrumb'>
                    <li><a href='#'><i class='fa fa-dashboard'></i>App</a></li>
                    <li class='active'>Lista</li>
                </ol>
    </section>
    <section class='content'>
        <div class='row'>
            <div class='col-xs-12'>
                <div class='box'>
                    <div class='box-body table-responsive'>
                        <table id='' class='table table-bordered table-hover'>
                            <thead>
                                <tr>
                                    <th style="font-size: 12px;"> 	Estatutos y reglamentos
                                    </th>
                                    <th style="font-size: 12px;"> 	Elaboración del POA JASS e Importancia de la cuota familiar
                                    </th>
                                    <th style="font-size: 12px;"> 	Manejo de  AOM
                                    </th>
                                    <th style="font-size: 12px;"> 	Partes del Sistema.
                                    </th>
                                    <th style="font-size: 12px;"> 	Desinfección y cloración
                                    </th>
                                    </th>
                                    <th style="font-size: 12px;"><center>Acciones</center> </th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($listaCapac as $item)
                                <tr style="font-size:16px">
                                    <td>{{$item->capEstatutoR}}</td>
                                    <td>{{$item->capPoa}}</td>
                                    <td>{{$item->capAOM}}</td>
                                    <td>{{$item->capPartesSap}}</td>
                                    <td>{{$item->capDesinfClorac}}</td>
                                    <td>
                                        <button class="btn btn-success btn-sm"  onclick="actualizar('{{$item->idCapCom}}')"><i class="fa fa-plus"></i> AGREGAR INFORMACIÓN ATM</button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        function actualizar(idCapCom)
        {
            window.location.href='{{url('capac/editar')}}/'+idCapCom;
        }
        function actualizarPregunta1(ubigeo)
        {
            window.location.href='{{url('preguntas1/editar')}}/'+ubigeo;
        }
        function actualizarPregunta2(ubigeo)
        {
            window.location.href='{{url('preguntas2/editar')}}/'+ubigeo;
        }
        function actualizarPregunta3(ubigeo)
        {
            window.location.href='{{url('preguntas3/editar')}}/'+ubigeo;
        }
        function actualizarPregunta4(ubigeo)
        {
            window.location.href='{{url('preguntas4/editar')}}/'+ubigeo;
        }
            $(document).ready(function() {
                $('#frmAtm').formValidation({
                    framework: 'bootstrap',
                    icon: {
                        valid: 'glyphicon glyphicon-ok',
                        invalid: 'glyphicon glyphicon-remove',
                        validating: 'glyphicon glyphicon-refresh'
                    },
                    fields:{
                        ver1:{
                            validators: {
                                file: {
                                    maxSize: 5242880 ,   // 2048 * 1024
                                    message: 'el archivo no debe pesar mas de 5MB'
                                },
                            }
                        },
                        ver2:{
                            validators: {
                                file: {
                                    maxSize: 5242880 ,   // 2048 * 1024
                                    message: 'el archivo no debe pesar mas de 5MB'
                                },
                            }
                        },
                        ver3:{
                            validators: {
                                file: {
                                    maxSize: 5242880 ,   // 2048 * 1024
                                    message: 'el archivo no debe pesar mas de 5MB'
                                },
                            }
                        },
                        ver4:{
                            validators: {
                                file: {
                                    maxSize: 5242880 ,   // 2048 * 1024
                                    message: 'el archivo no debe pesar mas de 5MB'
                                },
                            }
                        }
                    }
                });
            });
    </script>
@endsection