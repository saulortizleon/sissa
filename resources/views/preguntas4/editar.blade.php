@extends('layouts.templateatm')
@section('section')
    <section class='content-header'>
        <h1>Cuestionario 4:</h1>
        <ol class='breadcrumb'>
            <li><a href='#'><i class='fa fa-dashboard'></i>editar </a></li>
            <li class='active'>Formación</li>
        </ol>
    </section>
    <section class='content'>
        <div class='row'>
            <div class='col-xs-12'>
                <div class='box'>
                    <div class='box-body'>
                        <form id="frmPreguntas4" name="frmPreguntas4" action="{{url('preguntas4/editar')}}" method="post" enctype="multipart/form-data">
                            {{csrf_field()}}
                            <div class="row">
                                <h4 class="text-center"> <span class="label label-info"> Parte 1 </span> POI ATM 2019
                                </h4>
                                <hr>
                                <div class="col-sm-3 col-md-3">
                                        <div class="box box-info">
                                                <div class="box-body">
                                    <div class="form-group"  >
                                        <label for=""> POI N° de Resolución 2019</label>
                                        <input  type="input" placeholder="0.00"   class="form-control" id="ver2"  class="minimal" name="ver2" >

                                    </div>
                                </div>
                            </div>
                        </div>
                                <div class="col-sm-3 col-md-3">
                                        <div class="box box-info">
                                                <div class="box-body">
                                        <div class="form-group"  >
                                            <label for="">ppto POI 2019</label>
                                            <input  type="input" placeholder="0.00"   class="form-control" id="ver2"  class="minimal" name="ver2" >

                                        </div>
                                    </div>
                                </div>
                            </div>
                                    <div class="col-sm-3 col-md-3">
                                            <div class="box box-info">
                                                    <div class="box-body">
                                            <div class="form-group"  >
                                                <label for="">PPTO en PP083 (verificacion en PIM) 2019

                                                    </label>
                                                <input  type="input" placeholder="0.00"   class="form-control" id="ver2"  class="minimal" name="ver2" >

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-md-3">
                                        <div class="box box-info">
                                                <div class="box-body">
                                    <div class="form-group"  >
                                        <label for="">% de PPTO de aCuerdo a POI  (Gasto) 2019
                                        </label>
                                        <input  type="input" placeholder="0.00"   class="form-control" id="ver2"  class="minimal" name="ver2" >

                                    </div>
                                </div>
                            </div>
                        </div>
                                <div class="col-sm-3 col-md-3">
                                        <div class="box box-info">
                                                <div class="box-body">
                                        <div class="form-group"  >
                                            <label for="">ppto POA 2019</label>
                                            <input  type="input" placeholder="0.00"   class="form-control" id="ver2"  class="minimal" name="ver2" >

                                        </div>
                                    </div>
                                </div>
                            </div>
                                    <div class="col-sm-3 col-md-3">
                                            <div class="box box-info">
                                                    <div class="box-body">
                                            <div class="form-group"  >
                                                <label for="">PPTO en PP083 (verificacion en PIM) 2019
                                                    </label>
                                                <input  type="input" placeholder="0.00"   class="form-control" id="ver2"  class="minimal" name="ver2" >

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3 col-md-3">
                                        <div class="box box-info">
                                                <div class="box-body">
                                        <div class="form-group"  >
                                            <label for=""> <span class="label label-danger"> Verificable en formato PDF</span>
                                               <br> Subir Resumen POI ATM</label>
                                            <input  type="file" placeholder="0.00"   class="form-control" id="ver2"  class="minimal" name="ver2" >

                                        </div>
                                    </div>
                                </div>
                            </div>
                                <div class="col-sm-3 col-md-3">
                                        <div class="box box-info">
                                                <div class="box-body">
                                        <div class="form-group"  >
                                            <label for="">ATM cuenta con plan Supervisión: <br>
                                                    <input class="minimal" type="radio" name="casco" id="casco" value="SI" checked>
                                                    SI
                                                    <input class="minimal" type="radio" name="casco" id="casco" value="NO">
                                                    NO
                                                </label>
                                                    <label for=""> <span class="label label-danger"> Verificable en formato PDF</span>
                                                        <br> Subir plan de supervisión</label>
                                            <input  type="file"  class="form-control" id="utiles" name="utiles"  >
                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                            <h4 class="text-center"> <span class="label label-warning"> Parte 2 </span> POI ATM 2020
                            </h4>
                            <hr>
                            <div class="col-sm-3 col-md-3">
                                    <div class="box box-warning">
                                            <div class="box-body">
                                <div class="form-group"  >
                                    <label for=""> POI N° de Resolución 2020</label>
                                    <input  type="input" placeholder="0.00"   class="form-control" id="ver2"  class="minimal" name="ver2" >
                                    <label style="color:red;">Subir verificable  </label>
                                    <input  type="file"  class="form-control" id="utiles" name="utiles"  >
                                </div>
                            </div>
                        </div>
                    </div>
                            <div class="col-sm-3 col-md-3">
                                    <div class="box box-warning">
                                            <div class="box-body">
                                    <div class="form-group"  >
                                        <label for="">ppto POI 2020</label>
                                        <input  type="input" placeholder="0.00"   class="form-control" id="ver2"  class="minimal" name="ver2" >

                                    </div>
                                </div>
                            </div>
                        </div>
                                <div class="col-sm-3 col-md-3">
                                        <div class="box box-warning">
                                                <div class="box-body">
                                        <div class="form-group"  >
                                            <label for="">PPTO en PP083 (verificacion en PIM) 2020

                                                </label>
                                            <input  type="input" placeholder="0.00"   class="form-control" id="ver2"  class="minimal" name="ver2" >

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-md-3">
                                    <div class="box box-warning">
                                            <div class="box-body">
                                <div class="form-group"  >
                                    <label for="">% de PPTO de aCuerdo a POI  (Gasto) 2020
                                    </label>
                                    <input  type="input" placeholder="0.00"   class="form-control" id="ver2"  class="minimal" name="ver2" >
                                    <label style="color:red;">Subir verificable  </label>
                                    <input  type="file"  class="form-control" id="utiles" name="utiles"  >
                                </div>
                            </div>
                        </div>
                    </div>
                            <div class="col-sm-3 col-md-3">
                                    <div class="box box-warning">
                                            <div class="box-body">
                                    <div class="form-group"  >
                                        <label for="">ppto POA 2020</label>
                                        <input  type="input" placeholder="0.00"   class="form-control" id="ver2"  class="minimal" name="ver2" >

                                    </div>
                                </div>
                            </div>
                        </div>
                                <div class="col-sm-3 col-md-3">
                                        <div class="box box-warning">
                                                <div class="box-body">
                                        <div class="form-group"  >
                                            <label for="">PPTO en PP083 (verificacion en PIM) 2020

                                                </label>
                                            <input  type="input" placeholder="0.00"   class="form-control" id="ver2"  class="minimal" name="ver2" >

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 col-md-3">
                                    <div class="box box-warning">
                                            <div class="box-body">
                                    <div class="form-group"  >
                                        <label for="">ATM cuenta con plan Supervisión: <br>
                                                <input class="minimal" type="radio" name="casco" id="casco" value="SI" checked>
                                                SI
                                                <input class="minimal" type="radio" name="casco" id="casco" value="NO">
                                                NO
                                                </label>   <label style="color:red;">Subir verificable      </label>
                                        <input  type="file"  class="form-control" id="utiles" name="utiles"  >
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
                            <div class="row">
                                <h4 class="text-center"><span class="label label-success"> Parte 3  </span> ATM aplica fichas de seguimiento y evaluación	:
                                    </h4>
                                    <hr>
                                <div class="col-sm-4 col-md-4">
                                        <div class="box box-success">
                                                <div class="box-body">
                                    <div class="form-group"  >
                                        <label for="">◘ N° de fichas de SyE aplicadas a las OC  <br><i>	Fecha del ultimo mes</i>
                                            </label>
                                        <input  type="date" placeholder="0"  class="form-control" id="ver2"  class="minimal" name="ver2" >
                                    </label>   <label style="color:red;">Subir verificable      </label>
                                    <input  type="file"  class="form-control" id="utiles" name="utiles"  >
                                    </div>
                                </div>
                            </div>
                        </div>
                                <div class="col-sm-4 col-md-4">
                                        <div class="box box-success">
                                                <div class="box-body">
                                    <div class="form-group"  >
                                        <label for="">◘ N° de fichas de SyE aplicadas a las OC  <br><i>	Fecha del ultimo 6 meses
                                            </i>
                                            </label>
                                        <input  type="date" placeholder="0"  class="form-control" id="ver2"  class="minimal" name="ver2" >
                                    </label>   <label style="color:red;">Subir verificable      </label>
                                    <input  type="file"  class="form-control" id="utiles" name="utiles"  >
                                    </div>
                                </div>
                            </div>
                        </div>
                                <div class="col-sm-4 col-md-4">
                                        <div class="box box-success">
                                                <div class="box-body">
                                    <div class="form-group"  >
                                        <label for="">◘ N° de fichas de SyE aplicadas a las OC  <br><i>	Fecha del ultimo año
                                            </i>
                                            </label>
                                        <input  type="date" placeholder="0"  class="form-control" id="ver2"  class="minimal" name="ver2" >
                                    </label>   <label style="color:red;">Subir verificable      </label>
                                    <input  type="file"  class="form-control" id="utiles" name="utiles"  >
                                    </div>
                                </div>
                            </div>
                        </div>
                                <div class="col-sm-4 col-md-4">
                                        <div class="box box-success">
                                                <div class="box-body">
                                    <div class="form-group"  >
                                        <label for="">N° de fichas de SyE aplicadas a los hogares rurales
                                                <br><i> Fecha del ultimo mes

                                            </i>
                                            </label>
                                        <input  type="date" placeholder="0"  class="form-control" id="ver2"  class="minimal" name="ver2" >
                                    </label>   <label style="color:red;">Subir verificable      </label>
                                    <input  type="file"  class="form-control" id="utiles" name="utiles"  >
                                    </div>
                                </div>
                            </div>
                        </div>
                                <div class="col-sm-4 col-md-4">
                                        <div class="box box-success">
                                                <div class="box-body">
                                    <div class="form-group"  >
                                        <label for="">N° de fichas de SyE aplicadas a los hogares rurales
                                                <br><i>Fecha del ultimo 6 meses

                                            </i>
                                            </label>
                                        <input  type="date" placeholder="0"  class="form-control" id="ver2"  class="minimal" name="ver2" >
                                    </label>   <label style="color:red;">Subir verificable      </label>
                                    <input  type="file"  class="form-control" id="utiles" name="utiles"  >
                                    </div>
                                </div>
                            </div>
                        </div>
                                <div class="col-sm-4 col-md-4">
                                        <div class="box box-success">
                                                <div class="box-body">
                                    <div class="form-group"  >
                                        <label for="">N° de fichas de SyE aplicadas a los hogares rurales
                                                <br><i>Fecha del ultimo año
                                            </i>
                                            </label>
                                        <input  type="date" placeholder="0"  class="form-control" id="ver2"  class="minimal" name="ver2" >
                                    </label>   <label style="color:red;">Subir verificable      </label>
                                    <input  type="file"  class="form-control" id="utiles" name="utiles"  >
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                            <div class="row">
                                    <h4 class="text-center"><span class="label label-warning"> Parte 4</span> ATM con cuaderno de registro de monitoreo de cloro residual	y Caracterizacion de fuentes de agua
                                        </h4>
                                        <hr>
                                    <div class="col-sm-3">
                                            <div class="form-group"  >
                                                    <div class="box box-warning">
                                                            <div class="box-body">
                                                    <label for="">ATM registra el monitoreo de cloro residual:

                                                    <input class="minimal" type="radio" name="casco" id="casco" value="SI" checked>
                                                    SI

                                                    <input class="minimal" type="radio" name="casco" id="casco" value="NO">
                                                    NO
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="box box-warning">
                                                <div class="box-body">
                                        <div class="form-group"  >
                                                <label for=""> Fecha de ultimo registro
                                                    </label>
                                                <input type="date"  class="form-control" id="ver2"  class="minimal" name="ver2">
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="box box-warning">
                                                <div class="box-body">
                                        <div class="form-group"  >
                                                <label for="">Codigo de ultimo sistema de cloración monitoreado
                                                    </label>
                                                <input  type="input"     class="form-control" id="ver2"  class="minimal" name="ver2" >
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="box box-warning">
                                            <div class="box-body">
                                                <div class="form-group" >
                                                <label for=""><span class="label label-danger"> Subir resumen de sistema de cloracion</span> <br> subir verificable en formato PDF</label>
                                                <input  type="file" placeholder="0" class="form-control" id="ver2"  class="minimal" name="ver2" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                            <div class="box box-warning">
                                                    <div class="box-body">
                                        <div class="form-group"  >
                                                <label for=""> ATM cuenta con archivos de caracterización :
                                                        <input class="minimal" type="radio" name="casco" id="casco" value="SI" checked>
                                                        SI
                                                        <input class="minimal" type="radio" name="casco" id="casco" value="NO">
                                                        NO
                                                        </label>
                                                <input  type="input"     class="form-control" id="ver2"  class="minimal" name="ver2" >
                                                <label for="">N° de sistemas de agua potable con caracterizacion </label>
                                                <input  type="input"     class="form-control" id="ver2"  class="minimal" name="ver2" >
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-sm-3">
                                            <div class="box box-warning">
                                                    <div class="box-body">
                                            <div class="form-group"  >
                                                    <label for="">Código del sistema de agua de utima caracterizacion
                                                        </label>
                                                    <input  type="input"     class="form-control" id="ver2"  class="minimal" name="ver2" >
                                                </div>
                                            </div>
                                         </div>
                                        </div>
                                        <div class="col-sm-3">
                                                <div class="box box-warning">
                                                        <div class="box-body">
                                                <div class="form-group"  >

                                                        <label for="">Fecha  de ultima caracterizacion
                                                            </label>
                                                        <input  type="date"     class="form-control" id="ver2"  class="minimal" name="ver2" >
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                            <div class="col-sm-3">
                                                    <div class="box box-warning">
                                                            <div class="box-body">
                                                    <div class="form-group" >
                                                            <label for=""><span class="label label-danger"> Subir caracterizacion sistemas de agua potable</span> <br> Verificable en formato PDF	</label>
                                                            <input  type="file" placeholder="0" class="form-control" id="ver2"  class="minimal" name="ver2" >
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            <div class="col-sm-12 col-12 bg-info" >

                                        <label class=""><i>Asegúrese de llenar correctamente los campos antes de guardar los cambios... </i> </label>
                                        <label class=""><b>No olvide subir los verificables </b> </label>
                                        <input type="submit" value="Guardar" class="btn btn-primary btn-sm " class="form-control">
                                        <a href="{{url('atm/lista')}}" class="btn btn-danger btn-sm " class="form-control">Regresar</a>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>
        $(document).ready(function() {
                $('#frmPreguntas4').formValidation({
                framework: 'bootstrap',
                icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
                },
                fields:{
                ver1:{
                validators: {
                    notEmpty: {
                        message: 'El Verificable es requerido'
                    },
                    file: {
                        maxSize: 10485760,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 10MB'
                    },
                }
                },
                ver2:{
                validators: {
                    notEmpty: {
                        message: 'El Verificable es requerido'
                    },
                    file: {
                        maxSize: 10485760,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 2MB'
                    },
                }
                },
                ver3:{
                validators: {
                    notEmpty: {
                        message: 'El Verificable es requerido'
                    },
                    file: {
                        maxSize: 10485760,   // 2048 * 1024
                        message: 'el archivo no debe pesar mas de 2MB'
                    },
                }
                },
                ver4:{
                    validators: {
                        notEmpty: {
                            message: 'El Verificable es requerido'
                        },
                        file: {
                            maxSize: 10485760,   // 2048 * 1024
                            message: 'el archivo no debe pesar mas de 2MB'
                        },
                    }
                    },
                    ver4:{
                        validators: {
                            notEmpty: {
                                message: 'El Verificable es requerido'
                            },
                            file: {
                                maxSize: 10485760,   // 2048 * 1024
                                message: 'el archivo no debe pesar mas de 2MB'
                            },
                        }
                        },
                        ver5:{
                            validators: {
                                notEmpty: {
                                    message: 'El Verificable es requerido'
                                },
                                file: {
                                    maxSize: 10485760,   // 2048 * 1024
                                    message: 'el archivo no debe pesar mas de 2MB'
                                },
                            }
                            },
                            ver6:{
                                validators: {
                                    notEmpty: {
                                        message: 'El Verificable es requerido'
                                    },
                                    file: {
                                        maxSize: 10485760,   // 2048 * 1024
                                        message: 'el archivo no debe pesar mas de 2MB'
                                    },
                                }
                                },
                                ver7:{
                                    validators: {
                                        notEmpty: {
                                            message: 'El Verificable es requerido'
                                        },
                                        file: {
                                            maxSize: 10485760,   // 2048 * 1024
                                            message: 'el archivo no debe pesar mas de 2MB'
                                        },
                                    }
                                    },
                                    ver8:{
                                        validators: {
                                            notEmpty: {
                                                message: 'El Verificable es requerido'
                                            },
                                            file: {
                                                maxSize: 10485760,   // 2048 * 1024
                                                message: 'el archivo no debe pesar mas de 2MB'
                                            },
                                        }
                                        },
                                        casco:{
                                            validators: {
                                                notEmpty: {
                                                    message: 'El Verificable es requerido'
                                                },
                                                file: {
                                                    maxSize: 10485760,   // 2048 * 1024
                                                    message: 'el archivo no debe pesar mas de 2MB'
                                                },
                                            }
                                            },
                                            ver2:{
                                                validators: {
                                                    notEmpty: {
                                                        message: 'El Verificable es requerido'
                                                    },
                                                    file: {
                                                        maxSize: 10485760,   // 2048 * 1024
                                                        message: 'el archivo no debe pesar mas de 2MB'
                                                    },
                                                }
                                                },
                }
            });
        });
        </script>
@endsection
