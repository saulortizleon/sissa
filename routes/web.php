<?php
//pagina de inicio
Route::get('/limpiar', function() {
    Artisan::call('cache:clear');
    return "Cache limpiado";
});
//Route::get('index','IndexController@Index');
Route::get('/','IndexController@header');
Route::get('popup','blog\NoticiasController@popup');
//vistas de atm,mas y adm
Route::get('atm','IndexController@IndexAtm');
Route::get('adm','IndexController@IndexAdm');
Route::get('mas','IndexController@IndexMas');
//login general
Route::get('comunicado','blog\NoticiasController@comunicado');
Route::get('login','LoginController@IndexLogin');
Route::post('login','LoginController@actionLogIn');

Route::get('logout','LoginController@actionLogout');

Route::group(['middleware'=>['admin']], function()
{
   Route::get('admin/listaformacion','admin\GeneralController@listaFormacion');
    Route::get('admin/listaatm','admin\GeneralController@listaAtm');
    Route::get('admin/usuarios','admin\GeneralController@listaUsuarios');
    //insertar usuarios
    Route::get('usuarios/insertar','admin\GeneralController@insertarUsuarios');
    Route::post('usuarios/insertar','admin\GeneralController@insertarUsuarios');
    //eliminar
    Route::get('usuario/eliminar/{idUsuario}','admin\GeneralController@eliminarAccesos');
    //editar usuarios
    Route::get('usuarios/editar/{idusuario}','admin\GeneralController@editarUsuarios');
    Route::post('usuarios/editar','admin\GeneralController@editarUsuarios');
    //verificables
    Route::get('/formacion/ver/{ubigeo}','Verificables@formacion');
    Route::get('/herramientas/ver/{ubigeo}','Verificables@herramientas');
    Route::get('/gestion/ver/{ubigeo}','Verificables@gestion');
    //ver verificables
    Route::get('admin/backup','admin\GeneralController@backup');
    Route::get('admin/avance','admin\GeneralController@avance');
    //NOTICIAS
    Route::get('admin/noticias','admin\GeneralController@adminNoticias');
    //noticias insert e edit
    Route::get('noticias/insertar','admin\GeneralController@insertarNoticia');
    Route::post('noticias/insertar','admin\GeneralController@insertarNoticia');

    Route::post('noticias/editar','admin\GeneralController@editarNoticia');
    Route::get('noticias/editar/{idPublicacion}','admin\GeneralController@editarNoticia');

});

Route::group(['middleware'=>['sup']], function()
{
    //respuesta
    Route::get('respuesta/lista','ArchivoController@respuesta');
    Route::post('respuesta/insertar','ArchivoController@respuesta');
    Route::post('respuesta/editar','ArchivoController@respuestaEditar');
    //archivo
    Route::get('archivo/lista','ArchivoController@archivo');
    Route::post('archivo/editar','ArchivoController@archivoEditar');
    Route::post('archivo/insertar','ArchivoController@archivo');
    Route::get('buscar/archivo','ArchivoController@archivo');
    //tramite
    Route::get('tramite/lista','blog\MpvController@tramite');
    Route::post('tramite/insertar','blog\MpvController@tramite');
    //enviar correo
    Route::get('documento/correo/{mail}','blog\MpvController@documento');
    //documentos virtuales
    Route::get('documento/lista','blog\MpvController@documento');
    Route::get('buscar/documento','blog\MpvController@documento');
    Route::post('documento/insertar','blog\MpvController@documento');

    //concurso
    Route::get('concurso/lista','blog\NoticiasController@listaConcurso');
    Route::get('concurso/editar/{idatm}','blog\NoticiasController@editarConcurso1');
    Route::post('concurso/editar','blog\NoticiasController@editarConcurso1');
    //EDITAR 2 concurso
    Route::get('concurso/editar2/{idatm}','blog\NoticiasController@editarConcurso2');
    Route::post('concurso/editar2','blog\NoticiasController@editarConcurso2');
    //EDITAR 3 concurso
    Route::get('concurso/editar3/{idatm}','blog\NoticiasController@editarConcurso3');
    Route::post('concurso/editar3','blog\NoticiasController@editarConcurso3');
    //EDITAR 4 concurso
    Route::get('concurso/editar4/{idatm}','blog\NoticiasController@editarConcurso4');
    Route::post('concurso/editar4','blog\NoticiasController@editarConcurso4');
    //supervisión
    Route::get('yakukawsay/calendario','SupController@calendario');
    Route::get('supervision/material','SupController@materiales');
    Route::get('supervision','IndexController@IndexSup');
    //atm
    Route::get('atm','IndexController@IndexAtm');
    Route::get('atm/lista','AtmController@lista');
    Route::get('atm/editar/{ubigeo}','AtmController@editarAtm');
    Route::post('atm/editar','AtmController@editarAtm');
    //herramienta
    Route::post('funcion/editar','FuncionController@editarFuncion');
    Route::get('funcion/editar/{idFuncion}','FuncionController@editarFuncion');
    Route::get('funcion/lista','FuncionController@listaFuncion');
    //app pnsr
    Route::post('pnsr/editar','ApppnsrController@editarApppnsr');
    Route::get('pnsr/editar/{idApp}','ApppnsrController@editarApppnsr');
    Route::get('pnsr/lista','ApppnsrController@listaPnsr');
     //sap consolidado
     Route::post('csap/editar','CsapController@editarCsap');
     Route::get('csap/editar/{idSapC}','CsapController@editarCsap');
     Route::get('csap/lista','CsapController@listaCsap');
      //sap riesgo consolidado
      Route::post('csap/reditar','CsapController@editarRsap');
      Route::get('csap/reditar/{idSapR}','CsapController@editarRsap');
     //jass consolidado
     Route::post('cjass/editar','CjassController@editarCjass');
     Route::get('cjass/editar/{idJass}','CjassController@editarCjass');
     Route::get('cjass/lista','CjassController@listaCjass');
    //comunicacion
    Route::post('comuni/editar','ComuniController@editarComuni');
    Route::get('comuni/editar/{idComu}','ComuniController@editarComuni');
    Route::get('comuni/lista','ComuniController@listaComu');
    //capacitacion
    Route::post('capac/editar','CapacController@editarCapac');
    Route::get('capac/editar/{idCapCom}','CapacController@editarCapac');
    Route::get('capac/lista','CapacController@listaCapac');
    //formacion
    Route::post('formacion/editar','FormacionController@editarFormacion');
    Route::get('formacion/editar/{idFormacion}','FormacionController@editarFormacion');
    Route::get('formacion/lista','FormacionController@listaFormacion');
    Route::post('formacion/agregar','FormacionController@insertarFormacion');
    //Centro poblado
    Route::get('centrop/lista','CentroPController@lista');
    Route::get('centrop/editar/{ubigeo}','CentroPController@editarCentroP');
    Route::post('centrop/editar','CentroPController@editarCentroP');
    Route::get('atm/fichaseg','AtmController@listaFichaSeg');
    Route::get('atm/planseg','AtmController@listaPlan');

     // lista usuarios
    Route::get('usuario/perfil','AtmController@listaResponsable');
    Route::get('usuario/editar/{dniUsuario}','AtmController@editarResponsable');
    Route::post('usuario/editar','AtmController@editarResponsable');

    //preguntas vista principal
    Route::get('preguntas/lista','PreguntasController@listaPreguntas');
    //preguntas1
    Route::post('preguntas1/editar','Preguntas1@editarPreguntas1');
    Route::get('preguntas1/editar/{ubigeo}','Preguntas1@editarPreguntas1');
    //preguntas2
    Route::post('preguntas2/editar','Preguntas2@editarPreguntas2');
    Route::get('preguntas2/editar/{ubigeo}','Preguntas2@editarPreguntas2');
    //preguntas3
    Route::post('preguntas3/editar','Preguntas3@editarPreguntas3');
    Route::get('preguntas3/editar/{ubigeo}','Preguntas3@editarPreguntas3');
    //preguntas3
    Route::post('preguntas4/editar','Preguntas4@editarPreguntas4');
    Route::get('preguntas4/editar/{ubigeo}','Preguntas4@editarPreguntas4');
});

Route::group(['middleware'=>['atm']], function()
{
    //perfil usuario
    Route::get('usuario/perfil','AtmController@listaResponsable');
    Route::get('usuario/editar/{dniUsuario}','AtmController@editarResponsable');
    Route::post('usuario/editar','AtmController@editarResponsable');
    //concurso atm
    Route::get('finalistas/lista','AtmController@finalistas');
    //atm index finalistas
    Route::get('atm-finalista','AtmController@indexAtm');
    //editar finalista 1
    Route::get('finalistas/editar/{idatm}','AtmController@editar1');
    Route::post('finalistas/editar','AtmController@editar1');
    //2
    Route::get('finalistas/editar2/{idatm}','AtmController@editar2');
    Route::post('finalistas/editar2','AtmController@editar2');
    //3
    Route::get('finalistas/editar3/{idatm}','AtmController@editar3');
    Route::post('finalistas/editar3','AtmController@editar3');

});
//concurso atm 2021
Route::get('concurso-atm-apurimac','IndexController@concursoAtm');
//Verificar Certificados


Route::get('verificar-certificados','blog\NoticiasController@verificar');
Route::get('verificar-certificados2','blog\NoticiasController@verificar2');
Route::get('verificar-certificados3','blog\NoticiasController@verificar3');
Route::get('munis','blog\NoticiasController@munis');
Route::get('atm/{idatm}','blog\NoticiasController@stand');

Route::get('premio-gestion-agua-saneamiento','blog\NoticiasController@concurso');

//temas
Route::get('viviendas-saludables','blog\NoticiasController@viviendas');
Route::get('atm-apurimac','blog\NoticiasController@atm');
Route::get('entrega-cloro','blog\NoticiasController@entrega');
Route::get('jass','blog\NoticiasController@jass');

Route::get('planos-prediales','blog\NoticiasController@planos');
Route::get('comursaba','blog\NoticiasController@comursaba');
Route::get('comunicados','blog\NoticiasController@avisos');
Route::get('edusa','blog\NoticiasController@edusa');
Route::get('romas','blog\NoticiasController@romas');
Route::get('wasichakuy','blog\NoticiasController@wasichakuy');
Route::get('edificaciones','blog\NoticiasController@edificaciones');
Route::get('urbanismo','blog\NoticiasController@urbanismo');
Route::get('proyectos-romas','blog\NoticiasController@listaRomas');
//escuela saneamiento
Route::get('escuela-saneamiento','blog\EscuelaController@listar');
//sitemap
Route::get('/sitemap.xml', 'blog\SitemapController@sitemap');
//todo avisos
Route::get('noticias','blog\NoticiasController@listaNoticias');

Route::get('noticias/detalle','blog\NoticiasController@detalle');

Route::get('noticias/detalle/{id}','blog\NoticiasController@detalle');
//informativos

Route::get('nuestra-institucion','blog\NoticiasController@institucional');
Route::get('documentos-institucionales','blog\NoticiasController@documento');
//Route::get('oficinas','blog\NoticiasController@oficinas');
//Route::get('transparencia','blog\NoticiasController@transparencia');
Route::get('galeria-fotos','blog\NoticiasController@verGaleria');


Route::get('directorio-institucional','blog\NoticiasController@directorio');
route::get('plan-regional-saneamiento','blog\NoticiasController@plan');
//paginas principales

Route::get('vivienda-y-urbanismo', function () {
    return view('blog/noticias/urbanismo');
});

Route::get('construccion-y-saneamiento', function () {
    return view('blog/noticias/saneamiento');
});
Route::get('calendario', function () {
    return view('blog/app/calendario');
});
Route::get('materiales-capacitacion', function () {
    return view('blog/app/material');
});
//mpv
Route::get('mpv','blog\MpvController@insertar');
Route::post('mpv/insertar','blog\MpvController@insertar');

//CALCULADORA CLORO
Route::get('calculador-de-cloro', function () {
    return view('blog/app/calcularcloro');
});
Route::get('funciones', function () {
    return view('blog/noticias/funciones');
});
Route::get('objetivos', function () {
    return view('blog/app/objetivos');
});

Route::get('contactenos', function () {
    return view('blog/noticias/contacto');
});

Route::get('base-legal', function () {
    return view('blog/noticias/base-legal');
});
Route::get('transmisiones-vivo', function () {
    return view('blog/noticias/vivo');
});
Route::get('videos', function () {
    return view('blog/noticias/videos');
});
